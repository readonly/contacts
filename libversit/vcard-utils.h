/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __VCARD_UTILS_H__
#define __VCARD_UTILS_H__

#include <glib.h>

G_BEGIN_DECLS

#include "mainwindow.h"
#include "plugin.h"
#include "vobject.h"

Contact* vcard2contact(VObject* vcard, Plugin* plugin, gchar** error);
GSList* contacts2vcard(GList* contacts, Plugin* plugin, gchar** error);
VObject* contact2vcard(Contact* contact, Plugin* plugin, gchar** error);
gchar* personal_vcard_get(const gchar* account, gchar** error);
void personal_vcard_make(const MainWindow* mainwindow, gchar** error);
Contact* vcard_ptr2contact(Plugin* plugin, const gchar* vcard, gint len, gchar** error);
 
G_END_DECLS

#endif
