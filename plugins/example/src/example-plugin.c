/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>
#include "plugin.h"
#include "gtk-utils.h"
#include "utils.h"

#define NAME "Example plugin"

/* Reference to self */
static Plugin* self = NULL;
/* See plugin.h */
static PluginFeature* feature = NULL;
/* Description */
static const gchar subtype[] = "Example plugin";
/* List of AddressBook */
static GList* abooks = NULL;
/* List of closed AddressBook */
static GList* closed_books = NULL;
/*
 * HashTable containing names of supported attributes.
 * Each keys holds an AttribDef.
 */
static GHashTable* attribs = NULL;

/* List of inactive supported attributes */
static GSList* inactive_attribs = NULL;

/* List of not active or inactive attributes this plugin supports*/
static GSList* remaining_attribs = NULL;

/**
 * Remove newly open book from closed_books
 * @param book AddressBook to remove
 */
static void closed_books_remove(AddressBook* book) {
	GList* found = g_list_find_custom(closed_books, book, address_book_compare);
	if (found)
		closed_books = g_list_remove_link(closed_books, found);
}

/**
 * Free list of abooks.
 * @param error Pointer to memory where error is supposed to be saved
 */
void plugin_reset(gchar** error) {
	if (*error != NULL) {
		g_free(*error);
		*error = NULL;
	}
}

/**
 * Return list of AttributeDef for currently
 * supported attributes.
 * @return GSList* list of AttributeDef
 */
GSList* plugin_attrib_list(void) {
	GSList* list = NULL;
	
	if (attribs) {
		g_hash_table_foreach(attribs, hash_table_keys_to_slist, &list);
	}
	return list;
}

/**
 * Return list of AttribDef of attributes supported
 * by this plugin.
 * Returning NULL means list of supported attributes are infinite
 * Returning an empty list means that no more supported attributes
 * are available for activation
 * @return GSList* list of AttributeDef
 */
GSList* plugin_remaining_attribs(void) {
	return gslist_deep_copy(remaining_attribs, attrib_def_copy);
}

/**
 * Return list of AttributeDef for currently
 * supported inactive attributes.
 * @return GSList* list of AttributeDef
 */
GSList* plugin_inactive_attribs(void) {
	return gslist_deep_copy(inactive_attribs, attrib_def_copy);
}

/**
 * Set supported attributes.
 * @param attributes pointer to GHashTable containing keys
 * naming supported attributes.
 */
void plugin_attribs_set(GHashTable* attributes) {
	hash_table_free(&attribs);
	attribs = hash_table_copy(attributes);
}

/**
 * Get a contact(s) using a search string.
 * @param abook Pointer to AddressBook
 * @param search_token Search string
 * @param Pointer to memory where error is supposed to be saved
 * @return GSList* of Contact
 */
GSList* plugin_get_contact(
		AddressBook* abook, const gchar* search_token, gchar** error) {
	GList* cur;
	GSList *contacts = NULL, *basic, *list;
	gboolean found = FALSE;
	Contact* contact = NULL;

	if (abook && search_token) {
		for (cur = abook->contacts; cur; cur = g_list_next(cur)) {
			contact = (Contact *) cur->data;
			if (! contact)
				continue;
			
			list = get_basic_attributes(contact);
			for (basic = list; !found && basic; basic = g_slist_next(basic)) {
				gchar* attr = (gchar *) basic->data;
				if (attr) {
					found = match_string_pattern(search_token, attr, FALSE);
				}
			}
			gslist_free(&list, g_free);
			if (found) {
				contacts = g_slist_prepend(contacts, contact);
				found = FALSE;
			}
		}
	}

	return contacts;
}

/**
 * Get a contact(s) using all supported attributes.
 * @param abook Pointer to AddressBook
 * @param search_tokens Search strings from a Contact
 * @param is_and If TRUE searching using AND else search using OR
 * @param Pointer to memory where error is supposed to be saved
 * @return GSList* of Contact
 */
GSList* plugin_search_contact(AddressBook* abook,
							  const Contact* search_tokens,
							  gboolean is_and,
							  gchar** error) {
	GList* cur;
	GSList *contacts = NULL;
	Contact* contact = NULL;
	Compare* comp;
	
	if (abook && search_tokens && (search_tokens->data || search_tokens->emails)) {
		comp = g_new0(Compare, 1);
		for (cur = abook->contacts; cur; cur = g_list_next(cur)) {
			contact = (Contact *) cur->data;
			if (! contact)
				continue;
			
			comp->hash = contact->data;
			comp->is_and = is_and;
			comp->equal = -1;
			
			g_hash_table_foreach(search_tokens->data,
					contact_compare_values, comp);
			if ((comp->is_and && comp->equal > 0) ||
				(comp->equal < 1 && !comp->is_and)) {
				gint res = email_compare_values(
					search_tokens->emails, contact->emails, is_and);
				if (res != -1)
					comp->equal = res;
			}
			if (comp->equal > 0)			
				contacts = g_slist_prepend(contacts, contact);
		}
		g_free(comp);
	}

	return contacts;
}

/**
 * Save a contact in AddressBook.
 * @param abook Pointer to AddressBook
 * @param contact Contact to save
 * @param Pointer to memory where error is supposed to be saved
 * @return FALSE if success TRUE otherwise
 */
gboolean plugin_set_contact(
		AddressBook* abook, const Contact* contact, gchar** error) {
	
	/* Save contact */
	
	abook->dirty = TRUE;

	return FALSE;
}

/**
 * Delete a contact in AddressBook.
 * @param abook Pointer to AddressBook
 * @param contact Contact to delete
 * @param Pointer to memory where error is supposed to be saved
 * @return FALSE if success TRUE otherwise
 */
gboolean plugin_delete_contact(
		AddressBook* abook, const Contact* contact, gchar** error) {
	
	/* Delete contact */
			
	abook->dirty = TRUE;

	return FALSE;
}

/**
 * Update a contact in AddressBook.
 * @param abook Pointer to AddressBook
 * @param contact Contact to update
 * @param Pointer to memory where error is supposed to be saved
 * @return FALSE if success TRUE otherwise
 */
gboolean plugin_update_contact(
		AddressBook* abook, const Contact* contact, gchar** error) {
	
	/* Update contact */
	
	abook->dirty = TRUE;

	return FALSE;
}

/**
 * Open an AddressBook.
 * @param abook Pointer to AddressBook
 * @param Pointer to memory where error is supposed to be saved
 * @return TRUE if success FALSE otherwise
 */
gboolean plugin_abook_open(AddressBook* abook, gchar** error) {
	closed_books_remove(abook);
	if (abook->dirty) {
		/* Open Address book */
		if (*error) {
			show_message(NULL, GTK_UTIL_MESSAGE_WARNING, "%s", *error);
			g_free(*error);
			*error = NULL;
			closed_books = g_list_prepend(closed_books, abook);
			return FALSE;
		}
		abook->dirty = FALSE;
	}
	abooks = g_list_prepend(abooks, abook);
	abook->open = TRUE;

	return TRUE;	
}

/**
 * Delete an AddressBook.
 * @param abook Pointer to AddressBook
 * @param Pointer to memory where error is supposed to be saved
 * @return TRUE if success FALSE otherwise
 */
gboolean plugin_abook_delete(AddressBook* abook, gchar** error) {
	if (! abook)
		return FALSE;
	
	if (abook->open)	
		self->abook_close(abook, error);
	/* Remove from closed books since deleting */
	closed_books = g_list_remove(closed_books, abook);
	if (abook->URL) {
		/* Delete address book */
	}
	
	return TRUE;	
}

/**
 * Close an AddressBook.
 * @param abook Pointer to AddressBook
 * @param Pointer to memory where error is supposed to be saved
 * @return TRUE if success FALSE otherwise
 */
gboolean plugin_abook_close(AddressBook* abook, gchar** error) {
	debug_print("List contains %d elements before\n", g_list_length(abooks));
	abooks = g_list_remove(abooks, abook);
	address_book_contacts_free(abook);
	debug_print("List contains %d elements after\n", g_list_length(abooks));
	closed_books = g_list_prepend(closed_books, abook);
	abook->open = FALSE;
	
	return TRUE;
}

/**
 * Get list of all address books.
 * @return GSList* List of AddressBook
 */
GSList* plugin_addrbook_all_get() {
	GSList *books = NULL;
	GList *cur;
	
	for (cur = abooks; cur; cur = g_list_next(cur)) {
		AddressBook* adrs = (AddressBook *) cur->data;
		books = g_slist_prepend(books, adrs);
	}
	
	return books;
}

/**
 * Get list of all address book names.
 * @return GSList* List of AddressBook names
 */
GSList* plugin_addrbook_names_all() {
	GList *list;
	GSList* names = NULL;
	
	for (list = abooks; list; list = g_list_next(list)) {
		AddressBook* book = (AddressBook *) list->data;
		names = g_slist_prepend(names, g_strdup(book->abook_name));
	}

	for (list = closed_books; list; list = g_list_next(list)) {
		AddressBook* book = (AddressBook *) list->data;
		names = g_slist_prepend(names, g_strdup(book->abook_name));
	}
	
	return names;
}

/**
 * Save config for an address book. If new is NULL write config for
 * new address book update otherwise.
 * @param old Pointer to AddressBook
 * @param new Pointer to AddressBook
 * @param Pointer to memory where error is supposed to be saved
 * @return TRUE if success FALSE otherwise
 */
gboolean plugin_abook_set_config(AddressBook* old,
								 AddressBook* new,
								 gchar** error) {
	if (new) {
		/* Update config for existing address book */
	}
	else {
		/* Write config for new address book */
	}

	return (*error) ? TRUE : FALSE;
}

/**
 * URL or URI to saved address book(s).
 * @param name Name of AddressBook
 * @return URL or URI. NULL means no special URL or URI required
 */
gchar* plugin_default_url(const gchar* name) {
	gchar* url = NULL;
	
	return url;
}

/**
 * Commit changes for all address books.
 * @param Pointer to memory where error is supposed to be saved
 * @return TRUE if success FALSE otherwise
 */
gboolean plugin_commit_all(gchar** error) {
	if (*error != NULL) {
		g_free(*error);
		*error = NULL;
	}
	return TRUE;
}

/**
 * Get list of closed address books. The list and data must be freed
 * by the caller.
 * @return GList of AddressBook
 */
GList* plugin_closed_books_get(void) {
	GList *cur, *list = NULL;
	
	for (cur = closed_books; cur; cur = g_list_next(cur)) {
	}
	
	return list;
}

/**
 * Initialize plugin.
 * @param Pointer to memory where error is supposed to be saved
 * @return FALSE if success TRUE otherwise
 */
gboolean plugin_init(gpointer self_ref, gchar** error) {
	self = (Plugin *) self_ref;
	if (*error != NULL) {
		g_free(*error);
		*error = NULL;
	}
	return FALSE;
}

/**
 * Unload plugin.
 * @return TRUE if success FALSE otherwise
 */
gboolean plugin_done(void) {
	g_free(feature);
	
	self = NULL;

	return TRUE;
}

/**
 * Get plugin features. Returned structure is owned by the plugin.
 * @return PluginFeature* Struct holding features for this plugin. NULL
 * in case of error
 */
const PluginFeature* plugin_provides (void) {
	feature = g_new0(PluginFeature, 1);
	
	if (feature == NULL) {
		return NULL;
	}

	feature->support = PLUGIN_READ_ONLY;
	feature->subtype = subtype;

	return feature;
}

/**
 * Get name of plugin. Returned memory is owned by the plugin.
 * @return name
 */
const gchar* plugin_name(void) {
	return NAME;
}

/**
 * Get description for plugin. Returned memory is owned by the plugin.
 * @return description
 */
const gchar* plugin_desc(void) {
	return _("This plugin is only a demo.");
}

/**
 * Get version of plugin. Returned memory is owned by the plugin.
 * @return version
 */
const gchar* plugin_version(void) {
	return PLUGINVERSION;
}

/**
 * Get functional type of plugin. Returned memory is owned by the plugin.
 * @return type
 */
PluginType plugin_type(void) {
	return PLUGIN_TYPE_SIMPLE;
}

/**
 * Get license of plugin. Returned memory is owned by the plugin.
 * @return license
 */
const gchar* plugin_license(void) {
	return "GPL3+";
}

/**
 * Does the plugin needs credentials for address books
 * @return bool
 */
gboolean plugin_need_credentials(void) {
	return FALSE;
}

/**
 * Get list of additional config
 * @return NULL if no additional config is required, a list of
 * ExtraConfig otherwise
 */
GSList* plugin_extra_config(void) {
	return NULL;
}

/**
 * Get file filter for this plugin
 * @return filter or NULL. If returning NULL means data storage is
 * URL based URI based otherwise
 */
const gchar* plugin_file_filter(void) {
	return "demo";
}
