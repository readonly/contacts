/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>
#include <errno.h>
#include <stdlib.h>

#include "plugin.h"
#include "plugin-init.h"
#include "gtk-utils.h"
#include "utils.h"
#include "parser.h"

#define NAME "XML plugin"

typedef struct {
	gchar* uid;
	gchar* image;
} Image;

/*
typedef struct {
	GHashTable* hash;
	gboolean	is_and;
	gint		equal;
} Compare;
*/
static Plugin* self = NULL;
static PluginFeature* feature = NULL;
static const gchar subtype[] = "Claws-mail native addressbook";
static GHashTable* attribs = NULL;
static gboolean old_abook = TRUE;
static GList* abooks = NULL;
static GList* closed_books = NULL;
static gchar self_home[] = "xml";
static gchar configrc[] = "xmlrc";
static ConfigFile* config = NULL;
static GSList* inactive_attribs = NULL;
static GSList* remaining_attribs = NULL;

static void contact_set_uid(AddressBook* abook, Contact* contact) {
	GHashTable* data = contact->data;
	
	if (data) {
		gchar* uid = g_strdup_printf("%ld", abook->next_uid);
		abook->next_uid++;
		swap_data(data, "uid", uid);
		g_free(uid);
	}
}

static void abook_set_next_uid(AddressBook* abook) {
	GList* cur;
	gulong uid = 0;
	gchar* id = NULL;
	
	for (cur = abook->contacts; cur; cur = g_list_next(cur)) {
		Contact* contact = (Contact *) cur->data;
		if (contact && contact->data) {
			extract_data(contact->data, "uid", (void **) &id);
			gulong n = strtol(id, NULL, 10);
			g_free(id);
			uid = (n > uid) ? n : uid;
		}	
	}
	abook->next_uid = ++uid;
}

static void abook_convert_uid(AddressBook* abook) {
	GList* cur;
	Contact* contact;
	
	for (cur = abook->contacts; cur; cur = g_list_next(cur)) {
		contact = (Contact *) cur->data;
		if (contact) {
			contact_set_uid(abook, contact);
		}
	}
}

static void abook_merge_uid(GList* abooks, AddressBook* book) {
	GList *copy, *cur;
	gboolean found = FALSE;
	
	copy = g_list_copy(abooks);
	copy = g_list_reverse(copy);
	for (cur = copy; cur; cur = g_list_next(cur)) {
		AddressBook* b = (AddressBook *) cur->data;
		if (strcmp(b->abook_name, book->abook_name) == 0) {
			if (found) {
				book->next_uid = b->next_uid;
				abook_convert_uid(book);
				return;
			}
			else
				found = TRUE;
		}
	}
	g_list_free(copy);
}

static void abook_get_images(AddressBook* abook) {
	GList* cur2;
	GDir* dir;
	GError* error = NULL;
	const gchar* filename;
	gchar* path;
	const gchar* dir_path = old_abook_addr_dir();
	Image* image;
	GSList* images = NULL, *cur1;
	gboolean found;
	gchar* uid = NULL;
	
	dir = g_dir_open(dir_path, 0, &error);
	if (!dir || error) {
		g_clear_error(&error);
		if (dir)
			g_dir_close(dir);
		return;
	}
	while ((filename = g_dir_read_name(dir))) {
		if (g_str_has_suffix(filename, ".png")) {
			image = g_new0(Image, 1);
			gchar* pos = g_strrstr(filename, ".png");
			image->uid = g_strndup(filename, pos - filename);
			path = g_build_filename (dir_path, filename, NULL);
			image->image = base64_encode_data(path);
			g_free(path);
			images = g_slist_prepend(images, image);
		}
	}
	g_dir_close(dir);

	for (cur1 = images; cur1; cur1 = g_slist_next(cur1)) {
		image = (Image *) cur1->data;
		found = FALSE;
		for (cur2 = abook->contacts; !found && cur2; cur2 = g_list_next(cur2)) {
			Contact* contact = (Contact *) cur2->data;
			if (contact && contact->data) {
				extract_data(contact->data, "uid", (void **) &uid);
				if (uid && strcmp(uid, image->uid) == 0) {
					g_free(uid);
					AttribDef* attr = 
						pack_data(ATTRIB_TYPE_STRING, "image", image->image);
					g_hash_table_replace(contact->data, g_strdup("image"), attr);
					found = TRUE;
				}
				uid = NULL;
			}
		}
		g_free(image->uid);
		g_free(image->image);
		g_free(image);
	}
	gslist_free(&images, NULL);
}

static void abooks_free() {
	GList* cur;
	
	if (! abooks && ! closed_books)
		return;
		
	for (cur = abooks; cur; cur = g_list_next(cur)) {
		AddressBook* abook = (AddressBook *) cur->data;
		address_book_free(&abook);
	}
	g_list_free(abooks);
	abooks = NULL;

	for (cur = closed_books; cur; cur = g_list_next(cur)) {
		AddressBook* abook = (AddressBook *) cur->data;
		address_book_free(&abook);
	}
	g_list_free(closed_books);
	closed_books = NULL;
}

static void open_addr_book(AddressBook* abook, gchar** error) {
	debug_print("Open book: %s Value: %s\n", abook->abook_name, abook->URL);
	parse_addr_book(abook, error);
	if (*error) {
		show_message(NULL, GTK_UTIL_MESSAGE_ERROR, "%s", *error);
	}
}

static gboolean person_attrib(gchar** str, gchar* key, AttribDef* attr) {
	gchar *tmp, *k, *v;
	
	if (strcasecmp("uid", key) == 0 ||
		strcasecmp("first-name", key) == 0 ||
		strcasecmp("last-name", key) == 0 ||
		strcasecmp("nick-name", key) == 0 ||
		strcasecmp("cn", key) == 0) {
		
		k = g_markup_escape_text(key, -1);
		v = g_markup_escape_text(attr->value.string, -1);
		if (*str) {
			tmp = g_strdup(*str);
			g_free(*str);
			*str = g_strconcat(tmp, " ", k, "=\"", v, "\"", NULL);
			g_free(tmp);
		}
		else
			*str = g_strconcat(k, "=\"", v, "\"", NULL);
		return TRUE;
	}
	
	return FALSE;
}

static void attributes_attrib(gchar** str, gchar* key, AttribDef* attr) {
	gchar *tmp = NULL, *k, *v;
	void* value = NULL;
	AttribType type;
	gboolean bool;
	gchar ch;
	gint i;
	
	type = get_data(attr, &value);
	switch (type) {
		case ATTRIB_TYPE_BOOLEAN:
			bool = *(gboolean *) value;
			tmp = (bool) ? g_strdup("true") : g_strdup("false");
			break;
		case ATTRIB_TYPE_CHAR:
			ch = *(gchar *) value;
			tmp = g_strdup_printf("%c", ch);
			break;
		case ATTRIB_TYPE_INT:
			i = *(gint *) value;
			tmp = g_strdup_printf("%i", i);
			break;
		case ATTRIB_TYPE_STRING:
			tmp = g_strdup((gchar *) value);
			break;
	}
	g_free(value);
	if (key)
		k = g_markup_escape_text(key, -1);
	else
		k = g_strdup("");
	if (tmp) {
		v = g_markup_escape_text(tmp, -1);
		g_free(tmp);
	}
	else
		v = g_strdup("");
	if (*str) {
		tmp = g_strdup(*str);
		g_free(*str);
		*str = g_strconcat(tmp, "\n\t\t\t<attribute name=\"", k, "\">",
				v, "</attribute>", NULL);
		g_free(tmp);
	}
	else
		*str = g_strconcat("<attribute name=\"", k, "\">", v,
				"</attribute>", NULL);
	g_free(k);
	g_free(v);
}

static gchar* serialize(GHashTable* hash, GSList* emails) {
	gchar *str = NULL, *tmp;
	gchar *person = NULL, *mails = NULL, *attributes = NULL;
	gchar *alias, *addr, *remarks;
	GSList *attr = NULL, *cur;
	GSList* mail;
	
	alias = addr = remarks = NULL;
	g_hash_table_foreach(hash, hash_table_keys_to_slist, &attr);
	for (cur = attr; cur; cur = g_slist_next(cur)) {
		AttribDef* attrdef = (AttribDef *) cur->data;
		AttribDef* attr = g_hash_table_lookup(hash, attrdef->attrib_name);
		if (person_attrib(&person, attrdef->attrib_name, attr))
			continue;
		attributes_attrib(&attributes, attrdef->attrib_name, attr);
	}
	gslist_free(&attr, attrib_def_free);
	for (mail = emails; mail; mail = g_slist_next(mail)) {
		Email* email = (Email *) mail->data;
		if (email->alias)
			alias = g_markup_escape_text(email->alias, -1);
		else
			alias = g_strdup("");
		if (email->email)
			addr = g_markup_escape_text(email->email, -1);
		else
			addr = g_strdup("");
		if (email->remarks)
			remarks = g_markup_escape_text(email->remarks, -1);
		else
			remarks = g_strdup("");
		debug_print("Email address: alias: %s email: %s remarks: %s\n",
			alias, addr, remarks);
		if (mails) {
			tmp = g_strdup(mails);
			g_free(mails);
			mails = g_strdup_printf("%s\t\t\t<address alias=\"%s\" "
					"email=\"%s\" remarks=\"%s\" />",
					tmp, alias, addr, remarks);
			g_free(tmp);
		}
		else
			mails = g_strdup_printf("<address alias=\"%s\" "
					"email=\"%s\" remarks=\"%s\" />",
					alias, addr, remarks);
	}
	if (! mails)
		mails = g_strdup("");
	if (! attributes)
		attributes = g_strdup("");

	str = g_strdup_printf("\t<person %s>\n"
		"\t\t<address-list>\n\t\t\t%s\n\t\t</address-list>\n"
		"\t\t<attribute-list>\n\t\t\t%s\n\t\t</attribute-list>\n"
		"\t</person>",
		person, mails, attributes);
	
	g_free(person);
	g_free(mails);
	g_free(attributes);
	g_free(alias);
	g_free(addr);
	g_free(remarks);
	
	return str;
}

static gchar* get_file_content(const gchar* path) {
	FILE *file;
	gchar *buffer;
	long length;
	
	file = g_fopen(path, "r");
	fseek(file, 0L, SEEK_END);
	length = ftell(file);
	rewind(file);
	buffer = g_new0(gchar, length + 1);
	fread(buffer, length, 1, file);
	fclose(file);
	
	return buffer;
}

static void merge_file(const gchar* path) {
	gchar *new_path, *buffer, *line;
	FILE* file;
	gint pos;
	
	new_path = g_strconcat(path, ".new", NULL);
	file = g_fopen(new_path, "w");
	buffer = get_file_content(path);
	gchar* head = buffer;
	pos = 0;
	line = NULL;
	while (*buffer) {
		if (*buffer != '\n') {
			if (pos)
				line = g_renew(gchar, line, pos + 1);
			else
				line = g_new0(gchar, 1);
			line[pos++] = *buffer++;
		}
		else {
			if (strcmp("</address-book>", line) != 0) {
				fwrite(line, pos, 1, file);
				fwrite("\n", 1, 1, file);
			}
			else
				break;
			pos = 0;
			g_free(line);
			line = NULL;
			buffer += 1;
		}
	}
	if (line)
		g_free(line);

	g_free(head);
	fclose(file);
	g_unlink(path);
	g_rename(new_path, path);
	g_free(new_path);
}

static void write_config_file(GSList** books, GSList** closed) {
	GSList *cur, *tmp, *cur_attribs;
	gchar* error = NULL;
	gchar* path = NULL;
	
	if (config) {
		path = g_strdup(config->path);
		plugin_config_free(&config);
	}
	else {
		gchar* basedir = get_self_home();
		path = g_strconcat(basedir, G_DIR_SEPARATOR_S, self_home,
					G_DIR_SEPARATOR_S, configrc, NULL);
		g_free(basedir);
	}
	config = plugin_config_new(path);
	g_free(path);
	config->comment = g_strconcat("Configuration file for ", NAME,
		".\nDo not change unless you KNOW what your are doing.", NULL);
	config->configured_books->group = g_strdup("configured address books");

	for (cur = *books; cur; cur = g_slist_next(cur)) {
		gchar* book = (gchar *) cur->data;
		config->configured_books->books =
			g_slist_prepend(config->configured_books->books, g_strdup(book));
		g_free(book);
	}
	gslist_free(books, NULL);

	config->closed_books->group = g_strdup("closed address books");

	for (cur = *closed; cur; cur = g_slist_next(cur)) {
		gchar* book = (gchar *) cur->data;
		config->closed_books->books =
			g_slist_prepend(config->closed_books->books, g_strdup(book));
		g_free(book);
	}
	gslist_free(closed, NULL);
	
	plugin_config_set(config, &error);
	if (error) {
		show_message(NULL, GTK_UTIL_MESSAGE_ERROR, "%s", error);
		g_free(error);
	}

	cur = NULL;
	cur_attribs = self->attrib_list();
	for (tmp = cur_attribs; tmp; tmp = g_slist_next(tmp)) {
		AttribDef* attrdef = (AttribDef *) tmp->data;
		cur = g_slist_prepend(cur, g_strdup(attrdef->attrib_name));
	}
	gslist_free(&cur_attribs, attrib_def_free);
	config_set_value(config, "supported attributes", "attributes", cur);
	gslist_free(&cur, g_free);

	cur = NULL;
	cur_attribs = self->inactive_attribs();
	for (tmp = cur_attribs; tmp; tmp = g_slist_next(tmp)) {
		AttribDef* attrdef = (AttribDef *) tmp->data;
		cur = g_slist_prepend(cur, g_strdup(attrdef->attrib_name));
	}
	gslist_free(&cur_attribs, attrib_def_free);
	config_set_value(config, "deactivated attributes", "attributes", cur);
	gslist_free(&cur, g_free);
}

static gint compare_book_names(gconstpointer a, gconstpointer b) {
	return strcmp((const gchar *) a, (const gchar *) b);
}

static void set_configured_books(GSList** addr_books, AddressBook* book) {
	gchar *path, *filename;
	
	path = g_path_get_basename(book->URL);
	gchar* pos = strrchr(path, '.');
	filename = g_strndup(path, pos - path);
	*addr_books = g_slist_prepend(
			*addr_books, g_strdup(filename));
	g_free(path);
	g_free(filename);
}
 
static void write_config() {
	gchar *basedir, *new_path, *buffer, *path;
	GList *books, *contacts;
	GSList *addr_books = NULL, *cl_books = NULL, *cur;
	FILE *file;
	gboolean backup = TRUE;
	gboolean append;
	gboolean merged = FALSE;

	basedir = get_self_home();
	
	debug_print("Number of address books: %d\n", g_list_length(abooks));
	for (books = abooks; books; books = g_list_next(books)) {
		AddressBook* book = (AddressBook *) books->data;
		if (old_abook) {
			path = g_strconcat(basedir, G_DIR_SEPARATOR_S, self_home,
					G_DIR_SEPARATOR_S, book->abook_name, ".xml", NULL);
		}
		else {
			if (! book->dirty) {
				set_configured_books(&addr_books, book);
				continue;
			}
			path = g_strdup(book->URL);
		}
		debug_print("Name: %s URL: %s\n", book->abook_name, path);

		if (g_file_test(path, G_FILE_TEST_EXISTS)) {
			if (backup) {
				buffer = get_file_content(path);
				new_path = g_strconcat(path, ".bak", NULL);
				file = g_fopen(new_path, "w");
				fwrite(buffer, strlen(buffer), 1, file);
				g_free(buffer);
				fclose(file);
				g_free(new_path);
				if (old_abook) {
					/*
					 * Old address book can result in several books
					 * having the same name
					 */
					/* fix uid since book are goind to be merge */
					abook_merge_uid(abooks, book);
					merge_file(path);
					append = TRUE;
					file = g_fopen(path, "a");
					merged = TRUE;
				}
				else {
					append = FALSE;
					file = g_fopen(path, "w");
				}
				backup = FALSE;
			}
			else {
				merge_file(path);
				file = g_fopen(path, "a");
			}
		}
		else {
			if (old_abook) {
				/* File names is book->abook_name + ".xml" */
				g_free(book->URL);
				book->URL = g_strdup(path);
			}
			backup = TRUE;
			append = FALSE;
			file = g_fopen(path, "w");
		}
		if (file == NULL) {
			g_error("%s: Could not create", path);
			g_free(path);
			g_free(basedir);
			return;
		}
		if (! append) {
			fprintf(file, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
			fprintf(file, "<address-book name=\"%s\">\n", book->abook_name);
		}
		for (contacts = book->contacts; contacts; contacts = g_list_next(contacts)) {
			Contact* contact = (Contact *) contacts->data;
			gchar* str = serialize(contact->data, contact->emails);
			fprintf(file, "%s\n", str);
			g_free(str);
		}
		fprintf(file, "</address-book>\n");
		fclose(file);
		g_free(path);
		backup = TRUE;
		cur = g_slist_find_custom(
				addr_books, book->abook_name, compare_book_names);
		if (! cur) {
			set_configured_books(&addr_books, book);
		}
	}

	for (books = closed_books; books; books = g_list_next(books)) {
		AddressBook* book = (AddressBook *) books->data;
		set_configured_books(&cl_books, book);
	}
	
	write_config_file(&addr_books, &cl_books);
	
	g_free(basedir);
	
	if (old_abook && merged) {
		show_message(NULL, GTK_UTIL_MESSAGE_INFO, 
			_("One or more address books had identical names.\n"
			  "They have therefore been merged under a common name."));
	}
}

static void reactivate_attribs(gpointer key, gpointer value, gpointer data) {
	GSList** new_list = (GSList**) data;
	GSList *cur;
	gboolean more = TRUE;

	for (cur = inactive_attribs; cur && more; cur = g_slist_next(cur)) {
		AttribDef* attr = (AttribDef *) cur->data;
		if (utf8_collate(attr->attrib_name, (gchar *) key) == 0) {
			*new_list = g_slist_remove(*new_list, attr);
			attrib_def_free(attr);
			more = FALSE;
		}
	}
}

static void deactivate_attribs(gpointer key, gpointer value, gpointer data) {
	GHashTable* new_attr = (GHashTable *) data;
	
	//hash_table_dump(new_attr, stderr);
	if (! g_hash_table_lookup_extended(new_attr, key, NULL, NULL)) {
		inactive_attribs = g_slist_prepend(inactive_attribs, attrib_def_copy(value));
	}
}

static void closed_books_remove(AddressBook* book) {
	GList* found = g_list_find_custom(closed_books, book, address_book_compare);
	if (found)
		closed_books = g_list_remove_link(closed_books, found);
}

gboolean plugin_init(gpointer self_ref, gchar** error) {
	gchar *basedir, *path;
	GSList *list = NULL, *cur, *found;
	ConfiguredBooks* cf_books;
	ClosedBooks* cl_books;
	
	self = (Plugin *) self_ref;
	if (*error != NULL) {
		g_free(*error);
		*error = NULL;
	}
	hash_table_free(&attribs);
	old_abook = first_time(self_home, configrc, error);
	if (*error)
		return TRUE;
	
	if (old_abook) {
		attribs = plugin_get_attribs(&old_abook, error);
		if (*error || ! attribs) {
			return TRUE;
		}
		self->abook_open(NULL, error);
	}
	else {
		basedir = get_self_home();
		path = g_strconcat(
			basedir, G_DIR_SEPARATOR_S, self_home,
			G_DIR_SEPARATOR_S, configrc, NULL);
		
		config = plugin_config_new(path);
		config_get(config, error);
		
		if (*error) {
			show_message(NULL, GTK_UTIL_MESSAGE_ERROR, "%s", *error);
		}
		else {
			if (config) {
				cf_books = config->configured_books;
				cl_books = config->closed_books;
				
				if (cf_books) {
					g_free(path);
					path = g_strconcat(basedir, G_DIR_SEPARATOR_S,
						self_home, G_DIR_SEPARATOR_S, NULL);
					for (cur = cf_books->books; cur; cur = g_slist_next(cur)) {
						gchar* book = (gchar *) cur->data;
						found = g_slist_find_custom(
							cl_books->books, book, compare_book_names);
						if (! cl_books || ! found) {
							AddressBook* abook = address_book_new();
							abook->URL = g_strconcat(path, book, ".xml", NULL);
							gboolean ok = self->abook_open(abook, error);
							if (! ok)
								closed_books = g_list_prepend(closed_books, abook);
						}
					}
				}
				if (cl_books) {
					g_free(path);
					path = g_strconcat(basedir, G_DIR_SEPARATOR_S,
						self_home, G_DIR_SEPARATOR_S, NULL);
					for (cur = cl_books->books; cur; cur = g_slist_next(cur)) {
						gchar* book = (gchar *) cur->data;
						found = g_slist_find_custom(
							cf_books->books, book, compare_book_names);
						if (! found) {
							AddressBook* abook = address_book_new();
							abook->URL = g_strconcat(path, book, ".xml", NULL);
							addr_book_set_name(abook, error);
							if (*error) {
								show_message(NULL, GTK_UTIL_MESSAGE_ERROR, "%s", *error);
								g_free(*error);
								*error = NULL;
							}
							closed_books = g_list_prepend(closed_books, abook);
						}
					}
				}
				config_get_value(config, "supported attributes", "attributes", &list);				
				attribs = hash_table_new();
				for (cur = list; cur; cur = g_slist_next(cur)) {
					gchar* key = (gchar *) cur->data;
					debug_print("Adding '%s' to attributes\n", key);
					AttribDef* attr = g_new0(AttribDef, 1);
					attr->attrib_name = g_strdup(key);
					attr->type = ATTRIB_TYPE_STRING;
					g_hash_table_replace(attribs, g_strdup(key), attr);
				}
				gslist_free(&list, g_free);

				config_get_value(config, "deactivated attributes", "attributes", &list);				
				for (cur = list; cur; cur = g_slist_next(cur)) {
					gchar* key = (gchar *) cur->data;
					debug_print("Adding '%s' to deactivated attributes\n", key);
					AttribDef* attr = g_new0(AttribDef, 1);
					attr->attrib_name = g_strdup(key);
					attr->type = ATTRIB_TYPE_STRING;
					inactive_attribs = g_slist_prepend(inactive_attribs, attr);
				}
				gslist_free(&list, g_free);

				//hash_table_dump(attribs, stderr);
			}
		}
		g_free(basedir);
		g_free(path);
	}

	return FALSE;
}

void plugin_reset(gchar** error) {
	abooks_free();
}

gboolean plugin_done(void) {
	gchar* error = NULL;
	
	write_config();
	plugin_config_free(&config);
	g_free(error);
	g_free(feature);
	feature = NULL;
	hash_table_free(&attribs);
	abooks_free();
	old_abook_free_dir();
	gslist_free(&inactive_attribs, attrib_def_free);
	
	self = NULL;

	return TRUE;
}

const PluginFeature* plugin_provides(void) {
	feature = g_new0(PluginFeature, 1);
	
	if (feature == NULL) {
		return NULL;
	}

	feature->support = PLUGIN_READ_WRITE | PLUGIN_ADVANCED_SEARCH;
	feature->subtype = subtype;

	return feature;
}

const gchar* plugin_name(void) {
	return NAME;
}

const gchar* plugin_desc(void) {
	return _("This plugin provides access to the Claws-mail native "
			 "addressbook\nand thereby replacing the old addressbook.\n"
			 "\n"
			 "The plugin provides lookup for a contact using any\n"
			 "available attribute. Supported wildcards are:\n"
			 "   *: Means any number of character(s) matches\n"
			 "   ?: Means any character match"
			);
}

const gchar* plugin_version(void) {
	return VERSION;
}

PluginType plugin_type(void) {
	return PLUGIN_TYPE_SIMPLE;
}

const gchar* plugin_file_filter(void) {
	return "xml";
}

const gchar* plugin_license(void) {
	return "GPL3+";
}

GSList* plugin_attrib_list(void) {
	GSList* list = NULL;
	
	if (attribs) {
		g_hash_table_foreach(attribs, hash_table_keys_to_slist, &list);
	}
	return list;
}

void plugin_attribs_set(GHashTable* attributes) {
	GSList* new_list = gslist_deep_copy(inactive_attribs, attrib_def_copy);

	//hash_table_dump(attributes, stderr);	
	g_hash_table_foreach(attributes, reactivate_attribs, &new_list);
	gslist_free(&inactive_attribs, attrib_def_free);
	inactive_attribs = gslist_deep_copy(new_list, attrib_def_copy);
	gslist_free(&new_list, attrib_def_free);
	g_hash_table_foreach(attribs, deactivate_attribs, attributes);
	hash_table_free(&attribs);
	attribs = hash_table_copy(attributes);
}
	
GSList* plugin_get_contact(
		AddressBook* abook, const gchar* search_token, gchar** error) {
	GList* cur;
	GSList *contacts = NULL, *basic, *list;
	gboolean found = FALSE;
	Contact* contact = NULL;

	if (abook && search_token) {
		for (cur = abook->contacts; cur; cur = g_list_next(cur)) {
			contact = (Contact *) cur->data;
			if (! contact)
				continue;
			
			list = get_basic_attributes(contact);
			for (basic = list; !found && basic; basic = g_slist_next(basic)) {
				gchar* attr = (gchar *) basic->data;
				if (attr) {
					found = match_string_pattern(search_token, attr, FALSE);
				}
			}
			gslist_free(&list, g_free);
			if (found) {
				contacts = g_slist_prepend(contacts, contact);
				found = FALSE;
			}
		}
	}

	return contacts;
}

gboolean plugin_set_contact(
		AddressBook* abook, const Contact* contact, gchar** error) {
	gchar* uid = NULL;
	
	if (debug_get_mode())
		contact_dump(contact, stderr);
	extract_data(contact->data, "uid", (void **) &uid);
	if (! uid || strlen(uid) < 1)
		contact_set_uid(abook, (Contact *) contact);
	g_free(uid);

	addr_book_set_contact(abook, (Contact *) contact, error);
	if (*error)
		return TRUE;
	else
		abook->dirty = TRUE;
			
	return FALSE;
}

gboolean plugin_delete_contact(
		AddressBook* abook, const Contact* contact, gchar** error) {
	
	addr_book_delete_contact(abook, (Contact *) contact, error);
	if (*error)
		return TRUE;
	else
		abook->dirty = TRUE;
		
	return FALSE;
}

gboolean plugin_update_contact(
		AddressBook* abook, const Contact* contact, gchar** error) {
	
	if (debug_get_mode())
		contact_dump(contact, stderr);
	self->delete_contact(abook, contact, error);
	if (*error)
		return TRUE;
	self->set_contact(abook, contact, error);
	if (*error)
		return TRUE;

	return FALSE;
}

GSList* plugin_search_contact(AddressBook* abook,
							  const Contact* search_tokens,
							  gboolean is_and,
							  gchar** error) {
	GList* cur;
	GSList *contacts = NULL;
	Contact* contact = NULL;
	Compare* comp;
	
	if (abook && search_tokens && (search_tokens->data || search_tokens->emails)) {
		comp = g_new0(Compare, 1);
		for (cur = abook->contacts; cur; cur = g_list_next(cur)) {
			contact = (Contact *) cur->data;
			if (! contact)
				continue;
			
			comp->hash = contact->data;
			comp->is_and = is_and;
			comp->equal = -1;
			
			g_hash_table_foreach(search_tokens->data,
					contact_compare_values, comp);
			if ((comp->is_and && comp->equal > 0) ||
				(comp->equal < 1 && !comp->is_and)) {
				gint res = email_compare_values(
					search_tokens->emails, contact->emails, is_and);
				if (res != -1)
					comp->equal = res;
			}
			if (comp->equal > 0)			
				contacts = g_slist_prepend(contacts, contact);
		}
		g_free(comp);
	}

	return contacts;
}

gboolean plugin_abook_open(AddressBook* abook, gchar** error) {
	GSList *books = NULL, *cur;

	if (abook == NULL) {
		gchar* index = g_strconcat(
				old_abook_addr_dir(),
				G_DIR_SEPARATOR_S,
				old_abook_index_file(),
				NULL);
		if (index) {
		    books = parse_index_file(index, error);
		    g_free(index);
		    if (*error) {
			show_message(NULL, GTK_UTIL_MESSAGE_WARNING, "%s", *error);
			return FALSE;
		    }
		    for (cur = books; cur; cur = g_slist_next(cur)) {
			Attribute* attr = (Attribute *) cur->data;
			abook = address_book_new();
			abook->abook_name = g_strdup(attr->attr_name);
			abook->URL = g_strconcat(
			    old_abook_addr_dir(), G_DIR_SEPARATOR_S, attr->file, NULL);
			open_addr_book(abook, error);
			if (*error)
				continue;
			abook_get_images(abook);
			abook_convert_uid(abook);
			abooks = g_list_prepend(abooks, abook);
		    }
		    attribute_list_free(&books);
		}
	}
	else {
		closed_books_remove(abook);
		if (abook->dirty) {
			open_addr_book(abook, error);
			if (*error) {
				show_message(NULL, GTK_UTIL_MESSAGE_WARNING, "%s", *error);
				g_free(*error);
				*error = NULL;
				closed_books = g_list_prepend(closed_books, abook);
				return FALSE;
			}
			abook_set_next_uid(abook);
			abook->dirty = FALSE;
		}
		abooks = g_list_prepend(abooks, abook);
	}
	if (abook)	
	    abook->open = TRUE;

	return TRUE;	
}

gboolean plugin_abook_delete(AddressBook* abook, gchar** error) {
	if (! abook)
		return FALSE;
	
	if (abook->open)
		self->abook_close(abook, error);
	/* Remove from closed books since deleting */
	closed_books = g_list_remove(closed_books, abook);
	if (abook->URL) {
		if (g_unlink(abook->URL) < 0) {
			*error = g_new0(gchar, 1024);
			strerror_r(errno, *error, 1024);
		}
	}
	
	return TRUE;	
}

gboolean plugin_abook_close(AddressBook* abook, gchar** error) {
	debug_print("List contains %d elements before\n", g_list_length(abooks));
	abooks = g_list_remove(abooks, abook);
	address_book_contacts_free(abook);
	debug_print("List contains %d elements after\n", g_list_length(abooks));
	closed_books = g_list_prepend(closed_books, abook);
	abook->open = FALSE;
	
	return TRUE;
}

GSList* plugin_addrbook_all_get() {
	GSList *books = NULL;
	GList *cur;
	
	for (cur = abooks; cur; cur = g_list_next(cur)) {
		AddressBook* adrs = (AddressBook *) cur->data;
		books = g_slist_prepend(books, adrs);
	}
	
	return books;
}

GSList* plugin_addrbook_names_all() {
	GList *list;
	GSList* names = NULL;
	
	for (list = abooks; list; list = g_list_next(list)) {
		AddressBook* book = (AddressBook *) list->data;
		names = g_slist_prepend(names, g_strdup(book->abook_name));
	}

	for (list = closed_books; list; list = g_list_next(list)) {
		AddressBook* book = (AddressBook *) list->data;
		names = g_slist_prepend(names, g_strdup(book->abook_name));
	}
	
	return names;
}

gboolean plugin_abook_set_config(AddressBook* old,
								 AddressBook* new,
								 gchar** error) {
	if (new) {
		addr_book_update_config(old, new, error);
	}
	else {
		addr_book_set_config(old, error);
	}

	return (*error) ? TRUE : FALSE;
}

gchar* plugin_default_url(const gchar* name) {
	gchar* basedir;
	basedir = get_self_home();
	gchar* url;
	
	if (name) {
		url = g_strconcat(basedir, G_DIR_SEPARATOR_S, 
			self_home, G_DIR_SEPARATOR_S, name, ".xml", NULL);
	}
	else {
		url = g_strconcat(basedir, G_DIR_SEPARATOR_S, self_home, NULL);
	}
	
	return url;
}

gboolean plugin_commit_all(gchar** error) {
	return TRUE;
}

GSList* plugin_remaining_attribs(void) {
	return gslist_deep_copy(remaining_attribs, attrib_def_copy);
}

GSList* plugin_inactive_attribs(void) {
	return gslist_deep_copy(inactive_attribs, attrib_def_copy);
}

GList* plugin_closed_books_get(void) {
	GList *cur, *list = NULL;
	
	for (cur = closed_books; cur; cur = g_list_next(cur)) {
		/* No deep copy since list of contacts will always be NULL */
		list = g_list_prepend(list, address_book_copy(cur->data, FALSE));
	}
	
	return list;
}

gboolean plugin_need_credentials(void) {
	return FALSE;
}

GSList* plugin_extra_config(void) {
	return NULL;
}
