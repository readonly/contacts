/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>
#include <ldap.h>
#include <stdlib.h>
#include "plugin.h"
#include "gtk-utils.h"
#include "utils.h"

#define NAME "LDAP plugin"
#define TIMEOUT 20

typedef struct {
	gboolean	ldaps;
	gchar*		host;
	gint		port;
} Connection;

typedef struct {
	LDAP*		ldap;
	GSList*		baseDN;
	gchar*		search_base;
	gchar*		dn;
	gint		max_entries;
	gint		timeout;
	gint		error_code;
} Server;

typedef struct {
	gchar*			id;
	const Contact*	contact;
	gboolean		intersection;
	Connection*		connection;
	Server*			server;
} AbookConnection;

/* Reference to self */
static Plugin* self = NULL;
/* See plugin.h */
static PluginFeature* feature = NULL;
/* Description */
static const gchar subtype[] = "LDAP support";
/* List of AddressBook */
static GList* abooks = NULL;
/* List of closed AddressBook */
static GList* closed_books = NULL;

/* Object classes used by the plugin */
static const gchar*  objectclass[] = {
	"person", "organizationalPerson", "inetOrgPerson", NULL};
static const gchar* required_attributes[] = {"cn", "sn", NULL};
/*
 * HashTable containing names of supported attributes.
 * Each keys holds an AttribDef.
 */
static GHashTable* attribs = NULL;

/* List of inactive supported attributes */
static GSList* inactive_attribs = NULL;

/* List of not active or inactive attributes this plugin supports*/
static GSList* remaining_attribs = NULL;

/* List of extra config */
static GSList* extra_config = NULL;

static gchar self_home[] = "ldap";
static gchar configrc[] = "ldaprc";
static ConfigFile* config = NULL;

static GSList* abook_connection = NULL;

static const char *other_attributes[] = {
    "telephoneNumber",
    "description", /* comments */
    "postalAddress",
    "homePhone",
    "homePostalAddress",
    "mobile",
    "ou",
    "title",
    "telexNumber",
    "facsimileTelephoneNumber",
    "street",
    "postOfficeBox",
    "postalCode",
    "st", /* state or province */
    "l", /* locality Name */
    "departmentNumber",
    "initials",
    "labeledURI",
    "pager",
    "roomNumber",
    NULL
};

static void deconnect(Server* server) {
    if (server->ldap) {
        ldap_unbind_ext(server->ldap, NULL, NULL);
        server->ldap = NULL;
    }
    if (server->baseDN) {
        gslist_free(&server->baseDN, g_free);
    }
	if (server->dn) {
		g_free(server->dn);
		server->dn = NULL;
	}
	if (server->search_base) {
		g_free(server->search_base);
		server->search_base = NULL;
	}
}

static void connect_info_free(Connection** connection) {
	g_free((*connection)->host);
	g_free(*connection);
	*connection = NULL;
}

static AbookConnection* abook_connection_new() {
	AbookConnection* ac = g_new0(AbookConnection, 1);
	ac->id = NULL;
	ac->connection = NULL;
	ac->server = g_new0(Server, 1);
	ac->server->timeout = TIMEOUT;

	return ac;
}

static void abook_connection_free(gpointer abook_connection) {
	AbookConnection* abook_con = (AbookConnection *) abook_connection;
	if (! abook_con)
		return;

	if (abook_con->id) {
		g_free(abook_con->id);
		abook_con->id = NULL;
	}
	if (abook_con->connection) {
		connect_info_free(&abook_con->connection);
	}
	if (abook_con->server) {
		deconnect(abook_con->server);
		g_free(abook_con->server);
		abook_con->server = NULL;
	}
	g_free(abook_con);
	abook_con = NULL;
}

static AbookConnection* get_abook_connection(AddressBook* abook) {
	GSList* cur;
	gchar* id;
	AbookConnection* ac = NULL;
	gboolean found = FALSE;

	if (! abook || !abook->abook_name || !abook->URL)
		return NULL;

	gchar* plain = g_strconcat(abook->abook_name, abook->URL, NULL);
	id = sha256(plain);
	debug_print("plain: %s id: %s\n", plain, id);
	g_free(plain);

	for (cur = abook_connection; cur && !found; cur = g_slist_next(cur)) {
		ac = (AbookConnection *) cur->data;
		if (ac && strcmp(ac->id, id) == 0)
			found = TRUE;
		else
			ac = NULL;
	}
	g_free(id);

	return ac;
}

static AddressBook* find_addressbook(AbookConnection* ac) {
	GList* cur;
	gchar* id = NULL;
	AddressBook* abook = NULL;

	for (cur = abooks; cur; cur = g_list_next(cur)) {
		abook = (AddressBook *) cur->data;
		gchar* plain = g_strconcat(abook->abook_name, abook->URL, NULL);
		id = sha256(plain);
		g_free(plain);
		if (strcmp(ac->id, id) == 0) {
			g_free(id);
			return abook;
		}
	}

	g_free(id);

	return abook;
}

static Connection* split_url(AddressBook* abook, gchar** error) {
	Connection* info = g_new0(Connection, 1);
	gchar** cur;
	gchar* tmp;

	gchar** parts = g_strsplit(abook->URL, ":", 0);

	if (*parts) {
		cur = parts;
		while (*cur && ! *error) {
			if (strcmp("ldaps", *cur) == 0)
				info->ldaps = TRUE;
			else if (strcmp("ldap", *cur) == 0)
				info->ldaps = FALSE;
			else if (atoi(*cur) > 0)
				info->port = atoi(*cur);
			else {
				if (info->host)
					*error = g_strconcat(abook->URL, ": ", _("Wrong URL"), NULL);
				else {
					if (g_str_has_prefix(*cur, "//")) {
						tmp = g_strdup(*cur+2);
					}
					else
						tmp = g_strdup(*cur);
					info->host = g_strdup(tmp);
					g_free(tmp);
				}
			}
			cur += 1;
		}
		g_strfreev(parts);
	}
	else
		*error = g_strconcat(abook->URL, ": ", _("Wrong URL"), NULL);

	if (info->ldaps) {
		if (info->port < 1)
			info->port = 636;
	}
	else {
		if (info->port < 1)
			info->port = 389;
	}

	return info;
}

static const gchar* get_ldap_error(int rc) {
	const gchar* error = ldap_err2string(rc);


	debug_print("%s\n", error);
	if (rc == LDAP_TIMELIMIT_EXCEEDED) {
		show_message(NULL, GTK_UTIL_MESSAGE_WARNING,
			_("Configured timeout limit exceeded. Try increase timeout on "
			  "advanced configuration page"));
		error = NULL;
	}
	else if (rc == LDAP_SIZELIMIT_EXCEEDED) {
		show_message(NULL, GTK_UTIL_MESSAGE_WARNING,
			_("Configured number of entries limit exceeded. Try increase entries on "
			  "advanced configuration page"));
		error = NULL;
	}

	return error;
}

static int ldap_bind(AbookConnection* abook_con) {
	struct berval cred;
	AddressBook* abook = find_addressbook(abook_con);
	Server* server = abook_con->server;

	if (abook && abook->password) {
            cred.bv_val = abook->password;
            cred.bv_len = strlen(abook->password);
        }
        else {
            cred.bv_val = "";
            cred.bv_len = 0;
	}

	debug_print("binding: DN->%s\n", (abook->username) ? abook->username : "null");
	return ldap_sasl_bind_s(server->ldap, abook->username,
			LDAP_SASL_SIMPLE, &cred, NULL, NULL, NULL);
}

static gchar* get_uri(Connection* info) {
	gchar *tmp, *uri;

	if (info->ldaps)
		tmp = g_strconcat("ldaps://", info->host, NULL);
	else
		tmp = g_strconcat("ldap://", info->host, NULL);
	if (info->port > 0)
		uri = g_strdup_printf("%s:%d", tmp, info->port);
	else
		uri = g_strdup(tmp);

	g_free(tmp);

	return uri;
}

static void set_default_values(Server** server) {
	GSList *config, *cur;
	ExtraConfig* conf;

	config = self->extra_config();
	for (cur = config; cur; cur = g_slist_next(cur)) {
		conf = (ExtraConfig *) cur->data;
		if (conf->label && strcasecmp("timeout (sec)", conf->label) == 0)
			(*server)->timeout = conf->preset_value.spin_btn;
		else if (conf->label && strcasecmp("search base", conf->label) == 0)
			(*server)->search_base = g_strdup(conf->preset_value.entry);
		else if (conf->label && strcasecmp("max entries", conf->label) == 0)
			(*server)->max_entries = conf->preset_value.spin_btn;
	}

	gslist_free(&config, extra_config_free);
}

static Server* get_server_config(AddressBook* abook) {
	GSList* cur;
	ExtraConfig* conf;
	Server* server = g_new0(Server, 1);

	set_default_values(&server);

	for (cur = abook->extra_config; cur; cur = g_slist_next(cur)) {
		conf = (ExtraConfig *) cur->data;
		if (conf->label && strcasecmp("timeout (sec)", conf->label) == 0)
			server->timeout = conf->current_value.spin_btn;
		else if (conf->label && strcasecmp("search base", conf->label) == 0)
			server->search_base = g_strdup(conf->current_value.entry);
		else if (conf->label && strcasecmp("max entries", conf->label) == 0)
			server->max_entries = conf->current_value.spin_btn;
	}

	return server;
}

#define SEARCH_BASE ""
#define TEST_FILTER "(objectClass=*)"
#define TEST_ATTRIB "namingContexts"
static void find_search_base(AbookConnection* abook_con, int* err, gchar** error) {
    gchar* attribs[2];
    struct timeval timeOut;
    LDAPMessage* result = NULL;
    LDAPMessage* entry;
    BerElement *ber;
    gchar *attribute;
    struct berval **vals;
    gint rc, i;
    Server* server = abook_con->server;

    if (server->timeout > 0)
    	timeOut.tv_sec = server->timeout;
	else
    	timeOut.tv_sec = TIMEOUT;
    timeOut.tv_usec = 0;
    server->baseDN = NULL;
    attribs[0] = TEST_ATTRIB;
    attribs[1] = NULL;
    rc = ldap_search_ext_s(server->ldap, SEARCH_BASE, LDAP_SCOPE_BASE,
        TEST_FILTER, attribs, 0, NULL, NULL, &timeOut, 0, &result);

    if (rc == LDAP_SUCCESS) {
        /* Process entries */
        for (entry = ldap_first_entry(server->ldap, result);
                entry; entry = ldap_next_entry(server->ldap, entry)) {
            /* Process attributes */
            for (attribute = ldap_first_attribute(server->ldap, entry, &ber);
                   attribute; attribute = ldap_next_attribute(server->ldap, entry, ber)) {
                if (strcasecmp(attribute, TEST_ATTRIB) == 0) {
                    vals = ldap_get_values_len(server->ldap, entry, attribute);
                    if (vals) {
                        for (i = 0; vals[i]; i++) {
                            server->baseDN = g_slist_prepend(
                                server->baseDN, g_strndup(vals[i]->bv_val,
                                    vals[i]->bv_len));
                        }
                    }
                    ldap_value_free_len(vals);
                }
                ldap_memfree(attribute);
            }
            if (ber) {
                ber_free(ber, 0);
            }
            ber = NULL;
        }
    }
    else {
		if (error)
        	*error = g_strdup(get_ldap_error(rc));
        if (*error)
        	*err = rc;
    }
    if (result)
        ldap_msgfree(result);
}

static gboolean ldap_connect(AbookConnection* abook_con, gchar** error) {
    struct timeval timeOut;
    int version = LDAP_VERSION3;
    int reqcert = LDAP_OPT_X_TLS_ALLOW;
    int rc;
    gchar* uri;
    gboolean TLS = FALSE;
    AddressBook* abook = find_addressbook(abook_con);
    Server* server = NULL;
    Connection* info = NULL;

    if (! abook || ! abook->URL || strlen(abook->URL) < 0) {
		*error = g_strdup(_("Missing URL"));
        return TRUE;
	}

	abook_con->server = server = get_server_config(abook);
	abook_con->connection = info = split_url(abook, error);
	if (*error)
		return TRUE;

    if (server->ldap) {
        ldap_unbind_ext(server->ldap, NULL, NULL);
        server->ldap = NULL;
    }

    timeOut.tv_usec = 0;
    if (server->timeout > 0)
    	timeOut.tv_sec = server->timeout;
	else
    	timeOut.tv_sec = TIMEOUT;

    rc = ldap_set_option(NULL, LDAP_OPT_PROTOCOL_VERSION, &version);
    if (rc != LDAP_OPT_SUCCESS) {
        ldap_unbind_ext(server->ldap, NULL, NULL);
        server->ldap = NULL;
        *error = g_strdup(ldap_err2string(rc));
        debug_print("%s\n", ldap_err2string(rc));
        return TRUE;
    }
    ldap_set_option(NULL, LDAP_OPT_NETWORK_TIMEOUT, &timeOut);
    ldap_set_option(NULL, LDAP_OPT_REFERRALS, LDAP_OPT_OFF);

#ifdef LDAP_TLS
    if (info->ldaps) {
        debug_print("LDAP_OPT_X_TLS_ALLOW\n");
        rc = ldap_set_option(NULL, LDAP_OPT_X_TLS_REQUIRE_CERT, &reqcert);
        if (rc != LDAP_OPT_SUCCESS) {
            ldap_unbind_ext(server->ldap, NULL, NULL);
            server->ldap = NULL;
            *error = g_strdup(ldap_err2string(rc));
            debug_print("%s\n", ldap_err2string(rc));
            return TRUE;
        }
    }
#else
	if (info->ldaps) {
		debug_print("LDAPS requested but SSL is unavailable. Trying LDAP\n");
	}
#endif

	uri = get_uri(info);
    rc = ldap_initialize(&server->ldap, uri);
    g_free(uri);
    if (rc != LDAP_SUCCESS) {
        ldap_unbind_ext(server->ldap, NULL, NULL);
        server->ldap = NULL;
        *error = g_strdup(
                _("initialize: LDAP session initialization failed."));
        debug_print("initialize: LDAP session initialization failed\n");
        return TRUE;
    }

#ifdef LDAP_TLS
    if (! info->ldaps) {
        rc = ldap_start_tls_s(server->ldap, NULL, NULL);
        if (rc != LDAP_SUCCESS) {
            debug_print("start_tls: %s\n", ldap_err2string(rc));
        }
        else
        	TLS = TRUE;
    }
#endif

    /* anonymous? */
    if (abook->username && strlen(abook->username) > 0) {
        rc = ldap_bind(abook_con);
        if (rc != LDAP_SUCCESS) {
            ldap_unbind_ext(server->ldap, NULL, NULL);
            server->ldap = NULL;
            *error = g_strdup(ldap_err2string(rc));
            debug_print("bind: %s\n", ldap_err2string(rc));
            return TRUE;
        }
    }

    debug_print("Connected to %s on port %d\n", info->host, info->port);
    if (info->ldaps || TLS)
        debug_print("Connected using %s\n", (info->ldaps) ? "SSL" : "TLS");

	if (! server->search_base) {
		find_search_base(abook_con, &rc, error);
		if (rc == LDAP_SUCCESS)
			server->search_base = g_strdup((gchar *) server->baseDN->data);
		else {
			ldap_unbind_ext(server->ldap, NULL, NULL);
			server->ldap = NULL;
			*error = g_strdup_printf(ldap_err2string(rc));
			debug_print("Search base: %s\n", ldap_err2string(rc));
			return TRUE;
		}
	}

    return FALSE;
}

static void abooks_free() {
	GList* cur;

	if (! abooks && ! closed_books)
		return;

	for (cur = abooks; cur; cur = g_list_next(cur)) {
		AddressBook* abook = (AddressBook *) cur->data;
		address_book_free(&abook);
	}
	g_list_free(abooks);
	abooks = NULL;

	for (cur = closed_books; cur; cur = g_list_next(cur)) {
		AddressBook* abook = (AddressBook *) cur->data;
		address_book_free(&abook);
	}
	g_list_free(closed_books);
	closed_books = NULL;
}

static void write_config_file() {
	GList *cur;
	gchar* error = NULL;
	gchar* path = NULL;

	if (config) {
		path = g_strdup(config->path);
		plugin_config_free(&config);
	}
	else {
		gchar* basedir = get_self_home();
		path = g_strconcat(basedir, G_DIR_SEPARATOR_S, self_home,
					G_DIR_SEPARATOR_S, configrc, NULL);
		g_free(basedir);
	}
	config = plugin_config_new(path);
	g_free(path);
	config->comment = g_strconcat("Configuration file for ", NAME,
		".\nDo not change unless you KNOW what your are doing.", NULL);
	config->configured_books->group = g_strdup("configured address books");

	for (cur = abooks; cur; cur = g_list_next(cur)) {
		AddressBook* book = (AddressBook *) cur->data;
		config->configured_books->books =
			g_slist_prepend(config->configured_books->books, g_strdup(book->abook_name));
	}

	config->closed_books->group = g_strdup("closed address books");

	for (cur = closed_books; cur; cur = g_list_next(cur)) {
		AddressBook* book = (AddressBook *) cur->data;
		config->closed_books->books =
			g_slist_prepend(config->closed_books->books, g_strdup(book->abook_name));
	}

	plugin_config_set(config, &error);
	if (error) {
		show_message(NULL, GTK_UTIL_MESSAGE_ERROR, "%s", error);
		g_free(error);
	}
}

static gchar** ldap2native_list(const gchar** list) {
	gchar **new_list, **tmp, **head;
	guint len;

	tmp = (gchar **) list;
	if (! tmp)
		return tmp;

	len = g_strv_length(tmp);
	new_list = g_new0(gchar *, len + 1);

	if (len < 1)
		return new_list;

	head = new_list;
	while (*tmp) {
		debug_print("%s : %s\n", *tmp, native2ldap(*tmp));
		*new_list++ = g_strdup(native2ldap(*tmp++));
	}

	return head;
}

static gchar** attriblist2ldap() {
	GHashTableIter iter;
	gpointer key, value;
	GSList *attr_list = NULL;
	gchar** res;

	g_hash_table_iter_init(&iter, attribs);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		const gchar* attr  = native2ldap((gchar *) key);
		attr_list = g_slist_prepend(attr_list, g_strdup(attr));
	}

	res = gslist_to_array(attr_list, NULL);
	gslist_free(&attr_list, NULL);

	return res;
}

static void set_attribs() {
	const gchar** ptr = standard_attribs;
	AttribDef* attrib_def;

	if (attribs) {
		hash_table_free(&attribs);
	}
	attribs = hash_table_new();
	while(ptr && *ptr) {
		attrib_def = g_new0(AttribDef, 1);
		attrib_def->type = ATTRIB_TYPE_STRING;
		attrib_def->attrib_name = g_strdup(*ptr);
		g_hash_table_replace(attribs, g_strdup(*ptr++), attrib_def);
	}

	ptr = other_attributes;
	while(ptr && *ptr) {
		attrib_def = g_new0(AttribDef, 1);
		attrib_def->type = ATTRIB_TYPE_STRING;
		attrib_def->attrib_name = g_strdup(*ptr);
		g_hash_table_replace(attribs, g_strdup(*ptr++), attrib_def);
	}
}

static AddressBook* get_addressbook(const gchar* name) {
	gchar **keys, **cur;
	GSList* values = NULL;
	gsize len;
	GError* err = NULL;
	gchar* value;
	AddressBook* abook;
	ExtraConfig* conf;

	abook = g_new0(AddressBook, 1);
	if (g_key_file_has_group(config->key_file, name)) {
		abook->abook_name = g_strdup(name);
		abook->dirty = TRUE;
		keys = g_key_file_get_keys(config->key_file, name, &len, &err);
		for (cur = keys; *cur; cur += 1) {
			gslist_free(&values, g_free);
			if (config_get_value(config, name, *cur, &values)) {
				value = (gchar *) values->data;
				if (strcmp("url", *cur) == 0)
					abook->URL = g_strdup(value);
				else if (strcmp("username", *cur) == 0)
					abook->username = g_strdup(value);
				else if (strcmp("password", *cur) == 0)
					abook->password = aes256_decrypt(value, TRUE);
				else {
					conf = get_extra_config(self->extra_config(), *cur);
					if (conf) {
						switch (conf->type) {
							case PLUGIN_CONFIG_EXTRA_CHECKBOX:
								conf->current_value.check_btn =
									(strcmp("true", value) == 0) ? TRUE : FALSE;
								break;
							case PLUGIN_CONFIG_EXTRA_ENTRY:
								conf->current_value.entry = g_strdup(value);
								break;
							case PLUGIN_CONFIG_EXTRA_SPINBUTTON:
								conf->current_value.spin_btn = atoi(value);
								break;
						}
						abook->extra_config =
							g_slist_prepend(abook->extra_config, conf);
					}
				}
			}
		}
		if (values)
			gslist_free(&values, g_free);
		 g_strfreev(keys);
	}
	else {
		address_book_free(&abook);
	}

	return abook;
}

/**
 * Remove addressbook from list
 * @param list List to change
 * @param book AddressBook to remove
 * @return TRUE if item was removed
 */
static gboolean g_list_book_remove(GList** list, AddressBook* book) {
	if (! list || !*list || ! book)
		return FALSE;

	GList* found = g_list_find_custom(*list, book, address_book_compare);
	if (found)
		*list = g_list_remove_link(*list, found);

	return (found != NULL);
}

static gchar* filter_token_add(const gchar* token, const gchar* search) {
    return g_strconcat("(", token, "=", search, ")", NULL);
}

static gchar* filter_string_object(const gchar** list, const gchar* object) {
	GString* filter;
	gchar *token, **head;
	gboolean multi = FALSE;

	filter = g_string_new("");
	head = (gchar **) list;
	while (head && *head) {
		token = filter_token_add(object, *head++);
		if (multi) {
	    	filter = g_string_prepend(filter, "(|");
		    filter = g_string_append(filter, token);
		    filter = g_string_append(filter, ")");
		}
		else
			filter = g_string_append(filter, token);
		multi = TRUE;
		g_free(token);
	}

	return g_string_free(filter, FALSE);
}

static gchar* filter_string_value(const gchar** list, const gchar* value) {
	GString* filter;
	gchar *token, **head;
	gboolean multi = FALSE;

	filter = g_string_new("");
	head = (gchar **) list;
	while (head && *head) {
		token = filter_token_add(*head++, value);
		if (multi) {
	    	filter = g_string_prepend(filter, "(|");
		    filter = g_string_append(filter, token);
		    filter = g_string_append(filter, ")");
		}
		else
			filter = g_string_append(filter, token);
		multi = TRUE;
		g_free(token);
	}

	return g_string_free(filter, FALSE);
}

static gchar* filter_build(Contact* contact, gboolean intersection) {
	GString* filter = g_string_new("");
	gchar* token;
	gboolean multi = FALSE;

	token = filter_string_object(objectclass, "objectClass");

	if (contact) {
		if (multi) {
		    if (intersection)
		    	filter = g_string_prepend(filter, "(&");
		    else
		    	filter = g_string_prepend(filter, "(|");
		    filter = g_string_append(filter, token);
		    filter = g_string_append(filter, ")");
		}
		else
			filter = g_string_append(filter, token);
	}
	else
		filter = g_string_prepend(filter, token);

	g_free(token);

	return g_string_free(filter, FALSE);
}

static gchar* filter_build_standard() {
	gchar** list;
	gchar* filter;

	list = ldap2native_list(standard_attribs);
	filter = filter_string_value((const gchar **) list, "*");
	g_strfreev(list);

	return filter;
}

static void gslist_free_ldapmessage(GSList** list) {
	GSList* cur;

	if (! list || ! *list)
		return;

	cur = *list;
	while (cur) {
		LDAPMessage* msg = (LDAPMessage *) cur->data;
		ldap_msgfree(msg);
		msg = NULL;
		cur = cur->next;
	}
	g_slist_free(*list);
	*list = NULL;
}

static GSList* call_server(Server* server,
								int scope,
								const gchar* search_filter,
								gchar** error) {
	GSList *cur, *response = NULL;
	gchar** ldap_attributes;
    int rc = 0;
    struct timeval timeOut;
    LDAPMessage* res = NULL;
	gint max_size;

    if (server->timeout > 0)
    	timeOut.tv_sec = server->timeout;
	else
    	timeOut.tv_sec = TIMEOUT;
	timeOut.tv_usec = 0;
	max_size = server->max_entries;
	ldap_attributes = attriblist2ldap();

	if (server->search_base) {
        rc = ldap_search_ext_s(server->ldap, server->search_base,
        	scope, search_filter, ldap_attributes,
        	0, NULL, NULL, &timeOut, max_size, &res);
        if (rc == LDAP_SUCCESS || rc == LDAP_TIMELIMIT_EXCEEDED || LDAP_SIZELIMIT_EXCEEDED)
        	response = g_slist_append(response, res);
        else
        	ldap_msgfree(res);
	}
	else {
	    for (cur = server->baseDN; cur; cur = g_slist_next(cur)) {
	        rc = ldap_search_ext_s(server->ldap, (gchar *) cur->data,
	        	scope, search_filter, ldap_attributes,
	        	0, NULL, NULL, &timeOut, max_size, &res);
	        if (rc == LDAP_SUCCESS || rc == LDAP_TIMELIMIT_EXCEEDED || LDAP_SIZELIMIT_EXCEEDED) {
				response = g_slist_append(response, res);
	        }
	        else
	        	ldap_msgfree(res);
	    }
	}

    g_strfreev(ldap_attributes);

    if (rc) {
		if (error)
        	*error = g_strdup(get_ldap_error(rc));
        server->error_code = rc;
    }

    return response;
}

static GList* fetch_data(Server* server, LDAPMessage* res) {
    LDAPMessage*    entry;
    BerElement*     ber;
    Contact*		contact;
    Email*			email;
    gchar*          attribute;
    struct berval** vals;
    GList*			result = NULL;
    gchar*          value;
    int				i, count = 0;

    /* Process entries */
    for (entry = ldap_first_entry(server->ldap, res); entry;
            entry = ldap_next_entry(server->ldap, entry), count++) {
        debug_print("------------------------\n");
        contact = contact_new();
        value = ldap_get_dn(server->ldap, entry);
        debug_print("Found: DN->%s\n", value);
		swap_data(contact->data, "dn", value);
        ldap_memfree(value);
        /* Process attributes */
        for (attribute = ldap_first_attribute(server->ldap, entry, &ber);
            attribute; attribute = ldap_next_attribute(server->ldap, entry, ber)) {

            vals = ldap_get_values_len(server->ldap, entry, attribute);
            if (vals) {
                /* We only handle one value per attribute except for mail */
                if (strcasecmp(attribute, "mail") == 0) {
                    for (i = 0; vals[i]; i++) {
						email = g_new0(Email, 1);
                        value = g_strndup(vals[i]->bv_val, vals[i]->bv_len);
						email->email = g_strdup(value);
						debug_print("Found (email): %s->%s\n", attribute, value);
                        g_free(value);
                        contact->emails = g_slist_prepend(contact->emails, email);
                    }
                }
                else {
                    if (strcasecmp(attribute, "jpegPhoto") == 0) {
						guchar* image = g_memdup(vals[0]->bv_val, vals[0]->bv_len);
						value = jpeg_to_png_base64(image, vals[0]->bv_len);
						g_free(image);
					}
					else
						value = g_strndup(vals[0]->bv_val, vals[0]->bv_len);
                    debug_print("Found (other): %s->%s\n", attribute, value);
                    const gchar* key = ldap2native(attribute);
					swap_data(contact->data, key, value);
                    g_free(value);
                }
            }
            ldap_memfree(attribute);
            ldap_value_free_len(vals);
        }
        if (ber) {
            ber_free(ber, 0);
        }
        ber = NULL;
        result = g_list_prepend(result, contact);
    }
    if (count)
    	debug_print("------------------------\n");
    debug_print("Found: %d contacts\n", count);

    return result;
}

static void find_dn(AbookConnection* ac, gchar** error) {
	GString* filter;
	GSList *msgs, *cur, *msgs1, *cur1;
    LDAPMessage *res, *res1, *entry;
    gchar* search_filter;
	gchar* tmp;
	gboolean found = FALSE;
	Contact* contact = (Contact *) ac->contact;
	gchar* err = NULL;

	search_filter = filter_build(contact, ac->intersection);
	filter = g_string_new("(&");
	filter = g_string_append(filter, search_filter);
	g_free(search_filter);
	search_filter = filter_build_standard();
	filter = g_string_append(filter, search_filter);
	filter = g_string_append(filter, ")");
	g_free(search_filter);
	search_filter = g_string_free(filter, FALSE);
	debug_print("Filter: %s\n", search_filter);

	msgs = call_server(ac->server, LDAP_SCOPE_BASE, search_filter, error);

	for (cur = msgs; ! err && cur && ! found; cur = g_slist_next(cur)) {
		res = (LDAPMessage *) cur->data;
		if (res) {
			entry = ldap_first_entry(ac->server->ldap, res);
			if (entry) {
				tmp = ldap_get_dn(ac->server->ldap, entry);
				debug_print("Useing '%s' as DN for new entries\n", tmp);
				ac->server->dn = g_strdup(tmp);
				ldap_memfree(tmp);
				found = TRUE;
			}
			else {
				msgs1 = call_server(ac->server, LDAP_SCOPE_ONELEVEL, search_filter, &err);
				for (cur1 = msgs1; cur1 && ! found; cur1 = g_slist_next(cur1)) {
					res1 = (LDAPMessage *) cur1->data;
					if (res1) {
						entry = ldap_first_entry(ac->server->ldap, res1);
						if (entry) {
							tmp = ldap_get_dn(ac->server->ldap, entry);
							debug_print("Useing '%s' as DN for new entries\n", tmp);
							ac->server->dn = g_strdup(tmp);
							ldap_memfree(tmp);
							found = TRUE;
						}
						else {
							err = g_strdup_printf(_("%s: Cannot get DN\n"), search_filter);
						}
					}
				}

				gslist_free_ldapmessage(&msgs1);
			}
		}
	}
	gslist_free_ldapmessage(&msgs);

	if (err) {
		if (error) {
			*error = g_strdup(err);
		}
		debug_print("%s\n", err);
		g_free(err);
	}

	g_free(search_filter);
}

static gboolean fetch_all(AddressBook* book,
						  AbookConnection* ac,
						  gchar** error) {
	GSList *msgs, *cur;
	Server* server = ac->server;
    LDAPMessage* res = NULL;
    gchar* search_filter;
	GString* filter;
	Contact* contact = (Contact *) ac->contact;

	search_filter = filter_build(contact, ac->intersection);
	filter = g_string_new("(&");
	filter = g_string_append(filter, search_filter);
	g_free(search_filter);
	search_filter = filter_build_standard();
	filter = g_string_append(filter, search_filter);
	filter = g_string_append(filter, ")");
	g_free(search_filter);
	search_filter = g_string_free(filter, FALSE);
	debug_print("Filter: %s\n", search_filter);

	msgs = call_server(ac->server, LDAP_SCOPE_SUBTREE, search_filter, error);

	g_free(search_filter);

	if (*error) {
		if (! (ac->server->error_code == LDAP_TIMELIMIT_EXCEEDED ||
			   ac->server->error_code == LDAP_SIZELIMIT_EXCEEDED))
			goto skip;
	}
	for (cur = msgs; cur; cur = g_slist_next(cur)) {
		res = (LDAPMessage *) cur->data;
		if (res)
			book->contacts = g_list_concat(book->contacts, fetch_data(server, res));
	}
	skip:
	gslist_free_ldapmessage(&msgs);

	return (error) ? TRUE : FALSE;
}

static void set_extra_config() {
	ExtraConfig* ec;

	ec = g_new0(ExtraConfig, 1);
	ec->type = PLUGIN_CONFIG_EXTRA_ENTRY;
	ec->label = g_strdup(_("Search base"));
	ec->tooltip = g_strdup(_("This specifies the name of the directory to be searched "
                			 "on the server. Examples include:\ndc=claws-mail,dc=org\n"
                			 "  ou=people,dc=domainname,dc=com\n"
                			 "  o=Organization Name,c=Country."));
    extra_config = g_slist_append(extra_config, ec);

	ec = g_new0(ExtraConfig, 1);
	ec->type = PLUGIN_CONFIG_EXTRA_SPINBUTTON;
	ec->label = g_strdup(_("Timeout (Sec)"));
	ec->tooltip = g_strdup(_("Timeout in seconds. 0 means wait infinite."));
	ec->preset_value.spin_btn = 20;
    extra_config = g_slist_append(extra_config, ec);

	ec = g_new0(ExtraConfig, 1);
	ec->type = PLUGIN_CONFIG_EXTRA_SPINBUTTON;
	ec->label = g_strdup(_("Max entries"));
	ec->tooltip = g_strdup(_("Maximum number of contacts to receive. 0 means all."));
	ec->preset_value.spin_btn = 30;
    extra_config = g_slist_append(extra_config, ec);
}

/**
 * Free list of abooks.
 * @param error Pointer to memory where error is supposed to be saved
 */
void plugin_reset(gchar** error) {
	abooks_free();
}

/**
 * Return list of AttributeDef for currently
 * supported attributes.
 * @return GSList* list of AttributeDef
 */
GSList* plugin_attrib_list(void) {
	GSList* list = NULL;

	if (attribs) {
		g_hash_table_foreach(attribs, hash_table_keys_to_slist, &list);
	}
	return list;
}

/**
 * Return list of AttribDef of attributes supported
 * by this plugin.
 * Returning NULL means list of supported attributes are infinite
 * Returning an empty list means that no more supported attributes
 * are available for activation
 * @return GSList* list of AttributeDef
 */
GSList* plugin_remaining_attribs(void) {
	remaining_attribs = g_slist_append(remaining_attribs, NULL);
	return gslist_deep_copy(remaining_attribs, attrib_def_copy);
}

/**
 * Return list of AttributeDef for currently
 * supported inactive attributes.
 * @return GSList* list of AttributeDef
 */
GSList* plugin_inactive_attribs(void) {
	return gslist_deep_copy(inactive_attribs, attrib_def_copy);
}

/**
 * Set supported attributes.
 * @param attributes pointer to GHashTable containing keys
 * naming supported attributes.
 */
void plugin_attribs_set(GHashTable* attributes) {
}

/**
 * This macro is borrowed from the Balsa project
 * Creates a LDAPMod structure
 *
 * \param mods Empty LDAPMod structure
 * \param modarr Array with values to insert
 * \param op Operation to perform on LDAP
 * \param attr Attribute to insert
 * \param strv Empty array which is NULL terminated
 * \param val Value for attribute
 */
#define SETMOD(mods,modarr,op,attr,strv,val) \
   do { (mods) = &(modarr); (modarr).mod_type=attr; (modarr).mod_op=op;\
        (strv)[0]=(val); (modarr).mod_values=strv; \
      } while(0)

/**
 * For binary data
 * \param mods Empty LDAPMod structure
 * \param modarr Array with values to insert
 * \param op Operation to perform on LDAP
 * \param attr Attribute to insert
 * \param strv Empty array which is NULL terminated
 * \param val Value for attribute
 */
#define SETMODB(mods,modarr,op,attr,bval,val) \
   do { (mods) = &(modarr); (modarr).mod_type=attr; \
   		(modarr).mod_op=op|LDAP_MOD_BVALUES;\
        (bval)[0]=(val); (bval)[1]=NULL; (modarr).mod_bvalues=bval; \
      } while(0)

/**
 * Creates a LDAPMod structure
 *
 * \param mods Empty LDAPMod structure
 * \param modarr Array with values to insert
 * \param op Operation to perform on LDAP
 * \param attr Attribute to insert
 * \param strv Array with values to insert. Must be NULL terminated
 */
#define SETMODS(mods,modarr,op,attr,strv) \
   do { (mods) = &(modarr); (modarr).mod_type=attr; \
	   	(modarr).mod_op=op; (modarr).mod_values=strv; \
      } while(0)

/**
 * Get a contact(s) using a search string.
 * @param abook Pointer to AddressBook
 * @param search_token Search string
 * @param Pointer to memory where error is supposed to be saved
 * @return GSList* of Contact
 */
GSList* plugin_get_contact(
		AddressBook* abook, const gchar* search_token, gchar** error) {
	GList* cur;
	GSList *contacts = NULL, *basic, *list;
	gboolean found = FALSE;
	Contact* contact = NULL;

	if (abook && search_token) {
		for (cur = abook->contacts; cur; cur = g_list_next(cur)) {
			contact = (Contact *) cur->data;
			if (! contact)
				continue;

			list = get_basic_attributes(contact);
			for (basic = list; !found && basic; basic = g_slist_next(basic)) {
				gchar* attr = (gchar *) basic->data;
				if (attr) {
					found = match_string_pattern(search_token, attr, FALSE);
				}
			}
			gslist_free(&list, g_free);
			if (found) {
				contacts = g_slist_prepend(contacts, contact);
				found = FALSE;
			}
		}
	}

	return contacts;
}

/**
 * Get a contact(s) using all supported attributes.
 * @param abook Pointer to AddressBook
 * @param search_tokens Search strings from a Contact
 * @param is_and If TRUE searching using AND else search using OR
 * @param Pointer to memory where error is supposed to be saved
 * @return GSList* of Contact
 */
GSList* plugin_search_contact(AddressBook* abook,
							  const Contact* search_tokens,
							  gboolean is_and,
							  gchar** error) {
	GList* cur;
	GSList *contacts = NULL;
	Contact* contact = NULL;
	Compare* comp;

	if (abook && search_tokens && (search_tokens->data || search_tokens->emails)) {
		comp = g_new0(Compare, 1);
		for (cur = abook->contacts; cur; cur = g_list_next(cur)) {
			contact = (Contact *) cur->data;
			if (! contact)
				continue;

			comp->hash = contact->data;
			comp->is_and = is_and;
			comp->equal = -1;

			g_hash_table_foreach(search_tokens->data,
					contact_compare_values, comp);
			if ((comp->is_and && comp->equal > 0) ||
				(comp->equal < 1 && !comp->is_and)) {
				gint res = email_compare_values(
					search_tokens->emails, contact->emails, is_and);
				if (res != -1)
					comp->equal = res;
			}
			if (comp->equal > 0)
				contacts = g_slist_prepend(contacts, contact);
		}
		g_free(comp);
	}

	return contacts;
}

static gboolean contact_has_dn(Contact* contact) {
	gpointer key, val;

	return g_hash_table_lookup_extended(contact->data, "dn", &key, &val);
}

static gchar* contact_get_dn(AbookConnection* ac) {
	gchar* dn = NULL;
	GSList* cur;
	Contact* contact = (Contact *) ac->contact;

	if (! contact)
		dn = create_dummy_dn(ac->server->dn);
	else if (! contact_has_dn(contact)) {
		cur = contact->emails;
		while(cur && ! dn) {
			Email* e = (Email *) cur->data;
			if (e->email) {
				if (! ac->server->dn)
					find_dn(ac, NULL);
				dn = g_strconcat("mail=", e->email, ",", ac->server->dn, NULL);
			}
			else
				cur = cur->next;
		}
		if (! dn) {
			gchar* tmp = create_dummy_dn(NULL);
			if (! ac->server->dn)
				find_dn(ac, NULL);
			dn = g_strconcat("mail=", tmp, ",", ac->server->dn, NULL);
			Email* a = g_new0(Email, 1);
			a->email = g_strdup(tmp);
			g_free(tmp);
			contact->emails = g_slist_append(contact->emails, a);
		}
	}
	else {
		extract_data(contact->data, "dn", (gpointer) &dn);
	}

	return dn;
}

static void print_ldapmod(LDAPMod *mods[]) {
    gchar *mod_op;
    int i;

    for (i = 0; NULL != mods[i]; i++) {
		LDAPMod *mod = (LDAPMod *) mods[i];
		gchar** vals;
		struct berval** bvals;
		switch (mod->mod_op) {
            case LDAP_MOD_ADD:
            case LDAP_MOD_ADD|LDAP_MOD_BVALUES:
                mod_op = g_strdup("ADD");
                break;
            case LDAP_MOD_REPLACE:
            case LDAP_MOD_REPLACE|LDAP_MOD_BVALUES:
                mod_op = g_strdup("MODIFY");
                break;
            case LDAP_MOD_DELETE:
            case LDAP_MOD_DELETE|LDAP_MOD_BVALUES:
                mod_op = g_strdup("DELETE");
                break;
            default: mod_op = g_strdup("UNKNOWN");
	    }
        debug_print("Operation: %s\tType:%s\nValues:\n",
                mod_op, mod->mod_type);
        g_free(mod_op);
		if ((mod->mod_op & LDAP_MOD_BVALUES) == LDAP_MOD_BVALUES) {
			bvals = mod->mod_vals.modv_bvals;
			while (*bvals) {
				debug_print("\tSize: %d bytes\n", (*bvals)->bv_len);
				debug_print("\t%s\n", (*bvals)->bv_val);
				bvals += 1;
			}
		}
		else {
	        vals = mod->mod_vals.modv_strvals;
	        while (*vals) {
	            debug_print("\t%s\n", *vals++);
	        }
		}
   	}
}

static void init_mods(gchar* mods[][2], guint size) {
	guint i;

	for (i = 0; i < size; i++) {
		mods[i][0] = mods[i][1] = NULL;
	}
}

static gchar** get_object_classes() {
	gchar** obj;
	guint size = g_strv_length((gchar **) objectclass) + 1;
	int i;

	obj = g_new0(gchar *, size);
	for (i = 0; i < size; i++)
		obj[i] = g_strdup(objectclass[i]);

	return obj;
}

static void mods_free(LDAPMod *mods[]) {
    int i;
	gchar** vals;
	struct berval** bvals;

    for (i = 0; NULL != mods[i]; i++) {
		LDAPMod* mod = (LDAPMod *) mods[i];
		g_free(mod->mod_type);
		if ((mod->mod_op & LDAP_MOD_BVALUES) == LDAP_MOD_BVALUES) {
			bvals = mod->mod_vals.modv_bvals;
			while (*bvals) {
				g_free((*bvals)->bv_val);
				bvals += 1;
			}
		}
		else {
        	vals = mod->mod_vals.modv_strvals;
        	while (*vals) {
            	g_free(*vals++);
        	}
		}
	}
}

static gboolean has_required_info(Contact* contact) {
	gboolean has = TRUE;
	gchar* attr = NULL;
	const gchar** required = required_attributes;

	while (*required && has) {
		const gchar* key = ldap2native(*required++);
		extract_data(contact->data, key, (gpointer) &attr);
		if (!attr || strlen(attr) < 1)
			has = FALSE;
		g_free(attr);
		attr = NULL;
	}

	return has;
}

static Contact* address_book_contact_get(AddressBook* abook, const gchar* dn) {
	GList* cur;
	Contact* contact = NULL;
	gchar* uid;

	if (! abook || ! dn)
		return NULL;

	cur = abook->contacts;
	while (cur && ! contact) {
		contact = (Contact *) cur->data;
		extract_data(contact->data, "dn", (gpointer) &uid);
		if (! uid || utf8_collate((gchar *) dn, uid) != 0) {
			contact = NULL;
			cur = cur->next;
		}
		g_free(uid);
	}

	return contact;
}

static gchar* extract_email_from_dn(const gchar* dn) {
	gchar *tmp1, *tmp2;

	if (! dn)
		return NULL;

	tmp1 = strchr(dn, '=');
	tmp1 += 1;
	tmp2 = strchr(tmp1, ',');

	return g_strndup(tmp1, tmp2 - tmp1);
}

static int ldap_set_dn(Server* server, GSList* list, gchar** dn) {
	gchar* new_dn = NULL;
	gchar *rdn = NULL;
	Email* email;
	int rc = 0;
	GSList* cur;
	gboolean found = FALSE;

	if (! dn || ! *dn)
		return rc;

	rdn = extract_email_from_dn(*dn);
	cur = list;
	while (cur && ! found) {
		ContactChange* c = (ContactChange *) cur->data;
		if (c->type == CONTACT_CHANGE_EMAIL) {
			email = (Email *) c->value;
			if (utf8_collate(email->email, rdn) == 0)
				found = TRUE;
			else {
				if (! new_dn)
					new_dn = g_strconcat("mail=", email->email, NULL);
			}
		}
		cur = cur->next;
	}

	if (! found) {
		if (! new_dn) {
			gchar* tmp = create_dummy_dn(NULL);
			new_dn = g_strconcat("mail=", tmp, NULL);
			g_free(tmp);
		}
		g_free(rdn);
		debug_print("Old DN: %s -> New DN: %s\n", *dn, new_dn);
		rc = ldap_rename_s(server->ldap, *dn, new_dn, NULL, 1, NULL, NULL);
		if (! rc) {
			g_free(*dn);
			*dn = g_strconcat(new_dn, ",", server->dn, NULL);
		}
	}

	g_free(new_dn);

	return rc;
}

/**
 * Save a contact in AddressBook.
 * @param abook Pointer to AddressBook
 * @param contact Contact to save
 * @param Pointer to memory where error is supposed to be saved
 * @return FALSE if success TRUE otherwise
 */
gboolean plugin_set_contact(
		AddressBook* abook, const Contact* contact, gchar** error) {
	gchar* dn = NULL;
	AbookConnection* ac;
	Server* server;
	guint row = 0;
	GSList *cur, *attributes;
	guint size = g_strv_length((gchar **) standard_attribs) +
			g_strv_length((gchar **) other_attributes);
    LDAPMod *mods[size + 2]; /* including NULL and objects */
    LDAPMod modarr[size + 2]; /* including NULL and objects */
	gchar* attr[size + 1][2];
	struct berval* battr[size + 1][2];
	gchar** obj;
	int rc;
	gchar** emails = NULL;

	/* Save contact */
	if (! abook || ! contact) {
		if (error)
			*error = g_strdup(_("Missing address book or contact"));
		return TRUE;
	}

	if (! has_required_info((Contact *) contact)) {
		if (error)
			*error = g_strdup(_("Missing mandatory attributes"));
		return TRUE;
	}

	ac = get_abook_connection(abook);
	if (! ac) {
		if (error)
			*error = g_strdup(_("Missing address book connection"));
		return TRUE;
	}
	if (ldap_connect(ac, error))
		return TRUE;

	server = ac->server;
	ac->contact = (Contact *) contact;

	dn = contact_get_dn(ac);
	if (! dn) {
		debug_print("LDAP Add: Unresolved error");
		if (*error)
			*error = g_strdup(_("LDAP Add: Unresolved error"));
		abook->dirty = FALSE;
		deconnect(ac->server);
		return TRUE;
	}

	/* Initialize structure */
	init_mods(attr, size + 2);
	obj = get_object_classes();

	if (! contact) {
		Email* a = g_new0(Email, 1);
		a->email = extract_email_from_dn(dn);
		((Contact *) contact)->emails = g_slist_append(((Contact *) contact)->emails, a);
	}

	cur = contact->emails;
	/* First email is used as dn and will therefore be
	 * added automatically
	 */
	if (cur)
		cur = cur->next;
	rc = 0;
	while(cur) {
		Email* e = (Email *) cur->data;
		if (e->email) {
			emails = g_renew(gchar *, emails, rc + 1);
			emails[rc] = g_memdup(e->email, strlen(e->email) + 1);
		}
		cur = cur->next;
		rc++;
	}
	if (rc) {
		emails = g_renew(gchar *, emails, rc + 1);
		emails[rc] = NULL;
		SETMODS(mods[row], modarr[row], LDAP_MOD_ADD, g_strdup("mail"), emails);
		row++;
	}

	debug_print("Adding: %s\n", dn);
	SETMODS(mods[row], modarr[row], LDAP_MOD_ADD, g_strdup("objectClass"), obj);
	row++;

	attributes = self->attrib_list();
	for (cur = attributes; cur; cur = g_slist_next(cur)) {
		AttribDef* def = (AttribDef *) cur->data;
		AttribDef* res = (AttribDef *) g_hash_table_lookup(contact->data, def->attrib_name);
		if (res) {
			gchar* value;
			if (strcasecmp(res->attrib_name, "image") == 0) {
				struct berval jpeg;
				jpeg.bv_val = (gchar *) g_base64_decode(res->value.string, &jpeg.bv_len);
				SETMODB(mods[row], modarr[row], LDAP_MOD_ADD, \
					g_strdup(native2ldap(def->attrib_name)), battr[row], &jpeg);
			}
			else {
				value = g_strdup(res->value.string);
				SETMOD(mods[row], modarr[row], LDAP_MOD_ADD, \
					g_strdup(native2ldap(def->attrib_name)), attr[row], value);
			}
			row++;
		}
	}
	gslist_free(&attributes, attrib_def_free);
	mods[row] = NULL;
	if (debug_get_mode())
		print_ldapmod(mods);

	rc = ldap_add_ext_s(server->ldap, dn, mods, NULL, NULL);
	mods_free(mods);
	if (rc) {
		debug_print("LDAP ADD: %s\n", ldap_err2string(rc));
		if (error)
			*error = g_strconcat("dn: ", dn, "\n", ldap_err2string(rc), NULL);
		abook->dirty = FALSE;
		g_free(dn);
	}
	else {
		g_free(dn);
		abook->dirty = TRUE;
		deconnect(ac->server);
		return FALSE;
	}

	deconnect(ac->server);
	return TRUE;
}

/**
 * Delete a contact in AddressBook.
 * @param abook Pointer to AddressBook
 * @param contact Contact to delete
 * @param Pointer to memory where error is supposed to be saved
 * @return FALSE if success TRUE otherwise
 */
gboolean plugin_delete_contact(
		AddressBook* abook, const Contact* contact, gchar** error) {
	gchar* dn;
	AbookConnection* ac;
	Server* server;
	int rc;

	/* Delete contact */
	if (! abook || ! contact) {
		if (error)
			*error = g_strdup(_("Missing address book or contact"));
		return TRUE;
	}

	ac = get_abook_connection(abook);
	if (! ac) {
		if (error)
			*error = g_strdup(_("Missing address book connection"));
		return TRUE;
	}
	if (ldap_connect(ac, error))
		return TRUE;

	server = ac->server;
	ac->contact = (Contact *) contact;

	dn = contact_get_dn(ac);
    if (dn) {
        rc = ldap_delete_ext_s(server->ldap, dn, NULL, NULL);
        g_free(dn);
        if (rc) {
			debug_print("LDAP Delete: %s\n", ldap_err2string(rc));
			if (*error)
            	*error = g_strdup(ldap_err2string(rc));
            abook->dirty = FALSE;
            deconnect(ac->server);
            return TRUE;
        }
    }
    else {
        debug_print("LDAP Delete: No such contact\n");
		if (*error)
        	*error = g_strdup(_("No such contact"));
        abook->dirty = FALSE;
        deconnect(ac->server);
        return TRUE;
    }

	abook->dirty = TRUE;
	deconnect(ac->server);

	return FALSE;
}

/**
 * Update a contact in AddressBook.
 * @param abook Pointer to AddressBook
 * @param contact Contact to update
 * @param Pointer to memory where error is supposed to be saved
 * @return FALSE if success TRUE otherwise
 */
gboolean plugin_update_contact(
		AddressBook* abook, const Contact* contact, gchar** error) {
	gchar* dn = NULL;
	gchar* old_dn = NULL;
	AbookConnection* ac;
	Contact* old;
	GSList *diff, *cur;
	AttribDef* attrib = NULL;
	Email* email = NULL;
	Server* server;
	guint row = 0;
	guint num = 0;
	guint size = g_strv_length((gchar **) standard_attribs) +
			g_strv_length((gchar **) other_attributes);
    LDAPMod *mods[size + 2]; // including NULL and objects
    LDAPMod modarr[size + 2]; // including NULL and objects
	gchar* attr[size + 1][2];
	struct berval* battr[size + 1][2];
	int rc;
	gchar** emails = NULL;

	/* Update contact */
	if (! abook || ! contact) {
		if (error)
			*error = g_strdup(_("Missing address book or contact"));
		return TRUE;
	}

	if (! has_required_info((Contact *) contact)) {
		if (error)
			*error = g_strdup(_("Missing mandatory attributes"));
		return TRUE;
	}

	ac = get_abook_connection(abook);
	if (! ac) {
		if (error)
			*error = g_strdup(_("Missing address book connection"));
		return TRUE;
	}
	if (ldap_connect(ac, error))
		return TRUE;

	server = ac->server;
	ac->contact = (Contact *) contact;

	dn = contact_get_dn(ac);
	if (dn) {
		/* Initialize structure */
		init_mods(attr, size + 2);

		debug_print("Updating: %s\n", dn);

		old = address_book_contact_get(abook, dn);
		if (old) {
			diff = contact_diff(old, ac->contact);
			cur = diff;
			old_dn = g_strdup(dn);
			rc = ldap_set_dn(server, cur, &dn);
			if (utf8_collate(dn, old_dn) != 0) {
				AttribDef* attr = pack_data(ATTRIB_TYPE_STRING, "dn", (gpointer) dn);
				g_hash_table_replace(contact->data, g_strdup("dn"), attr);
			}
			g_free(old_dn);

			while (cur && !rc) {
				ContactChange* c = (ContactChange *) cur->data;
				switch (c->type) {
					case CONTACT_CHANGE_ATTRIB:
						attrib = (AttribDef *) c->value;
						/*
						 * if c->action == ATTRIBUTE_MODIFY && c->value == ""
						 * then change c->action to ATTRIBUTE_DELETE since
						 * an empty string is to be treated as a missing attribute
						 */
						if (c->action == ATTRIBUTE_MODIFY && attrib->type == ATTRIB_TYPE_STRING
							&& attrib->value.string && strlen(attrib->value.string) < 1)
							c->action = ATTRIBUTE_DELETE;
						break;
					case CONTACT_CHANGE_EMAIL:
						email = (Email *) c->value;
						break;
				}

				switch (c->action) {
					case ATTRIBUTE_ADD:
						if (c->type == CONTACT_CHANGE_EMAIL) {
							/* Ignore - emails always modify */
						}
						else {
							if (strcasecmp(attrib->attrib_name, "image") == 0) {
								struct berval jpeg;
								jpeg.bv_val = (gchar *) g_base64_decode(attrib->value.string, &jpeg.bv_len);
								SETMODB(mods[row], modarr[row], LDAP_MOD_ADD, \
									g_strdup(native2ldap(attrib->attrib_name)), battr[row], &jpeg);
							}
							else {
								gchar* val = g_strdup(attrib->value.string);
								SETMOD(mods[row], modarr[row], LDAP_MOD_ADD, \
									g_strdup(native2ldap(attrib->attrib_name)), attr[row], val);
							}
							row++;
						}
						break;
					case ATTRIBUTE_DELETE:
						if (c->type == CONTACT_CHANGE_EMAIL) {
							SETMOD(mods[row], modarr[row], LDAP_MOD_REPLACE, \
								g_strdup("mail"), attr[row], extract_email_from_dn(dn));
							row++;
						}
						else {
							SETMOD(mods[row], modarr[row], LDAP_MOD_DELETE, \
								g_strdup(native2ldap(attrib->attrib_name)), \
								attr[row], NULL);
							row++;
						}
						/* check if dn is to be deleted */
						break;
					case ATTRIBUTE_MODIFY:
						if (c->type == CONTACT_CHANGE_EMAIL) {
							/* check if dn is to be modified */
							emails = g_renew(gchar *, emails, num + 1);
							emails[num] = g_memdup(email->email, strlen(email->email) + 1);
							num++;
						}
						else {
							if (strcasecmp(attrib->attrib_name, "image") == 0) {
								struct berval jpeg;
								jpeg.bv_val = (gchar *) g_base64_decode(attrib->value.string, &jpeg.bv_len);
								SETMODB(mods[row], modarr[row], LDAP_MOD_REPLACE, \
									g_strdup(native2ldap(attrib->attrib_name)), battr[row], &jpeg);
							}
							else {
								gchar* val = g_strdup(attrib->value.string);
								SETMOD(mods[row], modarr[row], LDAP_MOD_REPLACE, \
									g_strdup(native2ldap(attrib->attrib_name)), attr[row], val);
							}
							row++;
						}
						break;
				}
				cur = cur->next;
			}
			if (! rc) {
				if (emails) {
					emails = g_renew(gchar *, emails, num + 1);
					emails[num] = NULL;
					SETMODS(mods[row], modarr[row], LDAP_MOD_REPLACE, \
						g_strdup("mail"), emails);
					row++;
				}
				mods[row] = NULL;
				if (debug_get_mode())
					print_ldapmod(mods);

				rc = ldap_modify_ext_s(server->ldap, dn, mods, NULL, NULL);
				mods_free(mods);
			}
			if (rc) {
				debug_print("LDAP MODIFY: %s\n", ldap_err2string(rc));
				if (error)
					*error = g_strconcat("dn: ", dn, "\n", ldap_err2string(rc), NULL);
				g_free(dn);
				abook->dirty = FALSE;
				deconnect(ac->server);
				return TRUE;
			}
			else {
				g_free(dn);
				abook->dirty = TRUE;
				deconnect(ac->server);
				return FALSE;
			}
			gslist_free(&diff, contact_change_free);
		}
		else {
			g_free(dn);
			dn = NULL;
			if (self->set_contact(abook, contact, error)) {
				abook->dirty = FALSE;
				deconnect(ac->server);
				return TRUE;
			}
		}
	}
	else {
		debug_print("LDAP Update: Unresolved error");
		if (*error)
			*error = g_strdup(_("LDAP Update: Unresolved error"));
		abook->dirty = FALSE;
		deconnect(ac->server);
		return TRUE;
	}

	abook->dirty = TRUE;
	deconnect(ac->server);

	return FALSE;
}

/**
 * Open an AddressBook.
 * @param abook Pointer to AddressBook
 * @param Pointer to memory where error is supposed to be saved
 * @return TRUE if success FALSE otherwise
 */
gboolean plugin_abook_open(AddressBook* abook, gchar** error) {
	gboolean removed;
	AbookConnection* ac = get_abook_connection(abook);

	if (! ac) {
		ac = abook_connection_new();
		gchar* plain = g_strconcat(abook->abook_name, abook->URL, NULL);
		gchar* id = sha256(plain);
		g_free(plain);
		ac->id = g_strdup(id);
		g_free(id);
		abook_connection = g_slist_prepend(abook_connection, ac);
	}

	removed = g_list_book_remove(&closed_books, abook);
	abooks = g_list_prepend(abooks, abook);
	if (abook->dirty) {
		/* Open Address book */
		ldap_connect(ac, error);
		if (*error) {
			show_message(NULL, GTK_UTIL_MESSAGE_WARNING, "%s", *error);
			g_free(*error);
			*error = NULL;
			if (removed)
				closed_books = g_list_prepend(closed_books, abook);
			g_list_book_remove(&abooks, abook);
			return FALSE;
		}
		find_dn(ac, error);
		if (*error) {
			show_message(NULL, GTK_UTIL_MESSAGE_WARNING, "%s", *error);
			g_free(*error);
			*error = NULL;
			if (removed)
				closed_books = g_list_prepend(closed_books, abook);
			g_list_book_remove(&abooks, abook);
			deconnect(ac->server);
			return FALSE;
		}
		if (fetch_all(abook, ac, error)) {
			if (*error) {
				show_message(NULL, GTK_UTIL_MESSAGE_WARNING, "%s", *error);
				g_free(*error);
				*error = NULL;
				if (removed)
					closed_books = g_list_prepend(closed_books, abook);
				g_list_book_remove(&abooks, abook);
				deconnect(ac->server);
				return FALSE;
			}
		}
		abook->dirty = FALSE;
	}
	abook->open = TRUE;
	deconnect(ac->server);

	return TRUE;
}

/**
 * Delete an AddressBook.
 * @param abook Pointer to AddressBook
 * @param Pointer to memory where error is supposed to be saved
 * @return TRUE if success FALSE otherwise
 */
gboolean plugin_abook_delete(AddressBook* abook, gchar** error) {
	if (! abook)
		return FALSE;

	AbookConnection* ac = get_abook_connection(abook);
	if (! ac) {
		if (error)
			*error = g_strdup_printf(_("%s: Not found"), abook->abook_name);
		return FALSE;
	}
	if (ldap_connect(ac, error))
		return FALSE;

	if (abook->open)
		self->abook_close(abook, error);

	deconnect(ac->server);

	/* Remove from closed books since deleting */
	closed_books = g_list_remove(closed_books, abook);
	abook_connection = g_slist_remove(abook_connection, ac);
	abook_connection_free(ac);
	ac = NULL;
	return TRUE;
}

/**
 * Close an AddressBook.
 * @param abook Pointer to AddressBook
 * @param Pointer to memory where error is supposed to be saved
 * @return TRUE if success FALSE otherwise
 */
gboolean plugin_abook_close(AddressBook* abook, gchar** error) {
	AbookConnection* ac = get_abook_connection(abook);
	if (! ac) {
		if (error)
			*error = g_strdup_printf(_("%s: Not found"), abook->abook_name);
		return FALSE;
	}

	deconnect(ac->server);
	debug_print("List contains %d elements before\n", g_list_length(abooks));
	abooks = g_list_remove(abooks, abook);
	address_book_contacts_free(abook);
	debug_print("List contains %d elements after\n", g_list_length(abooks));
	closed_books = g_list_prepend(closed_books, abook);
	abook->open = FALSE;

	return TRUE;
}

/**
 * Get list of all address books.
 * @return GSList* List of AddressBook
 */
GSList* plugin_addrbook_all_get() {
	GSList *books = NULL;
	GList *cur;

	for (cur = abooks; cur; cur = g_list_next(cur)) {
		AddressBook* adrs = (AddressBook *) cur->data;
		books = g_slist_prepend(books, adrs);
	}

	return books;
}

/**
 * Get list of all address book names.
 * @return GSList* List of AddressBook names
 */
GSList* plugin_addrbook_names_all() {
	GList *list;
	GSList* names = NULL;

	for (list = abooks; list; list = g_list_next(list)) {
		AddressBook* book = (AddressBook *) list->data;
		names = g_slist_prepend(names, g_strdup(book->abook_name));
	}

	for (list = closed_books; list; list = g_list_next(list)) {
		AddressBook* book = (AddressBook *) list->data;
		names = g_slist_prepend(names, g_strdup(book->abook_name));
	}

	return names;
}

/**
 * Save config for an address book. If new is NULL write config for
 * new address book update otherwise.
 * @param old Pointer to AddressBook
 * @param new Pointer to AddressBook
 * @param Pointer to memory where error is supposed to be saved
 * @return TRUE if success FALSE otherwise
 */
gboolean plugin_abook_set_config(AddressBook* old,
								 AddressBook* new,
								 gchar** error) {
	AddressBook* abook;
	AbookConnection* ac = NULL;
	GSList* cur;

	if (! config) {
		if (error) {
			*error = g_strdup(_("Missing config file"));
		}
		return TRUE;
	}

	if (new) {
		/* Update config for existing address book */
		self->abook_delete(old, error);
		if (*error) {
			/* ignore in case we open a previously closed book */
			g_free(*error);
			*error = NULL;
		}
		abook = new;
		ac = get_abook_connection(abook);
	}
	else {
		/* Write config for new address book */
		abook = old;
	}

	if (! abook->abook_name || strlen(abook->abook_name) < 1) {
		if (error) {
			*error = g_strdup(_("Missing name for address book"));
		}
		return FALSE;
	}

	if (! ac) {
		ac = abook_connection_new();
		abook_connection = g_slist_prepend(abook_connection, ac);
	}

	gchar* plain = g_strconcat(abook->abook_name, abook->URL, NULL);
	gchar* id = sha256(plain);
	g_free(plain);
	ac->id = g_strdup(id);
	g_free(id);

	g_key_file_set_string(config->key_file, abook->abook_name, "url", abook->URL);
	g_key_file_set_string(config->key_file, abook->abook_name, "username", abook->username);
	g_key_file_set_string(config->key_file, abook->abook_name,
			"password", aes256_encrypt(abook->password, TRUE));

	if (abook->extra_config) {
		for (cur = abook->extra_config; cur; cur = g_slist_next(cur)) {
			ExtraConfig* conf = (ExtraConfig *) cur->data;
			switch (conf->type) {
				case PLUGIN_CONFIG_EXTRA_CHECKBOX:
					g_key_file_set_boolean(config->key_file,
						abook->abook_name, conf->label, conf->current_value.check_btn);
					break;
				case PLUGIN_CONFIG_EXTRA_ENTRY:
					g_key_file_set_string(config->key_file,
						abook->abook_name, conf->label, conf->current_value.entry);
					break;
				case PLUGIN_CONFIG_EXTRA_SPINBUTTON:
					g_key_file_set_integer(config->key_file,
						abook->abook_name, conf->label, conf->current_value.spin_btn);
					break;
			}
		}
	}

	abook->dirty = TRUE;

	return (*error) ? TRUE : FALSE;
}

/**
 * URL or URI to saved address book(s).
 * @param name Name of AddressBook
 * @return URL or URI. NULL means no special URL or URI required
 */
gchar* plugin_default_url(const gchar* name) {
	gchar* url = NULL;

	return url;
}

/**
 * Commit changes for all address books.
 * @param Pointer to memory where error is supposed to be saved
 * @return TRUE if success FALSE otherwise
 */
gboolean plugin_commit_all(gchar** error) {
	if (*error != NULL) {
		g_free(*error);
		*error = NULL;
	}
	return TRUE;
}

/**
 * Get list of closed address books. The list and data must be freed
 * by the caller.
 * @return GList of AddressBook
 */
GList* plugin_closed_books_get(void) {
	GList *cur, *list = NULL;

	for (cur = closed_books; cur; cur = g_list_next(cur)) {
		/* No deep copy since list of contacts will always be NULL */
		list = g_list_prepend(list, address_book_copy(cur->data, FALSE));
	}

	return list;
}

/**
 * Initialize plugin.
 * @param Pointer to memory where error is supposed to be saved
 * @return FALSE if success TRUE otherwise
 */
gboolean plugin_init(gpointer self_ref, gchar** error) {
	gchar *basedir, *path;
	GSList *cur;
	ConfiguredBooks* cf_books;
	ClosedBooks* cl_books;
	gboolean ok = FALSE;

	self = (Plugin *) self_ref;
	if (*error != NULL) {
		g_free(*error);
		*error = NULL;
	}

	hash_table_free(&attribs);

	basedir = get_self_home();
	path = g_strconcat(
		basedir, G_DIR_SEPARATOR_S, self_home,
		G_DIR_SEPARATOR_S, configrc, NULL);

	set_extra_config();
	config = plugin_config_new(path);
	g_free(path);
	config_get(config, error);

	if (*error) {
		show_message(NULL, GTK_UTIL_MESSAGE_ERROR, "%s", *error);
	}
	else {
		if (config) {
			set_attribs();
			if (debug_get_mode()) {
				hash_table_dump(attribs, stderr);
			}

			cf_books = config->configured_books;
			cl_books = config->closed_books;

			if (cf_books) {
				for (cur = cf_books->books; cur; cur = g_slist_next(cur)) {
					gchar* book = (gchar *) cur->data;
					if (book) {
						AddressBook* abook = get_addressbook(book);
						if (abook) {
							ok = self->abook_open(abook, error);
							if (! ok)
								closed_books = g_list_prepend(closed_books, abook);
						}
						else {
							if (error)
								*error = g_strconcat(
									"'", book, "': ",
									_("Missing matching configuration"),
									NULL);
						}
					}
				}
			}
			if (cl_books) {
				for (cur = cl_books->books; cur; cur = g_slist_next(cur)) {
					gchar* book = (gchar *) cur->data;
					AddressBook* abook = get_addressbook(book);
					closed_books = g_list_prepend(closed_books, abook);
				}
			}
		}
	}
	g_free(basedir);

	return FALSE;
}

/**
 * Unload plugin.
 * @return TRUE if success FALSE otherwise
 */
gboolean plugin_done(void) {
	write_config_file();
	abooks_free();
	plugin_config_free(&config);
	g_free(feature);
	hash_table_free(&attribs);
	gslist_free(&remaining_attribs, NULL);
	gslist_free(&abook_connection, abook_connection_free);
	gslist_free(&extra_config, extra_config_free);

	self = NULL;

	return TRUE;
}

/**
 * Get plugin features. Returned structure is owned by the plugin.
 * @return PluginFeature* Struct holding features for this plugin. NULL
 * in case of error
 */
const PluginFeature* plugin_provides (void) {
	feature = g_new0(PluginFeature, 1);

	if (feature == NULL) {
		return NULL;
	}

	feature->support = PLUGIN_READ_WRITE | PLUGIN_ADVANCED_SEARCH;
	feature->subtype = subtype;

	return feature;
}

/**
 * Get name of plugin. Returned memory is owned by the plugin.
 * @return name
 */
const gchar* plugin_name(void) {
	return NAME;
}

/**
 * Get description for plugin. Returned memory is owned by the plugin.
 * @return description
 */
const gchar* plugin_desc(void) {
	return _("This plugin provides LDAP support.\n"
			 "The plugin only supports LDAPv3 and the format\n"
			 "for a URL is as follows:\n\n"
			 "[(ldap|ldaps)://](FQDN|IP)[:port]\n\n"
			 "If neither ldap nor ldaps is specified ldap is assumed.\n"
			 "If port is not specified 389 is assumed for ldap\n"
			 "and 636 is assumed for ldaps.\n"
			 "If ldap is used for schema then TLS will be tried\n"
			 "automatically before using plain text connection.\n"
			 "Display name and Lastname are mandatory attributes.");
}

/**
 * Get version of plugin. Returned memory is owned by the plugin.
 * @return version
 */
const gchar* plugin_version(void) {
	return VERSION;
}

/**
 * Get functional type of plugin. Returned memory is owned by the plugin.
 * @return type
 */
PluginType plugin_type(void) {
	return PLUGIN_TYPE_ADVANCED;
}

/**
 * Get file filter for this plugin
 * @return filter or NULL. If returning NULL means data storage is
 * URL based URI based otherwise
 */
const gchar* plugin_file_filter(void) {
	return NULL;
}

/**
 * Get license of plugin. Returned memory is owned by the plugin.
 * @return license
 */
const gchar* plugin_license(void) {
	return "GPL3+";
}

/**
 * Does the plugin needs credentials for address books
 * @return bool
 */
gboolean plugin_need_credentials(void) {
	return TRUE;
}

/**
 * Get list of additional config
 * @return NULL if no additional config is required, a list of
 * ExtraConfig otherwise
 */
GSList* plugin_extra_config(void) {
	return g_slist_reverse(gslist_deep_copy(extra_config, extra_config_copy));
}
