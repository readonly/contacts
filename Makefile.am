# $Id$
AUTOMAKE_OPTIONS = gnu no-dist-gzip dist-bzip2 1.10

ACLOCAL_AMFLAGS = -I m4

if BUILD_DBUS_CLIENT
DBUS_CLIENT = dbus-client
endif

SUBDIRS = \
	  m4 \
	  po \
	  config \
	  pixmaps \
	  xmllib \
	  libversit \
	  plugins \
	  extensions \
	  src \
	  ${DBUS_CLIENT}

DOCS = \
       AUTHORS \
       ABOUT-NLS \
       COPYING \
       ChangeLog \
       INSTALL \
       NEWS \
       README \
       TODO

docdir = $(datadir)/doc/@PACKAGE@
doc_DATA = $(DOCS)

servicedir = @DBUS_SERVICE_DIR@
service_in_files = org.clawsmail.ClawsContact.service.in
service_DATA = $(service_in_files:.service.in=.service)

$(service_DATA): $(service_in_files) Makefile
	@sed -e "s|@binddir@|$(bindir)|" $<> $@

# hicolor icon theme, base class of all icon themes
pixmap16dir=$(datadir)/icons/hicolor/16x16/apps
pixmap16_DATA=pixmaps/claws-contacts_16x16.png

pixmap32dir=$(datadir)/icons/hicolor/32x32/apps
pixmap32_DATA=pixmaps/claws-contacts_32x32.png

pixmap48dir=$(datadir)/icons/hicolor/48x48/apps
pixmap48_DATA=pixmaps/claws-contacts_48x48.png

pixmap64dir=$(datadir)/icons/hicolor/64x64/apps
pixmap64_DATA=pixmaps/claws-contacts_64x64.png

pixmap128dir=$(datadir)/icons/hicolor/128x128/apps
pixmap128_DATA=pixmaps/claws-contacts_128x128.png

gnomeappdir = $(datadir)/applications
gnomeapp_DATA=claws-contacts.desktop

pkgconfigdir = $(libdir)/pkgconfig
pkgconfig_DATA = claws-contacts.pc

rename-icons:
	@cd $(DESTDIR)$(datadir)/icons/hicolor/16x16/apps && \
            mv claws-contacts_16x16.png claws-contacts.png
	@cd $(DESTDIR)$(datadir)/icons/hicolor/32x32/apps && \
            mv claws-contacts_32x32.png claws-contacts.png
	@cd $(DESTDIR)$(datadir)/icons/hicolor/48x48/apps && \
            mv claws-contacts_48x48.png claws-contacts.png
	@cd $(DESTDIR)$(datadir)/icons/hicolor/64x64/apps && \
            mv claws-contacts_64x64.png claws-contacts.png
	@cd $(DESTDIR)$(datadir)/icons/hicolor/128x128/apps && \
            mv claws-contacts_128x128.png claws-contacts.png

remove-icons:
	@rm -f $(DESTDIR)$(datadir)/icons/hicolor/16x16/apps/claws-contacts.png
	@rm -f $(DESTDIR)$(datadir)/icons/hicolor/32x32/apps/claws-contacts.png
	@rm -f $(DESTDIR)$(datadir)/icons/hicolor/48x48/apps/claws-contacts.png
	@rm -f $(DESTDIR)$(datadir)/icons/hicolor/64x64/apps/claws-contacts.png
	@rm -f $(DESTDIR)$(datadir)/icons/hicolor/128x128/apps/claws-contacts.png

remove-dbus-service-file:
	@rm -f $(service_DATA)

gtk_update_icon_cache = gtk-update-icon-cache -f -t $(datadir)/icons/hicolor

install-data-hook: rename-icons update-icon-cache
uninstall-hook: remove-icons update-icon-cache remove-dbus-service-file

update-icon-cache:
	@-if test -z "$(DESTDIR)"; then                                 \
                echo "Updating Gtk icon cache.";                        \
                $(gtk_update_icon_cache);                               \
        else                                                            \
                echo "*** Icon cache not updated. Remember to run:";    \
                echo "***";                                             \
                echo "***   $(gtk_update_icon_cache)";                  \
                echo "***";                                             \
        fi

EXTRA_DIST = \
	     $(DOCS) \
	     autogen.sh \
	     claws-contacts.pc.in \
	     $(service_in_files)

CLEANFILES = $(service_DATA)
