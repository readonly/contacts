# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR The Claws Mail Team and Michael Rasmussen
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: claws-contacts 0.6.1\n"
"Report-Msgid-Bugs-To: mir@datanom.net\n"
"POT-Creation-Date: 2015-08-02 23:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: xmllib/parser.c:78
msgid "No message"
msgstr ""

#: xmllib/parser.c:88
msgid "empty document"
msgstr ""

#: xmllib/parser.c:95
#, c-format
msgid "document of the wrong type, root node != <%s>"
msgstr ""

#: xmllib/parser.c:557 xmllib/parser.c:578 xmllib/parser.c:612
#: xmllib/parser.c:673 xmllib/parser.c:717
#, c-format
msgid "%s: Not saved"
msgstr ""

#: xmllib/parser.c:569
#, c-format
msgid "%s: Backup not made"
msgstr ""

#: libversit/vcard-utils.c:677
msgid "Does not exists"
msgstr ""

#: libversit/vcard-utils.c:691 src/mainwindow.c:470 src/mainwindow.c:484
msgid "Firstname"
msgstr ""

#: libversit/vcard-utils.c:693
msgid "Your Given Name"
msgstr ""

#: libversit/vcard-utils.c:694
msgid "Additional Names"
msgstr ""

#: libversit/vcard-utils.c:696
msgid "Space separated list of additional names"
msgstr ""

#: libversit/vcard-utils.c:697 src/mainwindow.c:457 src/mainwindow.c:497
msgid "Lastname"
msgstr ""

#: libversit/vcard-utils.c:699
msgid "Your Family name"
msgstr ""

#: libversit/vcard-utils.c:700
msgid "Address"
msgstr ""

#: libversit/vcard-utils.c:702
msgid "Your Address"
msgstr ""

#: libversit/vcard-utils.c:703
msgid "City"
msgstr ""

#: libversit/vcard-utils.c:705
msgid "Your City"
msgstr ""

#: libversit/vcard-utils.c:706
msgid "ZIP"
msgstr ""

#: libversit/vcard-utils.c:708
msgid "Your ZIP code"
msgstr ""

#: libversit/vcard-utils.c:709
msgid "Country"
msgstr ""

#: libversit/vcard-utils.c:711
msgid "Your Country"
msgstr ""

#: libversit/vcard-utils.c:712
msgid "Title"
msgstr ""

#: libversit/vcard-utils.c:714
msgid "Your Title"
msgstr ""

#: libversit/vcard-utils.c:715 src/callbacks.c:1390
msgid "URL"
msgstr ""

#: libversit/vcard-utils.c:717
msgid "URL to website"
msgstr ""

#: libversit/vcard-utils.c:718
msgid "X509 Certificate"
msgstr ""

#: libversit/vcard-utils.c:720
msgid "Your personal X509 Certificate"
msgstr ""

#: libversit/vcard-utils.c:721
msgid "Mobile Phone"
msgstr ""

#: libversit/vcard-utils.c:723
msgid "Your Mobile Phone"
msgstr ""

#: libversit/vcard-utils.c:724
msgid "Home Phone"
msgstr ""

#: libversit/vcard-utils.c:726
msgid "Your Home Phone number"
msgstr ""

#: libversit/vcard-utils.c:727
msgid "Work Phone"
msgstr ""

#: libversit/vcard-utils.c:729
msgid "Your Work Phone number"
msgstr ""

#: libversit/vcard-utils.c:730
msgid "Personal Email Address"
msgstr ""

#: libversit/vcard-utils.c:732
msgid "Your Personal Email Address"
msgstr ""

#: libversit/vcard-utils.c:733
msgid "Work Email Address"
msgstr ""

#: libversit/vcard-utils.c:735
msgid "Your Work Email Address"
msgstr ""

#: libversit/vcard-utils.c:736
msgid "FAX number"
msgstr ""

#: libversit/vcard-utils.c:738
msgid "Your FAX number"
msgstr ""

#: libversit/vcard-utils.c:739
msgid "Geographical Location"
msgstr ""

#: libversit/vcard-utils.c:741
msgid "Latitude, Longitude"
msgstr ""

#: libversit/vcard-utils.c:742
msgid "Photo"
msgstr ""

#: libversit/vcard-utils.c:744
msgid "Picture in JPEG, GIF or PNG format"
msgstr ""

#: libversit/vcard-utils.c:745
msgid "TimeZone"
msgstr ""

#: libversit/vcard-utils.c:747
msgid "TimeZone specified in ISO-8601 format"
msgstr ""

#: libversit/vcard-utils.c:748
msgid "Account this vCard relates to"
msgstr ""

#: libversit/vcard-utils.c:1147
msgid "Image"
msgstr ""

#: libversit/vcard-utils.c:1154
msgid "New _Image"
msgstr ""

#: libversit/vcard-utils.c:1161
msgid "Certificate"
msgstr ""

#: libversit/vcard-utils.c:1170
msgid "New _Certificate"
msgstr ""

#: libversit/vcard-utils.c:1204
msgid "Choose Account"
msgstr ""

#: libversit/vcard-utils.c:1205
msgid ""
"\n"
"Name of account to connect this vCard to.\n"
"If vCard already exists open in edit mode.\n"
"A vCard file has this naming convention: account\".vcf\"\n"
msgstr ""

#: libversit/vcard-utils.c:1212
msgid "Cannot create a vcard when account name is missing"
msgstr ""

#: libversit/vcard-utils.c:1216
msgid "Create Personal vCard"
msgstr ""

#: plugins/example/src/example-plugin.c:484
msgid "This plugin is only a demo."
msgstr ""

#: plugins/xml/xml-plugin.c:612
msgid ""
"One or more address books had identical names.\n"
"They have therefore been merged under a common name."
msgstr ""

#: plugins/xml/xml-plugin.c:798
msgid ""
"This plugin provides access to the Claws-mail native addressbook\n"
"and thereby replacing the old addressbook.\n"
"\n"
"The plugin provides lookup for a contact using any\n"
"available attribute. Supported wildcards are:\n"
"   *: Means any number of character(s) matches\n"
"   ?: Means any character match"
msgstr ""

#: plugins/ldap/ldap-plugin.c:252 plugins/ldap/ldap-plugin.c:268
msgid "Wrong URL"
msgstr ""

#: plugins/ldap/ldap-plugin.c:289
msgid ""
"Configured timeout limit exceeded. Try increase timeout on advanced "
"configuration page"
msgstr ""

#: plugins/ldap/ldap-plugin.c:295
msgid ""
"Configured number of entries limit exceeded. Try increase entries on "
"advanced configuration page"
msgstr ""

#: plugins/ldap/ldap-plugin.c:450
msgid "Missing URL"
msgstr ""

#: plugins/ldap/ldap-plugin.c:506
msgid "initialize: LDAP session initialization failed."
msgstr ""

#: plugins/ldap/ldap-plugin.c:1027
#, c-format
msgid "%s: Cannot get DN\n"
msgstr ""

#: plugins/ldap/ldap-plugin.c:1095
msgid "Search base"
msgstr ""

#: plugins/ldap/ldap-plugin.c:1096
msgid ""
"This specifies the name of the directory to be searched on the server. "
"Examples include:\n"
"dc=claws-mail,dc=org\n"
"  ou=people,dc=domainname,dc=com\n"
"  o=Organization Name,c=Country."
msgstr ""

#: plugins/ldap/ldap-plugin.c:1104
msgid "Timeout (Sec)"
msgstr ""

#: plugins/ldap/ldap-plugin.c:1105
msgid "Timeout in seconds. 0 means wait infinite."
msgstr ""

#: plugins/ldap/ldap-plugin.c:1111
msgid "Max entries"
msgstr ""

#: plugins/ldap/ldap-plugin.c:1112
msgid "Maximum number of contacts to receive. 0 means all."
msgstr ""

#: plugins/ldap/ldap-plugin.c:1554 plugins/ldap/ldap-plugin.c:1685
#: plugins/ldap/ldap-plugin.c:1760
msgid "Missing address book or contact"
msgstr ""

#: plugins/ldap/ldap-plugin.c:1560 plugins/ldap/ldap-plugin.c:1766
msgid "Missing mandatory attributes"
msgstr ""

#: plugins/ldap/ldap-plugin.c:1567 plugins/ldap/ldap-plugin.c:1692
#: plugins/ldap/ldap-plugin.c:1773
msgid "Missing address book connection"
msgstr ""

#: plugins/ldap/ldap-plugin.c:1580
msgid "LDAP Add: Unresolved error"
msgstr ""

#: plugins/ldap/ldap-plugin.c:1717
msgid "No such contact"
msgstr ""

#: plugins/ldap/ldap-plugin.c:1924
msgid "LDAP Update: Unresolved error"
msgstr ""

#: plugins/ldap/ldap-plugin.c:2014 plugins/ldap/ldap-plugin.c:2043
#, c-format
msgid "%s: Not found"
msgstr ""

#: plugins/ldap/ldap-plugin.c:2112
msgid "Missing config file"
msgstr ""

#: plugins/ldap/ldap-plugin.c:2135
msgid "Missing name for address book"
msgstr ""

#: plugins/ldap/ldap-plugin.c:2278
msgid "Missing matching configuration"
msgstr ""

#: plugins/ldap/ldap-plugin.c:2348
msgid ""
"This plugin provides LDAP support.\n"
"The plugin only supports LDAPv3 and the format\n"
"for a URL is as follows:\n"
"\n"
"[(ldap|ldaps)://](FQDN|IP)[:port]\n"
"\n"
"If neither ldap nor ldaps is specified ldap is assumed.\n"
"If port is not specified 389 is assumed for ldap\n"
"and 636 is assumed for ldaps.\n"
"If ldap is used for schema then TLS will be tried\n"
"automatically before using plain text connection.\n"
"Display name and Lastname are mandatory attributes."
msgstr ""

#: src/extension-loader.c:124
msgid "Missing reference to callback function"
msgstr ""

#: src/extension-loader.c:163
#, c-format
msgid "%d: Extension not found"
msgstr ""

#: src/extension-loader.c:212
msgid "Missing filename"
msgstr ""

#: src/extension-loader.c:262
msgid "Extension was not found"
msgstr ""

#: src/plugin-loader.c:228
#, c-format
msgid ""
"The following error occurred while loading %s :\n"
"\n"
"%s\n"
msgstr ""

#: src/plugin-loader.c:281 src/plugin-loader.c:306
msgid ""
"\n"
"\n"
"Version: "
msgstr ""

#: src/plugin-loader.c:283 src/plugin-loader.c:308
msgid ""
"\n"
"License: "
msgstr ""

#: src/plugin-loader.c:304
msgid "Error: "
msgstr ""

#: src/plugin-loader.c:305
msgid "Plugin is not functional."
msgstr ""

#: src/plugin-loader.c:350
msgid "Loaded plugins"
msgstr ""

#: src/plugin-loader.c:387
msgid "Plugin provides no feature"
msgstr ""

#: src/plugin-loader.c:487
msgid "Manage Plugins dialog"
msgstr ""

#: src/plugin-loader.c:506
msgid "Close dialog"
msgstr ""

#: src/plugin-loader.c:521
msgid "Plugin description"
msgstr ""

#: src/plugin-loader.c:559
msgid "_Load"
msgstr ""

#: src/plugin-loader.c:561
msgid "Load a new resource"
msgstr ""

#: src/plugin-loader.c:564
msgid "_Unload"
msgstr ""

#: src/plugin-loader.c:566
msgid "Unload a resource"
msgstr ""

#: src/plugin-loader.c:615
msgid "Failed to allocate memory for Plugin"
msgstr ""

#: src/plugin-loader.c:745
#, c-format
msgid "Plugin: '%s' from file (%s) allready loaded\n"
msgstr ""

#: src/utils.c:330
msgid "Could not create"
msgstr ""

#: src/utils.c:345
msgid "Cannot find home directory"
msgstr ""

#: src/utils.c:358
msgid "File exists"
msgstr ""

#: src/utils.c:508
msgid "Missing path"
msgstr ""

#: src/dbus/dbus-service.c:93
msgid "Could not create process to run claws-contacts"
msgstr ""

#: src/dbus/dbus-service.c:105
msgid "claws-contacts already running as daemon"
msgstr ""

#: src/dbus/dbus-service.c:115 src/dbus/server-object.c:560
msgid "Could not create new session ID"
msgstr ""

#: src/dbus/dbus-service.c:122 src/dbus/server-object.c:568
msgid "Not able to change dir to root"
msgstr ""

#: src/dbus/server-object.c:473 src/dbus/server-object.c:742
#: src/dbus/server-object.c:844 src/callbacks.c:505
#: extensions/vcard/src/vcard-extension.c:305
msgid "Missing address book"
msgstr ""

#: src/dbus/server-object.c:502 src/dbus/server-object.c:798
#: src/dbus/server-object.c:890
#, c-format
msgid "%s: Address book not found here"
msgstr ""

#: src/dbus/server-object.c:541
msgid "Could not create process to run addressbook"
msgstr ""

#: src/dbus/server-object.c:550
msgid "Addressbook already running as daemon"
msgstr ""

#: src/dbus/server-object.c:605 src/dbus/server-object.c:660
msgid "Missing response structure"
msgstr ""

#: src/dbus/server-object.c:649
msgid "Missing search token"
msgstr ""

#: src/dbus/server-object.c:754
msgid "Missing new contact"
msgstr ""

#: src/dbus/server-object.c:856
msgid "Missing vCard"
msgstr ""

#: src/dbus/server-object.c:914
#, c-format
msgid "%s: Vcard not found here"
msgstr ""

#: src/settings.c:86
msgid "Choose address book which is added new contacts by default"
msgstr ""

#: src/settings.c:97
msgid "Choose default address book"
msgstr ""

#: src/callbacks.c:504
msgid "Missing plugin"
msgstr ""

#: src/callbacks.c:514 src/callbacks.c:1792 src/callbacks.c:1832
#: src/callbacks.c:1928
msgid "Plugin does not support updates"
msgstr ""

#: src/callbacks.c:651
msgid "No address book to edit"
msgstr ""

#: src/callbacks.c:722
msgid "Please highlight desired address book to edit"
msgstr ""

#: src/callbacks.c:758
#, c-format
msgid "Remove '%s'?"
msgstr ""

#: src/callbacks.c:775 src/callbacks.c:911
msgid "Please highlight desired address book for deletion"
msgstr ""

#: src/callbacks.c:900
#, c-format
msgid "Close '%s' ?"
msgstr ""

#: src/callbacks.c:955 src/callbacks.c:967
#, c-format
msgid "%s exists. Remove ?"
msgstr ""

#: src/callbacks.c:1018
msgid "Loading address books"
msgstr ""

#: src/callbacks.c:1236
msgid "Advanced settings"
msgstr ""

#: src/callbacks.c:1361
msgid "Edit address book"
msgstr ""

#: src/callbacks.c:1365
msgid "New address book"
msgstr ""

#: src/callbacks.c:1384
msgid "Name"
msgstr ""

#: src/callbacks.c:1386
msgid "The name for this address book"
msgstr ""

#: src/callbacks.c:1392
msgid ""
"URL or path to this address book\n"
"Must conform to plugin requirements."
msgstr ""

#: src/callbacks.c:1397
msgid "Open file dialog"
msgstr ""

#: src/callbacks.c:1412
msgid "Username"
msgstr ""

#: src/callbacks.c:1414
msgid "And optional username"
msgstr ""

#: src/callbacks.c:1419
msgid "Password"
msgstr ""

#: src/callbacks.c:1421
msgid "And optional password"
msgstr ""

#: src/callbacks.c:1441
msgid "Basic settings"
msgstr ""

#: src/callbacks.c:1448
msgid "Address book settings"
msgstr ""

#: src/callbacks.c:1772 src/contactwindow.c:1116
#: extensions/export/ldifexport_extension.c:215
#: extensions/import/ldifimport_extension.c:94
msgid "[New Contact] Choose address book"
msgstr ""

#: src/callbacks.c:1817
msgid "[Delete Contact] Choose address book"
msgstr ""

#: src/callbacks.c:1843
msgid "[Delete Contact] Choose contact to delete"
msgstr ""

#: src/callbacks.c:1883
#, c-format
msgid "Should contact '%s' be deleted?"
msgstr ""

#: src/callbacks.c:1935
msgid "[Edit Contact] Choose address book"
msgstr ""

#: src/callbacks.c:1953
msgid "[Edit Contact] Choose contact to edit"
msgstr ""

#: src/callbacks.c:2105
msgid "Plugin does not support advanced search"
msgstr ""

#: src/callbacks.c:2179
msgid "[Print Contact] Choose address book"
msgstr ""

#: src/printing.c:275
#, c-format
msgid ""
"Error printing file:\n"
"%s"
msgstr ""

#: src/claws-contacts.c:60
msgid "Run in compose mode"
msgstr ""

#: src/claws-contacts.c:62
msgid "Dont fork in service mode"
msgstr ""

#: src/claws-contacts.c:64
msgid "Avoid loading any extensions"
msgstr ""

#: src/claws-contacts.c:66
msgid "Run as a DBus service"
msgstr ""

#: src/claws-contacts.c:68
msgid "Run in debug mode"
msgstr ""

#: src/claws-contacts.c:82
#, c-format
msgid "Parsing options failed: %s\n"
msgstr ""

#: src/claws-contacts.c:103 src/mainwindow.c:612
msgid "Claws-mail Address Book"
msgstr ""

#: src/contactwindow.c:209
msgid "[plugin_update_contact] Unresolved error"
msgstr ""

#: src/contactwindow.c:303
msgid "Contact has unsaved data. Save?"
msgstr ""

#: src/contactwindow.c:433
#, c-format
msgid "%s: Delete email address?"
msgstr ""

#: src/contactwindow.c:442 src/contactwindow.c:447
msgid "Highlight desired email address to delete"
msgstr ""

#: src/contactwindow.c:559
msgid "Address book does not support saving images"
msgstr ""

#: src/contactwindow.c:854
msgid "Contact details"
msgstr ""

#: src/contactwindow.c:910
msgid "Mouse-left-click on photo, or Ctrl+I will activate edit mode"
msgstr ""

#: src/contactwindow.c:940
msgid ""
"Double-click, enter, or space on cell will activate edit mode\n"
"Mouse-Left-click to drag and drop for reordering email list\n"
"First email in list is set as default email"
msgstr ""

#: src/contactwindow.c:951
msgid "Alias"
msgstr ""

#: src/contactwindow.c:962 src/mainwindow.c:511
msgid "Email"
msgstr ""

#: src/contactwindow.c:974
msgid "Remarks"
msgstr ""

#: src/contactwindow.c:987
msgid "_Add"
msgstr ""

#: src/contactwindow.c:991
msgid "_Delete"
msgstr ""

#: src/contactwindow.c:1009
msgid "_Standard attributes"
msgstr ""

#: src/contactwindow.c:1026
msgid "_Extended attributes"
msgstr ""

#: src/contactwindow.c:1041
msgid "A_pply"
msgstr ""

#: src/contactwindow.c:1153 src/contactwindow.c:1283
msgid "Please highlight desired address book holding contacts"
msgstr ""

#: src/contactwindow.c:1171
msgid ""
"Seach using term for contact in current address book.\n"
"Search is done in all the listed fields if it contains a term.\n"
"Empty fields are disregarded in the search.\n"
"Use * or ? for wildcard. Search is case sensitive."
msgstr ""

#: src/contactwindow.c:1180
msgid "[Locate Contact] Choose address book"
msgstr ""

#: src/contactwindow.c:1194
msgid "Locate contact"
msgstr ""

#: src/contactwindow.c:1250
msgid "O_r"
msgstr ""

#: src/contactwindow.c:1253
msgid "_And"
msgstr ""

#: src/about.c:145
msgid "Copyright © 2008, 2011 Michael Rasmussen.\n"
msgstr ""

#: src/about.c:147
msgid "Claws-Mail Address Book."
msgstr ""

#: src/about.c:150
msgid "Claws-contacts Website"
msgstr ""

#: src/mainwindow.c:59
msgid "No open address book"
msgstr ""

#: src/mainwindow.c:60
msgid "Connected to address book: "
msgstr ""

#: src/mainwindow.c:61
msgid "Searching address book..."
msgstr ""

#: src/mainwindow.c:148
msgid "_Manage plugins"
msgstr ""

#: src/mainwindow.c:156
msgid "_Page setup"
msgstr ""

#: src/mainwindow.c:171
msgid "_File"
msgstr ""

#: src/mainwindow.c:177 src/mainwindow.c:221
msgid "_New"
msgstr ""

#: src/mainwindow.c:192
msgid "_Open"
msgstr ""

#: src/mainwindow.c:200
msgid "_Close"
msgstr ""

#: src/mainwindow.c:215
msgid "_Address books"
msgstr ""

#: src/mainwindow.c:251
msgid "_Contact"
msgstr ""

#: src/mainwindow.c:265
msgid "_Attributes"
msgstr ""

#: src/mainwindow.c:273
msgid "_Tools"
msgstr ""

#: src/mainwindow.c:282
msgid "_Help"
msgstr ""

#: src/mainwindow.c:298
msgid "Quit Address Book"
msgstr ""

#: src/mainwindow.c:307
msgid "Print contact information"
msgstr ""

#: src/mainwindow.c:315
msgid "Close address book"
msgstr ""

#: src/mainwindow.c:321
msgid "Open address book"
msgstr ""

#: src/mainwindow.c:329
msgid "Create a new address book"
msgstr ""

#: src/mainwindow.c:367
msgid "Address book in BOLD is the default address book"
msgstr ""

#: src/mainwindow.c:378
msgid "Address books"
msgstr ""

#: src/mainwindow.c:419
msgid "Double-click, enter, or space on cell will activate edit mode"
msgstr ""

#: src/mainwindow.c:436
msgid "Display name"
msgstr ""

#: src/mainwindow.c:526
msgid "To:"
msgstr ""

#: src/mainwindow.c:530
msgid "Add contact to To:"
msgstr ""

#: src/mainwindow.c:532
msgid "Cc:"
msgstr ""

#: src/mainwindow.c:536
msgid "Add contact to Cc:"
msgstr ""

#: src/mainwindow.c:538
msgid "Bcc:"
msgstr ""

#: src/mainwindow.c:542
msgid "Add contact to Bcc:"
msgstr ""

#: src/mainwindow.c:548
msgid "Add a new contact"
msgstr ""

#: src/mainwindow.c:550
msgid "_Locate"
msgstr ""

#: src/mainwindow.c:554
msgid "Open advanced search dialog"
msgstr ""

#: src/mainwindow.c:564
msgid "Search criteria"
msgstr ""

#: src/mainwindow.c:569
msgid ""
"Seach using term for contact in current address book.\n"
"Search is done in 'Display name', 'Firstname',\n"
"'Lastname', 'Nick name', 'Alias', and 'Email'.\n"
"Use [Locate] for a more fine grained control.\n"
"Use * or ? for wildcard. Search is case insensitive."
msgstr ""

#: src/mainwindow.c:582
msgid "Search contacts matching search criteria"
msgstr ""

#: src/mainwindow.c:585
msgid "Clea_r"
msgstr ""

#: src/mainwindow.c:589
msgid "Clear any active search criteria and restore default view"
msgstr ""

#: extensions/export/wizard.c:89
msgid "Create file"
msgstr ""

#: extensions/export/wizard.c:150
msgid "Enter Filename"
msgstr ""

#: extensions/export/wizard.c:209
msgid "Enter suffix"
msgstr ""

#: extensions/export/wizard.c:211
msgid ""
"Suffix is the common part of the Distinguished\n"
"Name (DN) shared with any other component\n"
"stored in this part of the directory"
msgstr ""

#: extensions/export/wizard.c:228
msgid "Enter Relative DN"
msgstr ""

#: extensions/export/wizard.c:232
msgid ""
"Relative DN (RDN) is the last component\n"
"(read from right to left) placed before\n"
"suffix. This must be the part of the DN\n"
"which uniquely distinguishes this entry\n"
"from any other entry"
msgstr ""

#: extensions/export/wizard.c:248
msgid "_Use DN attribute if present in data"
msgstr ""

#: extensions/export/wizard.c:249
msgid ""
"If the contact has a defined\n"
"DN should that be used instead\n"
"of a constructed one"
msgstr ""

#: extensions/export/wizard.c:270
msgid "Writing to file"
msgstr ""

#: extensions/export/wizard.c:283
msgid "Chosen suffix"
msgstr ""

#: extensions/export/wizard.c:295
msgid "Chosen RDN"
msgstr ""

#: extensions/export/wizard.c:307
msgid "Use found DN"
msgstr ""

#: extensions/export/wizard.c:349
msgid "Choose file name"
msgstr ""

#: extensions/export/wizard.c:350
msgid "Construct DN for record"
msgstr ""

#: extensions/export/wizard.c:351
msgid "Create file with these values"
msgstr ""

#: extensions/export/wizard.c:370
msgid "Export to LDIF"
msgstr ""

#: extensions/export/ldifexport_extension.c:241
#, c-format
msgid ""
"Data successfully exported as LDIF to file\n"
"%s"
msgstr ""

#: extensions/export/ldifexport_extension.c:263
#: extensions/import/ldifimport_extension.c:191
#: extensions/vcard/src/vcard-extension.c:397
#: extensions/vcard/src/vcard-extension.c:409
#: extensions/vcard/src/vcard-extension.c:423
msgid "tools"
msgstr ""

#: extensions/export/ldifexport_extension.c:264
#: extensions/export/ldifexport_extension.c:275
#: extensions/vcard/src/vcard-extension.c:398
#: extensions/vcard/src/vcard-extension.c:435
#: extensions/vcard/src/vcard-extension.c:446
msgid "_Export"
msgstr ""

#: extensions/export/ldifexport_extension.c:271
#: extensions/import/ldifimport_extension.c:188
#: extensions/import/ldifimport_extension.c:199
msgid "_LDIF"
msgstr ""

#: extensions/export/ldifexport_extension.c:337
msgid "Export an address book in LDIF format"
msgstr ""

#: extensions/import/ldifimport_parser.c:111
#: extensions/import/ldifimport_parser.c:120
#: extensions/import/ldifimport_parser.c:180
#: extensions/import/ldifimport_parser.c:203
#: extensions/import/ldifimport_parser.c:224
msgid "Bad file format"
msgstr ""

#: extensions/import/ldifimport_parser.c:417
#, c-format
msgid "Key: %s "
msgstr ""

#: extensions/import/ldifimport_parser.c:420
#, c-format
msgid "Value: %s\n"
msgstr ""

#: extensions/import/ldifimport_parser.c:443
#, c-format
msgid "Info to save:\n"
msgstr ""

#: extensions/import/ldifimport_parser.c:450
#, c-format
msgid "Info which is not saved:\n"
msgstr ""

#: extensions/import/ldifimport_parser.c:556
msgid "cannot determine"
msgstr ""

#: extensions/import/ldifimport_extension.c:58
#: extensions/vcard/src/vcard-extension.c:75
msgid "Open file"
msgstr ""

#: extensions/import/ldifimport_extension.c:192
#: extensions/import/ldifimport_extension.c:202
#: extensions/example/src/example-extension.c:85
#: extensions/vcard/src/vcard-extension.c:410
#: extensions/vcard/src/vcard-extension.c:457
msgid "_Import"
msgstr ""

#: extensions/import/ldifimport_extension.c:260
msgid "Import file in LDIF format into an address book"
msgstr ""

#: extensions/example/src/example-extension.c:51
msgid "test"
msgstr ""

#: extensions/example/src/example-extension.c:59
msgid "_Test2"
msgstr ""

#: extensions/example/src/example-extension.c:62
msgid "file"
msgstr ""

#: extensions/example/src/example-extension.c:70
msgid "_Test3"
msgstr ""

#: extensions/example/src/example-extension.c:82
msgid "_Test"
msgstr ""

#: extensions/example/src/example-extension.c:93
msgid "_Zest2"
msgstr ""

#: extensions/example/src/example-extension.c:96
#: extensions/example/src/example-extension.c:107
msgid "_Menu"
msgstr ""

#: extensions/example/src/example-extension.c:104
msgid "_Yest1"
msgstr ""

#: extensions/example/src/example-extension.c:108
msgid "_Test1"
msgstr ""

#: extensions/example/src/example-extension.c:160
msgid "Example Claws-contacts extension"
msgstr ""

#: extensions/vcard/src/vcard-extension.c:73
msgid "Save file"
msgstr ""

#: extensions/vcard/src/vcard-extension.c:99
#, c-format
msgid "vCard parser: %s"
msgstr ""

#: extensions/vcard/src/vcard-extension.c:153
msgid "Insufficient memory"
msgstr ""

#: extensions/vcard/src/vcard-extension.c:196
msgid "[vCard Import] Choose address book"
msgstr ""

#: extensions/vcard/src/vcard-extension.c:250
msgid "Object is not in vCard format"
msgstr ""

#: extensions/vcard/src/vcard-extension.c:259
#, c-format
msgid ""
"%s: Imported %d contacts out of %d Objects.\n"
"Rejected Objects can be found in %s"
msgstr ""

#: extensions/vcard/src/vcard-extension.c:266
#, c-format
msgid "Parsed file can be found in %s"
msgstr ""

#: extensions/vcard/src/vcard-extension.c:290
msgid "[vCard Export] Choose address book"
msgstr ""

#: extensions/vcard/src/vcard-extension.c:354
#, c-format
msgid "%s: Exported %d contacts"
msgstr ""

#: extensions/vcard/src/vcard-extension.c:394
#: extensions/vcard/src/vcard-extension.c:406
#: extensions/vcard/src/vcard-extension.c:432
#: extensions/vcard/src/vcard-extension.c:443
#: extensions/vcard/src/vcard-extension.c:454
msgid "_vCard"
msgstr ""

#: extensions/vcard/src/vcard-extension.c:418
msgid "_Create vCard"
msgstr ""

#: extensions/vcard/src/vcard-extension.c:515
msgid "Export and import contacts in vCard 2.1 format"
msgstr ""
