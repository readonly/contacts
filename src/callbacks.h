/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __CALLBACKS_H__
#define __CALLBACKS_H__

#include <glib.h>

G_BEGIN_DECLS

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "plugin.h"
#include "plugin-loader.h"

gint delete_event(GtkWidget* widget, GdkEvent* event, gpointer data);
void menu_quit_cb(GtkMenuItem* menuitem, gpointer data);
void toolbar_quit_cb(GtkToolButton* toolbutton, gpointer data);
void file_plugin_cb(GtkMenuItem *menuitem, gpointer data);
void printer_page_setup_cb(GtkWidget* widget, gpointer data);
void abook_new_cb(GtkWidget* widget, gpointer data);
void abook_close_cb(GtkWidget* widget, gpointer data);
void abook_edit_cb(GtkWidget* widget, gpointer data);
void abook_delete_cb(GtkWidget* widget, gpointer data);
void abook_open_cb(GtkWidget* widget, gpointer data);
void update_abook_list(MainWindow* mainwindow);
void destroy_progress_dialog(MainWindow* mainwindow);
void show_progress_dialog(MainWindow* mainwindow);
void abook_list_cursor_changed_cb(GtkTreeView *tree_view, gpointer data);
void abook_list_row_activated_cb(GtkTreeView *tree_view,
								 GtkTreePath *path,
								 GtkTreeViewColumn *column,
								 gpointer data);
gboolean list_button_pressed_cb(GtkWidget *widget,
								GdkEventButton *event,
								gpointer data);
gboolean list_popup_menu_cb(GtkWidget* widget, gpointer data);
gboolean address_book_edit(GtkWidget* parent,
						   Plugin* plugin,
						   AddressBook** address_book);
void book_cell_data_format(GtkTreeViewColumn *col,
                           GtkCellRenderer   *renderer,
                           GtkTreeModel      *model,
                           GtkTreeIter       *iter,
                           gpointer           data);
void show_about(GtkWidget* widget, gpointer data);
void show_help(GtkWidget* widget, gpointer data);
void contact_list_cursor_changed_cb(GtkTreeView *tree_view, gpointer data);
void contact_list_row_activated_cb(GtkTreeView *tree_view,
								   GtkTreePath *path,
								   GtkTreeViewColumn *column,
								   gpointer data);
void contact_cell_edited_cb(GtkCellRendererText *cell,
							gchar *path_string,
							gchar *new_text,
							gpointer data);
void contact_add_to_cb(GtkWidget* widget, gpointer data);
void contact_add_cc_cb(GtkWidget* widget, gpointer data);
void contact_add_bcc_cb(GtkWidget* widget, gpointer data);
void contact_new_cb(GtkWidget* widget, gpointer data);
void contact_delete_cb(GtkWidget* widget, gpointer data);
void contact_edit_cb(GtkWidget* widget, gpointer data);
void contact_print_cb(GtkWidget* widget, gpointer data);
void contact_print_prepare_cb(GtkWidget* widget, gpointer data);
void tools_attribs_cb(GtkWidget* widget, gpointer data);
void tools_prefs_cb(GtkWidget* widget, gpointer data);
void contact_basic_search_cb(GtkWidget* widget, gpointer data);
void contact_clear_search_cb(GtkWidget* widget, gpointer data);
void contact_clear_sensitivity_cb(GtkWidget* widget, GdkEvent* event, gpointer data);
void contact_advanced_search_cb(GtkWidget* widget, gpointer data);

gchar* contact_write_to_backend(Plugin* plugin,
								AddressBook* abook,
								Contact* old,
								Contact* new);
void read_config(MainWindow* mainwindow);
void shutdown_application(gpointer data);
void markup_default_book(GtkTreeViewColumn* col,
						 GtkCellRenderer* renderer,
						 GtkTreeModel* model,
						 GtkTreeIter* iter,
						 gpointer data);
void config_main_free(ConfigFile* config);
gboolean mainwindow_key_press_event_cb(GtkWidget *widget,
									   GdkEventKey *event,
									   gpointer   user_data);

G_END_DECLS

#endif
