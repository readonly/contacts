/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <gcrypt.h>
#include "mainwindow.h"
#include "utils.h"
#include "dbus-service.h"

static gboolean compose = FALSE;
static gboolean service = FALSE;
static gboolean keep = FALSE;
static gboolean no_extensions = FALSE;
#if DEBUG
static gboolean debug_mode = TRUE;
#else
static gboolean debug_mode = FALSE;
#endif

static GOptionEntry entries[] = {
    {"compose", 'c', 0, G_OPTION_ARG_NONE, &compose,
        N_("Run in compose mode"), NULL},
    {"keep", 'k', 0, G_OPTION_ARG_NONE, &keep,
        N_("Dont fork in service mode"), NULL},
    {"noext", 'n', 0, G_OPTION_ARG_NONE, &no_extensions,
        N_("Avoid loading any extensions"), NULL},
    {"service", 's', 0, G_OPTION_ARG_NONE, &service,
        N_("Run as a DBus service"), NULL},
    {"debug", 'd', 0, G_OPTION_ARG_NONE, &debug_mode,
        N_("Run in debug mode"), NULL},
    {NULL}
};
 
static gboolean parse_cmdline(int argc, char** argv) {
    GError* error = NULL;
    GOptionContext* context;
    gboolean res = FALSE;

    context = g_option_context_new(NULL);
    g_option_context_add_main_entries(context, entries, PACKAGE_STRING);
    g_option_context_add_group(context, gtk_get_option_group(TRUE));
    g_option_context_set_help_enabled(context, TRUE);
    if (! g_option_context_parse(context, &argc, &argv, &error)) {
        g_print(_("Parsing options failed: %s\n"), error->message);
        g_clear_error(&error);
        res = TRUE;
    }
    g_option_context_free(context);
    return res;
}

int main(int argc, char** argv) {
    gchar* error = NULL;
    static int gcrypt_init;
    
    if (!gcrypt_init) {
        if (!gcry_check_version(NULL))
            return EXIT_FAILURE;
        gcry_control(GCRYCTL_DISABLE_SECMEM, 0);
        gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);
        gcrypt_init = 1;
    }
    mainloop = NULL;
    DAEMON = FALSE;
    g_set_application_name(_("Claws-mail Address Book"));

    if (parse_cmdline(argc, argv))
        return 1;

    debug_set_mode(debug_mode);

    first_time(NULL, NULL, &error);
    if (error) {
        g_printerr("%s", error);
        return EXIT_FAILURE;
    }
    
    if (service) {
        start_service(keep);
        //start_service(keep, no_extensions);
    }
    else {
        gtk_init(&argc, &argv);
        application_start(compose, no_extensions);
    }

    return EXIT_SUCCESS;
}
