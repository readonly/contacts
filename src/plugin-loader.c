/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gprintf.h>
#include <gmodule.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gcrypt.h>
#include <string.h>
#include "plugin.h"
#include "plugin-loader.h"
#include "gtk-utils.h"
#include "utils.h"
#include "callbacks.h"

#define WIDTH 500
#define HEIGHT 350

enum {
	PLUGINWINDOW_NAME,		/*<! plugin name */
	PLUGINWINDOW_DATA,		/*<! Plugin pointer */
	PLUGINWINDOW_STYLE,		/*<! italic if error */
	N_PLUGINWINDOW_COLUMNS
};

typedef struct {
	GtkWidget *window;
	GtkWidget *plugin_list_view;
	GtkWidget *plugin_desc;
	GtkWidget *unload_btn;

	Plugin *selected_plugin;
	MainWindow *main; 
} PluginWindow;

const gchar *standard_attribs[] = {
	"cn", "first-name", "last-name", "nick-name", "email", "image",	NULL
};

static GSList* plugins = NULL;
static GSList* unloaded_plugins = NULL;
 
static void close_cb(GtkWidget* widget, gpointer data) {
	PluginWindow* pluginwindow = (PluginWindow *) data;

	debug_print("Destroying plugin-loader dialog\n");
	gtk_widget_destroy(pluginwindow->window);
	g_free(pluginwindow);
}

static gint pluginwindow_delete_cb(GtkWidget* widget, GdkEvent* event,
		gpointer data) {
	close_cb(NULL, data);
	return TRUE;
}

static const gchar* plugin_get_name(Plugin* plugin) {
	return plugin->name();
}

static const gchar* plugin_get_error(Plugin* plugin) {
	return (const gchar *) plugin->error;
}

static const gchar* plugin_get_desc(Plugin* plugin) {
	return plugin->desc();
}

static const gchar* plugin_get_version(Plugin* plugin) {
	return plugin->version();
}

static const gchar* plugin_get_license(Plugin* plugin) {
	return plugin->license();
}

/*static const gchar* plugin_get_addrbook_name(Plugin* plugin) {
	return (const gchar *) plugin->addrbook_name;
}*/

static GSList* plugin_get_list(void) {
	GSList *new = NULL;
	GSList *cur = plugins;
	for (; cur; cur = cur->next) {
		Plugin *p = (Plugin *)cur->data;
		new = g_slist_prepend(new, p);
	}
	new = g_slist_reverse(new);
	return new;
}

static GSList* plugin_get_unloaded_list(void) {
	GSList *new = NULL;
	GSList *cur = unloaded_plugins;
	for (; cur; cur = cur->next) {
		Plugin *p = (Plugin *)cur->data;
		new = g_slist_prepend(new, p);
	}
	new = g_slist_reverse(new);
	return new;
}

static void set_plugin_list(PluginWindow* pluginwindow) {
	GSList *loaded, *cur, *unloaded;
	const gchar *text;
	GtkListStore *store;
	GtkTreeIter iter;
	GtkTextBuffer *textbuf;
	GtkTextIter start_iter, end_iter;
	GtkTreeSelection *selection;

	loaded = plugin_get_list();
	unloaded = plugin_get_unloaded_list();
	
	store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW
				(pluginwindow->plugin_list_view)));
 	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(store),
                                             0, GTK_SORT_ASCENDING);
	gtk_list_store_clear(store);
	
	textbuf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(pluginwindow->plugin_desc));
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(pluginwindow->plugin_desc), FALSE);
	gtk_text_view_set_editable(GTK_TEXT_VIEW(pluginwindow->plugin_desc), FALSE);
	gtk_text_buffer_get_start_iter(textbuf, &start_iter);
	gtk_text_buffer_get_end_iter(textbuf, &end_iter);
	gtk_text_buffer_delete(textbuf, &start_iter, &end_iter);
	gtk_widget_set_sensitive(pluginwindow->unload_btn, FALSE);

	for(cur = loaded; cur != NULL; cur = g_slist_next(cur)) {
		Plugin *plugin = (Plugin *) cur->data;

		gtk_list_store_append(store, &iter);
		text = plugin_get_name(plugin);
		gtk_list_store_set(store, &iter,
				   PLUGINWINDOW_NAME, text,
				   PLUGINWINDOW_DATA, plugin,
				   PLUGINWINDOW_STYLE, PANGO_STYLE_NORMAL,
				   -1);
	}

	for(cur = unloaded; cur != NULL; cur = g_slist_next(cur)) {
		Plugin *plugin = (Plugin *) cur->data;

		gtk_list_store_append(store, &iter);
		text = plugin_get_name(plugin);
		gtk_list_store_set(store, &iter,
				   PLUGINWINDOW_NAME, text,
				   PLUGINWINDOW_DATA, plugin,
				   PLUGINWINDOW_STYLE, PANGO_STYLE_ITALIC,
				   -1);
	}

	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(pluginwindow->plugin_list_view));
	if (gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter))
		gtk_tree_selection_select_iter(selection, &iter);
		
	gslist_free(&loaded, NULL);
	gslist_free(&unloaded, NULL);
}

static void load_plugin_cb(GtkWidget* widget, gpointer data) {
	PluginWindow* pluginwindow = (PluginWindow *) data;
	GtkWidget *dialog;
	GSList* files;
	
	GtkFileFilter *filter = gtk_file_filter_new();
	gtk_file_filter_set_name(filter, "*." G_MODULE_SUFFIX);
	gtk_file_filter_add_pattern(filter, "*." G_MODULE_SUFFIX);
	
	dialog = gtk_file_chooser_dialog_new("Select plugin",
					      GTK_WINDOW(pluginwindow->window),
					      GTK_FILE_CHOOSER_ACTION_OPEN,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					      NULL);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER (dialog), PLUGINDIR);
	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
	gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(dialog), TRUE);
	
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		files = gtk_file_chooser_get_filenames(GTK_FILE_CHOOSER(dialog));
		gtk_widget_destroy(dialog);
		if (files) {
			GSList *tmp;
			for (tmp = files; tmp; tmp = g_slist_next(tmp)) {
				gchar *file, *error = NULL;
	
				file = (gchar *) tmp->data;
				if (!file) continue;
				plugin_load(file, &error);
				if (error != NULL) {
					gchar *basename = g_path_get_basename(file);
					show_message(pluginwindow->window, 
						GTK_UTIL_MESSAGE_ERROR,
						_("The following error occurred while loading %s :\n\n%s\n"),
						basename, error);
					g_free(basename);
					g_free(error);
					error = NULL;
				}
	
				set_plugin_list(pluginwindow);
				g_free(file);
			}
			gslist_free(&files, NULL);
		}
	}
	else
		gtk_widget_destroy(dialog);
	update_abook_list(pluginwindow->main);
}

static void unload_plugin_cb(GtkWidget* widget, gpointer data) {
	PluginWindow* pluginwindow = (PluginWindow *) data;
	Plugin *plugin = pluginwindow->selected_plugin;

	plugin_unload(plugin);
	pluginwindow->selected_plugin = NULL;
	set_plugin_list(pluginwindow);
	update_abook_list(pluginwindow->main);
}

static GtkListStore* pluginwindow_create_data_store(void) {
	return gtk_list_store_new(N_PLUGINWINDOW_COLUMNS,
				  G_TYPE_STRING,	
				  G_TYPE_POINTER,
				  PANGO_TYPE_STYLE,
				  -1);
}

static void select_row_cb(Plugin *plugin, PluginWindow *pluginwindow) {
	GtkTextView *plugin_desc = GTK_TEXT_VIEW(pluginwindow->plugin_desc);
	GtkTextBuffer *textbuf = gtk_text_view_get_buffer(plugin_desc);
	GtkTextIter start_iter, end_iter;
	gchar *text, *tmp, *cap, *search;

	pluginwindow->selected_plugin = plugin;

	if (pluginwindow->selected_plugin != NULL) {
		const gchar* desc = plugin_get_desc(plugin);
		const gchar* err = plugin_get_error(plugin);
		const gchar* license = plugin_get_license(plugin);
		gtk_text_buffer_get_start_iter(textbuf, &start_iter);
		gtk_text_buffer_get_end_iter(textbuf, &end_iter);
		gtk_text_buffer_delete(textbuf, &start_iter, &end_iter);
		
		if (err == NULL) {
			tmp = g_strconcat(desc, _("\n\nVersion: "),
				   plugin_get_version(plugin),
				   _("\nLicense: "), license, NULL);
			PluginFeature* provide = plugin->provides();
			if (HAS_ADV_SEARCH(provide->support))
				search = g_strdup("Advanced search");
			else
				search = NULL;
			if (IS_READ_ONLY(provide->support))
				cap = g_strdup("Read-only");
			else
				cap = g_strdup("Read-write");
			if (search) {
				text = g_strconcat(tmp, "\nFeatures: ", search, ", ",
					cap, "\n", NULL);
				g_free(search);
			}
			else
				text = g_strconcat(tmp, "\nFeatures: ",	cap, "\n", NULL);
			g_free(tmp);
			g_free(cap);
		}
		else
			text = g_strconcat(_("Error: "),
				   err, "\n", _("Plugin is not functional."), 
				   "\n\n", desc, _("\n\nVersion: "),
				   plugin_get_version(plugin),
				   _("\nLicense: "), license, "\n", NULL);
		gtk_text_buffer_insert(textbuf, &start_iter, text, strlen(text));
		g_free(text);
		gtk_widget_set_sensitive(pluginwindow->unload_btn, TRUE);
	} else {
		gtk_widget_set_sensitive(pluginwindow->unload_btn, FALSE);
	}
}

static void unselect_row_cb(Plugin *plugin, PluginWindow *pluginwindow) {
	gtk_widget_set_sensitive(pluginwindow->unload_btn, FALSE);	
}

static gboolean pluginwindow_selected(GtkTreeSelection *selector,
				      GtkTreeModel *model, 
				      GtkTreePath *path,
				      gboolean currently_selected,
				      gpointer data) {
	GtkTreeIter iter;
	Plugin *plugin;
	
	if (!gtk_tree_model_get_iter(model, &iter, path))
		return TRUE;

	gtk_tree_model_get(model, &iter, 
			   PLUGINWINDOW_DATA, &plugin,
			   -1);

	if (currently_selected) 
		unselect_row_cb(plugin, data);
	else
		select_row_cb(plugin, data);

	return TRUE;
}

static void pluginwindow_create_list_view_columns(GtkWidget *list_view) {
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes
		(_("Loaded plugins"),
		 renderer,
		 "text", PLUGINWINDOW_NAME,
		 "style", PLUGINWINDOW_STYLE,
		 NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(list_view), column);		
}

static GtkWidget *pluginwindow_list_view_create(PluginWindow *pluginwindow) {
	GtkTreeView *list_view;
	GtkTreeSelection *selector;
	GtkTreeModel *model;

	model = GTK_TREE_MODEL(pluginwindow_create_data_store());
	list_view = GTK_TREE_VIEW(gtk_tree_view_new_with_model(model));
	g_object_unref(model);	

	//gtk_tree_view_set_rules_hint(list_view, prefs_common.use_stripes_everywhere);
	//gtk_tree_view_set_search_column (list_view, 0);

	selector = gtk_tree_view_get_selection(list_view);
	gtk_tree_selection_set_mode(selector, GTK_SELECTION_BROWSE);
	gtk_tree_selection_set_select_function(selector, pluginwindow_selected,
					       pluginwindow, NULL);

	/* create the columns */
	pluginwindow_create_list_view_columns(GTK_WIDGET(list_view));

	return GTK_WIDGET(list_view);
}

static gchar *plugin_check_features(Plugin* plugin) {
	gchar* msg = NULL;
	PluginFeature* features;
	
	features = plugin->provides();
	if (features == NULL)
		msg = g_strdup(_("Plugin provides no feature"));

	plugin->readonly =
		IS_READ_ONLY(features->support) || ! IS_READ_WRITE(features->support);
	plugin->can_adv_search = HAS_ADV_SEARCH(features->support);

	return msg;
}

static void plugin_free(Plugin* plugin) {
	debug_print("Plugin `%s' unloaded\n", plugin_get_name(plugin));
	g_module_close(plugin->module);
	g_free(plugin->filename);
	g_free(plugin->id);
	g_free(plugin->error);
	g_free(plugin);
}

static void compute_hash(Plugin* plugin) {
    gchar* cipher;

	gchar* plain = g_strconcat(
			plugin->name(), " ",
			plugin->desc(), " ",
			plugin->version(), NULL);

	cipher = sha256(plain);
    g_free(plain);
    debug_print("Computed hash: %s\n", cipher);

    plugin->id = g_strdup(cipher);
	g_free(cipher);
}

static Plugin* plugin_allready_loaded(Plugin* plugin, gchar** error) {
	Plugin* found = NULL;
	GSList *plugins, *cur;
	
	plugins = plugin_get_list();
	for (cur = plugins; cur; cur = g_slist_next(cur)) {
		Plugin* p = (Plugin *) cur->data;
		if (strcmp(p->id, plugin->id) == 0) {
			found = p;
			break;
		}
	}
	gslist_free(&plugins, NULL);

	if (! found) {
		plugins = plugin_get_unloaded_list();
		for (cur = plugins; cur; cur = g_slist_next(cur)) {
			Plugin* p = (Plugin *) cur->data;
			if (strcmp(p->id, plugin->id) == 0) {
				found = p;
				break;
			}
		}
		gslist_free(&plugins, NULL);
		
		if (found) {
			g_free(found->error);
			found->error = NULL;
			if (found->init(found, error)) {
				if (*error)
					found->error = g_strdup(*error);
				else
					found->error = g_strdup("User abort");
				return NULL;
			}
			unloaded_plugins = g_slist_remove(unloaded_plugins, found);
		}
	}
	
	return found;
}

void plugin_dialog(MainWindow* mainwindow) {
	PluginWindow* pluginwindow;
	GtkWidget* window;
	GtkWidget* vbox1;
	GtkWidget* bbox1;
	GtkWidget* close_btn;
	GtkWidget* hbox1;
	GtkWidget* scrolledwindow1;
	GtkWidget* scrolledwindow2;
	GtkWidget* bbox2;
	GtkWidget* load_btn;
	GtkWidget* unload_btn;
	GtkWidget* vbox2;
	GtkWidget* vbox3;
	GtkWidget* frame;
	GtkWidget* label;
	GtkWidget* plugin_list_view;
	GtkWidget* plugin_desc;
	
	pluginwindow = g_new0(PluginWindow, 1);
	
	window = g_object_new(
            GTK_TYPE_WINDOW,
            "type", GTK_WINDOW_TOPLEVEL,
            "title", _("Manage Plugins dialog"),
            "default-width", WIDTH,
            "default-height", HEIGHT,
            "window-position", GTK_WIN_POS_CENTER,
            "transient-for", GTK_WINDOW(mainwindow->window),
            "destroy-with-parent", TRUE,
            NULL);
    gtk_container_set_border_width(GTK_CONTAINER(window), 10);

    vbox1 = gtk_vbox_new(FALSE, 0);
    gtk_widget_show(vbox1);
    
    bbox1 = gtk_hbutton_box_new();
    gtk_widget_show(bbox1);
    gtk_button_box_set_layout(GTK_BUTTON_BOX(bbox1), GTK_BUTTONBOX_END);
    gtk_box_set_spacing(GTK_BOX(bbox1), 5);

    close_btn = gtk_button_new_from_stock(GTK_STOCK_CLOSE);
    gtk_widget_show(close_btn);
    gtk_widget_set_tooltip_text(close_btn, _("Close dialog"));
    gtk_container_add(GTK_CONTAINER(bbox1), close_btn);

    hbox1 = gtk_hbox_new(FALSE, 0);
    gtk_widget_show(hbox1);
    vbox2 = gtk_vbox_new(FALSE, 0);
    gtk_widget_show(vbox2);
    gtk_container_set_border_width(GTK_CONTAINER(vbox2), 5);
	
    vbox3 = gtk_vbox_new(FALSE, 0);
    gtk_widget_show(vbox3);
    gtk_container_set_border_width(GTK_CONTAINER(vbox3), 5);

	frame = gtk_frame_new(NULL);
	gtk_widget_show(frame);
	label = gtk_label_new(_("Plugin description"));
	gtk_widget_show(label);
	gtk_container_add(GTK_CONTAINER(frame), label);
	gtk_box_pack_start(GTK_BOX(vbox3), frame, FALSE, TRUE, 0);
	
	scrolledwindow1 = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_show(scrolledwindow1);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(scrolledwindow1),
					GTK_SHADOW_ETCHED_IN);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW
				       (scrolledwindow1), GTK_POLICY_NEVER,
				       GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start(GTK_BOX(vbox3), scrolledwindow1, TRUE, TRUE, 0);

	plugin_desc = gtk_text_view_new();
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(plugin_desc), FALSE);
	gtk_widget_show(plugin_desc);
	gtk_container_add(GTK_CONTAINER(scrolledwindow1), plugin_desc);

	scrolledwindow2 = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_show(scrolledwindow2);
    gtk_container_set_border_width(GTK_CONTAINER(scrolledwindow2), 5);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(scrolledwindow2),
					GTK_SHADOW_ETCHED_IN);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW
				       (scrolledwindow2), GTK_POLICY_NEVER,
				       GTK_POLICY_AUTOMATIC);
    
	plugin_list_view = pluginwindow_list_view_create(pluginwindow);
	gtk_widget_show(plugin_list_view);
	gtk_container_add(GTK_CONTAINER(scrolledwindow2), plugin_list_view);
	gtk_widget_grab_focus(GTK_WIDGET(plugin_list_view));

    bbox2 = gtk_hbutton_box_new();
    gtk_widget_show(bbox2);
    gtk_button_box_set_layout(GTK_BUTTON_BOX(bbox2), GTK_BUTTONBOX_END);
    gtk_box_set_spacing(GTK_BOX(bbox2), 5);

    load_btn = gtk_button_new_with_mnemonic(_("_Load"));
    gtk_widget_show(load_btn);
    gtk_widget_set_tooltip_text(load_btn, _("Load a new resource"));
    gtk_container_add(GTK_CONTAINER(bbox2), load_btn);

    unload_btn = gtk_button_new_with_mnemonic(_("_Unload"));
    gtk_widget_show(unload_btn);
    gtk_widget_set_tooltip_text(unload_btn, _("Unload a resource"));
    gtk_widget_set_sensitive(GTK_WIDGET(unload_btn), FALSE);
    gtk_container_add(GTK_CONTAINER(bbox2), unload_btn);

	gtk_box_pack_start(GTK_BOX(vbox2), scrolledwindow2, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(vbox2), bbox2, FALSE, TRUE, 0);
	
	gtk_box_pack_start(GTK_BOX(hbox1), vbox2, FALSE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(hbox1), vbox3, TRUE, TRUE, 0);	
    
    gtk_box_pack_start(GTK_BOX(vbox1), hbox1, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(vbox1), bbox1, FALSE, FALSE, 0);
    gtk_container_add(GTK_CONTAINER(window), vbox1);
    
    gtk_widget_grab_focus(close_btn);
    
    g_signal_connect(close_btn, "clicked", G_CALLBACK(close_cb), pluginwindow);
    g_signal_connect(unload_btn, "clicked", G_CALLBACK(unload_plugin_cb), pluginwindow);
    g_signal_connect(load_btn, "clicked", G_CALLBACK(load_plugin_cb), pluginwindow);
    g_signal_connect(
    	window, "delete_event", G_CALLBACK(pluginwindow_delete_cb), pluginwindow);        

    gtk_widget_show(window);

	pluginwindow->window = window;
	pluginwindow->plugin_desc = plugin_desc;
	pluginwindow->plugin_list_view = plugin_list_view;
	pluginwindow->unload_btn = unload_btn;
	pluginwindow->main = mainwindow;

	if (plugins || unloaded_plugins)
		set_plugin_list(pluginwindow);
}

Plugin* plugin_load(const gchar* filename, gchar** error) {
	Plugin* plugin;
	gpointer plugin_init, plugin_reset, plugin_name, plugin_provides,
			 plugin_desc, plugin_version, plugin_license, plugin_attrib_list,
			 get_contact, set_contact, delete_contact, search_contact, update_contact,
			 plugin_abook_open, plugin_abook_close, plugin_abook_delete,
			 plugin_addrbook_all_get, plugin_abook_set_config, plugin_type,
			 plugin_file_filter, plugin_url, plugin_attribs_set, plugin_commit_all,
			 plugin_remaining_attribs, plugin_inactive_attribs, plugin_addrbook_names_all,
			 plugin_closed_books_get, plugin_need_credentials, plugin_extra_config;

	cm_return_val_if_fail(filename != NULL, NULL);
	
	plugin = g_new0(Plugin, 1);
	if (plugin == NULL) {
		*error = g_strdup(_("Failed to allocate memory for Plugin"));
		return NULL;
	}
	
	debug_print("trying to load `%s'\n", filename);
	plugin->module = g_module_open(filename, 0);
	if (plugin->module == NULL) {
		*error = g_strdup(g_module_error());
		g_free(plugin);
		return NULL;
	}

	if (!g_module_symbol(plugin->module, "plugin_name", &plugin_name) ||
		!g_module_symbol(plugin->module, "plugin_desc", &plugin_desc) ||
		!g_module_symbol(plugin->module, "plugin_default_url", &plugin_url) ||
		!g_module_symbol(plugin->module, "plugin_type", &plugin_type) ||
		!g_module_symbol(plugin->module, "plugin_file_filter", &plugin_file_filter) ||
		!g_module_symbol(plugin->module, "plugin_version", &plugin_version) ||
		!g_module_symbol(plugin->module, "plugin_provides", &plugin_provides) ||
		!g_module_symbol(plugin->module, "plugin_license", &plugin_license) ||
		!g_module_symbol(plugin->module, "plugin_attrib_list", &plugin_attrib_list) ||
		!g_module_symbol(plugin->module, "plugin_attribs_set", &plugin_attribs_set) ||
		!g_module_symbol(plugin->module, "plugin_addrbook_all_get", &plugin_addrbook_all_get) ||
		!g_module_symbol(plugin->module, "plugin_addrbook_names_all", &plugin_addrbook_names_all) ||
		!g_module_symbol(plugin->module, "plugin_reset", &plugin_reset) ||
		!g_module_symbol(plugin->module, "plugin_abook_open", &plugin_abook_open) ||
		!g_module_symbol(plugin->module, "plugin_abook_close", &plugin_abook_close) ||
		!g_module_symbol(plugin->module, "plugin_abook_delete", &plugin_abook_delete) ||
		!g_module_symbol(plugin->module, "plugin_abook_set_config", &plugin_abook_set_config) ||
		!g_module_symbol(plugin->module, "plugin_commit_all", &plugin_commit_all) ||
		!g_module_symbol(plugin->module, "plugin_init", &plugin_init) ||
		!g_module_symbol(plugin->module, "plugin_need_credentials", &plugin_need_credentials) ||
		!g_module_symbol(plugin->module, "plugin_extra_config", &plugin_extra_config) ||
		!g_module_symbol(plugin->module, "plugin_closed_books_get", &plugin_closed_books_get) ||
		!g_module_symbol(plugin->module, "plugin_remaining_attribs", &plugin_remaining_attribs) ||
		!g_module_symbol(plugin->module, "plugin_inactive_attribs", &plugin_inactive_attribs)) {
		*error = g_strdup(g_module_error());
		g_module_close(plugin->module);
		g_free(plugin);
		return NULL;
	}

	plugin->provides = plugin_provides;
	if ((*error = plugin_check_features(plugin)) != NULL) {
		g_module_close(plugin->module);
		g_free(plugin);
		debug_print("%s\n", *error);
		g_free(*error);
		return NULL;
	}
	
	/* check addressbook functionality */
	if (!g_module_symbol(plugin->module, "plugin_get_contact", &get_contact)) {
		*error = g_strdup(g_module_error());
		g_module_close(plugin->module);
		g_free(plugin);
		return NULL;
	}
	if (! plugin->readonly) {
		if (!g_module_symbol(plugin->module, "plugin_set_contact", &set_contact)) {
			*error = g_strdup(g_module_error());
			g_module_close(plugin->module);
			g_free(plugin);
			return NULL;
		}
		if (!g_module_symbol(plugin->module, "plugin_delete_contact", &delete_contact)) {
			*error = g_strdup(g_module_error());
			g_module_close(plugin->module);
			g_free(plugin);
			return NULL;
		}
		if (!g_module_symbol(plugin->module, "plugin_update_contact", &update_contact)) {
			*error = g_strdup(g_module_error());
			g_module_close(plugin->module);
			g_free(plugin);
			return NULL;
		}
	}
	
	if (plugin->can_adv_search) {
		if (!g_module_symbol(plugin->module, "plugin_search_contact", &search_contact)) {
			*error = g_strdup(g_module_error());
			g_module_close(plugin->module);
			g_free(plugin);
			return NULL;
		}
	}
	
	plugin->init = plugin_init;
	plugin->file_filter = plugin_file_filter;
	plugin->reset = plugin_reset;
	plugin->name = plugin_name;
	plugin->desc = plugin_desc;
	plugin->version = plugin_version;
	plugin->type = plugin_type;
	plugin->license = plugin_license;
	plugin->filename = g_strdup(filename);
	plugin->need_credentials = plugin_need_credentials;
	plugin->extra_config = plugin_extra_config;
	plugin->error = NULL;
	plugin->abook_open = plugin_abook_open;
	plugin->abook_close = plugin_abook_close;
	plugin->abook_delete = plugin_abook_delete;
	plugin->attrib_list = plugin_attrib_list;
	plugin->attribs_set = plugin_attribs_set;
	plugin->addrbook_all_get = plugin_addrbook_all_get;
	plugin->addrbook_names_all = plugin_addrbook_names_all;
	plugin->get_contact = get_contact;
	plugin->set_contact = set_contact;
	plugin->delete_contact = delete_contact;
	plugin->update_contact = update_contact;
	plugin->search_contact = search_contact;
	plugin->abook_set_config = plugin_abook_set_config;
	plugin->default_url = plugin_url;
	plugin->abooks_commit_all = plugin_commit_all;
	plugin->remaining_attribs = plugin_remaining_attribs;
	plugin->inactive_attribs = plugin_inactive_attribs;
	plugin->closed_books_get = plugin_closed_books_get;
	
	compute_hash(plugin);

	Plugin* loaded = plugin_allready_loaded(plugin, error);
	if (*error || loaded) {
		g_module_close(plugin->module);
		g_free(plugin->id);
		g_free(plugin->filename);
		g_free(plugin);
		if (*error)
			return NULL;
		else {
			*error = g_strdup_printf(_("Plugin: '%s' from file (%s) allready loaded\n"),
					loaded->name(), filename);
			debug_print(*error);
			return loaded;
		}
	}
	else {
		if (plugin->init(plugin, error)) {
			if (*error)
				plugin->error = g_strdup(*error);
			else
				plugin->error = g_strdup("User abort");
			unloaded_plugins = g_slist_append(unloaded_plugins, plugin);
			return NULL;
		}
	}
	
	//addrbook_get_name(plugin, error);
	if (*error || plugin->error) {
		if (*error)
			plugin->error = g_strdup(*error);
		unloaded_plugins = g_slist_append(unloaded_plugins, plugin);
		return NULL;
	}

	plugins = g_slist_append(plugins, plugin);
	
	debug_print("Plugin %s (from file %s) loaded\n", plugin->name(), filename);

	return plugin;
}

void plugin_unload(Plugin* plugin) {
	gboolean (*plugin_done) (void);
	gboolean can_unload = TRUE;

	cm_return_if_fail(plugin != NULL);
	
	debug_print("trying to unload `%s'\n", plugin_get_name(plugin));
	if (plugin->error) {
		unloaded_plugins = g_slist_remove(unloaded_plugins, plugin);
		plugin_free(plugin);
	}
	else {
		if (g_module_symbol(plugin->module, "plugin_done", (gpointer) &plugin_done)) {
			can_unload = plugin_done();
		}
	
		if (can_unload) {
			plugins = g_slist_remove(plugins, plugin);
			plugin_free(plugin);
		}
		else {
			g_warning("Unloading '%s' failed!", plugin_get_name(plugin));
			if (plugin->error)
				g_warning("Error message: %s", plugin->error);
			/* FIXME. Should we forcefully unload a plugin which have
			 * failed to be unloaded?*/
			/*plugins = g_slist_remove(plugins, plugin);
			plugin_free(plugin);*/
		}
	}
}

static GSList* get_list_head(GSList* list) {
	GSList *new = NULL;
	GSList *cur = list;
	for (; cur; cur = cur->next) {
		Plugin *p = (Plugin *)cur->data;
		new = g_slist_prepend(new, p);
	}
	new = g_slist_reverse(new);
	return new;
}

void plugin_load_all(GtkWidget* parent, GSList* list) {
	gchar* error = NULL;
	GSList* cur;
	
	for (cur = list; cur; cur = g_slist_next(cur)) {
		gchar* file = (gchar *) cur->data;
		debug_print("Loading %s\n", file);
		plugin_load(file, &error);
		if (error && parent) {
			show_message(parent, GTK_UTIL_MESSAGE_WARNING, "%s", error);
		}
		if (error)
			g_free(error);
	}
}

void plugin_unload_all() {
	GSList *cur, *loaded, *unloaded;

	loaded = get_list_head(plugins);
	for(cur = loaded; cur != NULL; cur = g_slist_next(cur)) {
		Plugin *plugin = (Plugin *) cur->data;
		plugin_unload(plugin);
	}

	unloaded = get_list_head(unloaded_plugins);
	for(cur = unloaded; cur != NULL; cur = g_slist_next(cur)) {
		Plugin *plugin = (Plugin *) cur->data;
		plugin_unload(plugin);
	}
		
	gslist_free(&plugins, NULL);
	gslist_free(&unloaded_plugins, NULL);
	gslist_free(&loaded, NULL);
	gslist_free(&unloaded, NULL);
}

GSList* plugin_get_name_all() {
	GSList* names = NULL;
	GSList *cur, *loaded;

	loaded = plugin_get_list();
	for(cur = loaded; cur != NULL; cur = g_slist_next(cur)) {
		Plugin *plugin = (Plugin *) cur->data;
		names = g_slist_prepend(names, g_strdup(plugin->name()));
	}
	gslist_free(&loaded, NULL);

	return names;
}

GSList* plugin_get_path_all() {
	GSList* names = NULL;
	GSList *cur, *loaded;

	loaded = plugin_get_list();
	for(cur = loaded; cur != NULL; cur = g_slist_next(cur)) {
		Plugin *plugin = (Plugin *) cur->data;
		names = g_slist_prepend(names, g_strdup(plugin->filename));
	}
	gslist_free(&loaded, NULL);

	return names;
}

GSList* address_books_get() {
	GSList *loaded, *cur;
	GSList *addr_books = NULL;
	
	loaded = plugin_get_list();
	
	for (cur = loaded; cur; cur = g_slist_next(cur)) {
		Reference* ref = g_new0(Reference, 1);
		ref->plugin = (Plugin *) cur->data;
		ref->abooks = ref->plugin->addrbook_all_get();
		addr_books = g_slist_prepend(addr_books, ref);
	}
	gslist_free(&loaded, NULL);
	
	return addr_books;
}

Plugin* plugin_get_plugin(const gchar* name) {
	GSList *loaded, *cur;
	Plugin* plugin = NULL;

	cm_return_val_if_fail(name != NULL, NULL);

	loaded = plugin_get_list();
	
	for (cur = loaded; cur; cur = g_slist_next(cur)) {
		plugin = (Plugin *) cur->data;
		if (strcmp(plugin->name(), name) == 0)
			break;
		plugin = NULL;
	}
	gslist_free(&loaded, NULL);
	
	return plugin;
}

Reference* address_book_reference_get(const gchar* name) {
	GSList *cur1, *cur2, *books;
	gboolean found_book = FALSE;
	Reference* ref = NULL;

	cm_return_val_if_fail(name != NULL, NULL);
	
	books = address_books_get();
	
	for (cur1 = books; cur1 && !found_book; cur1 = g_slist_next(cur1)) {
		ref = (Reference *) cur1->data;
		for (cur2 = ref->abooks; cur2 && !found_book; cur2 = g_slist_next(cur2)) {
			AddressBook* ab = (AddressBook *) cur2->data;
			if (strcmp(ab->abook_name, name) == 0)
				found_book = TRUE;
		}
	}
	gslist_free(&books, NULL);
	
	return (found_book) ? ref : NULL;
}
