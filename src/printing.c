/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include <cairo.h>
#include <math.h>

#include "printing.h"
#include "utils.h"
#include "gtk-utils.h"

typedef struct {
  GSList*	contacts;
  gdouble	font_size;
  gint		lines_per_page;
  gchar**	lines;
  gchar*	abook_name;
  gint		total_lines;
  gint		total_pages;
  gdouble	header_height;
  gdouble	header_gap;
} PrintData;

static GtkPageSetup* page_setup = NULL;
static GtkPrintSettings* settings = NULL;

static void contact_get_data(gpointer key, gpointer value, gpointer data) {
	PrintData* pd = (PrintData *) data;
	gchar *k = NULL, *v = NULL;
	void* val = NULL;
	gboolean bool;
	gchar ch;
	gint i;
	
	k = (gchar *) key;
	AttribType type = get_data((AttribDef *) value, &val);
	switch (type) {
		case ATTRIB_TYPE_BOOLEAN:
			bool = *(gboolean *) val;
			v = (bool) ? g_strdup("true") : g_strdup("false");
			break;
		case ATTRIB_TYPE_CHAR:
			ch = *(gchar *) val;
			v = g_strdup_printf("%c", ch);
			break;
		case ATTRIB_TYPE_INT:
			i = *(gint *) val;
			v = g_strdup_printf("%i", i);
			break;
		case ATTRIB_TYPE_STRING:
			v = g_strdup((gchar *) val);
			break;
	}
	g_free(val);
	
	if (strcmp(k, "uid") != 0 && strcmp(k, "image") != 0 &&
			strcmp(k, "dn") != 0 &&	strlen(k) > 0 && strlen(v) > 0) {
		pd->lines[pd->total_lines++] = g_strdup_printf("%s: %s", k, v);
		pd->lines = g_renew(gchar *, pd->lines, pd->total_lines + 1);
		pd->lines[pd->total_lines] = NULL;
	}
	g_free(v);
}

static void email_get_data(GSList* emails, PrintData* pd) {
	GSList* cur;
	Email* email;

	for (cur = emails; cur; cur = g_slist_next(cur)) {
		pd->lines[pd->total_lines++] = g_strdup("");
		pd->lines = g_renew(gchar *, pd->lines, pd->total_lines + 1);
		email = (Email *) cur->data;
		pd->lines[pd->total_lines++] =
			g_strdup_printf("Email: %s Alias: %s", 
				email->email, email->alias);
		pd->lines = g_renew(gchar *, pd->lines, pd->total_lines + 1);
		pd->lines[pd->total_lines++] =
			g_strdup_printf("Remarks: %s", email->remarks);
		pd->lines = g_renew(gchar *, pd->lines, pd->total_lines + 1);
		pd->lines[pd->total_lines] = NULL;
	}
}

static void end_print(GtkPrintOperation *operation,
					  GtkPrintContext   *context,
					  gpointer           data) {
	PrintData* pd = (PrintData *) data;

	g_strfreev(pd->lines);
	gslist_free(&pd->contacts, NULL);
	g_free(pd);
}

static void begin_print(GtkPrintOperation *operation,
						GtkPrintContext   *context,
						gpointer           data) {
	PrintData* pd = (PrintData *) data;
	GSList* cur;
	Contact* contact;
	gdouble height;

	pd->lines = g_new0(gchar *, 1);
	pd->total_lines = 0;

	for (cur = pd->contacts; cur; cur = g_slist_next(cur)) {
		pd->lines[pd->total_lines++] = g_strdup("");
		pd->lines = g_renew(gchar *, pd->lines, pd->total_lines + 1);
		pd->lines[pd->total_lines] = NULL;
		contact = (Contact *) cur->data;
		g_hash_table_foreach(contact->data, contact_get_data, pd);
		email_get_data(contact->emails, pd);
		pd->lines[pd->total_lines++] = 
			g_strdup("------------------------------------------------------");
		pd->lines = g_renew(gchar *, pd->lines, pd->total_lines + 1);
		pd->lines[pd->total_lines] = NULL;
	}

	height = gtk_print_context_get_height(context) - pd->header_height - pd->header_gap;
	pd->lines_per_page = floor(height / (pd->font_size + 3));
	pd->total_pages = (pd->total_lines - 1) / pd->lines_per_page + 1;
	gtk_print_operation_set_n_pages(operation, pd->total_pages);
}

static void draw_page (GtkPrintOperation *operation,
       				   GtkPrintContext   *context,
       				   int                page_nr,
       				   gpointer			  data) {
	cairo_t *cr;
	PangoLayout *layout;
	gdouble width, text_height;
	gint line, i, text_width, layout_height;
	PangoFontDescription *desc;
	gchar *page_str;
	PrintData* pd = (PrintData *) data;

	cr = gtk_print_context_get_cairo_context(context);
	width = gtk_print_context_get_width(context);
	layout = gtk_print_context_create_pango_layout(context);
	desc = pango_font_description_from_string("Monospace");
	pango_font_description_set_size(desc, pd->font_size * PANGO_SCALE);
	/* Render the page header name and page number. */
	pango_layout_set_font_description(layout, desc);
	page_str = g_strdup_printf("Address Book: %s", pd->abook_name);
	pango_layout_set_text(layout, page_str, -1);
	g_free(page_str);
	pango_layout_set_width (layout, -1);
	pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);
	pango_layout_get_size(layout, NULL, &layout_height);
	text_height = (gdouble) layout_height / PANGO_SCALE;
	cairo_move_to(cr, 0, (pd->header_height - text_height) / 2);
	pango_cairo_show_layout(cr, layout);
	page_str = g_strdup_printf("%d of %d", page_nr + 1, pd->total_pages);
	pango_layout_set_text(layout, page_str, -1);
	pango_layout_get_size(layout, &text_width, NULL);
	pango_layout_set_alignment(layout, PANGO_ALIGN_RIGHT);
	cairo_move_to(cr, width - (text_width / PANGO_SCALE),
	             (pd->header_height - text_height) / 2);
	pango_cairo_show_layout(cr, layout);

	/* Render the page text with the specified font and size. */
	cairo_move_to(cr, 0, pd->header_height + pd->header_gap);
	line = page_nr * pd->lines_per_page;
	for (i = 0; i < pd->lines_per_page && line < pd->total_lines; i++) {
		pango_layout_set_text(layout, pd->lines[line], -1);
		pango_cairo_show_layout(cr, layout);
		cairo_rel_move_to(cr, 0, pd->font_size + 3);
		line++;
	}
	g_free(page_str);
	g_object_unref(layout);
	pango_font_description_free(desc);
}

static gboolean walk_list(GtkTreeModel *model, GtkTreePath *path,
						  GtkTreeIter *iter, gpointer data) {
	gboolean stop = FALSE;
	Contact* contact = NULL;
	PrintData* print_data = (PrintData *) data;
	
	gtk_tree_model_get(model, iter, CONTACT_DATA_COLUMN, &contact, -1);
	if (contact) {
		print_data->contacts = g_slist_prepend(print_data->contacts, contact);
	}
	
	return stop;
}

void show_print_dialog(PrintInfo* info) {
    GtkTreeModel* model;
    GtkTreeView* view;
    PrintData* print_data;
    GtkPrintOperation* print;
    GError* error = NULL;
    GList* cur;

	cm_return_if_fail(info != NULL);

	print_data = g_new0(PrintData, 1);
	if (! info->contact) {
		if (info->print_book) {
			for (cur = info->abook->contacts; cur; cur = g_list_next(cur)) {
				Contact* c = (Contact *) cur->data;
				print_data->contacts = g_slist_prepend(print_data->contacts, c);
			}
		}
		else {
			view = GTK_TREE_VIEW(info->win->contact_list);
			model = gtk_tree_view_get_model(view);
			gtk_tree_model_foreach(model, walk_list, print_data);
		}
	}
	else
		print_data->contacts = 
			g_slist_prepend(print_data->contacts, info->contact);

	print_data->font_size = 12.0;
	print_data->header_height = 20.0;
	print_data->header_gap = 8.5;
	print_data->abook_name = info->abook->abook_name;
		
	print = gtk_print_operation_new();

	if (settings != NULL)
		gtk_print_operation_set_print_settings(print, settings);
		
	if (page_setup != NULL)
		gtk_print_operation_set_default_page_setup(print, page_setup);
		
	g_signal_connect (print, "begin-print", G_CALLBACK(begin_print), print_data);
	g_signal_connect (print, "draw-page", G_CALLBACK(draw_page), print_data);
	g_signal_connect (print, "end-print", G_CALLBACK(end_print), print_data);
	
	gint res = gtk_print_operation_run (print, 
								   GTK_PRINT_OPERATION_ACTION_PRINT_DIALOG,
								   GTK_WINDOW(info->win->window), &error);

	if (res == GTK_PRINT_OPERATION_RESULT_ERROR) {
		show_message(info->win->window, GTK_UTIL_MESSAGE_WARNING,
			_("Error printing file:\n%s"), error->message);
   		g_clear_error(&error);
	}
	else if (res == GTK_PRINT_OPERATION_RESULT_APPLY) {
		if (settings != NULL)
			g_object_unref(settings);
		settings = g_object_ref(gtk_print_operation_get_print_settings(print));
	}	
}

void do_page_setup(MainWindow* win) {
	GtkPageSetup* new_page_setup;
	
	if (settings == NULL)
		settings = gtk_print_settings_new();
	
	new_page_setup = gtk_print_run_page_setup_dialog(
			GTK_WINDOW(win->window), page_setup, settings);
	
	if (page_setup)
		g_object_unref(page_setup);
	
	page_setup = new_page_setup;
}