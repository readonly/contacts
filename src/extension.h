/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __EXTENSION_H__
#define __EXTENSION_H__

#include <glib.h>

G_BEGIN_DECLS

#include "mainwindow.h"
#include "plugin.h"

/* Available hooks */
typedef enum {
	/* After main window but before plugins */
	EXTENSION_BEFORE_INIT_HOOK,
	/* After plugins */
	EXTENSION_AFTER_INIT_HOOK,
	EXTENSION_BEFORE_EDIT_ABOOK_HOOK,
	EXTENSION_AFTER_EDIT_ABOOK_HOOK,
	EXTENSION_BEFORE_OPEN_ABOOK_HOOK,
	EXTENSION_AFTER_OPEN_ABOOK_HOOK,
	EXTENSION_BEFORE_CLOSE_ABOOK_HOOK,
	EXTENSION_AFTER_CLOSE_ABOOK_HOOK,
	EXTENSION_BEFORE_DELETE_ABOOK_HOOK,
	EXTENSION_AFTER_DELETE_ABOOK_HOOK,
	/* Object will be searched address book (basic_search) */
	EXTENSION_BEFORE_SEARCH_ABOOK_HOOK,
	/* Object will be a GSList of found contact(s) */
	EXTENSION_AFTER_SEARCH_ABOOK_HOOK,
	EXTENSION_BEFORE_ADD_ABOOK_HOOK,
	EXTENSION_AFTER_ADD_ABOOK_HOOK,
	/* Object will be NULL */
	EXTENSION_BEFORE_INIT_ABOOK_HOOK,
	EXTENSION_AFTER_INIT_ABOOK_HOOK,
	EXTENSION_BEFORE_OPEN_CONTACT_HOOK,
	EXTENSION_AFTER_OPEN_CONTACT_HOOK,
	EXTENSION_BEFORE_CLOSE_CONTACT_HOOK,
	EXTENSION_AFTER_CLOSE_CONTACT_HOOK,
	EXTENSION_BEFORE_DELETE_CONTACT_HOOK,
	EXTENSION_AFTER_DELETE_CONTACT_HOOK,
	/* Object will be a Contact containing search data */
	EXTENSION_BEFORE_SEARCH_CONTACT_HOOK,
	/* Object will be a GSList of found contact(s) */
	EXTENSION_AFTER_SEARCH_CONTACT_HOOK,
	EXTENSION_BEFORE_ADD_CONTACT_HOOK,
	EXTENSION_AFTER_ADD_CONTACT_HOOK,
	/* Object will be NULL */
	EXTENSION_BEFORE_INIT_CONTACT_HOOK,
	EXTENSION_AFTER_INIT_CONTACT_HOOK,
	EXTENSION_HOOK_N
} ExtensionHook;

typedef enum {
	CONTACTS_NONE,
	CONTACTS_MAIN_MENU,
	CONTACTS_CONTACT_MENU,
	CONTACTS_ADDRESSBOOK_MENU
} ContactsMenu;

typedef struct {
	ContactsMenu		menu;
	gboolean			submenu;
	const gchar*		parent;
	const gchar*		sublabel;
	const MainWindow*	mainwindow;
} MenuItem;

typedef void (*HOOKFUNC) (const MainWindow* mainwindow, gpointer object);

/* Any extension must implement these functions */

/**
 * The main application will call this function after loading the
 * extension providing a uniq id for the extension which is to be
 * used for further references
 * @param id uniq id provided by main application
 * @return 0 if success 1 otherwise
 */
gint extension_init(guint id);

/**
 * Called by main application when the extension should be unloaded
 * @return TRUE if success FALSE otherwise
 */
gboolean extension_done(void);

/**
 * Called by main application to ensure extension license is compatible
 * @return license
 */
const gchar* extension_license(void);

/**
 * Called by main application to get name of extension
 * @return name
 */
const gchar* extension_name(void);

/**
 * Called by main application to get extension's describtion
 * @return description
 */
const gchar* extension_describtion(void);

/* Functions provided by claws-contacts */

/**
 * Extensions should call this function to register a callback hook
 * @param id uniq id identifying the extension provided by the
 * main application when extension_init was called
 * @param hook_type what kind of hook to subscribe
 * @param callback a pointer to the callback function
 * @param error a place holder for error messages
 * @return 0 if success 1 otherwise
 */
gint register_hook_function(guint id,
						    ExtensionHook hook_type,
						    HOOKFUNC callback,
						    gchar** error);

/**
 * Extensions should call this function to unregister a previous
 * registred callback hook
 * @param id uniq id identifying the extension provided by the
 * main application when extension_init was called
 * @param hook_type what kind of hook to unsubscribe
 * @param error a place holder for error messages
 */
void unregister_hook_function(guint id,
						      ExtensionHook hook_type,
						      gchar** error);

/**
 * If an extension wants to add an new MenuItem to the application menu
 * it can call this function. The predefined top-level menus 'File',
 * 'Tools' and 'Help' is accessable to extensions. If parent is not
 * found a new menu will be craeted with the name 'parent'
 * @param id uniq id identifying the extension provided by the
 * main application when extension_init was called
 * @param image_menu GtkImageMenuItem
 * @param menu_item 'Menu' to attach to. Do not free!
 * @return TRUE if error FALSE otherwise
 */
/*
gboolean add_menu_item(guint id, GtkImageMenuItem* image_menu, MenuItem* menu_item);
*/
MenuItem* menu_item_new(void);

gboolean add_menu_item(GtkImageMenuItem* image_menu, MenuItem* menu_item);

G_END_DECLS

#endif
