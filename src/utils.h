/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __UTILS_H__
#define __UTILS_H__

#include <glib.h>

G_BEGIN_DECLS

#include "plugin.h"
#include "plugin-loader.h"
#include "dbus-contact.h"

#ifdef HAVE_FUNCTION_DEFINED
    #define ___FUNC___ __FUNCTION__
#elif defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
    #define ___FUNC___ __func__
#else
    #define ___FUNC___ (const char *) "Missing function"
#endif

/* stolen from claws-mail:-) */
/* from NetworkManager */
#if (defined(HAVE_BACKTRACE) && !defined(__FreeBSD__))
#include <execinfo.h>
#include <stdlib.h>
#define print_backtrace()                                               \
G_STMT_START                                                            \
{                                                                       \
        void *_call_stack[512];                                         \
        int  _call_stack_size;                                          \
        char **_symbols;                                                \
        _call_stack_size = backtrace (_call_stack,                      \
                                      G_N_ELEMENTS (_call_stack));      \
        _symbols = backtrace_symbols (_call_stack, _call_stack_size);   \
        if (_symbols != NULL)                                           \
        {                                                               \
                int _i;                                                 \
                _i = 0;                                                 \
                g_print ("traceback:\n");                               \
                while (_i < _call_stack_size)                           \
                {                                                       \
                        g_print ("%d:\t%s\n", _i, _symbols[_i]);        \
                        _i++;                                           \
                }                                                       \
                free (_symbols);                                        \
        }                                                               \
}                                                                       \
G_STMT_END
#else
#define print_backtrace()                                               \
G_STMT_START                                                            \
{                                                                       \
}                                                                       \
G_STMT_END
#endif

#define cm_return_val_if_fail(expr,val) G_STMT_START {                  \
        if (!(expr)) {                                                  \
                g_print("%s:%d Condition %s failed\n", __FILE__, __LINE__, #expr);\
                print_backtrace();                                      \
                g_print("\n");                                          \
                return val;                                             \
        }                                                               \
} G_STMT_END

#define cm_return_if_fail(expr) G_STMT_START {                          \
        if (!(expr)) {                                                  \
                g_print("%s:%d Condition %s failed\n", __FILE__, __LINE__, #expr);\
                print_backtrace();                                      \
                g_print("\n");                                          \
                return;                                                 \
        }                                                               \
} G_STMT_END

#define debug_print \
	debug_print_real("%s:%d:[%s]: ", debug_srcname(__FILE__), __LINE__, ___FUNC___), \
	debug_print_real

typedef gpointer (*COPYFUNC) (gpointer);
typedef struct {
	enum { LIST, ATTRIBUTE} type;
	union {
		GSList* list;
		gchar*  attrib_name;
	} attr;
} AttrList;

typedef struct {
	GHashTable* hash;
	gboolean	is_and;
	gint		equal;
} Compare;

typedef enum {
	ATTRIBUTE_DELETE,
	ATTRIBUTE_MODIFY,
	ATTRIBUTE_ADD
} AttributeAction;

typedef enum {
	CONTACT_CHANGE_ATTRIB,
	CONTACT_CHANGE_EMAIL
} ContactChangeType;

typedef struct {
	AttributeAction		action;
	ContactChangeType	type;
	gchar*				key;
	gpointer			value;
} ContactChange;
	
void debug_print_real(const gchar *format, ...);
const char* debug_srcname(const char *file);
gboolean debug_get_mode();
void debug_set_mode(gboolean mode);
const gchar *conv_get_current_locale(void);
const gchar* get_home();
gchar* get_self_home();
gboolean create_file(const gchar* path, gchar** error);
gboolean first_time(const gchar* subpath, const gchar* config, gchar** error);
void close_log_file();

void swap_data(GHashTable* data, const gchar* key, void* value);
AttribDef* pack_data(AttribType type, const gchar* name, const void* value);
void extract_data(GHashTable* data, const gchar* key, void** value);
AttribType get_data(AttribDef* attrib_def, void** value);

void attrib_def_free(gpointer attrdef);
gpointer attrib_def_copy(gpointer attrdef);
GSList* gslist_deep_copy(GSList* list, COPYFUNC copy_func);
void gslist_free(GSList** list, void (*elem_free) (gpointer data));
void glist_free(GList** list, void (*elem_free) (gpointer data));
GHashTable* hash_table_new(void);
GHashTable * hash_table_copy(GHashTable* hash);
void hash_table_dump(GHashTable* hash, FILE* f);
void hash_table_free(GHashTable** hash_table);
void hash_table_keys_to_slist(gpointer key, gpointer value, gpointer slist);

void plugin_config_set(ConfigFile* config, gchar** error);
void plugin_config_free(ConfigFile** config_file);
ConfigFile* plugin_config_new(const gchar* path);
void config_get(ConfigFile* config, gchar** error);
gboolean config_get_value(ConfigFile* config, const gchar* group,
						  const gchar* key, GSList** values);
gboolean config_set_value(ConfigFile* config, const gchar* group,
						  const gchar* key, GSList* values);

gchar** gslist_to_array(const GSList* list, gint* len);
Contact* contact_copy(Contact* contact);
Contact* contact_new();
void contact_data_print(gpointer key, gpointer value, gpointer data);
//gboolean contact_compare(Contact* a, Contact* b, gboolean is_and);
GSList* contact_diff(const Contact* a, const Contact* b);
gboolean contact_compare_attrib(Contact* a, Contact* b, const AttribDef* compare);
gboolean compare_attrib(const AttribDef* a, const AttribDef* b, gint type);
void contact_free(gpointer contact);
void contact_dump(const Contact* contact, FILE* f);
gboolean email_equal(Email* a, Email* b);
gint email_sort(gconstpointer a, gconstpointer b);
void email_free(gpointer email);
Email* email_copy(Email* email);
void email_dump(Email* email, FILE* f);
AddressBook* address_book_new();
AddressBook* address_book_get(Plugin* plugin, const gchar* name);
AddressBook* address_book_copy(AddressBook* a, gboolean deep);
gboolean address_book_name_exists(Plugin* plugin, AddressBook* book);
void address_book_contacts_free(AddressBook* address_book);
void address_book_free(AddressBook** address_book);
gint address_book_compare(gconstpointer a, gconstpointer b);
gboolean xor(const gchar* a, const gchar* b);
gchar* base64_encode_data(const gchar* path);
guint set_log_handler();
gint utf8_collate(gchar* s1, gchar* s2);
gboolean attribute_supported(Plugin* plugin, const gchar* attribute);
void gslist_remove_member(GSList* haystack, AttrList* neddle);
gint gslist_get_index(GSList* list, gconstpointer data, GCompareFunc comp);
gint gslist_compare_gchar(gconstpointer a, gconstpointer b);
GPtrArray* g_value_email_new();
void dbus_contact_print(DBusContact* contact, FILE* f);
gchar* aes256_encrypt(const gchar* plain, gboolean base64_enc);
gchar* aes256_decrypt(const gchar* cipher_text, gboolean base64_enc);
gchar* sha256(const gchar* plain);
void extra_config_free(gpointer data);
gpointer extra_config_copy(gpointer data);
ExtraConfig* get_extra_config(GSList* list, const gchar* name);
GSList* find_name(GtkContainer* container, const gchar* name);
GSList* get_basic_attributes(Contact* contact);
gboolean match_string_pattern(const gchar* pattern, const gchar* haystack, gboolean case_aware);
void contact_compare_values(gpointer key, gpointer value, gpointer data);
gboolean email_compare_values(GSList* a, GSList* b, gboolean is_and);
gchar* create_dummy_dn(const gchar* baseDN);
void contact_change_free(gpointer contact_change);
gchar* get_domain_name(void);
gchar* base64_pixbuf_to_jpeg(gchar* base64_png, gsize* length);
gchar* jpeg_to_png_base64(guchar* jpeg, gsize length);
GdkPixbuf* contact_load_image(GtkWidget* parent, gchar* photo);
const gchar* native2ldap(const gchar* attr);
const gchar* ldap2native(const gchar* attr);

G_END_DECLS


#endif
