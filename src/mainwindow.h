/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __MAINWINDOW_H__
#define __MAINWINDOW_H__

#include <glib.h>

G_BEGIN_DECLS

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#define INITIAL_WIDTH 640
#define INITIAL_HEIGHT 500

enum {
    PIXMAP_16,
    PIXMAP_32,
    PIXMAP_48,
    PIXMAP_64,
    PIXMAP_128,
    PIXMAP_NULL,
    PIXMAP_N
};

extern const gchar* PIXMAPS[PIXMAP_N];

typedef struct {
    GtkWidget*          window;
    GtkWidget*          left_scroll;
    GtkWidget*          right_scroll;
    GtkAccelGroup*      accel;
    GtkWidget*          menu;
    GtkWidget*          tools_menu;
    GtkWidget*          help_menu;
    GtkWidget*          file_menu;
    GtkWidget*          toolbar;
    GtkWidget*          abook_list;
    GtkWidget*          contact_list;
    GtkWidget*          action_box;
    GtkWidget*          to_btn;
    GtkWidget*          cc_btn;
    GtkWidget*          bcc_btn;
    GtkWidget*          new_btn;
    GtkWidget*          search_text;
    GtkWidget*          search_btn;
    GtkWidget*          clear_btn;
    GtkWidget*          adv_search_btn;
    GtkWidget*          statusbar;
    GtkWidget*          progress_dialog;
    GtkWidget*          progress_bar;
    gpointer            selected_abook;
    gpointer            selected_contact;
    guint               source_id;
    gchar*              default_book;
    gboolean            compose_mode;
    gboolean            use_extensions;
} MainWindow;

enum {
    BOOK_NAME_COLUMN,
    BOOK_DATA_COLUMN,
    BOOK_PLUGIN_COLUMN,
    BOOK_N_COLUMNS
};

typedef enum {
    CONTACT_DISPLAYNAME_COLUMN,
    CONTACT_FIRSTNAME_COLUMN,
    CONTACT_LASTNAME_COLUMN,
    CONTACT_EMAIL_COLUMN,
    CONTACT_DATA_COLUMN,
    CONTACT_N_COLUMNS
} ContactColumn;

typedef enum {
    STATUS_MSG_NO_BOOK,
    STATUS_MSG_CONNECTED,
    STATUS_MSG_SEARCHING
} STATUS;

void application_start(gboolean compose, gboolean avoid_extensions);
void set_status_msg(MainWindow* mainwindow, STATUS id, const gchar* extra);

G_END_DECLS

#endif
