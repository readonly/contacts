/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __GTK_UTILS_H__
#define __GTK_UTILS_H__

#include <glib.h>

G_BEGIN_DECLS

#include <gtk/gtk.h>
#include "plugin-loader.h"

typedef enum {
	GTK_UTIL_MESSAGE_INFO,
	GTK_UTIL_MESSAGE_WARNING,
	GTK_UTIL_MESSAGE_ERROR
} MessageType;

enum {
	ROW_TEXT_COLUMN,
	ROW_DATA_COLUMN,
	ROW_N_COLUMNS
};

typedef struct {
	Plugin* plugin;
	AddressBook* abook;
	gchar* name;
} RowData;

/*
struct RowContainer {
	GSList* rows;
};
*/


typedef struct {
	GSList*	existing_attribs;
	GSList*	inactive_attribs;
	GSList*	remaining_attribs;
} AttribContainer;


typedef void (*AddAttribCallback) (GtkWidget* widget, gpointer data);

void show_message(GtkWidget* parent, MessageType type, const gchar* format, ...);
gboolean show_question(GtkWidget* parent, const gchar* format, ...);
gboolean show_input(GtkWidget* parent, const gchar* title,
					gchar** reply, const gchar* format, ...);
GHashTable* get_attrib_list(GtkWidget* parent,
							AttribContainer* attrib_lists,
							/*Plugin* plugin,*/
							const gchar* info_text, gboolean can_delete,
							gchar** error, AddAttribCallback callback);
void add_widget_attrib_list(GtkWidget* vbox, GtkWidget* check_btn);
void add_attrib_btn_cb(GtkWidget* widget, gpointer data);
gboolean show_choice_list(GtkWidget* parent,
						  const gchar* title,
						  GSList* choices,
						  gchar** response);
GtkTreeIter* set_selection_combobox(GtkWidget* parent,
									const gchar* title,
					   				GtkTreeModel* model,
					   				guint column);
void row_data_free(gpointer row);
void dialog_set_focus(GtkDialog* dialog, const gchar* button_label);
GtkWidget* create_combo_box_text(GSList* items);
gchar* combo_box_text_get_active(GtkWidget* combobox);
AddressBook* get_selected_address_book(GtkTreeView* view);
void set_selected_address_book(GtkTreeView* view, AddressBook* book);
Plugin* get_selected_plugin(GtkTreeView* view);
Contact* get_selected_contact(GtkTreeView* view);
void list_view_clear(GtkTreeView* view, MainWindow* mainwindow);
void list_view_append_contact(GtkTreeView* view, Contact* contact);

G_END_DECLS

#endif
