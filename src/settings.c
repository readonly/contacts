/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "mainwindow.h"
#include "callbacks.h"
#include "utils.h"
#include "gtk-utils.h"
#include "settings.h"
#include "plugin.h"

static void set_default_abook(MainWindow* win,
		ConfigFile* config, GtkWidget* combobox) {
	GtkTreeIter* iter = NULL;
    GtkTreeModel* model;
    gchar* name;
    GSList* list = NULL;

	model = gtk_tree_view_get_model(GTK_TREE_VIEW(win->abook_list));
	
	iter = g_new0(GtkTreeIter, 1);
	gtk_combo_box_get_active_iter(GTK_COMBO_BOX(combobox), iter);
	gtk_tree_model_get(model, iter, BOOK_NAME_COLUMN, &name, -1);
	list = g_slist_append(list, g_strdup(name));
	
	if (win->default_book)
		g_free(win->default_book);
	win->default_book = g_strdup(name);
	
	g_free(name);
	config_set_value(config, "Settings", "default book", list);
	gslist_free(&list, g_free);
	update_abook_list(win);
}

void show_settings(MainWindow* win, ConfigFile* config) {
    GtkTreeModel* model;
	GtkCellRenderer* cell;
    GtkWidget *combobox, *dialog, *label, *frame, *hbox, *vbox;
	
	model = gtk_tree_view_get_model(GTK_TREE_VIEW(win->abook_list));

	combobox = gtk_combo_box_new_with_model(model);
    cell = gtk_cell_renderer_text_new();
	gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(combobox), cell, TRUE);
	gtk_cell_layout_set_attributes(
			GTK_CELL_LAYOUT(combobox), cell, "text", BOOK_NAME_COLUMN, NULL);
	gtk_combo_box_set_active(GTK_COMBO_BOX(combobox), 0);
	gtk_widget_set_tooltip_text(combobox, 
		_("Choose address book which is added new contacts by default"));

	dialog = gtk_dialog_new_with_buttons (
		"Preferences", GTK_WINDOW(win->window),
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
		GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT, NULL);
	
	vbox = gtk_vbox_new(FALSE, 0);
	
	frame = gtk_frame_new(NULL);
	label = gtk_label_new(_("Choose default address book"));
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);
	gtk_box_pack_start(GTK_BOX(hbox), combobox, TRUE, TRUE, 2);
	gtk_container_add(GTK_CONTAINER(frame), hbox);
	gtk_container_add(GTK_CONTAINER(vbox), frame);

	gtk_widget_show_all(vbox);
	
	gtk_window_set_default_size(GTK_WINDOW(dialog), 400, 150);
	gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_REJECT);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), vbox, FALSE, TRUE, 5);
	dialog_set_focus(GTK_DIALOG(dialog), GTK_STOCK_CANCEL);
	
	gint reply = gtk_dialog_run(GTK_DIALOG(dialog));
	switch (reply) {
		case GTK_RESPONSE_ACCEPT:
			set_default_abook(win, config, combobox);
			break;
		default:
			break;
	}
	gtk_widget_destroy(dialog);

}
