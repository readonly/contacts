/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <glib-object.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "dbus-service.h"
#include "server-object.h"
#include "utils.h"
#include "mainwindow.h"
#include "callbacks.h"
#include "plugin-loader.h"
#include "plugin.h"
#include "callbacks.h"
#include "dbus-contact.h"
#include "vcard-utils.h"

#include "server-bindings.h"

#ifdef HAVE_CONFIG_H
#   include <config.h>
#endif

G_DEFINE_TYPE(ServerObject, server, G_TYPE_OBJECT);

struct sigaction sa_sigabrt_old;
struct sigaction sa_sighup_old;
struct sigaction sa_sigterm_old;
struct sigaction sa_sigcld_old;

static void signal_abort_handler(int signum) {
    gchar* sig;

    switch (signum) {
	case SIGABRT:
	    sig = g_strdup("Abort");
	    break;
	case SIGHUP:
            sig = g_strdup("SIGHUP");
            break;
	case SIGTERM:
            sig = g_strdup("SIGTERM");
            break;
	default:
            sig = g_strdup("Unknown");
	    break;
    }

    g_message("Caught [%s]: Shutting down application", sig);
    g_free(sig);
    shutdown_application(NULL);
    exit(0);
}

static void signal_child_handler(int signum) {
    gchar* sig;
    pid_t pid;
    int status;

    sig = g_strdup("SIGCLD");
    g_message("Caught [%s]: Shutting down application", sig);
    g_free(sig);
    
    while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
        if (WIFEXITED(status)) {
            g_message("[%d]: Child exited, status=%d", pid, WEXITSTATUS(status));
        }
        else if (WIFSIGNALED(status)) {
            g_message("[%d]: Child exited, status=%d", pid, WTERMSIG(status));
        }
        else if (WIFSTOPPED(status)) {
            g_message("[%d]: Child exited, status=%d", pid, WSTOPSIG(status));
        }
        else if (WIFCONTINUED(status)) {
           g_message("[%d]: Child exited, status=continued", pid);
        }
        else
            g_message("[%d]: Child exited, status=unknown", pid);
    }
}

static void server_class_init(ServerObjectClass *klass) {
    GError *error = NULL;
    gint i;

    const gchar* signal_names[CLAWS_CONTACTS_SIGNAL_COUNT] = {
        "contact_mail_to",
        "contact_mail_cc",
        "contact_mail_bcc"
    };
    
    for (i = 0; i < CLAWS_CONTACTS_SIGNAL_COUNT; i++) {
        guint signal_id;
        
        signal_id = g_signal_new(signal_names[i],
                                 G_OBJECT_CLASS_TYPE(klass),
                                 G_SIGNAL_RUN_LAST,
                                 0,
                                 NULL,
                                 NULL,
                                 g_cclosure_marshal_VOID__STRING,
                                 G_TYPE_NONE,
                                 1,
                                 G_TYPE_STRING);
        klass->signals[i] = signal_id;
    }
    
    /* Init the DBus connection, per-klass */
    klass->connection = dbus_g_bus_get(DBUS_BUS_SESSION, &error);
    if (klass->connection == NULL) {
        g_warning("Unable to connect to dbus: %s", error->message);
        g_clear_error(&error);
        return;
    }
    /* &dbus_glib__object_info is provided in the
     * server-bindings.h file
     * OBJECT_TYPE_SERVER is the GType of your server object
     */
    dbus_g_object_type_install_info(OBJECT_TYPE_SERVER,
            &dbus_glib_abook_object_info);
}


static void abook_emit_signal(ServerObject* server,
                              ClawsContactsSignals signum,
                              const gchar* message) {
    ServerObjectClass* klass = SERVER_OBJECT_GET_CLASS(server);
    
    g_signal_emit(server, klass->signals[signum], 0, message);
}

static void server_init(ServerObject *server) {
    GError *error = NULL;
    DBusGProxy *driver_proxy;
    ServerObjectClass *klass = SERVER_OBJECT_GET_CLASS(server);
    guint request_ret;
    struct sigaction sa_sigabrt;
    struct sigaction sa_sighup;
    struct sigaction sa_sigterm;

    /* Register DBUS path */
    dbus_g_connection_register_g_object(
            klass->connection, "/org/clawsmail/contacts",
            G_OBJECT(server));

    /* Register the service name, the constant here are
     * defined in dbus-glib-bindings.h
     */
    driver_proxy = dbus_g_proxy_new_for_name(
            klass->connection, DBUS_SERVICE_DBUS,
            DBUS_PATH_DBUS, DBUS_INTERFACE_DBUS);

    if(!org_freedesktop_DBus_request_name(
                driver_proxy, "org.clawsmail.Contacts",
                DBUS_NAME_FLAG_DO_NOT_QUEUE, &request_ret, &error)) {
        g_warning("Unable to register service: %s", error->message);
        g_clear_error(&error);
    }
    g_object_unref(driver_proxy);
    read_config(NULL);
   
    sa_sigabrt.sa_handler = signal_abort_handler;
    sa_sigabrt.sa_flags = 0;
    sigemptyset(&sa_sigabrt.sa_mask);
    if (sigaction(SIGABRT, &sa_sigabrt, &sa_sigabrt_old))
        g_warning("Could not install SIGABRT handler");
        
    sa_sighup.sa_handler = signal_abort_handler;
    sa_sighup.sa_flags = 0;
    sigemptyset(&sa_sighup.sa_mask);
    if (sigaction(SIGHUP, &sa_sighup, &sa_sighup_old))
        g_warning("Could not install SIGHUP handler");
        
    sa_sigterm.sa_handler = signal_abort_handler;
    sa_sigterm.sa_flags = 0;
    sigemptyset(&sa_sigterm.sa_mask);
    if (sigaction(SIGTERM, &sa_sigterm, &sa_sigterm_old))
        g_warning("Could not install SIGTERM handler");
        
    /* Preload all address books */
    read_config(NULL);
}

static gchar** mail_text(Contact* contact) {
    GSList* cur;
    GPtrArray* array;
    gchar *firstname, *lastname, *displayname, *nickname, *text;
    
    array = g_ptr_array_new();
    firstname = lastname = displayname = nickname = text = NULL;
    
    if (contact->data) {
        extract_data(contact->data, "cn", (gpointer) &displayname);
        if (! displayname) {
            extract_data(contact->data, "first-name", (gpointer) &firstname);
            extract_data(contact->data, "last-name", (gpointer) &lastname);
            if (! firstname && ! lastname) {
                extract_data(contact->data, "cn", (gpointer) &nickname);
                if (nickname)
                    text = g_strdup(nickname);
            }
            else {
                if (firstname && lastname)
                    text = g_strconcat(firstname, " ", lastname, NULL);
                else if (firstname)
                    text = g_strdup(firstname);
                else
                    text = g_strdup(lastname);
            }
        }
        else
            text = g_strdup(displayname);
        g_free(firstname);
        g_free(lastname);
        g_free(displayname);
        g_free(nickname);
    }
    
    for (cur= contact->emails; cur; cur = g_slist_next(cur)) {
        Email* e = (Email *) cur->data;
        if (! e->email)
            continue;
        if (text) {
            g_ptr_array_add(array, g_strconcat("\"", text, "\" <", e->email, ">", NULL));
        }
        else if (e->alias && strlen(e->alias) > 0) {
            g_ptr_array_add(array, g_strconcat("\"", e->alias, "\" <", e->email, ">", NULL));
        }
        else
            g_ptr_array_add(array, g_strconcat(e->email, NULL));
    }
    g_ptr_array_add(array, NULL);
    g_free(text);

    return (gchar **) g_ptr_array_free(array, FALSE);
}

static void get_emails(GPtrArray* array,
                       Plugin* plugin,
                       AddressBook* abook,
                       gchar* token) {
    gchar* err = NULL;
    GSList* contacts;
    guint i;
    
    plugin->abook_open(abook, &err);
    if (err) {
        g_message("%s", err);
        g_free(err);
        err = NULL;
        return;
    }
    GSList* result = plugin->get_contact(abook, token, &err);
    if (err) {
        g_message("%s", err);
        g_free(err);
        err = NULL;
        gslist_free(&result, NULL);
        return;
    }
    for (contacts = result; contacts; contacts = g_slist_next(contacts)) {
        Contact* c = (Contact *) contacts->data;
        gchar** text = mail_text(c);
        guint size = g_strv_length(text);
        for (i = 0; i < size; i++)
            g_ptr_array_add(array, g_strdup(text[i]));
        g_strfreev(text);
    }
    gslist_free(&result, NULL);
    plugin->abook_close(abook, &err);
}

static void email_ptr_foreach(gpointer data, gpointer user_data) {
#if GLIB_CHECK_VERSION(2,32,0)
	GArray* email = g_array_sized_new(FALSE, FALSE, sizeof(GValue), sizeof(data));
#else
	GValueArray* email = (GValueArray *) data;
#endif
	GValue* email_member;
	guint i, j;
    Contact* contact = (Contact *) user_data;
    Email* mail;
    
#if GLIB_CHECK_VERSION(2,32,0)
    if (! email || ! contact || email->len < 1)
#else
    if (! email || ! contact || email->n_values < 1)
#endif
        return;

    mail = g_new0(Email, 1);
#if GLIB_CHECK_VERSION(2,32,0)
	for (i = j = 0; i < email->len; j++, i++) {
#else
	for (i = j = 0; i < email->n_values; j++, i++) {
#endif
        if (j > 0 && j % 3 == 0) {
            mail = g_new0(Email, 1);
            contact->emails = g_slist_prepend(contact->emails, mail);
            j = 0;
        }
            
#if GLIB_CHECK_VERSION(2,32,0)
		email_member = &g_array_index(email, GValue, i);
#else
		email_member = g_value_array_get_nth(email, i);
#endif
        switch (j) {
            case 0:
                mail->alias = g_value_dup_string(email_member);
                break;
            case 1:
                mail->email = g_value_dup_string(email_member);
                break;
            case 2:
                mail->remarks = g_value_dup_string(email_member);
                break;
        }            
	}
    contact->emails = g_slist_prepend(contact->emails, mail);
}

static void contact_data_save(DBusContact* dbus_contact, Contact* contact) {
    GHashTableIter iter;
    gpointer key, value;
    
    if (! dbus_contact || ! contact)
        return;
        
	if (dbus_contact->emails) {
        g_ptr_array_foreach(dbus_contact->emails, email_ptr_foreach, contact);
    }
    if (dbus_contact->data) {
        g_hash_table_iter_init (&iter, dbus_contact->data);
        while (g_hash_table_iter_next (&iter, &key, &value)) {
            swap_data(contact->data, key, value);
        }        
    }
}

/**
 * If list only have one email address and alias, email and remarks are
 * all empty consider this client to have no email address.
 * The reason for this decision is that DBus does not allow to transfer
 * NULL references which means that the only way to indicate this contact
 * should be created without any email address is to send one email
 * address where every string is empty (empty as in "" and not NULL)
 */
static GPtrArray* get_email_list(GPtrArray* list) {
#if GLIB_CHECK_VERSION(2,32,0)
	GArray* email = g_array_sized_new(FALSE, FALSE, sizeof(GValue), 1);
#else
	GValueArray* email;
#endif
    GValue* item;
    guint i;
    gboolean found = FALSE;

    if (! list || list->len == 0 || list->len > 1)
        return list;
    
    email = g_ptr_array_index(list, 0);
#if GLIB_CHECK_VERSION(2,32,0)
    for (i = 0; !found && i < email->len; i++) {
        item = &g_array_index(email, GValue, i);
#else
    for (i = 0; !found && i < email->n_values; i++) {
        item = g_value_array_get_nth(email, i);
#endif
        gchar* str = (gchar *) g_value_get_string(item);
        if (str && strlen(str) > 0)
            found = TRUE;
    }
    
    return (found) ? list : NULL;
}

GQuark server_object_error_quark() {
        static GQuark quark = 0;
        if (!quark)
                quark = g_quark_from_static_string ("server_object_error");
                
        return quark;
}

void notify_to_selected(ServerObject* server, gchar* address) {
    g_return_if_fail(server != NULL);
    
    g_message("Signal To: %s\n", address);
    abook_emit_signal(server, CLAWS_CONTACTS_SIGNAL_CONTACT_TO, address);
}

void notify_cc_selected(ServerObject* server, gchar* address) {
    g_return_if_fail(server != NULL);
    
    g_message("Signal Cc: %s\n", address);
    abook_emit_signal(server, CLAWS_CONTACTS_SIGNAL_CONTACT_CC, address);
}

void notify_bcc_selected(ServerObject* server, gchar* address) {
    g_return_if_fail(server != NULL);
    
    g_message("Signal Bcc: %s\n", address);
    abook_emit_signal(server, CLAWS_CONTACTS_SIGNAL_CONTACT_BCC, address);
}

gboolean abook_ping(ServerObject* server,
                    gchar** reply,
                    GError** error) {
    gboolean response = TRUE;
    
    g_message("Receive 'ping' request");

    *reply = g_strdup("PONG");
    
    g_message("Response: %s\nEnd receive", *reply);

    return response;
}

gboolean abook_commit(ServerObject* server,
                      gchar* book,
                      gboolean* reply,
                      GError** error) {
    gboolean response = TRUE;
    gchar* err = NULL;
    
    g_message("Receive 'commit' request");
    
    if (! book || strlen(book) < 1) {
        g_set_error(error,
                    server_object_error_quark(),
                    MISSING_ADDRESS_BOOK,
                    _("Missing address book"));
        g_message("Missing address book");
        return FALSE;
    }

    Reference* reference = address_book_reference_get(book);
    if (reference) {
        g_message("Committing all changes in plugin '%s'",
                reference->plugin->name());
        reference->plugin->abooks_commit_all(&err);
        if (err) {
            if (error) {
                g_set_error(error,
                            server_object_error_quark(),
                            SAVING_ADDRESS_BOOK_FAIL,
                            "%s", err);
            }
            g_message("%s", err);
            g_free(err);
            *reply = FALSE;        
        }
        else
            *reply = TRUE;
    }
    else {
        if (error) {
            g_set_error(error,
                        server_object_error_quark(),
                        ADDRESS_BOOK_UNKNOWN,
                        _("%s: Address book not found here"),
                        book);
        }
        g_message("%s: Address book not found here", book);
        *reply = FALSE;        
    }
    
    g_message("Response: %s\nEnd receive", (*reply) ? "TRUE" : "FALSE");
    
    return response;
}

gboolean abook_show_addressbook(ServerObject*   server,
								gboolean        compose,
                                GError**        error) {
    pid_t pid;
    gboolean res = FALSE;
    const gchar* binary =
        (const gchar *) g_strconcat(BINDIR, "/", PACKAGE, NULL);
    gboolean compose_mode;
    struct sigaction sa_sigcld;

    compose_mode = compose;
    
    g_message("address book show called");
    g_message("compose mode: %s", (compose_mode) ? "YES" : "NO");


    sa_sigcld.sa_handler = signal_child_handler;
    sa_sigcld.sa_flags = 0;
    sigemptyset(&sa_sigcld.sa_mask);
    if (sigaction(SIGCLD, &sa_sigcld, &sa_sigcld_old))
        g_warning("Could not install SIGCLD handler");

    switch (pid = fork()) {
        case -1:
            g_set_error(error,
                server_object_error_quark(),
                START_SERVICE_FAIL,
                _("Could not create process to run addressbook"));
            g_message("Could not create process to run addressbook");
            break;
        case 0:
            g_message("spawning child");
            if (getppid() == 1) {
                g_set_error(error,
                    server_object_error_quark(),
                    ALL_READY_RUNNING_FAIL,
                    _("Addressbook already running as daemon"));
                g_message("Addressbook already running as daemon");
                break;
            }
            umask(0);
            pid = setsid();
            if (pid < 0) {
                g_set_error(error,
                    server_object_error_quark(),
                    CREATE_SESSION_ID_FAIL,
                    _("Could not create new session ID"));
                g_message("Could not create new session ID");
                break;
            }
            if ((chdir("/")) < 0) {
                g_set_error(error,
                    server_object_error_quark(),
                    CHANGE_TO_ROOT_DIR_FAIL,
                    _("Not able to change dir to root"));
                g_message("Not able to change dir to root");
                break;
            }

            freopen("/dev/null/", "r", stdin);
            freopen("/dev/null/", "w", stdout);
            freopen("/dev/null/", "w", stderr);
            
            g_message("Child: %s", binary);
            if (compose_mode)
                execlp(binary, binary, "--compose", (char *) NULL);
            else
                execlp(binary, binary, (char *) NULL);
            _exit(-1);
        default:
            res = TRUE;
    }

    return res;
}

gboolean abook_book_list(ServerObject*  server,
                         gchar***       books,
                         GError**       error) {
    gboolean response = TRUE;
    GSList *abooks = NULL, *cur, *pbooks, *list = NULL;
    Reference* ref;
    AddressBook* ab;
    
    g_message("Receive 'book_list' request");

    if (!books) {
        if (error) {
            g_set_error(error,
                        server_object_error_quark(),
                        MISSING_RESPONSE_STRUCTURE,
                        _("Missing response structure"));
        }
        g_message("Missing response structure");
        return FALSE;
    }
    
    abooks = address_books_get();
    for (cur = abooks; cur; cur = g_slist_next(cur)) {
        ref = (Reference *) cur->data;
        for (pbooks = ref->abooks; pbooks; pbooks = g_slist_next(pbooks)) {
            ab = (AddressBook *) pbooks->data;
            list = g_slist_prepend(list, g_strdup(ab->abook_name));
        }
    }
    gslist_free(&abooks, NULL);

    *books = gslist_to_array(list, NULL);
    gslist_free(&list, g_free);
        
    return response;
}

gboolean abook_search_addressbook(ServerObject* server,
                                  gchar*        token,
                                  gchar*        book,
                                  gchar***      emails,
                                  GError**      error) {
    gboolean response = TRUE;
    GPtrArray* array;
    GSList *abooks = NULL, *cur, *books;
    Reference* ref;
    AddressBook* ab;
    Plugin* plugin;
    gboolean found_book = FALSE;
    gchar** reply;
    
    g_message("Receive 'search' request\nToken: %s\nBook: %s\n",
        token, (book) ? book : "Non selected");

    if (!token) {
        if (error) {
            g_set_error(error,
                        server_object_error_quark(),
                        MISSING_SEARCH_TOKEN,
                        _("Missing search token"));
        }
        g_message("Missing search token");
        return FALSE;
    }

    if (!emails) {
        if (error) {
            g_set_error(error,
                        server_object_error_quark(),
                        MISSING_RESPONSE_STRUCTURE,
                        _("Missing response structure"));
        }
        g_message("Missing response structure");
        return FALSE;
    }
    
    array = g_ptr_array_new();
    
    if (book && strlen(book) > 0) {
        Reference* reference = address_book_reference_get(book);
        if (reference) {
            ab = address_book_get(reference->plugin, book);
            g_message("Searching book '%s' using '%s'",
                    ab->abook_name, token);
            ab->dirty = TRUE;
            get_emails(array, reference->plugin, ab, token);
        }
    }
    else {
        abooks = address_books_get();
        for (cur = abooks; cur; cur = g_slist_next(cur)) {
            ref = (Reference *) cur->data;
            plugin = ref->plugin;
            for (books = ref->abooks; plugin && books && !found_book;
                        books = g_slist_next(books)) {
                ab = (AddressBook *) books->data;
                g_message("Searching book '%s' using '%s'",
                        ab->abook_name, token);
                ab->dirty = TRUE;
                get_emails(array, plugin, ab, token);
            }
        }
    }
    
    g_ptr_array_add(array, NULL);
    *emails = (gchar **) g_ptr_array_free(array, FALSE);
    
    for (reply = *emails; *reply; reply += 1)
        debug_print("%s", *reply);
    g_message("End receive");

    gslist_free(&abooks, NULL);    
    return response;
}

gboolean abook_add_contact(ServerObject*   server,
                            gchar*          book,
                            GHashTable*     data,
                            GPtrArray*      emails,
                            GError**        error) {
    gboolean response = TRUE;
    Contact* abook_contact;
    DBusContact* dbus_contact;
    AddressBook* ab;
    gchar* err = NULL;
    ConfigFile* config;
    GSList* default_book = NULL;
    gchar* search_book = NULL;

    if (! book || strlen(book) < 1) {
        config = g_new0(ConfigFile, 1);
        gchar* home = get_self_home();
	    config->path = g_strdup_printf(
                "%s%s%src", home, G_DIR_SEPARATOR_S, PACKAGE);
	    g_free(home);

        config->key_file = g_key_file_new();
        if (g_file_test(config->path, G_FILE_TEST_EXISTS)) {
            g_key_file_load_from_file(config->key_file,
                    config->path, G_KEY_FILE_KEEP_COMMENTS, NULL);
            config_get_value(config, "Settings", "default book", &default_book);
        }
        config_main_free(config);
        g_free(config);
        if (default_book) {
            search_book = g_strdup((gchar *) default_book->data);
            gslist_free(&default_book, g_free);
        }
        if (! search_book) {
            g_set_error(error,
                        server_object_error_quark(),
                        MISSING_ADDRESS_BOOK,
                        _("Missing address book"));
            g_message("Missing address book");
            return FALSE;
        }
    }
    else
        search_book = g_strdup(book);

    if (! data) {
        g_set_error(error,
                    server_object_error_quark(),
                    MISSING_NEW_CONTACT,
                    _("Missing new contact"));
        g_message("Missing new contact");
        return FALSE;
    }
    
    dbus_contact = g_new0(DBusContact, 1);
    dbus_contact->data = data;
    dbus_contact->emails = get_email_list(emails);
    
    g_message("Receive 'add' request\nBook: %s", search_book);
    g_message("New contact:");
    dbus_contact_print(dbus_contact, NULL);
    g_message("End receive");

    Reference* reference = address_book_reference_get(search_book);
    if (reference) {
        ab = address_book_get(reference->plugin, search_book);
        g_message("Adding to address book '%s' using plugin '%s'",
                ab->abook_name, reference->plugin->name());
    
        abook_contact = contact_new();
        contact_data_save(dbus_contact, abook_contact);
        
        reference->plugin->set_contact(ab, abook_contact, &err);
        if (err) {
            if (error) {
                g_set_error(error,
                            server_object_error_quark(),
                            SAVING_ADDRESS_BOOK_FAIL,
                            "%s", err);
            }
            g_message("%s", err);
            g_free(err);
            response = FALSE;        
        }
        g_free(dbus_contact);
        contact_free(abook_contact);
        g_free(abook_contact);
    }
    else {
        if (error) {
            g_set_error(error,
                        server_object_error_quark(),
                        ADDRESS_BOOK_UNKNOWN,
                        _("%s: Address book not found here"),
                        search_book);
        }
        g_message("%s: Address book not found here", search_book);
        response = FALSE;        
    }
    g_free(search_book);
    
    return response;
}

gboolean abook_add_vcard(ServerObject* server,
                         gchar*        book,
                         gchar*        vcard,
                         GError**      error) {
    gboolean response = TRUE;
    gchar* search_book = NULL;
    Contact* abook_contact;
    AddressBook* ab;
    gchar* err = NULL;
    ConfigFile* config;
    GSList* default_book = NULL;
    
    if (! book || strlen(book) < 1) {
        config = g_new0(ConfigFile, 1);
        gchar* home = get_self_home();
	    config->path = g_strdup_printf(
                "%s%s%src", home, G_DIR_SEPARATOR_S, PACKAGE);
	    g_free(home);

        config->key_file = g_key_file_new();
        if (g_file_test(config->path, G_FILE_TEST_EXISTS)) {
            g_key_file_load_from_file(config->key_file,
                    config->path, G_KEY_FILE_KEEP_COMMENTS, NULL);
            config_get_value(config, "Settings", "default book", &default_book);
        }
        config_main_free(config);
        g_free(config);
        if (default_book) {
            search_book = g_strdup((gchar *) default_book->data);
            gslist_free(&default_book, g_free);
        }
        if (! search_book) {
            g_set_error(error,
                        server_object_error_quark(),
                        MISSING_ADDRESS_BOOK,
                        _("Missing address book"));
            g_message("Missing address book");
            return FALSE;
        }
    }
    else
        search_book = g_strdup(book);

    if (! vcard) {
        g_set_error(error,
                    server_object_error_quark(),
                    MISSING_NEW_CONTACT,
                    _("Missing vCard"));
        g_message("Missing vCard");
        return FALSE;
    }

    Reference* reference = address_book_reference_get(search_book);
    if (reference) {
        ab = address_book_get(reference->plugin, search_book);
        g_message("Adding to address book '%s' using plugin '%s'",
                ab->abook_name, reference->plugin->name());
    
        abook_contact = vcard_ptr2contact(reference->plugin,
            vcard, strlen(vcard), &err);
        
        reference->plugin->set_contact(ab, abook_contact, &err);
        if (err) {
            if (error) {
                g_set_error(error,
                            server_object_error_quark(),
                            SAVING_ADDRESS_BOOK_FAIL,
                            "%s", err);
            }
            g_message("%s", err);
            g_free(err);
            response = FALSE;        
        }
        contact_free(abook_contact);
        g_free(abook_contact);
    }
    else {
        if (error) {
            g_set_error(error,
                        server_object_error_quark(),
                        ADDRESS_BOOK_UNKNOWN,
                        _("%s: Address book not found here"),
                        search_book);
        }
        g_message("%s: Address book not found here", search_book);
        response = FALSE;        
    }
    g_free(search_book);

    return response;
}

gboolean abook_get_vcard(ServerObject* server,
                         gchar*        acount,
                         gchar**       vcard,
                         GError**      error) {
    gboolean response = TRUE;
    gchar* err = NULL;
    
    *vcard = personal_vcard_get(acount, &err);
    if (err) {
        if (error) {
            g_set_error(error,
                        server_object_error_quark(),
                        NO_SUCH_VCARD,
                        _("%s: Vcard not found here"),
                        err);
        }
        g_message("%s: Address book not found here", err);
        g_free(err);
        response = FALSE;        
    }
    
    return response;
}
