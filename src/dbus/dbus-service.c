/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <glib.h>
#include <glib-object.h>
#include <dbus/dbus-glib-bindings.h>
#include "dbus-service.h"
#include "server-object.h"
#include "utils.h"

#ifdef HAVE_CONFIG_H
#   include <config.h>
#endif

#define PID_FILE "claws-contacts.pid"

int start_service(gboolean keep) {
    pid_t pid;
    gchar *pid_id, *pid_file;
    GError* error = NULL;

    /* Initialize the GType/GObject system. */
#if !GLIB_CHECK_VERSION(2,35,0)
    g_type_init ();
#endif
    DAEMON = TRUE;
    mainloop = NULL;
    DBusProxy = NULL;
    
    gchar* tmp = get_self_home();
    pid_file = g_strconcat(tmp, G_DIR_SEPARATOR_S, PID_FILE, NULL);
    g_free(tmp);
    
    log_handler_id = set_log_handler();
     
    if (keep) {
        mainloop = g_main_loop_new(NULL, FALSE);
        if (mainloop == NULL) {
            g_printerr(PACKAGE " : Couldn't create GMainLoop.\n");
            return 1;
        }
       
        DBusProxy = g_object_new(OBJECT_TYPE_SERVER, NULL);
        if(!DBusProxy) {
            g_printerr(PACKAGE " : Failed to create DBusProxy instance.\n");
            return 1;
        }
        
        g_main_loop_run(mainloop);
        return 0;
    }
    switch (pid = fork()) {
        case -1:
            if (error) {
                g_clear_error(&error);
            }
            g_set_error(&error,
                        server_object_error_quark(),
                        START_SERVICE_FAIL,
                        _("Could not create process to run claws-contacts"));
            break;
        case 0:
            if (error) {
                g_clear_error(&error);
            }
	        debug_print("Entering child process\n");
	    
            if (getppid() == 1) {
                g_set_error(&error,
                            server_object_error_quark(),
                            ALL_READY_RUNNING_FAIL,
                            _("claws-contacts already running as daemon"));
                break;
            }
	    
            umask(0);
            pid = setsid();
            if (pid < 0) {
                g_set_error(&error,
                            server_object_error_quark(),
                            CREATE_SESSION_ID_FAIL,
                            _("Could not create new session ID"));
                break;
            }
            if ((chdir("/")) < 0) {
                g_set_error(&error,
                            server_object_error_quark(),
                            CHANGE_TO_ROOT_DIR_FAIL,
                            _("Not able to change dir to root"));
                break;
            }
            //freopen("/dev/null/", "r", stdin);
            //freopen("/dev/null/", "w", stdout);
            //freopen("/dev/null/", "w", stderr);

            /* Create a main loop that will dispatch callbacks. */
            mainloop = g_main_loop_new(NULL, FALSE);
            if (mainloop == NULL) {
                /* Print error and terminate. */
                g_printerr(PACKAGE " : Couldn't create GMainLoop.\n");
                return 1;
            }
           
            /*Create one instance*/
            DBusProxy = g_object_new(OBJECT_TYPE_SERVER, NULL);
            if(!DBusProxy) {
                /* Print error and terminate. */
                g_printerr(PACKAGE " : Failed to create DBusProxy instance.\n");
                return 1;
            }
            
            /*Run the program*/
            g_main_loop_run(mainloop);
        default:
            pid_id = g_strdup_printf("%d\n", pid);
            g_file_set_contents(pid_file, pid_id, -1, NULL);
            g_free(pid_file);
            g_free(pid_id);
            g_message("Starting daemon: %d\n", pid);
            _exit(-1);
            break;
    }
    
    if (error) {
        g_warning("%s\n", error->message);
        g_clear_error(&error);
    }
    
    return 0;
}
