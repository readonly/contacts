/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __SERVER_OBJECT_H__
#define __SERVER_OBJECT_H__

#include <glib.h>

G_BEGIN_DECLS

#include <glib-object.h>
#include <dbus/dbus-glib-bindings.h>
#include "dbus-contact.h"

#define OBJECT_TYPE_SERVER (server_get_type())
#define SERVER_OBJECT(object) \
    (G_TYPE_CHECK_INSTANCE_CAST ((object), OBJECT_TYPE_SERVER, ServerObject))
#define SERVER_OBJECT_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST ((klass), OBJECT_TYPE_SERVER, ServerObjectClass))
#define SERVER_IS_OBJECT(object) \
    (G_TYPE_CHECK_INSTANCE_TYPE ((object), OBJECT_TYPE_SERVER))
#define SERVER_IS_OBJECT_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE ((klass), OBJECT_TYPE_SERVER))
#define SERVER_OBJECT_GET_CLASS(object) \
    (G_TYPE_INSTANCE_GET_CLASS((object), OBJECT_TYPE_SERVER, ServerObjectClass))

typedef enum {
    CLAWS_CONTACTS_SIGNAL_CONTACT_TO,
    CLAWS_CONTACTS_SIGNAL_CONTACT_CC,
    CLAWS_CONTACTS_SIGNAL_CONTACT_BCC,
    CLAWS_CONTACTS_SIGNAL_COUNT
} ClawsContactsSignals;

/* Standard GObject class structures, etc */
typedef struct {
    GObjectClass parent;
    
    DBusGConnection* connection;
    guint signals[CLAWS_CONTACTS_SIGNAL_COUNT];
} ServerObjectClass;

typedef struct {
    GObject   parent;
} ServerObject;

enum {
    START_SERVICE_FAIL,
    ALL_READY_RUNNING_FAIL,
    CREATE_SESSION_ID_FAIL,
    CHANGE_TO_ROOT_DIR_FAIL,
    SAVING_ADDRESS_BOOK_FAIL,
    MISSING_ADDRESS_BOOK,
    ADDRESS_BOOK_UNKNOWN,
    MISSING_SEARCH_TOKEN,
    MISSING_RESPONSE_STRUCTURE,
    MISSING_NEW_CONTACT,
    NO_SUCH_VCARD
};

GType server_get_type();

GQuark server_object_error_quark();
void notify_to_selected(ServerObject* server, gchar* address);
void notify_cc_selected(ServerObject* server, gchar* address);
void notify_bcc_selected(ServerObject* server, gchar* address);

gboolean abook_search_addressbook(ServerObject* server,
                                  gchar*        token,
                                  gchar*        book,
                                  gchar***      emails,
                                  GError**      error);
gboolean abook_add_contact(ServerObject*    server,
                            gchar*          book,
                            GHashTable*     data,
                            GPtrArray*      emails,
                            GError**        error);
gboolean abook_show_addressbook(ServerObject*   server,
								gboolean        compose,
                                GError**        error);
gboolean abook_ping(ServerObject*   server,
                    gchar**         reply,
                    GError**        error);
gboolean abook_commit(ServerObject* server,
                      gchar*        book,
                      gboolean*     reply,
                      GError**      error);
gboolean abook_book_list(ServerObject*  server,
                         gchar***       books,
                         GError**       error);
gboolean abook_add_vcard(ServerObject* server,
                         gchar*        book,
                         gchar*        vcard,
                         GError**      error);
gboolean abook_get_vcard(ServerObject* server,
                         gchar*        acount,
                         gchar**       vcard,
                         GError**      error);

G_END_DECLS

#endif
