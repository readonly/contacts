/*
 *  * $Id$
*/
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "mainwindow.h"
#include "callbacks.h"
#include "utils.h"
#include "extension-loader.h"
#include "gtk-utils.h"

const gchar* PIXMAPS[PIXMAP_N] = {
    PIXDIR"/claws-contacts_16x16.png",
    PIXDIR"/claws-contacts_32x32.png",
    PIXDIR"/claws-contacts_48x48.png",
    PIXDIR"/claws-contacts_64x64.png",
    PIXDIR"/claws-contacts_128x128.png",
    NULL
};

const gchar* STATUS_MSG[] = {
    N_("No open address book"),
    N_("Connected to address book: "),
    N_("Searching address book...")
};

static guint current_status = STATUS_MSG_NO_BOOK;

static gint book_column_compare_func(GtkTreeModel* model,
                                     GtkTreeIter* a,
                                     GtkTreeIter* b,
                                     gpointer data) {
    gint sortcol = GPOINTER_TO_INT(data);
    gint ret = 0;
    gchar *sa, *sb;
    
    switch (sortcol) {
        case BOOK_NAME_COLUMN:
            gtk_tree_model_get(model, a, BOOK_NAME_COLUMN, &sa, -1);
            gtk_tree_model_get(model, b, BOOK_NAME_COLUMN, &sb, -1);
            ret = utf8_collate(sa, sb);
            g_free(sa);
            g_free(sb);
            break;
        default:
            g_return_val_if_reached(0);
    }
    
    return ret;
}

static gint contact_column_compare_func(GtkTreeModel* model,
                                        GtkTreeIter* a,
                                        GtkTreeIter* b,
                                        gpointer data) {
    gint sortcol = GPOINTER_TO_INT(data);
    gint ret = 0;
    gchar *sa, *sb;
    
    switch (sortcol) {
        case CONTACT_DISPLAYNAME_COLUMN:
            gtk_tree_model_get(model, a, CONTACT_DISPLAYNAME_COLUMN, &sa, -1);
            gtk_tree_model_get(model, b, CONTACT_DISPLAYNAME_COLUMN, &sb, -1);
            ret = utf8_collate(sa, sb);
            g_free(sa);
            g_free(sb);
            break;
        case CONTACT_FIRSTNAME_COLUMN:
            gtk_tree_model_get(model, a, CONTACT_FIRSTNAME_COLUMN, &sa, -1);
            gtk_tree_model_get(model, b, CONTACT_FIRSTNAME_COLUMN, &sb, -1);
            ret = utf8_collate(sa, sb);
            g_free(sa);
            g_free(sb);
            break;
        case CONTACT_LASTNAME_COLUMN:
            gtk_tree_model_get(model, a, CONTACT_LASTNAME_COLUMN, &sa, -1);
            gtk_tree_model_get(model, b, CONTACT_LASTNAME_COLUMN, &sb, -1);
            ret = utf8_collate(sa, sb);
            g_free(sa);
            g_free(sb);
            break;
        case CONTACT_EMAIL_COLUMN:
            gtk_tree_model_get(model, a, CONTACT_EMAIL_COLUMN, &sa, -1);
            gtk_tree_model_get(model, b, CONTACT_EMAIL_COLUMN, &sb, -1);
            ret = utf8_collate(sa, sb);
            g_free(sa);
            g_free(sb);
            break;
        default:
            g_return_val_if_reached(0);
    }
    
    return ret;
}
                                   
static void create_menu(MainWindow* mainwindow) {
    GtkWidget *item_space,
              *file, *file_quit, *file_plugin, *page_setup,
              *abook_menu, *abook, *abook_new, *abook_open, 
              *abook_delete, *abook_close, *abook_edit,
              *contact_menu, *contact, *contact_new, *contact_edit,
              *contact_print, *contact_delete, *tools, *tools_prefs,
              *tools_attribs, *help, *help_text, *help_about;

    mainwindow->accel = g_object_new(GTK_TYPE_ACCEL_GROUP, NULL);
    gtk_window_add_accel_group(GTK_WINDOW(mainwindow->window), mainwindow->accel);
    mainwindow->menu = gtk_menu_bar_new();

    mainwindow->file_menu = gtk_menu_new();

    file_plugin = gtk_image_menu_item_new_with_mnemonic(_("_Manage plugins"));
    gtk_image_menu_item_set_accel_group(GTK_IMAGE_MENU_ITEM(file_plugin), mainwindow->accel);
    gtk_widget_add_accelerator(file_plugin, "activate", mainwindow->accel,
            GDK_M, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(file_plugin, "activate",
            G_CALLBACK(file_plugin_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->file_menu), file_plugin);

    page_setup = gtk_image_menu_item_new_with_mnemonic(_("_Page setup"));
    gtk_image_menu_item_set_accel_group(GTK_IMAGE_MENU_ITEM(page_setup), mainwindow->accel);
    gtk_widget_add_accelerator(page_setup, "activate", mainwindow->accel,
            GDK_P, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(page_setup, "activate",
            G_CALLBACK(printer_page_setup_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->file_menu), page_setup);

    item_space = gtk_separator_menu_item_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->file_menu), item_space); 

    file_quit = gtk_image_menu_item_new_from_stock(GTK_STOCK_QUIT, mainwindow->accel);
    g_signal_connect(file_quit, "activate", G_CALLBACK(menu_quit_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->file_menu), file_quit);

    file = gtk_menu_item_new_with_mnemonic(_("_File"));
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->menu), file);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(file), mainwindow->file_menu);

    abook_menu = gtk_menu_new();
    
    abook_new = gtk_image_menu_item_new_with_mnemonic(_("_New"));
    gtk_image_menu_item_set_accel_group(GTK_IMAGE_MENU_ITEM(abook_new), mainwindow->accel);
    gtk_widget_add_accelerator(abook_new, "activate", mainwindow->accel,
            GDK_N, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(abook_new, "activate",
            G_CALLBACK(abook_new_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(abook_menu), abook_new);

    abook_edit = gtk_image_menu_item_new_from_stock(GTK_STOCK_EDIT, mainwindow->accel);
    gtk_widget_add_accelerator(abook_edit, "activate", mainwindow->accel,
            GDK_E, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(abook_edit, "activate",
            G_CALLBACK(abook_edit_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(abook_menu), abook_edit);

    abook_open = gtk_image_menu_item_new_with_mnemonic(_("_Open"));
    gtk_image_menu_item_set_accel_group(GTK_IMAGE_MENU_ITEM(abook_open), mainwindow->accel);
    gtk_widget_add_accelerator(abook_open, "activate", mainwindow->accel,
            GDK_O, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(abook_open, "activate",
            G_CALLBACK(abook_open_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(abook_menu), abook_open);

    abook_close = gtk_image_menu_item_new_with_mnemonic(_("_Close"));
    gtk_image_menu_item_set_accel_group(GTK_IMAGE_MENU_ITEM(abook_close), mainwindow->accel);
    gtk_widget_add_accelerator(abook_close, "activate", mainwindow->accel,
            GDK_L, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(abook_close, "activate",
            G_CALLBACK(abook_close_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(abook_menu), abook_close);

    abook_delete = gtk_image_menu_item_new_from_stock(GTK_STOCK_DELETE, mainwindow->accel);
    gtk_widget_add_accelerator(abook_delete, "activate", mainwindow->accel,
            GDK_D, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(abook_delete, "activate",
            G_CALLBACK(abook_delete_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(abook_menu), abook_delete);

    abook = gtk_menu_item_new_with_mnemonic(_("_Address books"));
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->menu), abook);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(abook), abook_menu);

    contact_menu = gtk_menu_new();

    contact_new = gtk_image_menu_item_new_with_mnemonic(_("_New"));
    gtk_image_menu_item_set_accel_group(GTK_IMAGE_MENU_ITEM(contact_new), mainwindow->accel);
    gtk_widget_add_accelerator(contact_new, "activate", mainwindow->accel,
            GDK_N, GDK_SHIFT_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(contact_new, "activate",
            G_CALLBACK(contact_new_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(contact_menu), contact_new);

    contact_edit = gtk_image_menu_item_new_from_stock(GTK_STOCK_EDIT, mainwindow->accel);
    gtk_widget_add_accelerator(contact_edit, "activate", mainwindow->accel,
            GDK_E, GDK_SHIFT_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(contact_edit, "activate",
            G_CALLBACK(contact_edit_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(contact_menu), contact_edit);

    contact_print = gtk_image_menu_item_new_from_stock(GTK_STOCK_PRINT, mainwindow->accel);
    gtk_widget_add_accelerator(contact_print, "activate", mainwindow->accel,
            GDK_P, GDK_SHIFT_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(contact_print, "activate",
            G_CALLBACK(contact_print_prepare_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(contact_menu), contact_print);

    contact_delete = gtk_image_menu_item_new_from_stock(
            GTK_STOCK_DELETE, mainwindow->accel);
    gtk_widget_add_accelerator(contact_delete, "activate", mainwindow->accel,
            GDK_D, GDK_SHIFT_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(contact_delete, "activate",
            G_CALLBACK(contact_delete_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(contact_menu), contact_delete);

    contact = gtk_menu_item_new_with_mnemonic(_("_Contact"));
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->menu), contact);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(contact), contact_menu);

    mainwindow->tools_menu = gtk_menu_new();

    tools_prefs = gtk_image_menu_item_new_from_stock(
            GTK_STOCK_PREFERENCES, mainwindow->accel);
    gtk_widget_add_accelerator(tools_prefs, "activate", mainwindow->accel,
            GDK_P, GDK_CONTROL_MASK | GDK_SHIFT_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(tools_prefs, "activate", 
            G_CALLBACK(tools_prefs_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->tools_menu), tools_prefs);

    tools_attribs = gtk_image_menu_item_new_with_mnemonic(_("_Attributes"));
    gtk_image_menu_item_set_accel_group(GTK_IMAGE_MENU_ITEM(tools_attribs), mainwindow->accel);
    gtk_widget_add_accelerator(tools_attribs, "activate", mainwindow->accel,
            GDK_A, GDK_CONTROL_MASK | GDK_SHIFT_MASK, GTK_ACCEL_VISIBLE);
    g_signal_connect(tools_attribs, "activate", 
            G_CALLBACK(tools_attribs_cb), mainwindow);
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->tools_menu), tools_attribs);

    tools = gtk_menu_item_new_with_mnemonic(_("_Tools"));
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->menu), tools);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(tools), mainwindow->tools_menu);

    mainwindow->help_menu = gtk_menu_new();
    help_text = gtk_image_menu_item_new_from_stock(GTK_STOCK_HELP, mainwindow->accel);
    help_about = gtk_image_menu_item_new_from_stock(GTK_STOCK_ABOUT, mainwindow->accel);
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->help_menu), help_text);
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->help_menu), help_about);
    help = gtk_menu_item_new_with_mnemonic(_("_Help"));
    gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->menu), help);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(help), mainwindow->help_menu);
    g_signal_connect(help_about, "activate",
            G_CALLBACK(show_about), mainwindow);
    g_signal_connect(help_text, "activate",
            G_CALLBACK(show_help), mainwindow);
}

static void create_toolbar(MainWindow* mainwindow) {
    GtkToolItem *item_quit, *item_open, *item_new,
                *item_close, *item_print, *item_space;

    mainwindow->toolbar = gtk_toolbar_new();

    item_quit = gtk_tool_button_new_from_stock(GTK_STOCK_QUIT);
    gtk_tool_item_set_tooltip_text(item_quit, _("Quit Address Book"));
    g_signal_connect(item_quit, "clicked", G_CALLBACK(toolbar_quit_cb), mainwindow);
    gtk_toolbar_insert(GTK_TOOLBAR(mainwindow->toolbar), item_quit, 0); 
    item_space = gtk_separator_tool_item_new();
    gtk_toolbar_insert(GTK_TOOLBAR(mainwindow->toolbar), item_space, 0); 

    item_print = gtk_tool_button_new_from_stock(GTK_STOCK_PRINT);
    g_signal_connect(item_print, "clicked",
            G_CALLBACK(contact_print_prepare_cb), mainwindow);
    gtk_tool_item_set_tooltip_text(item_print, _("Print contact information"));
    gtk_toolbar_insert(GTK_TOOLBAR(mainwindow->toolbar), item_print, 0); 
    item_space = gtk_separator_tool_item_new();
    gtk_toolbar_insert(GTK_TOOLBAR(mainwindow->toolbar), item_space, 0); 

    item_close = gtk_tool_button_new_from_stock(GTK_STOCK_CLOSE);
    g_signal_connect(item_close, "clicked",
            G_CALLBACK(abook_close_cb), mainwindow);
    gtk_tool_item_set_tooltip_text(item_close, _("Close address book"));
    gtk_toolbar_insert(GTK_TOOLBAR(mainwindow->toolbar), item_close, 0); 

    item_open = gtk_tool_button_new_from_stock(GTK_STOCK_OPEN);
    g_signal_connect(item_open, "clicked",
            G_CALLBACK(abook_open_cb), mainwindow);
    gtk_tool_item_set_tooltip_text(item_open, _("Open address book"));
    gtk_toolbar_insert(GTK_TOOLBAR(mainwindow->toolbar), item_open, 0); 
    item_space = gtk_separator_tool_item_new();
    gtk_toolbar_insert(GTK_TOOLBAR(mainwindow->toolbar), item_space, 0); 

    item_new = gtk_tool_button_new_from_stock(GTK_STOCK_NEW);
    g_signal_connect(item_new, "clicked",
            G_CALLBACK(abook_new_cb), mainwindow);
    gtk_tool_item_set_tooltip_text(item_new, _("Create a new address book"));
    gtk_toolbar_insert(GTK_TOOLBAR(mainwindow->toolbar), item_new, 0); 
}

static GtkWidget* create_view(MainWindow* mainwindow) {
    GtkWidget *vbox_main, *label, *hpaned;
    GtkCellRenderer *renderer;
    GtkTreeViewColumn *column;
    GtkListStore* list;
    GtkTreeSortable *sortable;
    const gchar *locale;

    vbox_main = gtk_vbox_new(FALSE, 5);
    
    hpaned = gtk_hpaned_new();
    gtk_box_pack_start(GTK_BOX(vbox_main), hpaned, TRUE, TRUE, 0);

    /* Left side of pane */
    mainwindow->left_scroll = gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_size_request(mainwindow->left_scroll, 150, -1);
    gtk_scrolled_window_set_policy(
            GTK_SCROLLED_WINDOW(mainwindow->left_scroll),
            GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_paned_add1(GTK_PANED(hpaned), mainwindow->left_scroll);

    list = gtk_list_store_new(
        BOOK_N_COLUMNS, G_TYPE_STRING, G_TYPE_POINTER, G_TYPE_POINTER, -1);
    sortable = GTK_TREE_SORTABLE(list);
    gtk_tree_sortable_set_sort_column_id(
            sortable, BOOK_NAME_COLUMN, GTK_SORT_ASCENDING);
    gtk_tree_sortable_set_sort_func(sortable, BOOK_NAME_COLUMN,
        book_column_compare_func, GINT_TO_POINTER(BOOK_NAME_COLUMN), NULL);

    mainwindow->abook_list = 
        gtk_tree_view_new_with_model(GTK_TREE_MODEL(list));
    g_object_unref(list);
    gtk_widget_set_name(mainwindow->abook_list, "abook_list");
    gtk_widget_set_tooltip_text(mainwindow->abook_list,
        _("Address book in BOLD is the default address book"));
    g_signal_connect(mainwindow->abook_list, "cursor-changed",
            G_CALLBACK(abook_list_cursor_changed_cb), mainwindow);
    g_signal_connect(mainwindow->abook_list, "row-activated",
            G_CALLBACK(abook_list_row_activated_cb), mainwindow);
    g_signal_connect(mainwindow->abook_list, "button-press-event",
            G_CALLBACK(list_button_pressed_cb), mainwindow);
    g_signal_connect(mainwindow->abook_list, "popup-menu",
            G_CALLBACK(list_popup_menu_cb), mainwindow);
    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new_with_attributes(
            _("Address books"), renderer, "text", BOOK_NAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column, 0);
    gtk_tree_view_column_set_cell_data_func(
            column, renderer, book_cell_data_format, NULL, NULL);
    gtk_tree_view_column_set_cell_data_func(
            column, renderer, markup_default_book, mainwindow, NULL);
    gtk_tree_view_column_set_alignment(column, 0.5);
    //gtk_tree_view_column_set_sort_indicator(column, TRUE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(
                mainwindow->abook_list), column);
    gtk_container_add(
            GTK_CONTAINER(mainwindow->left_scroll), mainwindow->abook_list);  
    /* Left side of pane end */
    
    /* Right side of pane */
    mainwindow->right_scroll = gtk_scrolled_window_new(NULL, NULL);
    gtk_paned_add2(GTK_PANED(hpaned), mainwindow->right_scroll);

    gtk_widget_set_size_request(mainwindow->right_scroll, 200, -1);
    gtk_scrolled_window_set_policy(
            GTK_SCROLLED_WINDOW(mainwindow->right_scroll),
            GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);    

    list = gtk_list_store_new(CONTACT_N_COLUMNS, G_TYPE_STRING,
            G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_POINTER, -1);
    sortable = GTK_TREE_SORTABLE(list);
    gtk_tree_sortable_set_sort_column_id(
            sortable, CONTACT_LASTNAME_COLUMN, GTK_SORT_ASCENDING);
    gtk_tree_sortable_set_sort_func(sortable, CONTACT_DISPLAYNAME_COLUMN,
        contact_column_compare_func, GINT_TO_POINTER(CONTACT_DISPLAYNAME_COLUMN), NULL);
    gtk_tree_sortable_set_sort_func(sortable, CONTACT_FIRSTNAME_COLUMN,
        contact_column_compare_func, GINT_TO_POINTER(CONTACT_FIRSTNAME_COLUMN), NULL);
    gtk_tree_sortable_set_sort_func(sortable, CONTACT_LASTNAME_COLUMN,
        contact_column_compare_func, GINT_TO_POINTER(CONTACT_LASTNAME_COLUMN), NULL);
    gtk_tree_sortable_set_sort_func(sortable, CONTACT_EMAIL_COLUMN,
        contact_column_compare_func, GINT_TO_POINTER(CONTACT_EMAIL_COLUMN), NULL);

    mainwindow->contact_list = 
        gtk_tree_view_new_with_model(GTK_TREE_MODEL(list));
    g_object_unref(list);
    gtk_widget_set_tooltip_text(GTK_WIDGET(mainwindow->contact_list),
        _("Double-click, enter, or space on cell will activate edit mode"));
    gtk_tree_view_set_rules_hint(
            GTK_TREE_VIEW(mainwindow->contact_list), TRUE);
    gtk_widget_set_name(mainwindow->contact_list, "contact_list");
    g_signal_connect(mainwindow->contact_list, "cursor-changed",
            G_CALLBACK(contact_list_cursor_changed_cb), mainwindow);
    g_signal_connect(mainwindow->contact_list, "row-activated",
            G_CALLBACK(contact_list_row_activated_cb), mainwindow);
    g_signal_connect(mainwindow->contact_list, "button-press-event",
            G_CALLBACK(list_button_pressed_cb), mainwindow);
    g_signal_connect(mainwindow->contact_list, "popup-menu",
            G_CALLBACK(list_popup_menu_cb), mainwindow);
    renderer = gtk_cell_renderer_text_new();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited",
            G_CALLBACK(contact_cell_edited_cb), mainwindow);
    column = gtk_tree_view_column_new_with_attributes(
            _("Display name"), renderer, "text", CONTACT_DISPLAYNAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column, CONTACT_DISPLAYNAME_COLUMN);
    gtk_tree_view_column_set_alignment(column, 0.5);
    gtk_tree_view_column_set_min_width(column, 100);
    gtk_tree_view_column_set_resizable(column, TRUE);
    gtk_tree_view_column_set_sort_indicator(column, TRUE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(
                mainwindow->contact_list), column);
    renderer = gtk_cell_renderer_text_new();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited",
            G_CALLBACK(contact_cell_edited_cb), mainwindow);

	locale = conv_get_current_locale();
	if (locale &&
		(!g_ascii_strncasecmp(locale, "hu", 2) ||
		 !g_ascii_strncasecmp(locale, "ja", 2) ||
		 !g_ascii_strncasecmp(locale, "ko", 2) ||
		 !g_ascii_strncasecmp(locale, "vi", 2) ||
		 !g_ascii_strncasecmp(locale, "zh", 2))) {
    	column = gtk_tree_view_column_new_with_attributes(
        	    _("Lastname"), renderer, "text", CONTACT_LASTNAME_COLUMN, NULL);
	    gtk_tree_view_column_set_sort_column_id(column, CONTACT_LASTNAME_COLUMN);
    	gtk_tree_view_column_set_alignment(column, 0.5);
    	gtk_tree_view_column_set_min_width(column, 100);
    	gtk_tree_view_column_set_resizable(column, TRUE);
    	gtk_tree_view_column_set_sort_indicator(column, TRUE);
    	gtk_tree_view_append_column(GTK_TREE_VIEW(
    	            mainwindow->contact_list), column);
    	renderer = gtk_cell_renderer_text_new();
    	g_object_set(renderer, "editable", TRUE, NULL);
    	g_signal_connect(renderer, "edited",
    	        G_CALLBACK(contact_cell_edited_cb), mainwindow);
    	column = gtk_tree_view_column_new_with_attributes(
    	        _("Firstname"), renderer, "text", CONTACT_FIRSTNAME_COLUMN, NULL);
    	gtk_tree_view_column_set_sort_column_id(column, CONTACT_FIRSTNAME_COLUMN);
    	gtk_tree_view_column_set_alignment(column, 0.5);
    	gtk_tree_view_column_set_min_width(column, 100);
    	gtk_tree_view_column_set_resizable(column, TRUE);
    	gtk_tree_view_column_set_sort_indicator(column, TRUE);
    	gtk_tree_view_append_column(GTK_TREE_VIEW(
    	            mainwindow->contact_list), column);
    	renderer = gtk_cell_renderer_text_new();
    	g_object_set(renderer, "editable", TRUE, NULL);
    	g_signal_connect(renderer, "edited",
    	        G_CALLBACK(contact_cell_edited_cb), mainwindow);
	} else {
    	column = gtk_tree_view_column_new_with_attributes(
    	        _("Firstname"), renderer, "text", CONTACT_FIRSTNAME_COLUMN, NULL);
    	gtk_tree_view_column_set_sort_column_id(column, CONTACT_FIRSTNAME_COLUMN);
    	gtk_tree_view_column_set_alignment(column, 0.5);
    	gtk_tree_view_column_set_min_width(column, 100);
    	gtk_tree_view_column_set_resizable(column, TRUE);
    	gtk_tree_view_column_set_sort_indicator(column, TRUE);
    	gtk_tree_view_append_column(GTK_TREE_VIEW(
    	            mainwindow->contact_list), column);
    	renderer = gtk_cell_renderer_text_new();
    	g_object_set(renderer, "editable", TRUE, NULL);
    	g_signal_connect(renderer, "edited",
    	        G_CALLBACK(contact_cell_edited_cb), mainwindow);
    	column = gtk_tree_view_column_new_with_attributes(
    	        _("Lastname"), renderer, "text", CONTACT_LASTNAME_COLUMN, NULL);
    	gtk_tree_view_column_set_sort_column_id(column, CONTACT_LASTNAME_COLUMN);
    	gtk_tree_view_column_set_alignment(column, 0.5);
    	gtk_tree_view_column_set_min_width(column, 100);
    	gtk_tree_view_column_set_resizable(column, TRUE);
    	gtk_tree_view_column_set_sort_indicator(column, TRUE);
    	gtk_tree_view_append_column(GTK_TREE_VIEW(
    	            mainwindow->contact_list), column);
    	renderer = gtk_cell_renderer_text_new();
    	g_object_set(renderer, "editable", TRUE, NULL);
    	g_signal_connect(renderer, "edited",
    	        G_CALLBACK(contact_cell_edited_cb), mainwindow);
	}
    column = gtk_tree_view_column_new_with_attributes(
            _("Email"), renderer, "text", CONTACT_EMAIL_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column, CONTACT_EMAIL_COLUMN);
    gtk_tree_view_column_set_alignment(column, 0.5);
    gtk_tree_view_column_set_resizable(column, TRUE);
    gtk_tree_view_column_set_sort_indicator(column, TRUE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(
                mainwindow->contact_list), column);
    gtk_container_add(
            GTK_CONTAINER(mainwindow->right_scroll), mainwindow->contact_list);
    /* Right side of pane end*/

    mainwindow->action_box = gtk_hbutton_box_new();
    gtk_button_box_set_layout(GTK_BUTTON_BOX(mainwindow->action_box), GTK_BUTTONBOX_END);
    gtk_box_set_spacing(GTK_BOX(mainwindow->action_box), 5);

    mainwindow->to_btn = gtk_button_new_with_label(_("To:"));
    g_signal_connect(mainwindow->to_btn, "clicked",
            G_CALLBACK(contact_add_to_cb), mainwindow);
    gtk_widget_set_tooltip_text(mainwindow->to_btn,
            _("Add contact to To:"));

    mainwindow->cc_btn = gtk_button_new_with_label(_("Cc:"));
    g_signal_connect(mainwindow->cc_btn, "clicked",
            G_CALLBACK(contact_add_cc_cb), mainwindow);
    gtk_widget_set_tooltip_text(mainwindow->cc_btn,
            _("Add contact to Cc:"));

    mainwindow->bcc_btn = gtk_button_new_with_label(_("Bcc:"));
    g_signal_connect(mainwindow->bcc_btn, "clicked",
            G_CALLBACK(contact_add_bcc_cb), mainwindow);
    gtk_widget_set_tooltip_text(mainwindow->bcc_btn,
            _("Add contact to Bcc:"));

    mainwindow->new_btn = gtk_button_new_from_stock(GTK_STOCK_NEW);
    g_signal_connect(mainwindow->new_btn, "clicked",
            G_CALLBACK(contact_new_cb), mainwindow);
    gtk_widget_set_tooltip_text(mainwindow->new_btn,
            _("Add a new contact"));

    mainwindow->adv_search_btn = gtk_button_new_with_mnemonic(_("_Locate"));
    g_signal_connect(mainwindow->adv_search_btn, "clicked",
            G_CALLBACK(contact_advanced_search_cb), mainwindow);
    gtk_widget_set_tooltip_text(mainwindow->adv_search_btn,
            _("Open advanced search dialog"));

    gtk_container_add(GTK_CONTAINER(mainwindow->action_box), mainwindow->to_btn);
    gtk_container_add(GTK_CONTAINER(mainwindow->action_box), mainwindow->cc_btn);
    gtk_container_add(GTK_CONTAINER(mainwindow->action_box), mainwindow->bcc_btn);
    gtk_container_add(GTK_CONTAINER(mainwindow->action_box), mainwindow->new_btn);
    gtk_container_add(GTK_CONTAINER(mainwindow->action_box), mainwindow->adv_search_btn);
    gtk_box_pack_start(GTK_BOX(vbox_main), mainwindow->action_box, FALSE, TRUE, 0);

    GtkWidget* hb = gtk_hbox_new(FALSE, 5);
    label = gtk_label_new(_("Search criteria")); 

    mainwindow->search_text = gtk_entry_new();
    gtk_entry_set_activates_default(GTK_ENTRY(mainwindow->search_text), TRUE);
    gtk_widget_set_tooltip_text(mainwindow->search_text, 
            _("Seach using term for contact in current address book.\n"
              "Search is done in 'Display name', 'Firstname',\n"
              "'Lastname', 'Nick name', 'Alias', and 'Email'.\n"
              "Use [Locate] for a more fine grained control.\n"
              "Use * or ? for wildcard. Search is case insensitive."));
    GTK_WIDGET_SET_FLAGS (mainwindow->search_text, GTK_CAN_DEFAULT);
	g_signal_connect(mainwindow->search_text, "key-release-event",
			G_CALLBACK(contact_clear_sensitivity_cb), mainwindow);

    mainwindow->search_btn = gtk_button_new_from_stock(GTK_STOCK_EXECUTE);
    g_signal_connect(mainwindow->search_btn, "clicked",
            G_CALLBACK(contact_basic_search_cb), mainwindow);
    gtk_widget_set_tooltip_text(mainwindow->search_btn,
            _("Search contacts matching search criteria"));

	mainwindow->clear_btn = gtk_button_new_from_stock(GTK_STOCK_CLEAR);
	gtk_button_set_label(GTK_BUTTON(mainwindow->clear_btn), _("Clea_r"));
	gtk_button_set_image(GTK_BUTTON(mainwindow->clear_btn),
		gtk_image_new_from_stock(GTK_STOCK_CLEAR, GTK_ICON_SIZE_BUTTON));
    gtk_widget_set_tooltip_text(mainwindow->clear_btn,
            _("Clear any active search criteria and restore default view"));
	g_signal_connect(mainwindow->clear_btn, "clicked",
			G_CALLBACK(contact_clear_search_cb), mainwindow);

    gtk_box_pack_start(GTK_BOX(hb), label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hb), mainwindow->search_text, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(hb), mainwindow->search_btn, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hb), mainwindow->clear_btn, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox_main), hb, FALSE, TRUE, 0);

    return vbox_main;
}

static void create_statusbar(MainWindow* mainwindow) {
    mainwindow->statusbar = gtk_statusbar_new();
    set_status_msg(mainwindow, STATUS_MSG_NO_BOOK, NULL);
}

static void mainwindow_create(MainWindow* mainwindow) {
    GtkWidget* window;
    
    window = g_object_new(
            GTK_TYPE_WINDOW,
            "title", _("Claws-mail Address Book"),
            "default-width", INITIAL_WIDTH,
            "default-height", INITIAL_HEIGHT,
            "window-position", GTK_WIN_POS_CENTER,
            NULL);
    mainwindow->window = window;
    
    GtkWidget* vbox1 = gtk_vbox_new(FALSE, 0);

    create_menu(mainwindow);
    gtk_box_pack_start(GTK_BOX(vbox1), mainwindow->menu, FALSE, FALSE, 0);

    create_toolbar(mainwindow);
    gtk_box_pack_start(GTK_BOX(vbox1), mainwindow->toolbar, FALSE, FALSE, 0);

    GtkWidget* vbox2 = create_view(mainwindow);
    gtk_container_set_border_width(GTK_CONTAINER(vbox2), 1);
    gtk_box_pack_start(GTK_BOX(vbox1), vbox2, TRUE, TRUE, 5);

    create_statusbar(mainwindow);
    gtk_box_pack_start(GTK_BOX(vbox1), mainwindow->statusbar, FALSE, FALSE, 0);

    gtk_container_add(GTK_CONTAINER(window), vbox1);
    gtk_widget_grab_default(mainwindow->search_text);

    gtk_widget_set_sensitive(mainwindow->to_btn, FALSE);
    gtk_widget_set_sensitive(mainwindow->cc_btn, FALSE);
    gtk_widget_set_sensitive(mainwindow->bcc_btn, FALSE);
    gtk_widget_set_sensitive(mainwindow->adv_search_btn, FALSE);
    gtk_widget_set_sensitive(mainwindow->new_btn, FALSE);
    gtk_widget_set_sensitive(mainwindow->clear_btn, FALSE);
    GTK_WIDGET_SET_FLAGS(mainwindow->search_btn, GTK_CAN_DEFAULT);
    gtk_window_set_default(
            GTK_WINDOW(window), mainwindow->search_btn);
    g_signal_connect(
        window, "delete_event", G_CALLBACK(delete_event), mainwindow);
    g_signal_connect(window, "key-press-event",
        G_CALLBACK(mainwindow_key_press_event_cb), mainwindow);
}

static GList* load_pixmaps() {
    GError* err = NULL;
    GList* list = NULL;
    const gchar** icons = PIXMAPS;
    
    while (*icons) {
        GdkPixbuf* pic = gdk_pixbuf_new_from_file(*icons++, &err);
        if (pic) {
            list = g_list_prepend(list, pic);
        }
        g_clear_error(&err);
        err = NULL;
    }
    if (err)
        g_clear_error(&err);

    return list;
}

void set_status_msg(MainWindow* mainwindow, STATUS id, const gchar* extra) {
    GtkStatusbar* status = GTK_STATUSBAR(mainwindow->statusbar);
    GString* msg;

    gtk_statusbar_pop(status, current_status);
    msg = g_string_new(STATUS_MSG[id]);
    if (extra)
        msg = g_string_append(msg, extra);
    current_status = gtk_statusbar_push(status, id, msg->str);
    g_string_free(msg, TRUE);
}

void application_start(gboolean compose, gboolean no_extensions) {
    MainWindow* mainwindow = g_new0(MainWindow, 1);
    gchar* error = NULL;
    
    mainwindow->compose_mode = compose;
    mainwindow->use_extensions = ! no_extensions;
    mainwindow_create(mainwindow);

    if (mainwindow->use_extensions) {
        init_hooks(mainwindow);
        load_extensions(&error);
        if (error) {
            show_message(mainwindow->window, GTK_UTIL_MESSAGE_WARNING, "%s", error);
            g_free(error);
            error = NULL;
        }
    }

    GList* pixmaps = load_pixmaps();
    gtk_window_set_default_icon_list(pixmaps);
    g_list_free(pixmaps);

   	show_progress_dialog(mainwindow);
    gtk_main();
}
