/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <gcrypt.h>
#include <stdlib.h>
#include <locale.h>

#include "utils.h"
#include "gtk-utils.h"
#include "plugin.h"
#include "plugin-loader.h"
#include "dbus-service.h"
#include "dbus-contact.h"

/*
typedef struct {
	GHashTable*	hash;
	gboolean	is_and;
	gboolean	equal;
} CompareHashData;

typedef struct {
	GSList*		list;
	gboolean	is_and;
	gboolean	equal;
} CompareSListData;
*/

typedef struct {
	size_t		size;
	guchar*		block;
} NormBlock;

#define ERRBUFSIZ 255

/* Forward declarations */
static void config_close(ConfigFile* config, gchar** error);
static void config_open(ConfigFile* config, gchar** error);

static gboolean debug_mode = FALSE;
static gchar self_home[] = ".contact";
static gchar configrc[] = "contactrc";
static gchar log_file[] = "contacts.log";
static int log_file_fd = 0;

//static GKeyFile* key_file = NULL;
//static gchar* key_file_path = NULL;

static void hash_table_copy_entry(gpointer key,
								  gpointer value,
								  gpointer user_data) {
    GHashTable* hash = (GHashTable *) user_data;

	debug_print("Key: %s - Value: %p\n", key, value);
    g_hash_table_replace(hash, g_strdup(key), attrib_def_copy(value));
}

/*
static gboolean hash_table_compare_each(gpointer key,
										gpointer value,
								  		gpointer data) {
    CompareHashData* chd = (CompareHashData *) data;
    gchar* cvalue;
    
    cvalue = g_hash_table_lookup(chd->hash, key);
    
    if (cvalue || (!cvalue && !chd->is_and)) {
		if (!cvalue)
			chd->equal = FALSE;
		else
			chd->equal = g_utf8_collate(cvalue, value) != 0;
	}
	else
		chd->equal = TRUE;
	
	return chd->equal;
}

static void gslist_compare_each(gpointer email, gpointer data) {
	CompareSListData* csd = (CompareSListData *) data;
	GSList *cur;
	Email* a = (Email *) email;	
	
	for (cur = csd->list; cur; cur = g_slist_next(cur)) {
		Email* b = (Email *) cur->data;
		csd->equal = g_utf8_collate(a->alias, b->alias);
		if (!csd->equal && csd->is_and)
			continue;
		csd->equal = g_utf8_collate(a->email, b->email);
		if (!csd->equal && csd->is_and)
			continue;
		csd->equal = g_utf8_collate(a->remarks, b->remarks);
		if (!csd->equal && csd->is_and)
			continue;
		
	}
	
	csd->equal = FALSE;
}
*/

static int open_log_file(const gchar* path) {
	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	gchar errbuf[ERRBUFSIZ];

	if (log_file_fd > 0)
		return log_file_fd;
		
	if (! g_file_test(path, G_FILE_TEST_EXISTS))
		log_file_fd = open(path, O_WRONLY | O_CREAT | O_NONBLOCK, mode);
	else
		log_file_fd = open(path, O_WRONLY | O_APPEND | O_NONBLOCK, mode);

	if (log_file_fd < 0) {
		strerror_r(errno, errbuf, ERRBUFSIZ);
		g_warning("%s: %s", path, errbuf);
	}
	
	return log_file_fd;
}

static void claws_contacts_log_handler(const gchar *log_domain,
									   GLogLevelFlags log_level,
									   const gchar *message,
									   gpointer data) {
	gchar* log_name;
	gchar* msg = NULL;
	
	gchar* log_dir = get_self_home();
	log_name = g_strconcat(log_dir, G_DIR_SEPARATOR_S, log_file, NULL);
	//debug_print("Logging to %s\n", log_name);
	g_free(log_dir);
	
	if (message[strlen(message) - 1] == '\n' ||
		message[strlen(message) - 1] == ' ')
		msg = g_strconcat(message, NULL);
	else
		msg = g_strconcat(message, "\n", NULL);
	
	int fd = open_log_file(log_name);
	if (fd > 0)
		write(fd, msg, strlen(msg));
	
	g_free(msg);
	g_free(log_name);
}

void close_log_file() {
	if (log_file_fd > 0)
		close(log_file_fd);
}

const char * debug_srcname(const char *file) {
	const char *s = strrchr (file, '/');
	return s? s+1:file;
}

gboolean debug_get_mode() {
    return debug_mode;
}

void debug_set_mode(gboolean mode) {
    debug_mode = mode;
}

void debug_print_real(const gchar* format, ...) {
	va_list args;
	gchar buf[BUFSIZ];

	if (!debug_mode) return;

	va_start(args, format);
	g_vsnprintf(buf, sizeof(buf), format, args);
	va_end(args);

	if (DAEMON)
		g_message("%s", buf);
	else
		g_print("%s", buf);
}

const gchar *conv_get_current_locale(void)
{
	const gchar *cur_locale;

#ifdef G_OS_WIN32
	cur_locale = g_win32_getlocale();
#else
	cur_locale = g_getenv("LC_ALL");
	if (!cur_locale) cur_locale = g_getenv("LC_CTYPE");
	if (!cur_locale) cur_locale = g_getenv("LANG");
	if (!cur_locale) cur_locale = setlocale(LC_CTYPE, NULL);
#endif /* G_OS_WIN32 */

	debug_print("current locale: %s\n",
		    cur_locale ? cur_locale : "(none)");

	return cur_locale;
}

const gchar* get_home() {
	const char *homedir = g_getenv("HOME");
 	if (!homedir)
    	homedir = g_get_home_dir();
    	
    return homedir;
}

gchar* get_self_home() {
	return g_strconcat(get_home(), G_DIR_SEPARATOR_S, self_home, NULL);
}

void gslist_free(GSList** list, void (*elem_free) (gpointer data)) {
	GSList* cur;
	
	if (!list || !*list)
		return;

	if (elem_free) {
		for (cur = *list; cur; cur = g_slist_next(cur)) {
			elem_free(cur->data);
			cur->data = NULL;
		}
	}
	g_slist_free(*list);
	*list = NULL;
}

void glist_free(GList** list, void (*elem_free) (gpointer data)) {
	GList* cur;
	
	if (!list || !*list)
		return;

	if (elem_free) {
		for (cur = *list; cur; cur = g_list_next(cur)) {
			elem_free(cur->data);
			cur->data = NULL;
		}
	}
	g_list_free(*list);
	*list = NULL;
}

void hash_table_free(GHashTable** hash_table) {
	cm_return_if_fail(hash_table != NULL);

	if (*hash_table == NULL)
		return;
	
	debug_print("Destroying hashtable [%p]\n", *hash_table);	
	g_hash_table_destroy(*hash_table);
	*hash_table = NULL;
	
}

GHashTable* hash_table_new(void) {
	GHashTable* hash_table;
	
	hash_table = g_hash_table_new_full(
			g_str_hash, g_str_equal, g_free, attrib_def_free);
		
	return hash_table;
}

GHashTable * hash_table_copy(GHashTable* hash) {
    GHashTable* copied_hash;
    
    copied_hash = hash_table_new();
    if (hash)
    	g_hash_table_foreach(hash, hash_table_copy_entry, copied_hash);

    return copied_hash;
}

gboolean create_file(const gchar* path, gchar** error) {
	gboolean fail = FALSE;
	
	cm_return_val_if_fail(path != NULL, FALSE);

	debug_print("Creating: %s\n", path);
	mode_t mode = umask(0);
	if (g_mkdir(path, 0700) < 0) {
		*error = g_strconcat(path, ": ", N_("Could not create"), NULL);
		fail = TRUE;
	}
	umask(mode);
	
	return fail;
}

gboolean first_time(const gchar* subpath, const gchar* config, gchar** error) {
	const gchar* homedir;
	gchar *home, *config_file;
	gboolean first = TRUE;
	
	homedir = get_home();
	if (! homedir) {
		*error = g_strdup(N_("Cannot find home directory"));
		return FALSE;
	}
	homedir = get_self_home();
	
	if (subpath)
		home = g_strconcat(homedir, G_DIR_SEPARATOR_S, subpath, NULL);
	else
		home = g_strdup(homedir);
	
	debug_print("Exists: %s\n", home);
	if (! g_file_test(home, G_FILE_TEST_IS_DIR)) {
		if (g_file_test(home, G_FILE_TEST_IS_REGULAR)) {
			*error = g_strconcat(home, ": ", N_("File exists"), NULL);
			g_free(home);
			return FALSE;
		}
		if (create_file(home, error)) {
			g_free(home);
			return FALSE;
		}
		first = TRUE;
	}
	else {
		if (config)
			config_file = g_strconcat(home, G_DIR_SEPARATOR_S, config, NULL);
		else
			config_file = g_strconcat(home, G_DIR_SEPARATOR_S, configrc, NULL);
		debug_print("Exists: %s\n", config_file);
		if (g_file_test(config_file, G_FILE_TEST_EXISTS))
			first = FALSE;
		else
			first = TRUE;
		g_free(config_file);
	}

	g_free(home);
	
	return first;
}

void attrib_def_free(gpointer attrdef) {
	AttribDef* a = (AttribDef *) attrdef;
	
	if (! a)
		return;
		
	if (a->attrib_name) {
		g_free(a->attrib_name);
		a->attrib_name = NULL;
	}
	if (a->type == ATTRIB_TYPE_STRING && a->value.string) {
		g_free(a->value.string);
		a->value.string = NULL;
	}
	g_free(a);
	a = NULL;
}

gpointer attrib_def_copy(gpointer a) {
	AttribDef* a1;
	AttribDef* a2;
	
	if (!a)
		return NULL;

	a1 = g_new0(AttribDef, 1);
	a2 = (AttribDef *) a;
		
	if (a2->attrib_name)
		a1->attrib_name = g_strdup(a2->attrib_name);
	a1->type = a2->type;
	if (a2->type == ATTRIB_TYPE_STRING)
		a1->value.string = g_strdup(a2->value.string);
	else
		a1->value = a2->value;
	
	return (gpointer) a1;
}

void hash_table_keys_to_slist(gpointer key, gpointer value, gpointer slist) {
	GSList** list = (GSList **) slist;
	AttribDef* attrdef;

	cm_return_if_fail(slist != NULL);
	
	attrdef = g_new0(AttribDef, 1);
	attrdef->attrib_name = g_strdup((gchar *) key);
	attrdef->type = ATTRIB_TYPE_STRING;
	
	*list = g_slist_prepend(*list, attrdef);
}

void plugin_config_set(ConfigFile* config, gchar** error) {
	gchar** list;
	int num;
	ConfiguredBooks* cf_books;
	ClosedBooks* cl_books;

	cm_return_if_fail(config != NULL);
	
	if (! config->key_file) {
		config_open(config, error);
		if (*error)
			return;
	}
	if (config->comment) {
		g_key_file_set_comment(config->key_file, NULL, NULL, config->comment, NULL);
	}
	
	cf_books = config->configured_books;
	cl_books = config->closed_books;
	
	if (cf_books && cf_books->group) {
		if (cf_books->books) {
			list = gslist_to_array(cf_books->books, &num);
			g_key_file_set_string_list(config->key_file, cf_books->group,
				"books", (const gchar* const *) list, num);
			g_strfreev(list);
		}
		else {
			g_key_file_set_string(config->key_file, cf_books->group,
				"books", "");
		}
	}
	if (cl_books && cl_books->group) {
		if (cl_books->books) {
			list = gslist_to_array(cl_books->books, &num);
			g_key_file_set_string_list(config->key_file, cl_books->group,
				"books", (const gchar* const *) list, num);
			g_strfreev(list);
		}
		else {
			g_key_file_set_string(config->key_file, cl_books->group,
				"books", "");
		}
	}
}

ConfigFile* plugin_config_new(const gchar* path) {
	ConfigFile* config;
	
	config = g_new0(ConfigFile, 1);
	config->configured_books = g_new0(ConfiguredBooks, 1);
	config->closed_books = g_new0(ClosedBooks, 1);
	if (path)
		config->path = g_strdup(path);
		
	return config;
}

void config_get(ConfigFile* config, gchar** error) {
	ConfiguredBooks* cf_books;
	ClosedBooks* cl_books;
	GError* err = NULL;
	gchar** str;
	gsize len = 0;
	gsize num;

	cm_return_if_fail(config != NULL);
	
	if (! config->key_file) {
		if (! config->path) {
			*error = g_strdup(N_("Missing path"));
			return;
		}
		config_open(config, error);
		if (*error)
			return;
	}
	else {
		if (config->path) {
			config_open(config, error);
			if (*error)
				return;
		}
	}
	
	cf_books = config->configured_books;
	cl_books = config->closed_books;

	config->comment = g_key_file_get_comment(config->key_file, NULL, NULL, &err);
	cf_books->group = g_strdup("configured address books");
	str = g_key_file_get_string_list(config->key_file,
		cf_books->group, "books", &len, &err);
	for (num = 0; num < len; num++) {
		cf_books->books = g_slist_prepend(cf_books->books, g_strdup(str[num]));
	}
	g_clear_error(&err);
	g_strfreev(str);
	
	cl_books->group = g_strdup("closed address books");
	str = g_key_file_get_string_list(config->key_file,
		cl_books->group, "books", &len, &err);
	for (num = 0; num < len; num++) {
		cl_books->books = g_slist_prepend(cl_books->books, g_strdup(str[num]));
	}
	g_clear_error(&err);
	g_strfreev(str);
}

static void config_close(ConfigFile* config, gchar** error) {
	gchar *data, *dir;
	GError* err = NULL;
	FILE* fp;
	gsize len;
		
	if (config->key_file) {
		data = g_key_file_to_data(config->key_file, &len, &err);
		if (err) {
			if (error)
				*error = g_strdup(err->message);
			g_clear_error(&err);
		}
		else {
			dir = g_path_get_dirname(config->path);
			if (strcmp(".", dir) != 0) {
				if (!g_file_test(dir, G_FILE_TEST_EXISTS)) {
					if (g_mkdir(dir, 0700)) {
						*error = g_strconcat(dir, ": Could not create", NULL);
						goto end;
					}
				}
			}
			g_free(dir);
			if (g_file_test(config->path, G_FILE_TEST_EXISTS)) {
				gchar* old = g_strconcat(config->path, ".bak", NULL);
				g_rename(config->path, old);
				g_free(old);
			}
			fp = g_fopen(config->path, "w");
			if (fp) {
				fwrite(data, len, 1, fp);
				fclose(fp);
			}
			else
				*error = g_strconcat(config->path, ": Could not create", NULL);
		}
end:
		g_free(data);
		g_key_file_free(config->key_file);
		config->key_file = NULL;
	}
}

static void config_open(ConfigFile* config, gchar** error) {
	GError* err = NULL;
	
	if (config->key_file) {
		config_close(config, error);
		if (*error)
			return;
	}
	config->key_file = g_key_file_new();
	
	if (g_file_test(config->path, G_FILE_TEST_EXISTS)) {
		g_key_file_load_from_file(
			config->key_file, config->path, G_KEY_FILE_KEEP_COMMENTS, &err);
		if (err && err->code > 1) {
			config_close(config, error);
			g_free(*error);
			if (error)
				*error = g_strdup(err->message);
			g_clear_error(&err);
			return;
		}
	}
}

gboolean config_get_value(ConfigFile* config, const gchar* group,
						  const gchar* key, GSList** values) {
	GError* err = NULL;
	gchar** str;
	gsize len;
	gsize num;

	cm_return_val_if_fail(config != NULL, FALSE);

	if (! config->key_file || ! group || ! key) {
		return FALSE;
	}
	
	str = g_key_file_get_string_list(config->key_file, group, key, &len, &err); 
	for (num = 0; num < len; num++) {
		*values = g_slist_prepend(*values, g_strdup(str[num]));
	}
	g_strfreev(str);
			
	return (len > 0);
}

gboolean config_set_value(ConfigFile* config, const gchar* group,
							   const gchar* key, GSList* values) {
	gchar** list;
	int num;

	cm_return_val_if_fail(config != NULL, FALSE);
	
	if (! config->key_file || ! group || ! key) {
		return FALSE;
	}

	list = gslist_to_array(values, &num);
	g_key_file_set_string_list(config->key_file, group, key,
			(const gchar* const *) list, num);
	g_strfreev(list);

	return TRUE;
}

void plugin_config_free(ConfigFile** config_file) {
	ConfigFile* config;
	ConfiguredBooks* cf_books;
	ClosedBooks* cl_books;
	
	if (! config_file || ! *config_file)
		return;
	
	config = *config_file;
	cf_books = config->configured_books;
	cl_books = config->closed_books;

	g_free(config->comment);
	if (cf_books) {
		g_free(cf_books->group);
		gslist_free(&cf_books->books, g_free);
		g_free(cf_books);
		cf_books = NULL;
	}
	if (cl_books) {
		g_free(cl_books->group);
		gslist_free(&cl_books->books, g_free);
		g_free(cl_books);
		cl_books = NULL;
	}
	config_close(config, NULL);
	g_free(config->path);

	g_free(*config_file);
	*config_file = NULL;
}

gchar** gslist_to_array(const GSList* list, gint* len) {
	GSList* cur;
	gchar **array = NULL, *str;
	int num = 0;

	for (cur = (GSList *) list; cur; cur = g_slist_next(cur), num++) {
		str = (gchar *) cur->data;
		array = g_renew(gchar *, array, num + 1);
		array[num] = g_memdup(str, strlen(str) + 1);
	}

	array = g_renew(gchar *, array, num + 1);
	array[num] = NULL;
	
	if (len)
		*len = num;
		
	return array;
}

void email_free(gpointer email) {
	Email* mail = (Email *) email;
	if (! mail)
		return;

	debug_print("Destroying Email [%p]\n", mail);
	g_free(mail->alias);
	g_free(mail->email);
	g_free(mail->remarks);
	g_free(mail);
}

Email* email_copy(Email* email) {
	Email* new_email = NULL;

	if (! email)
		return new_email;

	new_email = g_new0(Email, 1);
	
	new_email->alias = g_strdup(email->alias);
	new_email->email = g_strdup(email->email);
	new_email->remarks = g_strdup(email->remarks);
	
	return new_email;
}

/*
gboolean email_compare(gpointer comparedata, GSList* a, GSList* b) {
	gboolean equal = FALSE;
	CompareSListData* csd = (CompareSListData *) comparedata;
	
	csd->list = b;
	g_slist_foreach(a, gslist_compare_each, csd);
	equal = csd->equal;
	
	return equal;
}
*/

void contact_free(gpointer contact) {
	Contact* c = (Contact *) contact;
	if (! c)
		return;
	
	if (debug_get_mode())
		contact_dump(c, stderr);	
	hash_table_free(&c->data);
	gslist_free(&c->emails, email_free);
}

AddressBook* address_book_get(Plugin* plugin, const gchar* name) {
	GSList *books, *cur;
	AddressBook* abook = NULL;
	
	if (name == NULL || plugin == NULL)
		return NULL;
	
	books = plugin->addrbook_all_get();
	for (cur = books; cur; cur = g_slist_next(cur)) {
		abook = (AddressBook *) cur->data;
		if (abook->abook_name && strcmp(abook->abook_name, name) == 0)
			break;
		abook = NULL;
	}
	
	gslist_free(&books, NULL);
	
	return abook;
}

/*
gboolean contact_compare(Contact* a, Contact* b, gboolean is_and) {
	CompareHashData* chd = g_new0(CompareHashData, 1);
	CompareSListData* csd = g_new0(CompareSListData, 1);
	gboolean equal = FALSE;
	
	chd->hash	= b->data;
	chd->is_and	= is_and;
	csd->is_and = is_and;

	contact_dump(a, stderr);
	contact_dump(b, stderr);
	gchar* found = g_hash_table_find(a->data, hash_table_compare_each, chd);
	
	if (found && !is_and) {
		chd->equal = email_compare(csd, a->emails, b->emails);
	}
	else {
		if (found)
			chd->equal = FALSE;
		else
			chd->equal = TRUE;
	}
		
	equal = chd->equal;
	g_free(chd);
	g_free(csd);
	
	return equal;
}
*/

gboolean compare_attrib(const AttribDef* a, const AttribDef* b, gint type) {
	gboolean result = FALSE;
	gint res;
	AttribType ctype;

	cm_return_val_if_fail(a != NULL, FALSE);
	cm_return_val_if_fail(b != NULL, FALSE);
	
	if (type >= 0)
		ctype = type;
	else
		ctype = a->type;
		
	if (a->type != ctype || b->type != ctype)
		return result;

	switch (ctype) {
		case ATTRIB_TYPE_BOOLEAN:
			result = a->value.boolean == b->value.boolean;
			break;
		case ATTRIB_TYPE_INT:
			result = a->value.number == b->value.number;
			break;
		case ATTRIB_TYPE_CHAR:
			result = a->value.character == b->value.character;
			break;
		case ATTRIB_TYPE_STRING:
			res = g_utf8_collate(a->value.string, b->value.string);
			result = (res == 0);
	}
	
	return result;
}

gboolean contact_compare_attrib(Contact* a, Contact* b, const AttribDef* compare) {
	gboolean result = FALSE;
	AttribDef *a_id, *b_id;
	//gint res;
	
	if (!a || !a->data || !b || !b->data || !compare || !compare->attrib_name)
		return result;
		
	a_id = (AttribDef *) g_hash_table_lookup(a->data, compare->attrib_name);
	b_id = (AttribDef *) g_hash_table_lookup(b->data, compare->attrib_name);
	if (!a_id || !b_id)
		return result;

/*	
	switch (compare->type) {
		case ATTRIB_TYPE_BOOLEAN:
			result = a_id->value.boolean == b_id->value.boolean;
			break;
		case ATTRIB_TYPE_INT:
			result = a_id->value.number == b_id->value.number;
			break;
		case ATTRIB_TYPE_CHAR:
			result = a_id->value.character == b_id->value.character;
			break;
		case ATTRIB_TYPE_STRING:
			res = g_utf8_collate(a_id->value.string, b_id->value.string);
			result = (res == 0);
	}
*/
	result = compare_attrib(a_id, b_id, compare->type);
		
	return result;
}

void address_book_contacts_free(AddressBook* address_book) {
	GList* cur;

	if (! address_book)
		return;
		
	for (cur = address_book->contacts; cur; cur = g_list_next(cur)) {
		Contact* c = (Contact *) cur->data;
		contact_free(c);
		g_free(c);
	}
	g_list_free(address_book->contacts);
	address_book->contacts = NULL;
	address_book->dirty = TRUE;
}

void address_book_free(AddressBook** address_book) {
	AddressBook* a;
	
	if (! address_book || ! *address_book)
		return;
	
	a  = *address_book;
	g_free(a->abook_name);
	g_free(a->URL);
	g_free(a->username);
	g_free(a->password);
	address_book_contacts_free(a);
	gslist_free(&a->extra_config, extra_config_free);
	*address_book = NULL;
}

AddressBook* address_book_new() {
	AddressBook* abook;
	
	abook = g_new0(AddressBook, 1);
	abook->next_uid = 1;
	abook->dirty = TRUE;
	
	return abook;
}

Contact* contact_copy(Contact* contact) {
	Contact* new = NULL;
	GSList* cur;
	
	if (! contact)
		return new;
		
	new = contact_new();
	if (contact->data) {
		hash_table_free(&new->data);
		new->data = hash_table_copy(contact->data);
	}
	
	for (cur = contact->emails; cur; cur = g_slist_next(cur)) {
		Email* a = (Email *) cur->data;
		if (a) {
			Email* b = email_copy(a);
			if (b)
				new->emails = g_slist_append(new->emails, b);
		}
	}
	
	return new;
}

Contact* contact_new() {
	Contact* contact;
	
	contact = g_new0(Contact, 1);
	contact->data = hash_table_new();
	
	return contact;	
}

AddressBook* address_book_copy(AddressBook* a, gboolean deep) {
	AddressBook* b = g_new0(AddressBook, 1);
	GList* cur;

	cm_return_val_if_fail(a != NULL, NULL);
	
	b->abook_name = g_strdup(a->abook_name);
	b->URL = g_strdup(a->URL);
	b->username = g_strdup(a->username);
	b->password = g_strdup(a->password);
	b->next_uid = a->next_uid;
	b->dirty = a->dirty;

	if (deep) {
		for (cur = a->contacts; cur; cur = g_list_next(cur)) {
			b->contacts = g_list_prepend(
				b->contacts, contact_copy((Contact* ) cur->data));
		}
	}
	else
		b->contacts = g_list_copy(a->contacts);

	b->extra_config = g_slist_reverse(gslist_deep_copy(
		a->extra_config, extra_config_copy));
	
	debug_print("from: %d contacts to: %d contacts\n",
		g_list_length(a->contacts), g_list_length(b->contacts));
	return b;
}

void contact_data_print(gpointer key, gpointer value, gpointer data) {
	FILE* f = (FILE *) data;
	AttribDef* attr = (AttribDef *) value;
	gchar* val = NULL;
	
	if (attr) {
		switch (attr->type) {
			case ATTRIB_TYPE_BOOLEAN:
				val = (attr->value.boolean) ? g_strdup("true") : g_strdup("false");
				break;
			case ATTRIB_TYPE_INT:
				val = g_strdup_printf("%i", attr->value.number);
				break;
			case ATTRIB_TYPE_CHAR:
				val = g_strdup_printf("%c", attr->value.character);
				break;
			case ATTRIB_TYPE_STRING:
				val = g_strdup(attr->value.string);
				break;
		}
	}
		
	if (f)
		fprintf(f, "Key: %s - Value: %s\n", (gchar *) key, val);
	else
		g_message("Key: %s - Value: %s", (gchar *) key, val);
	
	g_free(val);
}

void contact_dump(const Contact* contact, FILE* f) {
	GSList* cur;

	cm_return_if_fail(contact != NULL);
	
	if (f)
		fprintf(f, "Contact data\n");
	else
		g_message("Contact data\n");
	g_hash_table_foreach(contact->data, contact_data_print, f);
	
	if (f)
		fprintf(f, "Email addresses\n");
	else
		g_message("Email addresses\n");
	for (cur = contact->emails; cur; cur = g_slist_next(cur)) {
		Email* e = (Email *) cur->data;
		email_dump(e, f);
	}
}

void email_dump(Email* email, FILE* f) {
	cm_return_if_fail(email != NULL);

	if (f)
		fprintf(f, "Alias: %s Email: %s Remarks: %s\n",
			email->alias, email->email, email->remarks);
	else
		g_message("Alias: %s Email: %s Remarks: %s\n",
			email->alias, email->email, email->remarks);
}

gboolean xor(const gchar* a, const gchar* b) {
	if (!a && !b)
		return FALSE;
	else if (a && b) {
		if (strlen(a) == 0 && strlen(b) == 0)
			return FALSE;
		return (g_utf8_collate(a, b) != 0);
	}
	else {
		if ((a && strlen(a) == 0 && !b ) ||
			(b && strlen(b) == 0 && !a ))
			return FALSE;
		return TRUE;
	}
}

gchar* base64_encode_data(const gchar* path) {
	gchar* contents;
	gchar* base64;
	gsize length;
	
	cm_return_val_if_fail(path != NULL, NULL);

	g_file_get_contents(path, &contents, &length, NULL);
	base64 = g_base64_encode((const guchar *) contents, length);
	g_free(contents);
	
	return base64;
}

guint set_log_handler() {
	guint id;
	
	id = g_log_set_handler(G_LOG_DOMAIN, G_LOG_LEVEL_MASK |
			G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION,
			claws_contacts_log_handler, NULL);

    return id;
}

void g_value_email_free(gpointer data) {
#if GLIB_CHECK_VERSION(2,32,0)
	GArray* email = g_array_sized_new(FALSE, FALSE, sizeof(GValue), sizeof(data));
#else
	GValueArray* email = (GValueArray *) data;
#endif
	GValue* email_member;
	guint i;
	
	if (! email)
		return;
	
#if GLIB_CHECK_VERSION(2,32,0)
	for (i = 0; i < email->len; i++) {
		email_member = &g_array_index(email, GValue, i);
#else
	for (i = 0; i < email->n_values; i++) {
		email_member = g_value_array_get_nth(email, i);
#endif
		g_value_unset(email_member);
	}
}

GPtrArray* g_value_email_new() {
	return g_ptr_array_new_with_free_func(g_value_email_free);
}

static void email_ptr_foreach(gpointer data, gpointer user_data) {
#if GLIB_CHECK_VERSION(2,32,0)
	GArray* email = g_array_sized_new(FALSE, FALSE, sizeof(GValue), sizeof(data));
#else
	GValueArray* email = (GValueArray *) data;
#endif
	GValue* email_member;
	guint i, j;
	FILE* f = (FILE *) user_data;
	gchar* text = NULL;
	
#if GLIB_CHECK_VERSION(2,32,0)
	for (i = j = 0; email && i < email->len; j++, i++) {
#else
	for (i = j = 0; email && i < email->n_values; j++, i++) {
#endif	
        if (j > 0 && j % 3 == 0) {
			if (f)
				fprintf(f, "---------------------------------------------\n");
			else
				g_message("---------------------------------------------\n");
            j = 0;
        }
#if GLIB_CHECK_VERSION(2,32,0)
		email_member = &g_array_index(email, GValue, i);
#else
		email_member = g_value_array_get_nth(email, i);
#endif
        switch (j) {
            case 0:
                text = g_strdup_printf("Alias: %s\n", g_value_get_string(email_member));
                break;
            case 1:
                text = g_strdup_printf("Email: %s\n", g_value_get_string(email_member));
                break;
            case 2:
                text = g_strdup_printf("Remarks: %s\n", g_value_get_string(email_member));
                break;
        }
        if (f)
        	fprintf(f, "%s", text);
        else
        	g_message("%s", text);
        g_free(text);
	}
}

void dbus_contact_print(DBusContact* contact, FILE* f) {
    GHashTableIter iter;
    gpointer key, value;
    GHashTable* data;

	cm_return_if_fail(contact != NULL);
    
	if (contact->data) {
		data = hash_table_new();
        g_hash_table_iter_init (&iter, contact->data);
        while (g_hash_table_iter_next (&iter, &key, &value)) {
            swap_data(data, key, value);
        }
        g_hash_table_foreach(data, contact_data_print, f);
        hash_table_free(&data);   
	}
	if (contact->emails)
		g_ptr_array_foreach(contact->emails, email_ptr_foreach, f);
}

gint utf8_collate(gchar* s1, gchar* s2) {
    gchar *a, *b;
    gint ret;
 
	if (! s1 && ! s2)
		return 0;
		
	if (! s1 && s2)
		return 1;
		
	if (s1 && ! s2)
		return -1;
   
    debug_print("s1: %s s2: %s\n", s1, s2);
    a = g_utf8_collate_key(s1, -1);
    b = g_utf8_collate_key(s2, -1);
    
    ret = strcmp(a, b);
    
    g_free(a);
    g_free(b);
    
    return ret;
}

gboolean attribute_supported(Plugin* plugin, const gchar* attribute) {
	GSList *attribs, *cur;
	gboolean found = FALSE;

	cm_return_val_if_fail(plugin != NULL, FALSE);
	
	attribs = plugin->attrib_list();
	
	for (cur= attribs; !found && cur; cur = g_slist_next(cur)) {
		AttribDef* def = (AttribDef *) cur->data;
		if (utf8_collate(def->attrib_name, (gchar *) attribute) == 0)
			found = TRUE;
	}
	gslist_free(&attribs, attrib_def_free);
	
	return found;
}

GSList* gslist_deep_copy(GSList* list, COPYFUNC copy_func) {
	GSList *new = NULL, *cur;
	
	if (!copy_func || !list)
		return new;
	
	for (cur = list; cur; cur = g_slist_next(cur)) {
		new = g_slist_prepend(new, copy_func(cur->data));
	}
	
	return new;
}

static void hash_print(gpointer key, gpointer value, gpointer data) {
	FILE* f = (FILE *) data;
	AttribDef* attr = (AttribDef *) value;
	const gchar* type = NULL;
	
	cm_return_if_fail(value != NULL);
	cm_return_if_fail(data != NULL);

	switch (attr->type) {
		case ATTRIB_TYPE_INT: type = "INT"; break;
		case ATTRIB_TYPE_CHAR: type = "CHAR"; break;
		case ATTRIB_TYPE_BOOLEAN: type = "BOOLEAN"; break;
		case ATTRIB_TYPE_STRING: type = "STRING"; break;
	}
	
	fprintf(f, "key: %s\n\tname: %s\n\ttype: %s\n",
		(gchar *) key, attr->attrib_name, type);
}

void hash_table_dump(GHashTable* hash, FILE* f) {
	if (!hash || !f)
		return;
		
	g_hash_table_foreach(hash, hash_print, (gpointer) f);
}

AttribDef* pack_data(AttribType type, const gchar* name, const void* value) {
	AttribDef* data;
	
	data = g_new0(AttribDef, 1);
	data->type = type;
	if (name)
		data->attrib_name = g_strdup(name);
	switch (type) {
		case ATTRIB_TYPE_INT:
			data->value.number = *(gint *) value;
			break;
		case ATTRIB_TYPE_BOOLEAN:
			data->value.boolean = *(gboolean *) value;
			break;
		case ATTRIB_TYPE_CHAR:
			data->value.character = *(char *) value;
			break;
		case ATTRIB_TYPE_STRING:
			data->value.string = g_strdup((const gchar *) value);
			break;
	}
	
	return data;
}

void extract_data(GHashTable* data, const gchar* key, void** value) {
	AttribDef* attr;
	
	if (! data || ! key || ! value)
		return;
		
	attr = g_hash_table_lookup(data, key);
	if (attr) {
		get_data(attr, value);
	}
	else
		*value = NULL;
}

void swap_data(GHashTable* data, const gchar* key, void* value) {
	AttribDef* attr;
	
	if (! data || ! key || ! value)
		return;
		
	attr = g_hash_table_lookup(data, key);

	if (attr) {
		switch (attr->type) {
			case ATTRIB_TYPE_INT:
				attr->value.number = *(gint *) value;
				break;
			case ATTRIB_TYPE_BOOLEAN:
				attr->value.boolean = *(gboolean *) value;
				break;
			case ATTRIB_TYPE_CHAR:
				attr->value.character = *(char *) value;
				break;
			case ATTRIB_TYPE_STRING:
				g_free(attr->value.string);
				attr->value.string = g_strdup(value);
				break;
		}
	}
	else {
		attr = pack_data(ATTRIB_TYPE_STRING, key, (gchar *) value);
		g_hash_table_replace(data, g_strdup(key), attr);
	}
}

AttribType get_data(AttribDef* attrib_def, void** value) {
	if (attrib_def && value) {
		switch (attrib_def->type) {
			case ATTRIB_TYPE_INT:
				*value = &attrib_def->value.number;
				break;
			case ATTRIB_TYPE_BOOLEAN:
				*value = &attrib_def->value.boolean;
				break;
			case ATTRIB_TYPE_CHAR:
				*value = &attrib_def->value.character;
				break;
			case ATTRIB_TYPE_STRING:
				*value = g_strdup(attrib_def->value.string);
				break;
		}
	}
	
	return attrib_def->type;
}

void gslist_remove_member(GSList* haystack, AttrList* neddle) {
	GSList *cur, *ptr;
	AttribDef *cur_attr, *ptr_attr;
	gboolean found;

	cm_return_if_fail(neddle != NULL);
	
	if (neddle->type == LIST) {
		for (cur = neddle->attr.list; cur; cur = g_slist_next(cur)) {
			cur_attr = (AttribDef *) cur->data;
			if (! cur)
				continue;
			found = FALSE;
			for (ptr = haystack; ptr && !found; ptr = g_slist_next(ptr)) {
				ptr_attr = (AttribDef *) ptr->data;
				if (utf8_collate(cur_attr->attrib_name, ptr_attr->attrib_name) == 0 &&
						cur_attr->type == ptr_attr->type) {
					haystack = g_slist_remove(haystack, cur_attr);
					attrib_def_free(cur_attr);
					found = TRUE;
				}
			}
		}
	}
	else {
		found = FALSE;
		for (ptr = haystack; ptr && !found; ptr = g_slist_next(ptr)) {
			ptr_attr = (AttribDef *) ptr->data;
			if (utf8_collate(ptr_attr->attrib_name, neddle->attr.attrib_name) == 0) {
				haystack = g_slist_remove(haystack, ptr_attr);
				attrib_def_free(ptr_attr);
				found = TRUE;
			}
		}
	}
}

gint address_book_compare(gconstpointer a, gconstpointer b) {
	const AddressBook* s1 = (const AddressBook *) a;
	const AddressBook* s2 = (const AddressBook *) b;
	gint found;
	
	if (!s1 && !s2)
		return 0;
		
	if (!s1 && s2)
		return 1;
		
	if (s1 && !s2)
		return -1;
	
	found = g_utf8_collate(s1->abook_name, s2->abook_name);
	if (found)
		return found;
		
	return g_utf8_collate(s1->URL, s2->URL);
}

gint gslist_compare_gchar(gconstpointer a, gconstpointer b) {
	const gchar* s1 = (const gchar *) a;
	const gchar* s2 = (const gchar *) b;
	
	if (!s1 && !s2)
		return 0;
		
	if (!s1 && s2)
		return 1;
		
	if (s1 && !s2)
		return -1;
	
	return g_utf8_collate(s1, s2);
}

gint gslist_get_index(GSList* list, gconstpointer data, GCompareFunc comp) {
	gint index = -1, i = 0;
	GSList* cur;
	
	if (comp) {
		for (cur = list; cur && index < 0; cur = g_slist_next(cur), i++) {
			if (comp(cur->data, data) == 0)
				index = i;
		}	 
	}
	else
		index = g_slist_index(list, data);

	return index;
}

#define AES_KEY "This is abook I1"
static gboolean aes_init(gcry_cipher_hd_t* digest) {
    gcry_error_t err = 0;
    const gchar iv[] = "AbC1234567890xYz";

	err = gcry_cipher_open(digest, GCRY_CIPHER_AES256,
		GCRY_CIPHER_MODE_CBC, GCRY_CIPHER_SECURE | GCRY_CIPHER_CBC_MAC);
    if (err) {
        g_printerr("%s\n", gcry_strerror(err));
    	gcry_cipher_close(*digest);
    	return FALSE;
    }

	err = gcry_cipher_setkey(*digest, AES_KEY, strlen(AES_KEY));
    if (err) {
        g_printerr("%s\n", gcry_strerror(err));
    	gcry_cipher_close(*digest);
    	return FALSE;
    }

	err = gcry_cipher_setiv(*digest, iv, gcry_cipher_get_algo_blklen(GCRY_CIPHER_AES256));
    if (err) {
        g_printerr("%s\n", gcry_strerror(err));
    	gcry_cipher_close(*digest);
    	return FALSE;
    }
    
    return TRUE;
}

static NormBlock* normalize(const gchar* text) {
	size_t blk_size = gcry_cipher_get_algo_blklen(GCRY_CIPHER_AES256);
	gint passes = strlen(text) / blk_size;
	gint rest = strlen(text) % blk_size;
	NormBlock* norm_block = g_new0(NormBlock, 1);
	norm_block->size = (passes * blk_size) + (rest) ? blk_size : 0;
	gint i;
	
	norm_block->block = g_new0(guchar, norm_block->size);
	for (i = 0; i < strlen(text); i++) {
		norm_block->block[i] = text[i];
	}
	
	return norm_block;
}

gchar* aes256_encrypt(const gchar* plain, gboolean base64_enc) {
    gcry_error_t err = 0;
    gcry_cipher_hd_t digest = NULL;
	guchar *cipher;
	NormBlock* text;
	gchar* base64 = NULL;
	
	if (! plain)
		return NULL;

	if (aes_init(&digest)) {
		text = normalize(plain);
		cipher = g_new0(guchar, text->size);
		err = gcry_cipher_encrypt(digest, cipher, text->size, text->block, text->size);
	    if (err) {
	        g_printerr("%s\n", gcry_strerror(err));
	    }
	    else {
			if (base64_enc)
				base64 = g_base64_encode(cipher, text->size);
			else
				base64 = g_memdup(cipher, text->size);	
		}
		
		g_free(cipher);
		g_free(text->block);
		g_free(text);
		gcry_cipher_close(digest);
	}
	
	return base64;
}

gchar* aes256_decrypt(const gchar* cipher_text, gboolean base64_enc) {
    gcry_error_t err = 0;
    gcry_cipher_hd_t digest = NULL;
	size_t size;
	guchar* cipher;
	guchar* plain = NULL;

	if (! cipher_text)
		return NULL;

	if (base64_enc)
		cipher = g_base64_decode(cipher_text, &size);
	else {
		cipher = (guchar *) g_strdup(cipher_text);
		size = strlen(cipher_text) + 1;
	}

	if (aes_init(&digest)) {
		plain = g_new0(guchar, size + 1);
		err = gcry_cipher_decrypt(digest, plain, size + 1, cipher, size);
	    if (err) {
	        g_printerr("%s\n", gcry_strerror(err));
	        g_free(plain);
	        plain = NULL;
	    }
		
		g_free(cipher);
		gcry_cipher_close(digest);
	}
	
	return (gchar *) plain;
}

static gchar* format_hash(const guchar* md_string) {
    int len = gcry_md_get_algo_dlen(GCRY_MD_SHA256);
    int i;
    gchar* hex = g_new0(gchar, 2 * len + 1);

	for (i = 0; i < len; i++)
        sprintf(hex + 2 * i, "%02x", md_string[i]);
	
	return hex;
}

#define SHA_KEY "claws-mail address book"
gchar* sha256(const gchar* plain) {
    gcry_error_t err = 0;
    gcry_md_hd_t digest = NULL;
    guchar* md_string = NULL;
    gchar* cipher;

	if (! plain)
		return NULL;

    err = gcry_md_open(
                &digest, GCRY_MD_SHA256,
                GCRY_MD_FLAG_SECURE | GCRY_MD_FLAG_HMAC);
    if (err) {
        g_printerr("%s\n", gcry_strerror(err));
        gcry_md_close(digest);
    }

    err = gcry_md_setkey(digest, SHA_KEY, strlen(SHA_KEY));
    if (err) {
        g_printerr("%s\n", gcry_strerror(err));
        gcry_md_close(digest);
    }

    gcry_md_write(digest, plain, strlen(plain));

    md_string = gcry_md_read(digest, 0);
    cipher = format_hash(md_string);
    gcry_md_close(digest);
    
    return cipher;
}

void extra_config_free(gpointer data) {
    ExtraConfig* ec;
    
    if (! data)
        return;
        
    ec = (ExtraConfig *) data;
    g_free(ec->label);
    g_free(ec->tooltip);
	if (ec->type == PLUGIN_CONFIG_EXTRA_ENTRY) {
		g_free(ec->current_value.entry);
		g_free(ec->preset_value.entry);
	}
    g_free(ec);
    ec = NULL;
}

gpointer extra_config_copy(gpointer data) {
	ExtraConfig *a, *b;
	
	if (! data)
		return NULL;
		
	a = (ExtraConfig *) data;
	b = g_new0(ExtraConfig, 1);
	
	b->label = g_strdup(a->label);
	b->tooltip = g_strdup(a->tooltip);
	b->type = a->type;
	if (a->type == PLUGIN_CONFIG_EXTRA_ENTRY) {
		b->current_value.entry = g_strdup(a->current_value.entry);
		b->preset_value.entry = g_strdup(a->preset_value.entry);
	}
	else {
		b->current_value = a->current_value;
		b->preset_value = a->preset_value;
	}
	
	return b; 
}

typedef struct {
	GSList* list;
	const gchar* name;
} GtkIteratorData;

static void gtk_iterator(GtkWidget* widget, gpointer data) {
	GtkIteratorData* iter_data = (GtkIteratorData *) data;
	const gchar* name;
	
	if (GTK_IS_CONTAINER(widget))
		gtk_container_foreach(GTK_CONTAINER(widget), gtk_iterator, iter_data);
	name = gtk_widget_get_name(widget);
	if (name && strcmp(name, iter_data->name) == 0)
		iter_data->list = g_slist_prepend(iter_data->list, widget);
}

GSList* find_name(GtkContainer* container, const gchar* name) {
	GSList* list = NULL;
	GtkIteratorData* data;
	
	if (! container || ! name)
		return list;
	
	data = g_new0(GtkIteratorData, 1);
	data->name = name;
	
	gtk_container_foreach(container, gtk_iterator, data);
	list = data->list;
	g_free(data);
	
	return list;
}

ExtraConfig* get_extra_config(GSList* list, const gchar* name) {
	GSList* cur;
	ExtraConfig* conf = NULL;
	gboolean found = FALSE;
	
	if (! list || ! name)
		return conf;
		
	for (cur = list; cur && !found; cur = g_slist_next(cur)) {
		conf = (ExtraConfig *) cur->data;
		if (conf->label && strcmp(conf->label, name) == 0)
			found = TRUE;
		else
			conf = NULL;
	}
	
	return extra_config_copy(conf);
}

gboolean address_book_name_exists(Plugin* plugin, AddressBook* book) {
	gboolean found = FALSE;
	GSList *list, *cur;
	
	if (! plugin || ! book)
		return found;
	
	list = plugin->addrbook_names_all();
	cur = list;
	
	while(cur && ! found) {
		gchar* name = (gchar *) cur->data;
		if (utf8_collate(name, book->abook_name) == 0)
			found = TRUE;
		cur = cur->next;
	}
	gslist_free(&list, g_free);
	
	return found;
}

GSList* get_basic_attributes(Contact* contact) {
	GSList *list = NULL, *cur;
	gchar* value = NULL;

	cm_return_val_if_fail(contact != NULL, NULL);
	
	extract_data(contact->data, "nick-name", (void **) &value);
	if (value) {
		list = g_slist_prepend(list, g_strdup(value));
		g_free(value);
		value = NULL;
	}
	extract_data(contact->data, "first-name", (void **) &value);
	if (value) {
		list = g_slist_prepend(list, g_strdup(value));
		g_free(value);
		value = NULL;
	}
	extract_data(contact->data, "last-name", (void **) &value);
	if (value) {
		list = g_slist_prepend(list, g_strdup(value));
		g_free(value);
		value = NULL;
	}
	extract_data(contact->data, "cn", (void **) &value);
	if (value) {
		list = g_slist_prepend(list, g_strdup(value));
		g_free(value);
		value = NULL;
	}
	
	for (cur = contact->emails; cur; cur = g_slist_next(cur)) {
		Email* e = (Email *) cur->data;
		list = g_slist_prepend(list, g_strdup(e->alias));		
		list = g_slist_prepend(list, g_strdup(e->email));		
	}
	
	return list;
}

gboolean match_string_pattern(const gchar* pattern,
							  const gchar* haystack,
							  gboolean case_aware) {
	const gchar *txt, *source;
	gunichar ch = 0, cp;
	gboolean in_str = FALSE, error = FALSE, has_source;
	
	if (! pattern || ! haystack)
		return FALSE;
	
	if (!*pattern && !*haystack)
		return TRUE;
	
	if (g_utf8_validate(pattern, -1, NULL) &&
			g_utf8_validate(haystack, -1, NULL)) {

		if (g_utf8_strlen(pattern, -1) == 1 && (
			*pattern == '*' || (*pattern == '?' &&
			g_utf8_strlen(haystack, -1) < 2)))
			return TRUE;
		
		if (*pattern && g_utf8_strlen(haystack, -1) < 1)
			return FALSE;
		
		txt = pattern;
		source = haystack;

		while (*txt && *source && !error) {
			ch = g_utf8_get_char(txt);
			cp = g_utf8_get_char(source);
			switch (ch) {
				case '*':
					in_str = TRUE;
					break;
				case '?':
					break;
				default:
					if (! case_aware) {
						cp = g_unichar_toupper(cp);
						ch = g_unichar_toupper(ch);
					}
					if (in_str) {
						while (ch != cp && *source) {
							source = g_utf8_find_next_char(source, NULL);
							cp = g_utf8_get_char(source);
							if (! case_aware)
								cp = g_unichar_toupper(cp);
						}
						in_str = FALSE;
					}
					else
						error = (cp == ch) ? FALSE : TRUE;
					break;
			}
			if (!error) {
				has_source = FALSE;
				if (*source && !in_str) {
					has_source = TRUE;
					source = g_utf8_find_next_char(source, NULL);
				}
				if (*source || has_source)
					txt = g_utf8_find_next_char(txt, NULL);
			}
		}
		if (!*source && (ch = g_utf8_get_char(txt)) != 0) {
			if (ch != '*') {
				if (ch == '?' && g_utf8_strlen(txt, -1) == 1) {
					/* keep value of error */
				}
				else {
					if (g_utf8_strlen(txt, -1) > 1)
						error = TRUE;
				}
			}
		}
		else if (*source && !g_utf8_get_char(txt)) {
			if (ch != '*') {
				if (ch == '?' && g_utf8_strlen(source, -1) < 1) {
					/* keep value of error */
				}
				else
					error = TRUE;
			}
		}
		else {
			/* Keep value of error */
		}
	}
	
	return (!error);
}

gboolean email_compare_values(GSList* a, GSList* b, gboolean is_and) {
	GSList *cur1, *cur2;
	gint equal = -1;
	
	for (cur1 = a; cur1; cur1 = g_slist_next(cur1)) {
		Email* e1 = (Email *) cur1->data;
		for (cur2 = b; cur2; cur2 = g_slist_next(cur2)) {
			Email* e2 = (Email *) cur2->data;
			if (e1->alias) {
				equal = match_string_pattern(e1->alias, e2->alias, TRUE);
				if ((!equal && is_and) || (equal && !is_and))
					break;
			}
			if (e1->email) { 
				equal = match_string_pattern(e1->email, e2->email, TRUE);
				if ((!equal && is_and) || (equal && !is_and))
					break;
			}
			if (e1->remarks) 
				equal = match_string_pattern(e1->remarks, e2->remarks, TRUE);
		}
		if (equal && !is_and)
			break;
	}
	
	return equal;
}

void contact_compare_values(gpointer key, gpointer value, gpointer data) {
	Compare* comp = (Compare *) data;
	AttribDef *attr;
	const gchar* term;

	cm_return_if_fail(value != NULL);
	
	term = ((AttribDef *) value)->value.string;
	
	if (debug_get_mode()) {
		Contact* c = g_new0(Contact, 1);
		c->data = comp->hash;
		contact_dump(c, stderr);
		fprintf(stderr, "Key: %s - Value: %s\n", (gchar *) key, term);
		g_free(c);
	}
	if ((comp->is_and && comp->equal) ||
		(!comp->is_and && (comp->equal < 0 || comp->equal == 0))) {
		attr = g_hash_table_lookup(comp->hash, key);
		if (attr) {
			if (attr->type == ATTRIB_TYPE_STRING) {
				comp->equal = match_string_pattern(
					term, attr->value.string, TRUE);
			}
			else if (attr->type == ATTRIB_TYPE_CHAR) {
				gchar* ch = g_strdup_printf("%c", attr->value.character);
				comp->equal = match_string_pattern(term, ch, TRUE);
				g_free(ch);
			}
			else if (attr->type == ATTRIB_TYPE_BOOLEAN) {
				comp->equal = attr->value.boolean = atoi(term);
			}
			else if (attr->type == ATTRIB_TYPE_INT) {
				comp->equal = attr->value.number = atoi(term);
			}
			else {
				comp->equal = FALSE;
			}
		}
	}
	else
		comp->equal = FALSE;
}

gchar* create_dummy_dn(const gchar* baseDN) {
	GTimeVal timeval;
	GRand* rand;
	gchar *dn = NULL, *tmp;

	g_get_current_time(&timeval);
	tmp = g_time_val_to_iso8601(&timeval);
	rand = g_rand_new();
	guint32 num = g_rand_int(rand);
	g_rand_free(rand);
	gchar* plain = g_strdup_printf("%s %d", tmp, num);
	g_free(tmp);
	tmp = sha256(plain);
	g_free(plain);
	if (baseDN)
		dn = g_strconcat("mail=", tmp, ",", baseDN, NULL);
	else
		dn = g_strdup(tmp);
	g_free(tmp);
	
	return dn;
}

gboolean email_equal(Email* a, Email* b) {
	if (! a && ! b)
		return TRUE;
		
	if ((! a && b) || (a && ! b))
		return FALSE;
	
	if (utf8_collate(a->alias, b->alias) == 0 &&
		utf8_collate(a->email, b->email) == 0 &&
		utf8_collate(a->remarks, b->remarks) == 0)
		return TRUE;

	return FALSE;
}
/*
static gint diff_list_find(gconstpointer a, gconstpointer b) {
	ContactChange* c = (ContactChange *) a;
	Email* e = (Email *) b;
	
	if (c->type == CONTACT_CHANGE_ATTRIB) {
		AttribDef* attr = (AttribDef *) c->value;
		return utf8_collate(e->email, attr->value.string);
	}
	else if (c->type == CONTACT_CHANGE_EMAIL) {
		Email* e1 = (Email *) c->value;
		return utf8_collate(e->email, e1->email);
	}
	
	return 0;
}
*/
GSList* contact_diff(const Contact* a, const Contact* b) {
	GSList* diff = NULL;
	GHashTableIter iter;
	gpointer key1, key2, val1, val2;
	ContactChange* c;
	GSList /**emaila, *emailb, *cura, *curb*/ *cur;

	cm_return_val_if_fail(a != NULL, FALSE);
	cm_return_val_if_fail(b != NULL, FALSE);

	g_hash_table_iter_init(&iter, a->data);
	while (g_hash_table_iter_next(&iter, &key1, &val1)) {
		AttribDef* attr1 = (AttribDef *) val1;
    	if (g_hash_table_lookup_extended(b->data, key1, &key2, &val2)) {
			AttribDef* attr2 = (AttribDef *) val2;
			if (! compare_attrib(attr1, attr2, -1)) {
				c = g_new0(ContactChange, 1);
				c->action = ATTRIBUTE_MODIFY;
				c->type = CONTACT_CHANGE_ATTRIB;
				c->key = g_strdup((gchar *) key2);
				c->value = (gpointer) attrib_def_copy(attr2);
				diff = g_slist_prepend(diff, c);
			}
		}
		else {
			c = g_new0(ContactChange, 1);
			c->action = ATTRIBUTE_DELETE;
			c->type = CONTACT_CHANGE_ATTRIB;
			c->key = g_strdup((gchar *) key1);
			c->value = (gpointer) attrib_def_copy(attr1);
			diff = g_slist_prepend(diff, c);
		}
	}

	g_hash_table_iter_init(&iter, b->data);
	while (g_hash_table_iter_next(&iter, &key1, &val1)) {
		AttribDef* attr1 = (AttribDef *) val1;
    	if (! g_hash_table_lookup_extended(a->data, key1, &key2, &val2)) {
			c = g_new0(ContactChange, 1);
			c->action = ATTRIBUTE_ADD;
			c->type = CONTACT_CHANGE_ATTRIB;
			c->key = g_strdup((gchar *) key1);
			c->value = (gpointer) attrib_def_copy(attr1);
			diff = g_slist_prepend(diff, c);
		}
	}
	
	cur = b->emails;
	while (cur) {
		c = g_new0(ContactChange, 1);
		c->action = ATTRIBUTE_MODIFY;
		c->type = CONTACT_CHANGE_EMAIL;
		c->key = g_strdup(((Email *) cur->data)->email);
		c->value = (gpointer) email_copy(cur->data);
		diff = g_slist_prepend(diff, c);
		cur = cur->next;
	}

	return diff;
}

void contact_change_free(gpointer contact_change) {
	ContactChange* c;
	
	if (! contact_change)
		return;
		
	c = (ContactChange *) contact_change;
	g_free(c->key);
	c->key = NULL;
	if (c->type == CONTACT_CHANGE_ATTRIB) {
		attrib_def_free(c->value);
	}
	else if (c->type == CONTACT_CHANGE_EMAIL) {
		email_free(c->value);
	}
	c->value = NULL;
	g_free(c);
	c = NULL;
}

gint email_sort(gconstpointer a, gconstpointer b) {
	gint res;
	const Email *a1, *b1;
	
	if (! a && ! b)
		return 0;
		
	if (! a && b)
		return 1;
		
	if (a && ! b)
		return -1;
	
	a1 = (Email *) a;
	b1 = (Email *) b;
	
	if ((res = utf8_collate(a1->email, b1->email)) == 0) {
		if ((res = utf8_collate(a1->alias, b1->alias)) == 0) {
			return utf8_collate(a1->remarks, b1->remarks);
		}
	}
		
	return res;
}

gchar* get_domain_name(void) {
	struct addrinfo hints, *result, *rp;
	int s;
	gchar *dn = NULL, *pos, hostname[BUFSIZ];
	
	memset(&hostname, 0, BUFSIZ);
	gethostname(hostname, 1023);
	
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC; 
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_CANONNAME;
	
	s = getaddrinfo(hostname, "http", &hints, &result);
	if (s != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
		exit(EXIT_FAILURE);
	}
	
	for (rp = result; rp != NULL; rp = rp->ai_next) {
		if (rp->ai_canonname) {
			if ((pos = strchr(rp->ai_canonname, '.')) != NULL) {
				dn = g_strdup(++pos);
			}
			else
				dn = g_strdup(rp->ai_canonname);
			break;
		}
	}
	freeaddrinfo(result);
	
	return dn;
}

#define PHOTO_WIDTH 140
#define PHOTO_HEIGHT 140
GdkPixbuf* contact_load_image(GtkWidget* parent, gchar* photo) {
	GdkPixbufLoader* pl;
	GdkPixbuf* pixbuf;
	GError *err = NULL;
	guchar* image_buf;
	gsize length; 
	
	image_buf = g_base64_decode(photo, &length);
	
	pl = gdk_pixbuf_loader_new();
	gdk_pixbuf_loader_set_size(pl, PHOTO_WIDTH, PHOTO_HEIGHT); 
	gdk_pixbuf_loader_write(pl, image_buf, length, &err); 
	g_free(image_buf);

	if (err) {
		show_message(parent, GTK_UTIL_MESSAGE_WARNING, "%s", err->message);
		g_clear_error(&err); 
		return NULL; 
	} 
	gdk_pixbuf_loader_close(pl, NULL);
	
	pixbuf = gdk_pixbuf_copy(gdk_pixbuf_loader_get_pixbuf(pl)); 
	g_object_unref(pl);
	
	return pixbuf;
}

gchar* base64_pixbuf_to_jpeg(gchar* base64_png, gsize* length) {
	gchar *picture = NULL, *buffer;
	GdkPixbufLoader* pl;
	GdkPixbuf* pixbuf;
	GError *err = NULL;
	guchar* image_buf;
	gsize len;

	cm_return_val_if_fail(base64_png != NULL, NULL);
	
	image_buf = g_base64_decode(base64_png, &len);
	
	pl = gdk_pixbuf_loader_new();

	gdk_pixbuf_loader_write(pl, image_buf, len, &err); 
	g_free(image_buf);

	if (err) {
		g_critical("Load: %s", err->message);
		g_clear_error(&err); 
		return NULL; 
	} 
	gdk_pixbuf_loader_close(pl, NULL);
	
	pixbuf = gdk_pixbuf_loader_get_pixbuf(pl);
	GdkPixbufFormat* format = gdk_pixbuf_loader_get_format(pl);
	gchar* name = gdk_pixbuf_format_get_name(format);
	if (name && strcasecmp(name, "jpeg") == 0) {
		if (gdk_pixbuf_save_to_buffer(pixbuf, &buffer, length, "jpeg", &err, "quality", "100", NULL)) {
			picture = g_base64_encode((const guchar *) buffer, *length);
			g_free(buffer);
		}
		else
			picture = NULL;
	}
	else {
		if (gdk_pixbuf_save_to_buffer(pixbuf, &buffer, length, name, &err, NULL)) {
			picture = g_base64_encode((const guchar *) buffer, *length);
			g_free(buffer);
		}
		else
			picture = NULL;
	}
	if (! picture) {
		g_critical("Write: %s", err->message);
		g_clear_error(&err);
	}
	g_object_unref(pl);
	
	return picture;
}

gchar* jpeg_to_png_base64(guchar* jpeg, gsize length) {
	gchar* base64 = NULL, *buffer;
	GdkPixbufLoader* pl;
	GdkPixbuf* pixbuf;
	guchar* image_buf;
	GError *err = NULL;
	gsize len;

	cm_return_val_if_fail(jpeg != NULL, NULL);

	image_buf = g_new0(guchar, length * 3 / 4 );
	pl = gdk_pixbuf_loader_new();

	gdk_pixbuf_loader_write(pl, jpeg, length, &err); 
	g_free(image_buf);
	if (err) {
		g_critical("Load: %s", err->message);
		g_clear_error(&err); 
		return NULL; 
	} 
	gdk_pixbuf_loader_close(pl, NULL);
	
	pixbuf = gdk_pixbuf_loader_get_pixbuf(pl);
	GdkPixbufFormat* format = gdk_pixbuf_loader_get_format(pl);
	if (gdk_pixbuf_save_to_buffer(
			pixbuf, &buffer, &len, gdk_pixbuf_format_get_name(format), &err, NULL)) {
		base64 = g_base64_encode((const guchar *) buffer, len);
		g_free(buffer);
	}
	else {
		g_critical("Write: %s", err->message);
		g_clear_error(&err);
	}
	
	g_object_unref(pl);
	
	return base64;
}

const gchar* native2ldap(const gchar* attr) {
	cm_return_val_if_fail(attr != NULL, NULL);

	if (strcasecmp(attr, "email") == 0)
		return "mail";
	else if (strcasecmp(attr, "first-name") == 0)
		return "givenName";
	else if (strcasecmp(attr, "last-name") == 0)
		return "sn";
	else if (strcasecmp(attr, "nick-name") == 0)
		return "displayName";
	else if (strcasecmp(attr, "image") == 0)
		return "jpegPhoto";
	else
		return attr;
}

const gchar* ldap2native(const gchar* attr) {
	cm_return_val_if_fail(attr != NULL, NULL);

	if (strcasecmp(attr, "mail") == 0)
		return "email";
	else if (strcasecmp(attr, "givenName") == 0)
		return "first-name";
	else if (strcasecmp(attr, "sn") == 0)
		return "last-name";
	else if (strcasecmp(attr, "displayName") == 0)
		return "nick-name";
	else if (strcasecmp(attr, "jpegPhoto") == 0)
		return "image";
	else
		return attr;
}
