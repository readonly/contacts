#!/bin/sh
# $Id$

if [ ! -e config ]; then
    mkdir config
fi

if [ ! -e m4 ]; then
    mkdir m4
fi

if [ ! -e auxdir ]; then
    mkdir auxdir
fi

aclocal --force -I m4 \
  && libtoolize --force --copy \
  && autoheader --force \
  && automake --add-missing --gnu --copy \
  && autoconf --force
  
if test -z "$NOCONFIGURE"; then
  exec ./configure --enable-maintainer-mode $@
fi
