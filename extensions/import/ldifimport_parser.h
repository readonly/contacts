/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __LDIFIMPORT_PARSER_H__
#define __LDIFIMPORT_PARSER_H__

#include <glib.h>

G_BEGIN_DECLS

#include <glib/gi18n.h>

enum {
	LDIF_PARSER_SUCCESS,
	LDIF_PARSER_BAD_FD,
	LDIF_PARSER_BAD_FILE,
	LDIF_PARSER_READ_FD_ERROR,
};

typedef struct _LdifParser LdifParser;

typedef void (*LDIFParserErrorFunc)	(LdifParser* parser, gboolean error);
typedef gboolean (*LDIFParserParserFunc) (LdifParser* parser);

typedef enum {
	LDIF_PARSER_VALS,
	LDIF_PARSER_BVALS
} TokenType;

typedef struct {
	TokenType				type;
	gchar*					name;
	union {
		gchar*				value;
		struct _TokenBVals {
			gsize			bv_len;
			gchar*			bv_val;
		} token_bvals;
	};
} Token;

typedef struct {
	gboolean				dirty;
	gchar*					dn;
	GSList*					tokens;
	GSList*					dropped;
} Record;

struct _LdifParser {
	/* Unused fields */
	guint					max_errors;
	gpointer				user_data;
	
	/* error handling fields */
	guint					errors; 
	guint					error_code;
	guint					error_line;
	guint					error_place;
	gchar*					error_msg;
	
	/* List of Records */
	GSList*					records;
	
	/* internal fields */
	gboolean				fatal_error;
	guint					line;
	guint					position;
	guint					end;
	gint					input_fd;
	gboolean				open_fd_by_me;
	gchar*					buffer;
	gchar*					input_name;
	GHashTable*				symbols;
	
	/* Unless provided by user handlers will use default implementation */
	LDIFParserErrorFunc		error_handler;
	LDIFParserParserFunc	parser_handler;
};

guint ldif_parser_get_errors(LdifParser* parser);
void ldif_parser_set_fd(LdifParser* parser, gint fd);
void ldif_parser_set_file(LdifParser* parser, const gchar* file);
void ldif_parser_set_user_data(LdifParser* parser, gpointer user_data);
void ldif_parser_set_max_errors(LdifParser* parser, guint max);
gboolean ldif_parser_parse(LdifParser* parser);
LdifParser* ldif_parser_new(void);
void ldif_parser_destroy(LdifParser* parser);
void ldif_parser_token_pairs_free(gpointer data);
void ldif_parser_record_free(gpointer data);
void ldif_parser_add_symbol(LdifParser* parser, const gchar* symbol);
void ldif_parser_set_symbols(LdifParser* parser, GHashTable* symbols);
GSList* ldif_parser_get_records(LdifParser* parser);
gpointer ldif_parser_token_pairs_copy(gpointer item);
void ldif_parser_token_pair_dump(gpointer data);
void ldif_parser_record_dump(gpointer data);

G_END_DECLS

#endif