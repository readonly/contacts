/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "extension.h"
#include "utils.h"
#include "gtk-utils.h"
#include "plugin.h"
#include "plugin-loader.h"
#include "ldifimport_parser.h"

#define NAME "LDIF Import extension"

static guint my_id;

static gchar* ldif_file_chooser(MenuItem* item) {
	GtkWidget* dialog;
	gchar* filename = NULL;
		
	dialog = gtk_file_chooser_dialog_new(_("Open file"),
					      GTK_WINDOW(item->mainwindow->window),
					      GTK_FILE_CHOOSER_ACTION_OPEN,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					      NULL);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), get_home());
/*	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
	gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);*/
	
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
	}
	gtk_widget_destroy(dialog);
	
	return filename;
}

static void ldif_import(GtkWidget* widget, gpointer data) {
	MenuItem* item = (MenuItem *) data;
	LdifParser* parser;
	GtkTreeView* view;
	AddressBook* abook;
	Plugin* plugin;
	Contact* contact;
	Record* record;
	GtkTreeIter* iter = NULL;
	GtkTreeSelection* row;
	GSList *attribs, *cur, *head;
	gchar* value;
	gchar* err;
	
	view = GTK_TREE_VIEW(item->mainwindow->abook_list);
	abook = get_selected_address_book(view);
	if (abook == NULL) {
		iter = set_selection_combobox(item->mainwindow->window,
						_("[New Contact] Choose address book"),
						gtk_tree_view_get_model(view),
						BOOK_NAME_COLUMN);
		if (! iter) {
			return;
		}
		row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
	}
	plugin = get_selected_plugin(view);
	
	gchar* file = ldif_file_chooser(item);
	if (!file)
		return;

	parser = ldif_parser_new();
	ldif_parser_set_file(parser, file);
	g_free(file);
	
	attribs = plugin->attrib_list();

	cur = attribs;
	while (cur) {
		AttribDef* attr = (AttribDef *) cur->data;
		const gchar* key = native2ldap(attr->attrib_name);
		ldif_parser_add_symbol(parser, key);
		cur = cur->next;
	}
	gslist_free(&attribs, attrib_def_free);
	ldif_parser_parse(parser);
	if (ldif_parser_get_errors(parser) > 0) {
		show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_WARNING,
			"%s:%d:%d [%d] %s",	parser->input_name, parser->error_line,
			parser->error_place, parser->error_code, parser->error_msg);
	}
	head = cur = ldif_parser_get_records(parser);
	while (cur) {
		if (debug_get_mode()) {
			ldif_parser_record_dump(cur->data);
		}
		record = (Record *) cur->data;
		contact = contact_new();
		for (attribs = record->tokens; attribs; attribs = g_slist_next(attribs)) {
			Token* token = (Token *) attribs->data;
			value = NULL;
			if (token->type == LDIF_PARSER_BVALS) {
				value = g_base64_encode(
					(const guchar *) token->token_bvals.bv_val,
					token->token_bvals.bv_len);
			}
			else {
				value = g_strdup(token->value);
			}
			debug_print("Attribute: %s - Value: %s\n", ldap2native(token->name), value);
			if (strcasecmp("mail", token->name) == 0) {
				/* add email */
				Email* email = g_new0(Email, 1);
				email->email = g_strdup(value);
				contact->emails = g_slist_prepend(contact->emails, email);
			}
			else {
				/* some other attribute */
				swap_data(contact->data, ldap2native(token->name), value);
			}
			g_free(value);
		}
		err = NULL;
		plugin->set_contact(abook, contact, &err);
		if (err) {
			show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_WARNING, "%s", err);
			g_free(err);
			err = NULL;
			contact_free(contact);
		}
		else {
			abook->contacts = g_list_prepend(abook->contacts, contact);
			abook->dirty = TRUE;
			list_view_append_contact(
				GTK_TREE_VIEW(item->mainwindow->contact_list), contact);
			while (gtk_events_pending())
				gtk_main_iteration();
		}
		cur = cur->next;
	}
	gslist_free(&head, ldif_parser_record_free);

	ldif_parser_destroy(parser);
}

static void setup(const MainWindow* mainwindow, gpointer object) {
	GtkWidget *menu;
	MenuItem* menu_item;
	
	menu = gtk_image_menu_item_new_with_mnemonic(_("_LDIF"));
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_MAIN_MENU;
    menu_item->parent = _("tools");
	menu_item->sublabel = _("_Import");
    menu_item->submenu = TRUE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
            G_CALLBACK(ldif_import), (gpointer) menu_item);
	add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);

	menu = gtk_image_menu_item_new_with_mnemonic(_("_LDIF"));
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_ADDRESSBOOK_MENU;
    menu_item->sublabel = _("_Import");
    menu_item->submenu = TRUE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
            G_CALLBACK(ldif_import), (gpointer) menu_item);
	add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);
}

/**
 * The main application will call this function after loading the
 * extension providing a uniq id for the extension which is to be
 * used for further references
 * @param id uniq id provided by main application
 * @return 0 if success 1 otherwise
 */
gint extension_init(guint id) {
	my_id = id;
	gchar* error = NULL;
	
	register_hook_function(my_id, EXTENSION_AFTER_INIT_HOOK, setup, &error);
	if (error) {
		show_message(NULL, GTK_UTIL_MESSAGE_ERROR, error);
		g_free(error);
		return 1;
	}
	
	return 0;
}

/**
 * Called by main application when the extension should be unloaded
 * @return TRUE if success FALSE otherwise
 */
gboolean extension_done(void) {
	return TRUE;
}

/**
 * Called by main application to ensure extension license is compatible
 * @return license
 */
const gchar* extension_license(void) {
	return "GPL3+";
}

/**
 * Called by main application to get name of extension
 * @return name
 */
const gchar* extension_name(void) {
	return NAME;
}

/**
 * Called by main application to get extension's describtion
 * @return description
 */
const gchar* extension_describtion(void) {
	return _("Import file in LDIF format into an address book");
}
