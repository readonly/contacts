/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gi18n.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>

#include "utils.h"
#include "ldifimport_parser.h"

static void ldif_parser_set_error(LdifParser* parser, gint code, gchar* msg) {
	parser->error_code = LDIF_PARSER_BAD_FD;
	if (parser->error_msg)
		g_free(parser->error_msg);
	parser->error_msg = g_strdup(msg);
	parser->errors++;
	if (parser->max_errors > 0 && parser->max_errors < parser->errors)
		parser->error_handler(parser, TRUE);
	else
		parser->error_handler(parser, FALSE);
}

static void ldif_parser_error_func(LdifParser* parser, gboolean error) {
	cm_return_if_fail(parser != NULL);

	if (error)
		parser->fatal_error = TRUE;

	g_print("%s:%d:%d [%d] %s\n", parser->input_name, parser->error_line,
		parser->error_place, parser->error_code, parser->error_msg);
}

#define BUF_SIZE 1024
static void ldif_parser_read_file(LdifParser* parser) {
	gchar buf[BUF_SIZE];
	ssize_t num_read, total = 0;
	gchar *end_pos;

	g_free(parser->buffer);
	parser->buffer = g_new0(gchar, 1);
	while ((num_read = read(parser->input_fd, &buf, BUF_SIZE)) > 0) {
		parser->buffer = g_realloc(parser->buffer, total + num_read + 1);
		if (total > 0)
			end_pos = &parser->buffer[total];
		else
			end_pos = parser->buffer;
		g_memmove(end_pos, buf, num_read);
		total += num_read;
		parser->buffer[total] = 0;
	}
	if (num_read < 0) {
		gchar* err = strerror(errno);
		ldif_parser_set_error(parser, LDIF_PARSER_READ_FD_ERROR, err);
		g_free(parser->buffer);
		parser->buffer = NULL;
		parser->fatal_error = TRUE;
	}
	debug_print("Contents of file '%s'\n%s\n", parser->input_name, parser->buffer);
}

static gboolean ldif_parser_advance_str(LdifParser* parser) {
	gchar* str = parser->buffer;
	gboolean first = TRUE;
	gboolean stop = FALSE;

	for (parser->position++; str && str[parser->position] && ! stop; parser->position++) {
		if (first) {
			if (str[parser->position] == ' ' || str[parser->position] == '<')
				first = FALSE;
			else {
				parser->error_place = parser->position;
				g_free(parser->error_msg);
				parser->error_msg = g_strdup(_("Bad file format"));
				return TRUE;
			}
		}
		if (str[parser->position] == '\n') {
			if (str[parser->position + 1] == ' ') {
				if (str[parser->position + 2] == ' ') {
					parser->error_place = parser->position + 2;
					g_free(parser->error_msg);
					parser->error_msg = g_strdup(_("Bad file format"));
					return TRUE;
				}
				/* continue since multi line */
			}
			else {
				parser->end = parser->position + 1;
				stop = TRUE;
			}
		}
	}
	
	return FALSE;
}

static gchar* ldif_parser_remove_multi_line(gchar* str) {
	gchar* clean = g_strdup(str);
	gchar *pos, *tmp1, *tmp2;
	
	while ((pos = strstr(clean, "\r\n ")) != NULL) {
		tmp1 = g_strndup(clean, pos - clean);
		pos += 3;
		tmp2 = g_strdup(pos);
		g_free(clean);
		clean = g_strdup_printf("%s%s", tmp1, tmp2);
		g_free(tmp1);
		g_free(tmp2);
	}
	/* If file is not using internet secure newline */
	while ((pos = strstr(clean, "\n ")) != NULL) {
		tmp1 = g_strndup(clean, pos - clean);
		pos += 2;
		tmp2 = g_strdup(pos);
		g_free(clean);
		clean = g_strdup_printf("%s%s", tmp1, tmp2);
		g_free(tmp1);
		g_free(tmp2);
	}
	
	return clean;		
}

static GSList* ldif_parser_strsplit(LdifParser* parser) {
	GSList* records = NULL;
	Record*	record = NULL;
	gchar* str = parser->buffer;
	gchar* tmp;
	gint head, line, start;
	Token* token = NULL;
	gboolean stop = FALSE;
	
	for (parser->position = 0, head = 1, line = 1; 
			str && str[parser->position]; parser->position++) {
		if (parser->position < 1)
			record = g_new0(Record, 1);
		if ((str[parser->position] == '\n' && str[parser->position + 1] != '\n') ||
			(str[parser->position] == ' ' && parser->position < 1)) {
			parser->error_line = line;
			parser->error_place = parser->position;
			g_free(parser->error_msg);
			parser->error_msg = g_strdup(_("Bad file format"));
			parser->error_handler(parser, TRUE);
			ldif_parser_record_free(record);
			gslist_free(&records, ldif_parser_record_free);
			return NULL;
		}
		/* Skip begining newline */
		if (parser->position > 0 && str[parser->position - 1] == '\n') {
			head++;
			records = g_slist_prepend(records, record);
			record->dirty = FALSE;
			record = g_new0(Record, 1);
			record->dirty = FALSE;
		}
		if (str[parser->position] == '#') {
			parser->position++;
			while (str[parser->position] && ! stop) {
				if (str[parser->position] == '\n') {
					if (str[parser->position + 1] == ' ') {
						if (str[parser->position + 2] == ' ') {
							parser->error_line = line;
							parser->error_place = parser->position;
							g_free(parser->error_msg);
							parser->error_msg = g_strdup(_("Bad file format"));
							parser->error_handler(parser, TRUE);
							ldif_parser_record_free(record);
							gslist_free(&records, ldif_parser_record_free);
							return NULL;
						}
						/* Continue due to multi line */
					}
					else {
						stop = TRUE;
					}
				}
				parser->position++;
			}
		}
		if (str[parser->position] && str[parser->position] == ':') {
			if (str[parser->position + 1] == ':') {
				if (str[parser->position + 2] == ':') {
					parser->error_line = line;
					parser->error_place = parser->position;
					g_free(parser->error_msg);
					parser->error_msg = g_strdup(_("Bad file format"));
					parser->error_handler(parser, TRUE);
					ldif_parser_record_free(record);
					gslist_free(&records, ldif_parser_record_free);
					return NULL;
				}
				else {
					token = g_new0(Token, 1);
					token->type = LDIF_PARSER_BVALS;
					tmp = g_strndup(&str[head - 1], parser->position - head + 1);
					token->name = g_ascii_strdown(tmp, -1);
					g_free(tmp);
					/* ahead one '::' */
					parser->position++;
					/* skip ':' and either ' '  or '<' */
					start = parser->position + 2;
					if (ldif_parser_advance_str(parser)) {
						parser->error_line = line;
						parser->error_handler(parser, TRUE);
						ldif_parser_record_free(record);
						gslist_free(&records, ldif_parser_record_free);
						return NULL;
					}
					tmp = g_strndup(&str[start], parser->position - start - 1);
					token->token_bvals.bv_val = ldif_parser_remove_multi_line(tmp);
					g_free(tmp);
					tmp = g_strdup(token->token_bvals.bv_val);
					g_free(token->token_bvals.bv_val);
					token->token_bvals.bv_val =
						(gchar *) g_base64_decode(tmp, &token->token_bvals.bv_len);
					g_free(tmp);
					if (strcasecmp("dn", token->name) == 0)
						record->dn = g_strdup(token->token_bvals.bv_val);
					line++;
					head = (parser->position + 1 == '\n') ? 
						parser->position + 2 : parser->position + 1;
					record->tokens = g_slist_prepend(record->tokens, token);
					record->dirty = TRUE;
				}
			}
			else {
				token = g_new0(Token, 1);
				token->type = LDIF_PARSER_VALS;
				tmp = g_strndup(&str[head - 1], parser->position - head + 1);
				token->name = g_ascii_strdown(tmp, -1);
				g_free(tmp);
				/* skip ':' and either ' '  or '<' */
				start = parser->position + 2;
				if (ldif_parser_advance_str(parser)) {
					parser->error_line = line;
					parser->error_handler(parser, TRUE);
					ldif_parser_record_free(record);
					gslist_free(&records, ldif_parser_record_free);
					return NULL;
				}
				tmp = g_strndup(&str[start], parser->position - start - 1);
				token->value = ldif_parser_remove_multi_line(tmp);
				g_free(tmp);
				if (strcasecmp("dn", token->name) == 0) {
					debug_print("Record adding dn: %s\n", token->value);
					record->dn = g_strdup(token->value);
				}
				line++;
				head = (parser->position + 1 == '\n') ? 
					parser->position + 2 : parser->position + 1;
				record->tokens = g_slist_prepend(record->tokens, token);
				record->dirty = TRUE;
			}
		}
				
	}
	if (record && record->dirty)
		records = g_slist_prepend(records, record);

	return records;
}

static gboolean ldif_parser_token_lookup(gpointer key,
										 gpointer value,
										 gpointer user_data) {
	gchar* lookup = (gchar *) key;
	gchar* find = (gchar *) user_data;
	
	//debug_print("Hash key: %s cmp Lookup: %s\n", lookup, find);
	if (strcasecmp(lookup, find) == 0)
		return TRUE;
	
	return FALSE;
}

static void ldif_parser_scan_file(LdifParser* parser) {
	GSList *list, *cur, *tokens, *cur1;
	gchar* found;
	Record* record;
	Token* token;
	
	list = ldif_parser_strsplit(parser);
	gslist_free(&parser->records, ldif_parser_record_free);
	cur = list;
	while (cur) {
		record = (Record *) cur->data;
		//ldif_parser_record_dump(record);
		tokens = gslist_deep_copy(record->tokens, ldif_parser_token_pairs_copy);
		gslist_free(&record->tokens, ldif_parser_token_pairs_free);
		gslist_free(&record->dropped, ldif_parser_token_pairs_free);
		for (cur1 = tokens; cur1; cur1 = g_slist_next(cur1)) {
			token = (Token *) cur1->data;
			//ldif_parser_token_pair_dump(token);
			found = g_hash_table_find(parser->symbols,
					ldif_parser_token_lookup, token->name);
			if (found || strcasecmp("dn", token->name) == 0) {
				debug_print("%s: adding to 'tokens'\n", token->name);
				record->tokens = g_slist_prepend(record->tokens, token);
			}
			else {
				debug_print("%s: adding to 'dropped'\n", token->name);
				record->dropped = g_slist_prepend(record->dropped, token);
			}
		}
		gslist_free(&tokens, NULL);
		parser->records = g_slist_prepend(parser->records, record);
		//ldif_parser_record_dump(record);
		debug_print("===========================================================\n");
		cur = cur->next;
	}
	gslist_free(&list, NULL);
}

static gboolean ldif_parser_parse_func(LdifParser* parser) {
	gboolean result = FALSE;
	
	cm_return_val_if_fail(parser != NULL, result);
	
	if (parser->fatal_error)
		result = TRUE;
	
	if (! parser->symbols || g_hash_table_size(parser->symbols) < 1)
		return result;

	ldif_parser_read_file(parser);
	if (parser->fatal_error)
		result = TRUE;
	
	ldif_parser_scan_file(parser);
	if (parser->fatal_error)
		result = TRUE;
	
	return result;
}

static void hash_table_copy_row(gpointer key,
								gpointer value,
								gpointer user_data) {
    GHashTable* hash = (GHashTable *) user_data;

	debug_print("Key: %s - Value: %s\n", key, value);
    g_hash_table_replace(hash, g_strdup(key), g_strdup(value));
}

static gpointer symbols_copy(GHashTable* hash) {
    GHashTable* copied_hash;
    
    if (! hash)
    	return NULL;
    	
    copied_hash = hash_table_new();
    if (hash)
    	g_hash_table_foreach(hash, hash_table_copy_row, copied_hash);

    return copied_hash;
}

/*
typedef struct {
	TokenType				type;
	gchar*					name;
	union {
		gchar*				value;
		struct _TokenVals {
			guint			bv_len;
			gchar*			bv_val;
		} token_vals;
	};
} Tokens;
*/
void ldif_parser_token_pair_dump(gpointer data) {
	Token* token;
	gint i;
	
	if (! data)
		return;
	
	token = (Token *) data;
	fprintf(stderr, _("Key: %s "), token->name);
	switch (token->type) {
		case LDIF_PARSER_VALS:
			fprintf(stderr, _("Value: %s\n"), token->value);
			break;
		case LDIF_PARSER_BVALS:
			fprintf(stderr, "Value: ");
			for (i = 0; i <	token->token_bvals.bv_len; i++)
				fprintf(stderr, "%c", token->token_bvals.bv_val[i]);
			fprintf(stderr, "\n");
			break;
		default:
			break;
	}
}

void ldif_parser_record_dump(gpointer data) {
	Record* record;
	GSList* cur;
	
	if (! data)
		return;
		
	record = (Record *) data;
	fprintf(stderr, "dn: %s\n", record->dn);

	fprintf(stderr, _("Info to save:\n"));
	cur = record->tokens;
	while (cur) {
		ldif_parser_token_pair_dump(cur->data);
		cur = cur->next;
	}

	fprintf(stderr, _("Info which is not saved:\n"));
	cur = record->dropped;
	while (cur) {
		ldif_parser_token_pair_dump(cur->data);
		cur = cur->next;
	}
}

gpointer ldif_parser_token_pairs_copy(gpointer item) {
	Token *new, *old;

	if (! item)
		return NULL;
	
	old = (Token *) item;
	//ldif_parser_token_pair_dump(old);
	new = g_new0(Token, 1);
	new->type = old->type;
	new->name = g_strdup(old->name);
	if (old->type == LDIF_PARSER_VALS)
		new->value = g_strdup(old->value);
	else {
		new->token_bvals.bv_len = old->token_bvals.bv_len;
		new->token_bvals.bv_val = 
			g_memdup(old->token_bvals.bv_val, old->token_bvals.bv_len);
	}
	//ldif_parser_token_pair_dump(new);
	return new;
}

gpointer ldif_parser_records_copy(gpointer item) {
	Record *new, *old;
	
	if (! item)
		return NULL;

	old = (Record *) item;
	new = g_new0(Record, 1);
	new->dn = g_strdup(old->dn);
	new->tokens = gslist_deep_copy(old->tokens, ldif_parser_token_pairs_copy);	
	new->dropped = gslist_deep_copy(old->dropped, ldif_parser_token_pairs_copy);
	new->dirty = old->dirty;
	
	return new;
}

LdifParser* ldif_parser_new(void) {
	LdifParser* parser = g_new0(LdifParser, 1);
	
	cm_return_val_if_fail(parser != NULL, NULL);
	
	parser->symbols = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
	parser->error_handler = ldif_parser_error_func;
	parser->parser_handler = ldif_parser_parse_func;
	
	return parser;
}

void ldif_parser_destroy(LdifParser* parser) {
	if (! parser)
		return;

	if (parser->open_fd_by_me)
		close(parser->input_fd);

	g_free(parser->buffer);
	g_free(parser->input_name);
	g_free(parser->error_msg);
	gslist_free(&parser->records, ldif_parser_record_free);
	g_hash_table_destroy(parser->symbols);
	g_free(parser);
	parser = NULL;
}

guint ldif_parser_get_errors(LdifParser* parser) {
	cm_return_val_if_fail(parser != NULL, INT_MAX);

	return parser->errors;
}

void ldif_parser_set_fd(LdifParser* parser, gint fd) {
	struct stat st;
	
	cm_return_if_fail(parser != NULL);
	cm_return_if_fail(fd != 0);
	
	parser->input_fd = fd;
	gint status = fstat(parser->input_fd, &st);
	if (status || st.st_size < 1) {
		gchar* err = strerror(errno);
		ldif_parser_set_error(parser, LDIF_PARSER_BAD_FD, err);
		parser->fatal_error = TRUE;
		return;
	}
	parser->open_fd_by_me = FALSE;
#ifdef G_OS_UNIX
	GError* error = NULL;
	gchar* name = g_strdup_printf("/proc/self/fd/%d", fd);
	g_free(parser->input_name);
	parser->input_name = g_file_read_link(name, &error);
	g_free(name);
	if (error) {
		ldif_parser_set_error(parser, error->code, error->message);
		g_clear_error(&error);
	}
#else
	parser->input_name = g_strdup(_("cannot determine"));
#endif
}

void ldif_parser_set_file(LdifParser* parser, const gchar* file) {
	struct stat st;
	
	cm_return_if_fail(parser != NULL);
	cm_return_if_fail(file != NULL);
	
	gint status = stat(file, &st);
	if (status || st.st_size < 1) {
		gchar* err = strerror(errno);
		ldif_parser_set_error(parser, LDIF_PARSER_BAD_FILE, err);
		return;
	}

	int fd = g_open(file, O_RDONLY);
	if (fd == -1) {
		gchar* err = strerror(errno);
		ldif_parser_set_error(parser, LDIF_PARSER_BAD_FILE, err);
		return;
	}
	parser->open_fd_by_me = TRUE;
	parser->input_fd = fd;
	g_free(parser->input_name);
	parser->input_name = g_strdup(file);
}

void ldif_parser_set_user_data(LdifParser* parser, gpointer user_data) {
	cm_return_if_fail(parser != NULL);
	
	parser->user_data = user_data;
}

void ldif_parser_set_max_errors(LdifParser* parser, guint max) {
	cm_return_if_fail(parser != NULL);
	
	parser->max_errors = max;
}

gboolean ldif_parser_parse(LdifParser* parser) {
	cm_return_val_if_fail(parser != NULL, TRUE);

	return parser->parser_handler(parser);
}

void ldif_parser_token_pairs_free(gpointer data) {
	Token* token;
	
	if (! data)
		return;
	
	token = (Token *) data;
	g_free(token->name);
	switch (token->type) {
		case LDIF_PARSER_VALS:
			g_free(token->value);
			break;
		case LDIF_PARSER_BVALS:
			g_free(token->token_bvals.bv_val);
			break;
		default:
			break;
	}
	
	g_free(token);
	token = NULL;
}

void ldif_parser_record_free(gpointer data) {
	Record* record;
	
	if (! data)
		return;
		
	record = (Record *) data;
	g_free(record->dn);
	gslist_free(&record->tokens, ldif_parser_token_pairs_free);
	gslist_free(&record->dropped, ldif_parser_token_pairs_free);	

	g_free(record);
	record = NULL;
}

void ldif_parser_add_symbol(LdifParser* parser, const gchar* symbol) {
	cm_return_if_fail(parser != NULL);
	cm_return_if_fail(symbol != NULL);
	cm_return_if_fail(parser->symbols != NULL);
	
	debug_print("symbol: %s\n", symbol);
	g_hash_table_replace(parser->symbols, g_strdup(symbol), g_strdup(symbol));
}

void ldif_parser_set_symbols(LdifParser* parser, GHashTable* symbols) {
	cm_return_if_fail(parser != NULL);
	
	if (parser->symbols)
		g_hash_table_destroy(parser->symbols);
		
	parser->symbols = symbols_copy(symbols);
}

GSList* ldif_parser_get_records(LdifParser* parser) {
	cm_return_val_if_fail(parser != NULL, NULL);
	
	return gslist_deep_copy(parser->records, ldif_parser_records_copy);
}


