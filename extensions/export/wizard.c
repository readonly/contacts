/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "wizard.h"
#include "utils.h"
#include "gtk-utils.h"
#include "mainwindow.h"
#include "extension.h"

#define NUMPAGES 3
typedef struct {
  GtkWidget *widget;
  gint index;
  const gchar *title;
  GtkAssistantPageType type;
  gboolean complete;
} PageInfo;

typedef struct {
	MenuItem*		item;
	GtkWidget*		filename;
	GtkWidget*		suffix;
	GtkWidget*		rdn;
	GtkWidget*		use_dn;
	ExportData*		data;
	WIZARD_CALLBACK	callback;
	GtkAssistant*	wizard;
	GSList*			confirm;
} Container;

const gchar* RDN[] = {
	"display name",
	"cn",
	"mail",
	NULL
};

static void wizard_close_cancel(GtkAssistant* assistant, gpointer data) {
	gtk_widget_destroy(GTK_WIDGET(assistant));
	g_free((Container* ) data);
}

static void wizard_apply(GtkAssistant* assistant, gpointer data) {
	Container* container = (Container *) data;
	
	debug_print("apply called\n");
	container->callback(container->data, container->item);
}

static void wizard_file_chooser(GtkButton* btn, gpointer data) {
	GtkWidget* dialog;
	Container* export = (Container *) data;
	
	dialog = gtk_file_chooser_dialog_new(_("Create file"),
					      GTK_WINDOW(export->item->mainwindow->window),
					      GTK_FILE_CHOOSER_ACTION_SAVE,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					      NULL);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), get_home());
/*	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);*/
	gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
	
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		export->data->filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		gtk_entry_set_text(GTK_ENTRY(export->filename), export->data->filename);
	}
	gtk_widget_destroy(dialog);
}

static void page1_entry_changed(GtkEditable *entry, Container* container) {
	const gchar* text = gtk_entry_get_text(GTK_ENTRY(entry));
	gint num = gtk_assistant_get_current_page(container->wizard);
	GtkWidget* page = gtk_assistant_get_nth_page(container->wizard, num);
	gtk_assistant_set_page_complete(container->wizard, page, (strlen(text) > 0));
}

static void page2_entry_changed(GtkEditable *entry, Container* container) {
	const gchar* text = gtk_entry_get_text(GTK_ENTRY(entry));
	gint num = gtk_assistant_get_current_page(container->wizard);
	GtkWidget* page = gtk_assistant_get_nth_page(container->wizard, num);
	gtk_assistant_set_page_complete(container->wizard, page, (strlen (text) > 0));
}

static void page3_entries_set(Container* container) {
	GSList* cur = container->confirm;
	
	while (cur) {
		GtkWidget* entry = (GtkWidget *) cur->data;
		const gchar* name = gtk_widget_get_name(entry);
		
		if (strcmp("filename", name) == 0)
			gtk_entry_set_text(GTK_ENTRY(entry), container->data->filename);
		else if (strcmp("suffix", name) == 0)
			gtk_entry_set_text(GTK_ENTRY(entry), container->data->suffix);
		else if (strcmp("rdn", name) == 0)
			gtk_entry_set_text(GTK_ENTRY(entry), container->data->rdn);
		else if (strcmp("use_dn", name) == 0)
			gtk_entry_set_text(GTK_ENTRY(entry),
			(container->data->use_dn) ? "Yes" : "No");
		else {
		}
		cur = cur->next;
	}
}

static GtkWidget* create_page_1(Container* container) {
	GtkWidget *hbox, *vbox, *label, *file_btn;
	
	vbox = gtk_vbox_new(FALSE, 15);
	
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 15);
	
	label = gtk_label_new(_("Enter Filename"));
	container->filename = gtk_entry_new();

	g_signal_connect(G_OBJECT(container->filename), "changed", G_CALLBACK(page1_entry_changed), container);
	file_btn = gtk_button_new_from_stock(GTK_STOCK_SAVE);
	g_signal_connect(G_OBJECT(file_btn), "clicked", G_CALLBACK(wizard_file_chooser), container);
	
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
	gtk_box_pack_start(GTK_BOX(hbox), container->filename, TRUE, TRUE, 5);
	gtk_box_pack_start(GTK_BOX(hbox), file_btn, FALSE, FALSE, 5);
	
	return vbox;
}

static gchar* get_dc_parts() {
	gchar* domainname = get_domain_name();
	gchar *dc , *pos, *tail, *token;
	GString* buf;
	
	if (domainname) {
		buf = g_string_new("");
		tail = domainname;
		pos = strchr(domainname, '.');
		while (tail) {
			token = strndup(tail, pos - tail);
			if (buf->len < 1)
				g_string_append_printf(buf, "dc=%s", token);
			else
				g_string_append_printf(buf, ",dc=%s", token);
			g_free(token);
			if (pos)
				pos += 1;
			tail = pos;
			if (pos)
				pos = strchr(pos, '.');
		}
		if (buf->len < 1) {
			g_string_free(buf, TRUE);
			dc = g_strdup("dc=localhost,dc=localdomain");
		}
		else
			dc = g_string_free(buf, FALSE);
	}
	else
		dc = g_strdup("dc=localhost,dc=localdomain");
		
	return dc;
}

static GtkWidget* create_page_2(Container* container) {
	GtkWidget *hbox, *vbox, *label;
	GSList* rdns = NULL;
	gint i;
	const gchar* user = g_get_user_name();
	
	vbox = gtk_vbox_new(FALSE, 15);
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 15);
	
	label = gtk_label_new(_("Enter suffix"));
	container->suffix = gtk_entry_new();
	gtk_widget_set_tooltip_text(container->suffix, _("Suffix is the common part of the Distinguished\n"
										 "Name (DN) shared with any other component\n"
										 "stored in this part of the directory"));

	gchar* dc = get_dc_parts();
	container->data->suffix = g_strconcat("uid=", user, ",ou=people,", dc, NULL);
	g_free(dc);
	gtk_entry_set_text(GTK_ENTRY(container->suffix), container->data->suffix);

	g_signal_connect(G_OBJECT(container->suffix), "changed", G_CALLBACK(page2_entry_changed), container);

	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
	gtk_box_pack_start(GTK_BOX(hbox), container->suffix, TRUE, TRUE, 5);
	
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);
	
	label = gtk_label_new(_("Enter Relative DN"));
	for (i = 0; RDN[i]; i++)
		rdns = g_slist_prepend(rdns, (gchar *) RDN[i]);
	container->rdn = create_combo_box_text(rdns);
	gtk_widget_set_tooltip_text(container->rdn, _("Relative DN (RDN) is the last component\n"
										 "(read from right to left) placed before\n"
										 "suffix. This must be the part of the DN\n"
										 "which uniquely distinguishes this entry\n"
										 "from any other entry"));
	gtk_combo_box_set_active(GTK_COMBO_BOX(container->rdn), 0);
	gtk_widget_set_size_request(container->rdn, 200, -1);
	gslist_free(&rdns, NULL);

	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
	gtk_box_pack_start(GTK_BOX(hbox), container->rdn, FALSE, FALSE, 5);

	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);

	container->use_dn = gtk_check_button_new_with_mnemonic(
			_("_Use DN attribute if present in data"));
	gtk_widget_set_tooltip_text(container->use_dn, _("If the contact has a defined\n"
													 "DN should that be used instead\n"
													 "of a constructed one"));
	gtk_box_pack_start(GTK_BOX(hbox), container->use_dn, TRUE, TRUE, 5);

	return vbox;	
}

static GtkWidget* create_page_3(Container* container) {
	GtkWidget *hbox, *vbox, *label, *entry;
	
	vbox = gtk_vbox_new(FALSE, 15);
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

	label = gtk_label_new(NULL);
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 10);

	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

	label = gtk_label_new(_("Writing to file"));
	gtk_widget_set_size_request(label, 100, -1);
	entry = gtk_entry_new();
	gtk_widget_set_size_request(entry, 180, -1);
	gtk_widget_set_sensitive(entry, FALSE);
	container->confirm = g_slist_prepend(container->confirm, entry);
	gtk_widget_set_name(entry, "filename");
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 10);
	gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 10);
	
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

	label = gtk_label_new(_("Chosen suffix"));
	gtk_widget_set_size_request(label, 100, -1);
	entry = gtk_entry_new();
	gtk_widget_set_sensitive(entry, FALSE);
	container->confirm = g_slist_prepend(container->confirm, entry);
	gtk_widget_set_name(entry, "suffix");
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 10);
	gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 10);
	
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

	label = gtk_label_new(_("Chosen RDN"));
	gtk_widget_set_size_request(label, 100, -1);
	entry = gtk_entry_new();
	gtk_widget_set_sensitive(entry, FALSE);
	container->confirm = g_slist_prepend(container->confirm, entry);
	gtk_widget_set_name(entry, "rdn");
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 10);
	gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 10);
	
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

	label = gtk_label_new(_("Use found DN"));
	gtk_widget_set_size_request(label, 100, -1);
	entry = gtk_entry_new();
	gtk_widget_set_sensitive(entry, FALSE);
	container->confirm = g_slist_prepend(container->confirm, entry);
	gtk_widget_set_name(entry, "use_dn");
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 10);
	gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 10);
	
	return vbox;
}

static void wizard_prepare(GtkWidget* widget, GtkWidget* page, gpointer data) {
	gint current_page;
	Container* container = (Container *) data;
	ExportData* export = container->data;
	
	current_page = gtk_assistant_get_current_page (GTK_ASSISTANT(widget));

	switch (current_page) {
		case 0:
			/* Initial show */
			break;
		case 1:
			g_free(export->filename);
			export->filename = gtk_editable_get_chars(GTK_EDITABLE(container->filename), 0, -1);
			break;
		case 2:
			g_free(export->suffix);
			export->suffix = gtk_editable_get_chars(GTK_EDITABLE(container->suffix), 0, -1);
			g_free(export->rdn);
			export->rdn = combo_box_text_get_active(container->rdn);
			export->use_dn = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(container->use_dn));
			page3_entries_set(container);
			break;
		default:
			break;
	}
}

void show_export_wizard(gpointer item, WIZARD_CALLBACK callback) {
	PageInfo page[NUMPAGES] = {
    	{ NULL, -1, _("Choose file name"),				GTK_ASSISTANT_PAGE_CONTENT,	FALSE},
    	{ NULL, -1, _("Construct DN for record"),		GTK_ASSISTANT_PAGE_CONTENT,	TRUE},
    	{ NULL, -1, _("Create file with these values"),	GTK_ASSISTANT_PAGE_CONFIRM,	TRUE},
	};
	gint i;
    GdkColor color;
    color.red = 65535;
    color.green = 65535;
    color.blue = 65535;

	Container* container = g_new0(Container, 1);
	container->data = g_new0(ExportData, 1);
	container->item = (MenuItem *) item;
	container->callback = callback;
	
	GtkWidget *wizard;
	
	wizard = gtk_assistant_new();
	gtk_widget_modify_bg(wizard, GTK_STATE_NORMAL, &color);
	gtk_window_set_transient_for(GTK_WINDOW(wizard), GTK_WINDOW(container->item->mainwindow->window));
	gtk_widget_set_size_request(wizard, 450, 300);
	gtk_window_set_title(GTK_WINDOW(wizard), _("Export to LDIF"));
	g_signal_connect(G_OBJECT(wizard), "cancel", G_CALLBACK(wizard_close_cancel), container);
	g_signal_connect(G_OBJECT(wizard), "close", G_CALLBACK(wizard_close_cancel), container);
	g_signal_connect(G_OBJECT(wizard), "apply", G_CALLBACK(wizard_apply), container);
	g_signal_connect(G_OBJECT(wizard), "prepare", G_CALLBACK(wizard_prepare), container);
	
	page[0].widget = create_page_1(container);
	page[1].widget = create_page_2(container);
	page[2].widget = create_page_3(container);
	
	for (i = 0; i < NUMPAGES; i++) {
    	page[i].index = gtk_assistant_append_page(GTK_ASSISTANT(wizard), page[i].widget);
    	gtk_assistant_set_page_title(GTK_ASSISTANT(wizard), page[i].widget, page[i].title);
    	gtk_assistant_set_page_type(GTK_ASSISTANT(wizard), page[i].widget, page[i].type);
		gtk_assistant_set_page_complete(GTK_ASSISTANT(wizard), page[i].widget, page[i].complete);
	}
	
	container->wizard = GTK_ASSISTANT(wizard);
	
	gtk_widget_show_all(wizard);
}

void export_data_free(ExportData** data) {
	if (! data || ! *data)
		return;
		
	g_free((*data)->filename);
	g_free((*data)->suffix);
	g_free((*data)->rdn);
	g_free(*data);
	*data = NULL;
}
