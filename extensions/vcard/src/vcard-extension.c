/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include "extension.h"
#include "plugin-loader.h"
#include "plugin.h"
#include "utils.h"
#include "gtk-utils.h"
#include "vobject.h"
#include "vcc.h"
#include "vcard-utils.h"

#define NAME "vCard extension"

typedef struct {
	gchar* buffer;
	long   size;
} NormalizeFile;


static guint my_id;

static gchar* vcard_file_chooser(MenuItem* item, int mode) {
	GtkWidget* dialog;
	gchar* filename = NULL;
	gchar* title;
		
	if (mode == GTK_FILE_CHOOSER_ACTION_SAVE)
		title = N_("Save file");
	else
		title = N_("Open file");
		
	dialog = gtk_file_chooser_dialog_new(title,
					      GTK_WINDOW(item->mainwindow->window),
					      mode,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					      NULL);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), get_home());
/*	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);*/
	if (mode == GTK_FILE_CHOOSER_ACTION_SAVE)
		gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
	
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
	}
	gtk_widget_destroy(dialog);
	
	return filename;
}

static void mime_error_handler(char *s, void* user_data) {
	MenuItem* item = (MenuItem *) user_data;
	
	gchar* msg = g_strdup_printf(_("vCard parser: %s"), s);
	debug_print("%s\n", msg);
	show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_WARNING, msg);
	g_free(msg);
}

static NormalizeFile* buffer_resize(NormalizeFile* buffer, long size) {
	if (buffer->buffer) {
		/*buffer->buffer = g_renew(gchar, buffer->buffer, size);*/
		gchar* tmp = g_new0(gchar, size);
		tmp = g_memdup(buffer->buffer, buffer->size);
		g_free(buffer->buffer);
		buffer->buffer = g_memdup(tmp, size);
		g_free(tmp);
	}
	else {
		buffer->buffer = g_new0(gchar, size);
	}
	buffer->size = size;

	return buffer;
}

#define bufsize 1024
static NormalizeFile* normalize_file(const MainWindow* mainwindow, gchar* filename) {
	NormalizeFile* file_buffer = NULL;
	size_t numread;
	gchar* buf = NULL;
	int i;
	int size = 0;
	long destpos = 0;
	
	FILE* fp = g_fopen(filename, "r");
	if (! fp) {
		show_message(mainwindow->window, GTK_UTIL_MESSAGE_ERROR, strerror(errno));
		return NULL;
	}
	file_buffer = g_new0(NormalizeFile, 1);
	do {
		g_free(buf);
		buf = g_new0(gchar, bufsize);
		numread = fread(buf, bufsize, 1, fp);
		if (ferror(fp)) {
			show_message(mainwindow->window, GTK_UTIL_MESSAGE_ERROR,
				strerror(errno));
			g_free(file_buffer->buffer);
			g_free(file_buffer);
			fclose(fp);
			return NULL;
		}
		size++;
		file_buffer = buffer_resize(file_buffer, size * bufsize);
		if (! file_buffer->buffer) {
			show_message(mainwindow->window, GTK_UTIL_MESSAGE_ERROR,
				_("Insufficient memory"));
			g_free(file_buffer->buffer);
			g_free(file_buffer);
			fclose(fp);
			return NULL;
		}
		for (i = 0; i < bufsize && buf[i]; i++) {
			if (destpos > 0 && file_buffer->buffer[destpos - 1] == '\n' && buf[i] == ' ') {
				destpos--;
			}
			else {
				file_buffer->buffer[destpos++] = buf[i];
			}
		}
	} while (numread != 0);
	g_free(buf);
	fclose(fp);
	if (destpos && (size * bufsize) % destpos == 0) {
		file_buffer = buffer_resize(file_buffer, ++destpos);
	}
	file_buffer->buffer[destpos] = 0;
	file_buffer->size = destpos;
	
	return file_buffer;
}

static void vcard_import(GtkWidget* widget, gpointer data) {
	MenuItem* item = (MenuItem *) data;
	VObject *t, *v;
	gchar* error = NULL;
	AddressBook* abook;
	Plugin* plugin;
	gint count = 0, total = 0;
	gchar* leftover;
	FILE* f = NULL;
	GtkTreeIter* iter = NULL;
	GtkTreeSelection* row;
	GtkTreeView* view;

	view = GTK_TREE_VIEW(item->mainwindow->abook_list);
	abook = get_selected_address_book(view);
	if (abook == NULL) {
		iter = set_selection_combobox(item->mainwindow->window,
						_("[vCard Import] Choose address book"),
						gtk_tree_view_get_model(view),
						BOOK_NAME_COLUMN);
		if (! iter)
			return;

		row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
		abook = get_selected_address_book(view);
	}
	plugin = get_selected_plugin(GTK_TREE_VIEW(item->mainwindow->abook_list));
	
	gchar* file = vcard_file_chooser(item, GTK_FILE_CHOOSER_ACTION_OPEN);
	if (! file)
		return;
	NormalizeFile* file_buffer = normalize_file(item->mainwindow, file);
	g_free(file);
	if (! file_buffer || ! file_buffer->buffer) {
		if (file_buffer)
			g_free(file_buffer);
		return;
	}
		
	gchar* filename = g_strdup_printf("vcard_rejected.%d", getpid());
	leftover = g_build_filename(get_home(), filename, NULL);
	g_free(filename);
	registerMimeErrorHandler(mime_error_handler, data);
	v = Parse_MIME(file_buffer->buffer, file_buffer->size);
	while (v) {
		total++;
		const gchar* name = vObjectName(v);
	    if (name && strcmp(name,VCCardProp) == 0) {
			t = v;
			Contact* c = vcard2contact(t, plugin, &error);
			if (c)
				plugin->set_contact(abook, c, &error);
			if (error) {
				show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_ERROR, error);
				g_free(error);
				error = NULL;
				if (! f)
					f = g_fopen(leftover, "w");
				if (f) {
					writeVObject(f, v);
				}
			}
			else {
				count++;
				abook->contacts = g_list_prepend(abook->contacts, c);
				list_view_append_contact(GTK_TREE_VIEW(item->mainwindow->contact_list), c);
			}
		}
		else
			mime_error_handler(_("Object is not in vCard format"), data);
		v = nextVObjectInList(v);
		cleanVObject(t);
	}

	if (f) {
		fclose(f);
		abook->contacts = g_list_reverse(abook->contacts);
		show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_INFO,
			_("%s: Imported %d contacts out of %d Objects.\n"
		  	  "Rejected Objects can be found in %s"),
			  abook->abook_name, count, total, leftover);
	}
	else {
		g_file_set_contents(leftover, file_buffer->buffer, file_buffer->size, NULL);
		show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_INFO,
			_("Parsed file can be found in %s"), leftover);
	}
	g_free(file_buffer->buffer);
	g_free(file_buffer);
	g_free(leftover);
}

static void vcard_export(GtkWidget* widget, gpointer data) {
	MenuItem* item = (MenuItem *) data;
	FILE* fp;
	gchar* error = NULL;
	VObject* o;
	GSList *list, *cur;
	AddressBook* abook;
	Plugin* plugin;
	gint count = 0;
	GtkTreeIter* iter = NULL;
	GtkTreeSelection* row;
	GtkTreeView* view;

	view = GTK_TREE_VIEW(item->mainwindow->abook_list);
	abook = get_selected_address_book(view);
	if (abook == NULL) {
		iter = set_selection_combobox(item->mainwindow->window,
						_("[vCard Export] Choose address book"),
						gtk_tree_view_get_model(view),
						BOOK_NAME_COLUMN);
		if (! iter)
			return;

		row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
		abook = get_selected_address_book(view);
	}
	plugin = get_selected_plugin(GTK_TREE_VIEW(item->mainwindow->abook_list));
	
	if (!abook && !plugin) {
		show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_ERROR,
			_("Missing address book"));
		return;
	}

	gchar* file = vcard_file_chooser(item, GTK_FILE_CHOOSER_ACTION_SAVE);
	if (! file)
		return;
	fp = g_fopen(file, "w");
	if (! fp) {
		show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_ERROR, strerror(errno));
		g_free(file);
		return;
	}
	g_free(file);

	if (item->menu == CONTACTS_ADDRESSBOOK_MENU ||
			item->menu == CONTACTS_MAIN_MENU) {
		list = contacts2vcard(abook->contacts, plugin, &error);
		if (error) {
			show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_ERROR, error);
		}
		else {
			for (cur = list; cur; cur = g_slist_next(cur)) {
				count++;
				o = (VObject *) cur->data;
				writeVObject(fp, o);
				cleanVObject(o);
			}
		}
		gslist_free(&list, NULL);
	}
	else {
		o = contact2vcard(item->mainwindow->selected_contact, plugin, &error);
		if (error) {
			show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_ERROR, error);
		}
		else if (o) {
			count++;
			writeVObject(fp, o);
		}
		if (o)
			cleanVObject(o);
	}
	fclose(fp);

	if (error)
		g_free(error);
	else
		show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_INFO,
			_("%s: Exported %d contacts"),	abook->abook_name, count);
}

static void vcard_create(GtkWidget* widget, gpointer data) {
	MenuItem* item = (MenuItem *) data;
	//gchar* vcard;
	gchar* error = NULL;
	//FILE* fp;

	personal_vcard_make(item->mainwindow, &error);
	if (error) {
		show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_ERROR, error);
	}
/*	else if (vcard) {
		gchar* file = vcard_file_chooser(item, GTK_FILE_CHOOSER_ACTION_SAVE);
		if (! file)
			return;
		fp = g_fopen(file, "w");
		if (! fp) {
			show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_ERROR, strerror(errno));
			g_free(file);
			return;
		}
		g_free(file);
		size_t chars = fwrite(vcard, sizeof(gchar), strlen(vcard), fp);
		if (chars < strlen(vcard))
			show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_ERROR,
				"%s: Write failed", vcard);
		fclose(fp);
	}
	if (vcard) {
		g_free(vcard);
	}*/
}

static void setup(const MainWindow* mainwindow, gpointer object) {
    GtkWidget *menu;
    MenuItem* menu_item;
	
	// Add a menu item into the tools menu
	menu = gtk_image_menu_item_new_with_mnemonic(_("_vCard"));
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_MAIN_MENU;
    menu_item->parent = _("tools");
	menu_item->sublabel = _("_Export");
    menu_item->submenu = TRUE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
            G_CALLBACK(vcard_export), (gpointer) menu_item);
	add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);
	
	// Add a menu item into the tools menu
	menu = gtk_image_menu_item_new_with_mnemonic(_("_vCard"));
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_MAIN_MENU;
    menu_item->parent = _("tools");
	menu_item->sublabel = _("_Import");
    menu_item->submenu = TRUE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
            G_CALLBACK(vcard_import), (gpointer) menu_item);
	add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);

	// Add a menu item into the tools menu
	menu = gtk_image_menu_item_new_with_mnemonic(_("_Create vCard"));
    gtk_widget_add_accelerator(menu, "activate", mainwindow->accel,
            GDK_C, GDK_CONTROL_MASK | GDK_SHIFT_MASK, GTK_ACCEL_VISIBLE);
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_MAIN_MENU;
    menu_item->parent = _("tools");
	menu_item->sublabel = NULL;
    menu_item->submenu = FALSE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
            G_CALLBACK(vcard_create), (gpointer) menu_item);
	add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);

	// Add a menu item into the context menu of contact
    menu = gtk_image_menu_item_new_with_mnemonic(_("_vCard"));
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_CONTACT_MENU;
    menu_item->sublabel = _("_Export");
    menu_item->submenu = TRUE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
		G_CALLBACK(vcard_export), (gpointer) menu_item);
    add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);

    // Add a menu item into the context menu of address book
    menu = gtk_image_menu_item_new_with_mnemonic(_("_vCard"));
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_ADDRESSBOOK_MENU;
    menu_item->sublabel = _("_Export");
    menu_item->submenu = TRUE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
		G_CALLBACK(vcard_export), (gpointer) menu_item);
    add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);

    // Add a menu item into the context menu of address book
    menu = gtk_image_menu_item_new_with_mnemonic(_("_vCard"));
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_ADDRESSBOOK_MENU;
    menu_item->sublabel = _("_Import");
    menu_item->submenu = TRUE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
		G_CALLBACK(vcard_import), (gpointer) menu_item);
    add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);
}

/**
 * The main application will call this function after loading the
 * extension providing a uniq id for the extension which is to be
 * used for further references
 * @param id uniq id provided by main application
 * @return 0 if success 1 otherwise
 */
gint extension_init(guint id) {
	my_id = id;
	gchar* error = NULL;
	
	register_hook_function(my_id, EXTENSION_AFTER_INIT_HOOK, setup, &error);
	if (error) {
		show_message(NULL, GTK_UTIL_MESSAGE_ERROR, error);
		g_free(error);
		return 1;
	}

	return 0;
}

/**
 * Called by main application when the extension should be unloaded
 * @return TRUE if success FALSE otherwise
 */
gboolean extension_done(void) {
	return TRUE;
}

/**
 * Called by main application to ensure extension license is compatible
 * @return license
 */
const gchar* extension_license(void) {
	return "GPL3+";
}

/**
 * Called by main application to get name of extension
 * @return name
 */
const gchar* extension_name(void) {
	return NAME;
}

/**
 * Called by main application to get extension's describtion
 * @return description
 */
const gchar* extension_describtion(void) {
	return _("Export and import contacts in vCard 2.1 format");
}
