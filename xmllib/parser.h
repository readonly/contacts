/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __PARSER_H__
#define __PARSER_H__

#include <glib.h>

G_BEGIN_DECLS

#include "plugin.h"

typedef enum {
	INDEXFILE,
	BOOKFILE,
	DOCTYPES
} DOCTYPE;
	
typedef struct {
	gchar* attr_name;
	gchar* file;
} Attribute;

GSList* parse_index_file(gchar* docname, gchar** error);
void addr_book_set_name(AddressBook* abook, gchar** error);
void parse_addr_book(AddressBook* abook, gchar** error);

gboolean addr_book_update_config(AddressBook* new,
								 AddressBook* old,
								 gchar** error);
gboolean addr_book_set_config(AddressBook* abook, gchar** error);

void attribute_free(gpointer attribute);
void attribute_list_free(GSList** list);

void addr_book_delete_contact(AddressBook* abook,
							  Contact* contact,
							  gchar** error);

void addr_book_set_contact(AddressBook* abook,
						   Contact* contact,
						   gchar** error);

G_END_DECLS

#endif
