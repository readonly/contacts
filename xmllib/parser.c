/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <string.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include "parser.h"
#include "plugin.h"
#include "utils.h"


const gchar *ROOTNODE[DOCTYPES] = {
									"addressbook",
									"address-book"
								  };
static const gchar *RESERVED[] = {
			"first-name",
			"last-name",
			"nick-name",
			"uid",
			"cn",
			NULL
};

static gboolean init_document(DOCTYPE type,
							  gchar* doc_path,
							  xmlDocPtr* document,
						  	  xmlNodePtr* current,
						  	  gchar** error) {
	xmlDocPtr doc;
	xmlNodePtr cur;
	gchar* message;
	
	debug_print("%s\n", doc_path);
	doc = xmlParseFile(doc_path);
	
	if (doc == NULL) {
		xmlErrorPtr xml_error = xmlGetLastError();
		if (xml_error->message)
			message = g_strdup(xml_error->message);
		else
			message =  g_strdup(_("No message"));
		*error = g_strconcat("File: ", doc_path, "\n", message, NULL);
		g_free(message);
		xmlResetError(xml_error);
		return TRUE;
	}
	
	cur = xmlDocGetRootElement(doc);
	
	if (cur == NULL) {
		*error = g_strdup(_("empty document"));
		xmlFreeDoc(doc);
		return TRUE;
	}
	
	if (xmlStrcmp(cur->name, (const xmlChar *) ROOTNODE[type])) {
            *error = g_strdup_printf(
            	_("document of the wrong type, root node != <%s>"), ROOTNODE[type]);
		xmlFreeDoc(doc);
		return TRUE;
	}
	*document = doc;
	*current = cur;
	return FALSE;
}

static void parse_attribute(GHashTable* person, xmlDocPtr doc, xmlAttr* attribs) {
    xmlChar *str;
    const gchar *attr;
    AttribDef* data;

	while (person && attribs) {
		if (g_hash_table_lookup(person, "uid") == NULL ||
				strcasecmp("uid", (const gchar *) attribs->name) != 0) {
			str = xmlNodeListGetString(doc, attribs->children, 1);
			attr = (const gchar *) attribs->name;
			debug_print("\tAttribute: %s->%s\n", attr, (const gchar *) str);
			data = pack_data(ATTRIB_TYPE_STRING, attr, (const gchar *) str);
			g_hash_table_replace(person, g_strdup(attr), data);
			xmlFree(str);
		}
		attribs = attribs->next;
 	}
}

static gboolean has_attribute(xmlDocPtr doc, xmlNodePtr cur,
							  const gchar* name, const gchar* value) {
    xmlChar *str = NULL;
	xmlAttr* attribs;
	gboolean found = FALSE;
	
	attribs = (xmlAttr *) cur->properties;
	while (attribs) {
		if (strcmp((const gchar *) attribs->name, name) == 0) {
			str = xmlNodeListGetString(doc, attribs->children, 1);
			if (str && strcmp((const gchar *) str, value) == 0)
				found = TRUE;
			break;
		}
		attribs = attribs->next;
 	}

	if (str)
		xmlFree(str);
		
	return found;
}

static gchar* get_abook_name(xmlDocPtr doc, xmlNodePtr cur) {
	xmlAttr* attribs;
	xmlChar* attr_name = NULL;
	gchar* name;

	attribs = (xmlAttr *) cur->properties;
	while (attribs) {
		if (strcmp((const gchar *) attribs->name, "name") == 0) {
			attr_name = xmlNodeListGetString(doc, attribs->children, 1);
			name = g_strdup((const gchar *) attr_name);
			xmlFree(attr_name);
			return name;
		}
		attribs = attribs->next;
 	}

	return NULL;
}

static void parse_attributes(GHashTable* person, xmlDocPtr doc, xmlNodePtr cur) {
    xmlAttr* attribs;
    xmlChar *xvalue, *xkey;
    gchar *key = NULL, *value = NULL;
    AttribDef* data;

    cur = cur->xmlChildrenNode;
    while (cur != NULL) {
        if ((cur->name && !xmlStrcmp(cur->name, (const xmlChar *) "attribute"))) {
			xvalue = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			value = g_strdup((const gchar *) xvalue);
			xmlFree(xvalue);
            attribs = (xmlAttr *) cur->properties;
			while (attribs) {
				if (attribs->name &&
						strcasecmp("name", (const gchar *) attribs->name) == 0) {
					xkey = xmlNodeListGetString(doc, attribs->children, 1);
					key = g_strdup((const gchar *) xkey);
					xmlFree(xkey);
					break;
				}
				attribs = attribs->next;
		 	}
			if (person && key && value) {
				debug_print("\tAttribute: %s->%s\n", key, value);
				data = pack_data(ATTRIB_TYPE_STRING, key, value);
				g_hash_table_replace(person, g_strdup(key), data);
			}
			if (key) {
				g_free(key);
				key = NULL;
			}
			if (value) {
				g_free(value);
				value = NULL;
			}
 		}
        cur = cur->next;
    }
}

static void parse_address(Contact* contact, xmlDocPtr doc, xmlNodePtr cur) {
    xmlAttr* attribs;
    xmlChar *str;

    cur = cur->xmlChildrenNode;
    while (cur != NULL) {
        if ((cur->name && !xmlStrcmp(cur->name, (const xmlChar *) "address"))) {
            attribs = (xmlAttr *) cur->properties;
            Email* email = g_new0(Email, 1);
            while (attribs) {
				str = xmlNodeListGetString(doc, attribs->children, 1);
				debug_print("\tAttribute: %s->%s\n",
					(const gchar *) attribs->name, (const gchar *) str);
				if (attribs->name) {
					if (strcasecmp("alias", (const gchar *) attribs->name) == 0)
						email->alias = g_strdup((const gchar *) str);
					else if (strcasecmp("email", (const gchar *) attribs->name) == 0)
						email->email = g_strdup((const gchar *) str);
					else if (strcasecmp("remarks", (const gchar *) attribs->name) == 0)
						email->remarks = g_strdup((const gchar *) str);
					else {
						/* Nothing */
					}
				}
				xmlFree(str);
				attribs = attribs->next;
			}
			contact->emails = g_slist_append(contact->emails, email);
 		}
        cur = cur->next;
    }
}

static Contact* parse_person(xmlDocPtr doc, xmlNodePtr cur) {
    xmlAttr* attribs;
    Contact* contact;
	
	contact = contact_new();
	attribs = (xmlAttr *) cur->properties;
	parse_attribute(contact->data, doc, attribs);
    cur = cur->xmlChildrenNode;
    while (cur != NULL) {
		if (cur->name) {
			if ((!xmlStrcmp(cur->name, (const xmlChar *) "address-list"))) {
				parse_address(contact, doc, cur);
			}
			else if ((!xmlStrcmp(cur->name, (const xmlChar *) "attribute-list"))) {
				parse_attributes(contact->data, doc, cur);
			}
		}
		while (gtk_events_pending())
			gtk_main_iteration();
        cur = cur->next;
    }
    
    return contact;
}

static GSList* parse_book(xmlDocPtr doc, xmlNodePtr cur) {
    xmlAttr* attribs;
    xmlChar *key;
    GSList* list = NULL;
    Attribute* book = NULL;

    cur = cur->xmlChildrenNode;
    while (cur != NULL) {
        if ((cur->name && !xmlStrcmp(cur->name, (const xmlChar *) "book"))) {
			if (debug_get_mode()) {
            	key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            	const gchar* tmp = (const gchar *) key;
            	debug_print("Element: %s = %s\n", cur->name, tmp? tmp : "Empty");
            	xmlFree(key);
			}
            attribs = (xmlAttr *) cur->properties;
            while (attribs) {
                key = xmlNodeListGetString(doc, attribs->children, 1);
                debug_print("Attribute: %s = %s\n", attribs->name, key);
                if (attribs->name) {
					if (strcasecmp("name", (const gchar *) attribs->name) == 0) {
						if (book == NULL)
							book = g_new0(Attribute, 1);
						book->attr_name = g_strdup((const gchar *) key);
						if (book->file) {
							list = g_slist_prepend(list, book);
							book = NULL;
						}
					}
					else if (strcasecmp("file", (const gchar *) attribs->name) == 0) {
						if (book == NULL)
							book = g_new0(Attribute, 1);
						book->file = g_strdup((const gchar *) key);
						if (book->attr_name) {
							list = g_slist_prepend(list, book);
							book = NULL;
						}
					}
				}
                xmlFree(key);
                attribs = attribs->next;
            }
 		}
        cur = cur->next;
    }
    
    return list;
}

static gchar* hash_table_get_entry(GHashTable* hash, const gchar* key) {
	AttribDef* attr;
	
	if (! hash || ! key)
		return "";
		
	attr = g_hash_table_lookup(hash, key);
	
	return (attr) ? attr->value.string : "";
}

static void hash_table_get_entries(gpointer key,
								   gpointer value,
								   gpointer data) {
	xmlNodePtr attribute;
	xmlNodePtr parent = (xmlNodePtr) data;
	AttribDef* attr = (AttribDef *) value;
	const gchar** reserved = RESERVED;
	
	while (key && *reserved) {
		if (strcmp((gchar *) key, *reserved++) == 0) {
			return;
		}
	}
	
	gchar* val = (attr) ? attr->value.string : "";
	attribute = xmlNewNode(NULL, (const xmlChar *) "attribute");
	xmlSetProp(attribute, (xmlChar *) "name", (xmlChar *) g_strdup(key));
	xmlNodeAddContent(attribute, (const xmlChar *) g_strdup(val));
	xmlAddChild(parent, attribute);
}

static void person_make_attr_list(xmlNodePtr parent, Contact* contact) {
	GHashTable* hash = contact->data;
	gchar* value;
	
	if (hash) {
		value = hash_table_get_entry(hash, "first-name");
		xmlSetProp(parent, (xmlChar *) "first-name", (xmlChar *) g_strdup(value));
		value = hash_table_get_entry(hash, "last-name");
		xmlSetProp(parent, (xmlChar *) "last-name", (xmlChar *) g_strdup(value));
		value = hash_table_get_entry(hash, "nick-name");
		xmlSetProp(parent, (xmlChar *) "nick-name", (xmlChar *) g_strdup(value));
		value = hash_table_get_entry(hash, "uid");
		xmlSetProp(parent, (xmlChar *) "uid", (xmlChar *) g_strdup(value));
		value = hash_table_get_entry(hash, "cn");
		xmlSetProp(parent, (xmlChar *) "cn", (xmlChar *) g_strdup(value));
	}
}

static void attribute_list_make(xmlNodePtr parent, Contact* contact) {	
	if (parent && contact && contact->data)
		g_hash_table_foreach(contact->data, hash_table_get_entries, parent);
}

static void address_list_make(xmlNodePtr parent, Contact* contact) {
	GSList* cur;
	xmlNodePtr address;
	
	for (cur = contact->emails; cur; cur = g_slist_next(cur)) {
		address = xmlNewNode(NULL, (const xmlChar *) "address");
		Email* email = (Email *) cur->data;
		if (email->remarks)
			xmlSetProp(address, (xmlChar *) "remarks",
					(xmlChar *) g_strdup(email->remarks));
		if (email->email)
			xmlSetProp(address, (xmlChar *) "email",
					(xmlChar *) g_strdup(email->email));
		if (email->alias)
			xmlSetProp(address, (xmlChar *) "alias",
					(xmlChar *) g_strdup(email->alias));
		xmlAddChild(parent, address);
	}
}

void attribute_free(gpointer attribute) {
	Attribute* a = (Attribute *) attribute;
	
	if (a == NULL)
		return;
		
	g_free(a->attr_name);
	g_free(a->file);
	g_free(a);
}

void attribute_list_free(GSList** list) {
	if (*list == NULL)
		return;
	
	gslist_free(list, attribute_free);
	*list = NULL;
}

GSList* parse_index_file(gchar* docname, gchar** error) {
	xmlDocPtr doc;
	xmlNodePtr cur;
	GSList* books = NULL;

	cm_return_val_if_fail(docname != NULL, NULL);

	if (init_document(INDEXFILE, docname, &doc, &cur, error)) {
		return books;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if ((cur->name && !xmlStrcmp(cur->name, (const xmlChar *) "book_list"))) {
			debug_print("Parsing: %s\n", cur->name);
			books = parse_book(doc, cur);
		}
		 
		cur = cur->next;
	}
	
	xmlFreeDoc(doc);
	
	return books;
}

void addr_book_set_name(AddressBook* abook, gchar** error) {
	xmlDocPtr doc;
	xmlNodePtr cur;
	xmlChar *key;
	xmlAttr* attribs;

	cm_return_if_fail(abook != NULL);

	if (abook->abook_name && strlen(abook->abook_name) > 0)
		return;

	if (init_document(BOOKFILE, abook->URL, &doc, &cur, error)) {
		return;
	}

	attribs = (xmlAttr *) cur->properties;
	while (attribs) {
		debug_print("Attribute: %s\n", attribs->name);
		if (attribs->name && strcasecmp("name", (const gchar *) attribs->name) == 0) {
			key = xmlNodeListGetString(doc, attribs->children, 1);
			if (abook->abook_name)
				g_free(abook->abook_name);
			abook->abook_name = g_strdup((const gchar *) key);
			debug_print("Address book name: %s\n", abook->abook_name);
			xmlFree(key);
		}
		attribs = attribs->next;
	}
	
	if (! abook->abook_name || strlen(abook->abook_name) < 1) {
		g_free(abook->abook_name);
		//abook->abook_name = g_strdup(_("Missing name"));
		abook->abook_name = NULL;
	}
}

void parse_addr_book(AddressBook* abook, gchar** error) {
	xmlDocPtr doc;
	xmlNodePtr cur;

	cm_return_if_fail(abook != NULL);

	if (init_document(BOOKFILE, abook->URL, &doc, &cur, error)) {
		return;
	}

	addr_book_set_name(abook, error);
	if (*error)
		return;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if ((cur->name && !xmlStrcmp(cur->name, (const xmlChar *) "person"))) {
			debug_print("Parsing: %s\n", cur->name);
			Contact* contact = parse_person(doc, cur);
			if (contact)
				abook->contacts = g_list_prepend(abook->contacts, contact);
		}
		cur = cur->next;
	}
	
	xmlFreeDoc(doc);
}

gboolean addr_book_update_config(AddressBook* old,
								 AddressBook* new,
								 gchar** error) {
	xmlDocPtr doc;
	xmlNodePtr cur;
	int bytes;
	gboolean result = FALSE;

	cm_return_val_if_fail(old != NULL, TRUE);

	if (! old && ! new) {
		*error = g_strdup("missing old and new address book");
		return TRUE;
	}		 

	if (init_document(BOOKFILE, old->URL, &doc, &cur, error)) {
		return TRUE;
	}
	
	xmlThrDefIndentTreeOutput(1);

	if (! new->abook_name) {
		gchar* name = get_abook_name(doc, cur);
		if (name) {
			xmlSetProp(cur, (const xmlChar *) "name", xmlCharStrdup(name));
			new->abook_name = g_strdup(name);
			g_free(name);
		}
		else {
			if (old->abook_name) {
				xmlSetProp(cur, (const xmlChar *) "name", xmlCharStrdup(old->abook_name));
				new->abook_name = g_strdup(old->abook_name);
			}
			else {
				xmlSetProp(cur, (const xmlChar *) "name", xmlCharStrdup(""));
				new->abook_name = g_strdup("");
			}
		}
	}
	else {
		if (old->abook_name) {
			if (strcmp(new->abook_name, old->abook_name) != 0) {
				xmlSetProp(cur, (const xmlChar *) "name", xmlCharStrdup(new->abook_name));
				new->abook_name = g_strdup(new->abook_name);
			}
			else {
				xmlSetProp(cur, (const xmlChar *) "name", xmlCharStrdup(old->abook_name));
				new->abook_name = g_strdup(old->abook_name);
			}
		}
		else {
			xmlSetProp(cur, (const xmlChar *) "name", xmlCharStrdup(new->abook_name));
			new->abook_name = g_strdup(new->abook_name);
		}
	}
	
	if (strcmp(new->URL, old->URL) != 0) {
		bytes = xmlSaveFormatFileEnc(new->URL, doc, "UTF-8", 1);
		if (bytes == -1) {
			if (error) {
				*error = g_strdup_printf(_("%s: Not saved"), new->URL);
			}
			result = TRUE;
		}
	}
	else {
		if (g_file_test(old->URL, G_FILE_TEST_EXISTS)) {
			gchar* name = g_strconcat(old->URL, ".bak", NULL);
			bytes = g_rename(old->URL, name);
			g_free(name);
			if (bytes) {
				if (error) {
					*error = g_strdup_printf(_("%s: Backup not made"), old->URL);
				}
				xmlFreeDoc(doc);
				return TRUE;
			}
		}
		bytes = xmlSaveFormatFileEnc(new->URL, doc, "UTF-8", 1);
		if (bytes == -1) {
			if (error) {
				*error = g_strdup_printf(_("%s: Not saved"), new->URL);
			}
			result = TRUE;
		}
	}
	
	xmlFreeDoc(doc);
	return result;
}

gboolean addr_book_set_config(AddressBook* abook, gchar** error) {
	xmlDocPtr doc;
	xmlNodePtr root;
	int bytes;
	gboolean result = FALSE;

	if (! abook) {
		*error = g_strdup("missing address book");
		return TRUE;
	}		 

	doc = xmlNewDoc((const xmlChar *) "1.0");

	xmlThrDefIndentTreeOutput(1);
	
	root = xmlNewDocNode(
		doc, NULL, (const xmlChar *) ROOTNODE[BOOKFILE], NULL);
	xmlNewProp(
		root, (const xmlChar *) "name", xmlCharStrdup(abook->abook_name));
	xmlDocSetRootElement(doc, root);

	bytes = xmlSaveFormatFileEnc(abook->URL, doc, "UTF-8", 1);
	if (bytes == -1) {
		if (error) {
			*error = g_strdup_printf(_("%s: Not saved"), abook->URL);
		}
		result = TRUE;
	}
	
	xmlFreeDoc(doc);
	return result;
}

void addr_book_delete_contact(AddressBook* abook,
							  Contact* contact,
							  gchar** error) {
	xmlDocPtr doc;
	xmlNodePtr cur;
	GList* head;
	Contact* remove = NULL;
	AttribDef* compare;
	gchar* value;
	int bytes;

	if (! abook || ! contact) {
		*error = g_strdup("missing address book and/or contact");
		return;
	}		 

	if (init_document(BOOKFILE, abook->URL, &doc, &cur, error)) {
		return;
	}

	xmlThrDefIndentTreeOutput(1);

	compare = g_new0(AttribDef, 1);
	compare->type = ATTRIB_TYPE_STRING;
	compare->attrib_name = g_strdup("uid");
	
	for (head = abook->contacts; head; head = g_list_next(head)) {
		remove = (Contact *) head->data;
		if (contact_compare_attrib(contact, remove, compare))
			break;
	}	
	
	if (debug_get_mode())
		contact_dump(contact, stderr);
	value = g_strdup(compare->attrib_name);
	attrib_def_free(compare);
	
	compare = g_hash_table_lookup(remove->data, value);
	g_free(value);
	
	value = compare->value.string;
	
	if (value) {
		cur = cur->xmlChildrenNode;
		while (cur != NULL) {
			if (cur->name && !xmlStrcmp(cur->name, (const xmlChar *) "person")) {
				debug_print("Parsing: %s\n", cur->name);
				if (has_attribute(doc, cur, compare->attrib_name, value)) {
					xmlUnlinkNode(cur);
					xmlFreeNode(cur);
					bytes = xmlSaveFormatFileEnc(abook->URL, doc, "UTF-8", 1);
					if (bytes == -1) {
						*error = g_strdup_printf(_("%s: Not saved"), abook->URL);
					}
					break;
				}
			}
			cur = cur->next;
		}
	}
	
	xmlFreeDoc(doc);
}

void addr_book_set_contact(AddressBook* abook,
						   Contact* contact,
						   gchar** error) {
	xmlDocPtr doc;
	xmlNodePtr cur, person, address, attribute;
	int bytes;

	if (! abook || ! contact) {
		*error = g_strdup("missing address book and/or contact");
		return;
	}		 

	if (init_document(BOOKFILE, abook->URL, &doc, &cur, error)) {
		return;
	}

	xmlThrDefIndentTreeOutput(1);
	
	person = xmlNewNode(NULL, (const xmlChar *) "person");
	person_make_attr_list(person, contact);
	xmlAddChild(cur, person);

	address = xmlNewNode(NULL, (const xmlChar *) "address-list");
	address_list_make(address, contact);
	xmlAddChild(person, address);

	attribute = xmlNewNode(NULL, (const xmlChar *) "attribute-list");
	attribute_list_make(attribute, contact);
	xmlAddChild(person, attribute);

	bytes = xmlSaveFormatFileEnc(abook->URL, doc, "UTF-8", 1);
	if (bytes == -1) {
		*error = g_strdup_printf(_("%s: Not saved"), abook->URL);
	}

	xmlFreeDoc(doc);
}